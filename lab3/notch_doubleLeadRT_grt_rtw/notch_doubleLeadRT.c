/*
 * notch_doubleLeadRT.c
 *
 * Code generation for model "notch_doubleLeadRT".
 *
 * Model version              : 1.18
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Mon Mar 06 10:18:13 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "notch_doubleLeadRT.h"
#include "notch_doubleLeadRT_private.h"

/* Block signals (auto storage) */
B_notch_doubleLeadRT_T notch_doubleLeadRT_B;

/* Continuous states */
X_notch_doubleLeadRT_T notch_doubleLeadRT_X;

/* Block states (auto storage) */
DW_notch_doubleLeadRT_T notch_doubleLeadRT_DW;

/* Real-time model */
RT_MODEL_notch_doubleLeadRT_T notch_doubleLeadRT_M_;
RT_MODEL_notch_doubleLeadRT_T *const notch_doubleLeadRT_M =
  &notch_doubleLeadRT_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 4;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  notch_doubleLeadRT_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void notch_doubleLeadRT_step(void)
{
  /* local block i/o variables */
  real_T rtb_HILReadEncoderTimebase_o1;
  real_T rtb_CartEncoder2mmcounts;
  if (rtmIsMajorTimeStep(notch_doubleLeadRT_M)) {
    /* set solver stop time */
    if (!(notch_doubleLeadRT_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&notch_doubleLeadRT_M->solverInfo,
                            ((notch_doubleLeadRT_M->Timing.clockTickH0 + 1) *
        notch_doubleLeadRT_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&notch_doubleLeadRT_M->solverInfo,
                            ((notch_doubleLeadRT_M->Timing.clockTick0 + 1) *
        notch_doubleLeadRT_M->Timing.stepSize0 +
        notch_doubleLeadRT_M->Timing.clockTickH0 *
        notch_doubleLeadRT_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(notch_doubleLeadRT_M)) {
    notch_doubleLeadRT_M->Timing.t[0] = rtsiGetT
      (&notch_doubleLeadRT_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(notch_doubleLeadRT_M)) {
    /* S-Function (hil_read_encoder_timebase_block): '<S1>/HIL Read Encoder Timebase' */

    /* S-Function Block: notch_doubleLeadRT/Quarc_Plant/HIL Read Encoder Timebase (hil_read_encoder_timebase_block) */
    {
      t_error result;
      result = hil_task_read_encoder
        (notch_doubleLeadRT_DW.HILReadEncoderTimebase_Task, 1,
         &notch_doubleLeadRT_DW.HILReadEncoderTimebase_Buffer[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
      } else {
        rtb_HILReadEncoderTimebase_o1 =
          notch_doubleLeadRT_DW.HILReadEncoderTimebase_Buffer[0];
        rtb_CartEncoder2mmcounts =
          notch_doubleLeadRT_DW.HILReadEncoderTimebase_Buffer[1];
      }
    }

    /* Gain: '<S1>/Cart Encoder (mm//counts)' */
    notch_doubleLeadRT_B.CartEncodermmcounts =
      notch_doubleLeadRT_P.CartEncodermmcounts_Gain *
      rtb_HILReadEncoderTimebase_o1;

    /* Gain: '<S1>/Cart Encoder 2 (mm//counts)' */
    rtb_CartEncoder2mmcounts *= notch_doubleLeadRT_P.CartEncoder2mmcounts_Gain;
  }

  /* FromWorkspace: '<Root>/From Workspace' */
  {
    real_T *pDataValues = (real_T *)
      notch_doubleLeadRT_DW.FromWorkspace_PWORK.DataPtr;
    real_T *pTimeValues = (real_T *)
      notch_doubleLeadRT_DW.FromWorkspace_PWORK.TimePtr;
    int_T currTimeIndex = notch_doubleLeadRT_DW.FromWorkspace_IWORK.PrevIndex;
    real_T t = notch_doubleLeadRT_M->Timing.t[0];

    /* Get index */
    if (t <= pTimeValues[0]) {
      currTimeIndex = 0;
    } else if (t >= pTimeValues[8003]) {
      currTimeIndex = 8002;
    } else {
      if (t < pTimeValues[currTimeIndex]) {
        while (t < pTimeValues[currTimeIndex]) {
          currTimeIndex--;
        }
      } else {
        while (t >= pTimeValues[currTimeIndex + 1]) {
          currTimeIndex++;
        }
      }
    }

    notch_doubleLeadRT_DW.FromWorkspace_IWORK.PrevIndex = currTimeIndex;

    /* Post output */
    {
      real_T t1 = pTimeValues[currTimeIndex];
      real_T t2 = pTimeValues[currTimeIndex + 1];
      if (t1 == t2) {
        if (t < t1) {
          notch_doubleLeadRT_B.FromWorkspace = pDataValues[currTimeIndex];
        } else {
          notch_doubleLeadRT_B.FromWorkspace = pDataValues[currTimeIndex + 1];
        }
      } else {
        real_T f1 = (t2 - t) / (t2 - t1);
        real_T f2 = 1.0 - f1;
        real_T d1;
        real_T d2;
        int_T TimeIndex= currTimeIndex;
        d1 = pDataValues[TimeIndex];
        d2 = pDataValues[TimeIndex + 1];
        notch_doubleLeadRT_B.FromWorkspace = (real_T) rtInterpolate(d1, d2, f1,
          f2);
        pDataValues += 8004;
      }
    }
  }

  /* Gain: '<Root>/Gain' incorporates:
   *  Sum: '<Root>/Sum'
   */
  notch_doubleLeadRT_B.Gain = (notch_doubleLeadRT_B.FromWorkspace -
    notch_doubleLeadRT_B.CartEncodermmcounts) * notch_doubleLeadRT_P.Kp;

  /* TransferFcn: '<Root>/lead Filter' */
  notch_doubleLeadRT_B.leadFilter = 0.0;
  notch_doubleLeadRT_B.leadFilter += notch_doubleLeadRT_P.leadFilter_C *
    notch_doubleLeadRT_X.leadFilter_CSTATE;
  notch_doubleLeadRT_B.leadFilter += notch_doubleLeadRT_P.leadFilter_D *
    notch_doubleLeadRT_B.Gain;

  /* TransferFcn: '<Root>/lead Filter1' */
  notch_doubleLeadRT_B.leadFilter1 = 0.0;
  notch_doubleLeadRT_B.leadFilter1 += notch_doubleLeadRT_P.leadFilter1_C *
    notch_doubleLeadRT_X.leadFilter1_CSTATE;
  notch_doubleLeadRT_B.leadFilter1 += notch_doubleLeadRT_P.leadFilter1_D *
    notch_doubleLeadRT_B.leadFilter;

  /* TransferFcn: '<Root>/notch Filter' */
  notch_doubleLeadRT_B.notchFilter = 0.0;
  notch_doubleLeadRT_B.notchFilter += notch_doubleLeadRT_P.notchFilter_C[0] *
    notch_doubleLeadRT_X.notchFilter_CSTATE[0];
  notch_doubleLeadRT_B.notchFilter += notch_doubleLeadRT_P.notchFilter_C[1] *
    notch_doubleLeadRT_X.notchFilter_CSTATE[1];
  notch_doubleLeadRT_B.notchFilter += notch_doubleLeadRT_P.notchFilter_D *
    notch_doubleLeadRT_B.leadFilter1;
  if (rtmIsMajorTimeStep(notch_doubleLeadRT_M)) {
    /* S-Function (hil_write_analog_block): '<S1>/HIL Write Analog' */

    /* S-Function Block: notch_doubleLeadRT/Quarc_Plant/HIL Write Analog (hil_write_analog_block) */
    {
      t_error result;
      result = hil_write_analog(notch_doubleLeadRT_DW.HILInitialize1_Card,
        &notch_doubleLeadRT_P.HILWriteAnalog_channels, 1,
        &notch_doubleLeadRT_B.notchFilter);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
      }
    }

    /* Scope: '<Root>/uSim' */
    if (rtmIsMajorTimeStep(notch_doubleLeadRT_M)) {
      StructLogVar *svar = (StructLogVar *)
        notch_doubleLeadRT_DW.uSim_PWORK.LoggedData;
      LogVar *var = svar->signals.values;

      /* time */
      {
        double locTime = (((notch_doubleLeadRT_M->Timing.clockTick1+
                            notch_doubleLeadRT_M->Timing.clockTickH1*
                            4294967296.0)) * 0.001);
        rt_UpdateLogVar((LogVar *)svar->time, &locTime, 0);
      }

      /* signals */
      {
        real_T up0[1];
        up0[0] = notch_doubleLeadRT_B.notchFilter;
        rt_UpdateLogVar((LogVar *)var, up0, 0);
      }
    }

    /* Scope: '<Root>/x1sim' */
    if (rtmIsMajorTimeStep(notch_doubleLeadRT_M)) {
      StructLogVar *svar = (StructLogVar *)
        notch_doubleLeadRT_DW.x1sim_PWORK.LoggedData;
      LogVar *var = svar->signals.values;

      /* time */
      {
        double locTime = (((notch_doubleLeadRT_M->Timing.clockTick1+
                            notch_doubleLeadRT_M->Timing.clockTickH1*
                            4294967296.0)) * 0.001);
        rt_UpdateLogVar((LogVar *)svar->time, &locTime, 0);
      }

      /* signals */
      {
        real_T up0[1];
        up0[0] = rtb_CartEncoder2mmcounts;
        rt_UpdateLogVar((LogVar *)var, up0, 0);
      }
    }

    /* Scope: '<Root>/x2sim ' */
    if (rtmIsMajorTimeStep(notch_doubleLeadRT_M)) {
      StructLogVar *svar = (StructLogVar *)
        notch_doubleLeadRT_DW.x2sim_PWORK.LoggedData;
      LogVar *var = svar->signals.values;

      /* time */
      {
        double locTime = (((notch_doubleLeadRT_M->Timing.clockTick1+
                            notch_doubleLeadRT_M->Timing.clockTickH1*
                            4294967296.0)) * 0.001);
        rt_UpdateLogVar((LogVar *)svar->time, &locTime, 0);
      }

      /* signals */
      {
        real_T up0[1];
        up0[0] = notch_doubleLeadRT_B.CartEncodermmcounts;
        rt_UpdateLogVar((LogVar *)var, up0, 0);
      }
    }

    /* Scope: '<Root>/xrSim' */
    if (rtmIsMajorTimeStep(notch_doubleLeadRT_M)) {
      StructLogVar *svar = (StructLogVar *)
        notch_doubleLeadRT_DW.xrSim_PWORK.LoggedData;
      LogVar *var = svar->signals.values;

      /* time */
      {
        double locTime = (((notch_doubleLeadRT_M->Timing.clockTick1+
                            notch_doubleLeadRT_M->Timing.clockTickH1*
                            4294967296.0)) * 0.001);
        rt_UpdateLogVar((LogVar *)svar->time, &locTime, 0);
      }

      /* signals */
      {
        real_T up0[1];
        up0[0] = notch_doubleLeadRT_B.FromWorkspace;
        rt_UpdateLogVar((LogVar *)var, up0, 0);
      }
    }
  }

  if (rtmIsMajorTimeStep(notch_doubleLeadRT_M)) {
    /* Matfile logging */
    rt_UpdateTXYLogVars(notch_doubleLeadRT_M->rtwLogInfo,
                        (notch_doubleLeadRT_M->Timing.t));
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(notch_doubleLeadRT_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(notch_doubleLeadRT_M)!=-1) &&
          !((rtmGetTFinal(notch_doubleLeadRT_M)-
             (((notch_doubleLeadRT_M->Timing.clockTick1+
                notch_doubleLeadRT_M->Timing.clockTickH1* 4294967296.0)) * 0.001))
            > (((notch_doubleLeadRT_M->Timing.clockTick1+
                 notch_doubleLeadRT_M->Timing.clockTickH1* 4294967296.0)) *
               0.001) * (DBL_EPSILON))) {
        rtmSetErrorStatus(notch_doubleLeadRT_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&notch_doubleLeadRT_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++notch_doubleLeadRT_M->Timing.clockTick0)) {
      ++notch_doubleLeadRT_M->Timing.clockTickH0;
    }

    notch_doubleLeadRT_M->Timing.t[0] = rtsiGetSolverStopTime
      (&notch_doubleLeadRT_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.001s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.001, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      notch_doubleLeadRT_M->Timing.clockTick1++;
      if (!notch_doubleLeadRT_M->Timing.clockTick1) {
        notch_doubleLeadRT_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void notch_doubleLeadRT_derivatives(void)
{
  XDot_notch_doubleLeadRT_T *_rtXdot;
  _rtXdot = ((XDot_notch_doubleLeadRT_T *)
             notch_doubleLeadRT_M->ModelData.derivs);

  /* Derivatives for TransferFcn: '<Root>/lead Filter' */
  _rtXdot->leadFilter_CSTATE = 0.0;
  _rtXdot->leadFilter_CSTATE += notch_doubleLeadRT_P.leadFilter_A *
    notch_doubleLeadRT_X.leadFilter_CSTATE;
  _rtXdot->leadFilter_CSTATE += notch_doubleLeadRT_B.Gain;

  /* Derivatives for TransferFcn: '<Root>/lead Filter1' */
  _rtXdot->leadFilter1_CSTATE = 0.0;
  _rtXdot->leadFilter1_CSTATE += notch_doubleLeadRT_P.leadFilter1_A *
    notch_doubleLeadRT_X.leadFilter1_CSTATE;
  _rtXdot->leadFilter1_CSTATE += notch_doubleLeadRT_B.leadFilter;

  /* Derivatives for TransferFcn: '<Root>/notch Filter' */
  _rtXdot->notchFilter_CSTATE[0] = 0.0;
  _rtXdot->notchFilter_CSTATE[1] = 0.0;
  _rtXdot->notchFilter_CSTATE[0] += notch_doubleLeadRT_P.notchFilter_A[0] *
    notch_doubleLeadRT_X.notchFilter_CSTATE[0];
  _rtXdot->notchFilter_CSTATE[0] += notch_doubleLeadRT_P.notchFilter_A[1] *
    notch_doubleLeadRT_X.notchFilter_CSTATE[1];
  _rtXdot->notchFilter_CSTATE[1] += notch_doubleLeadRT_X.notchFilter_CSTATE[0];
  _rtXdot->notchFilter_CSTATE[0] += notch_doubleLeadRT_B.leadFilter1;
}

/* Model initialize function */
void notch_doubleLeadRT_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)notch_doubleLeadRT_M, 0,
                sizeof(RT_MODEL_notch_doubleLeadRT_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&notch_doubleLeadRT_M->solverInfo,
                          &notch_doubleLeadRT_M->Timing.simTimeStep);
    rtsiSetTPtr(&notch_doubleLeadRT_M->solverInfo, &rtmGetTPtr
                (notch_doubleLeadRT_M));
    rtsiSetStepSizePtr(&notch_doubleLeadRT_M->solverInfo,
                       &notch_doubleLeadRT_M->Timing.stepSize0);
    rtsiSetdXPtr(&notch_doubleLeadRT_M->solverInfo,
                 &notch_doubleLeadRT_M->ModelData.derivs);
    rtsiSetContStatesPtr(&notch_doubleLeadRT_M->solverInfo, (real_T **)
                         &notch_doubleLeadRT_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&notch_doubleLeadRT_M->solverInfo,
      &notch_doubleLeadRT_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&notch_doubleLeadRT_M->solverInfo, (&rtmGetErrorStatus
      (notch_doubleLeadRT_M)));
    rtsiSetRTModelPtr(&notch_doubleLeadRT_M->solverInfo, notch_doubleLeadRT_M);
  }

  rtsiSetSimTimeStep(&notch_doubleLeadRT_M->solverInfo, MAJOR_TIME_STEP);
  notch_doubleLeadRT_M->ModelData.intgData.f[0] =
    notch_doubleLeadRT_M->ModelData.odeF[0];
  notch_doubleLeadRT_M->ModelData.contStates = ((X_notch_doubleLeadRT_T *)
    &notch_doubleLeadRT_X);
  rtsiSetSolverData(&notch_doubleLeadRT_M->solverInfo, (void *)
                    &notch_doubleLeadRT_M->ModelData.intgData);
  rtsiSetSolverName(&notch_doubleLeadRT_M->solverInfo,"ode1");
  rtmSetTPtr(notch_doubleLeadRT_M, &notch_doubleLeadRT_M->Timing.tArray[0]);
  rtmSetTFinal(notch_doubleLeadRT_M, 4.0);
  notch_doubleLeadRT_M->Timing.stepSize0 = 0.001;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    notch_doubleLeadRT_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(notch_doubleLeadRT_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(notch_doubleLeadRT_M->rtwLogInfo, (NULL));
    rtliSetLogT(notch_doubleLeadRT_M->rtwLogInfo, "tout");
    rtliSetLogX(notch_doubleLeadRT_M->rtwLogInfo, "");
    rtliSetLogXFinal(notch_doubleLeadRT_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(notch_doubleLeadRT_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(notch_doubleLeadRT_M->rtwLogInfo, 0);
    rtliSetLogMaxRows(notch_doubleLeadRT_M->rtwLogInfo, 1000);
    rtliSetLogDecimation(notch_doubleLeadRT_M->rtwLogInfo, 1);
    rtliSetLogY(notch_doubleLeadRT_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(notch_doubleLeadRT_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(notch_doubleLeadRT_M->rtwLogInfo, (NULL));
  }

  /* block I/O */
  (void) memset(((void *) &notch_doubleLeadRT_B), 0,
                sizeof(B_notch_doubleLeadRT_T));

  /* states (continuous) */
  {
    (void) memset((void *)&notch_doubleLeadRT_X, 0,
                  sizeof(X_notch_doubleLeadRT_T));
  }

  /* states (dwork) */
  (void) memset((void *)&notch_doubleLeadRT_DW, 0,
                sizeof(DW_notch_doubleLeadRT_T));

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(notch_doubleLeadRT_M->rtwLogInfo, 0.0,
    rtmGetTFinal(notch_doubleLeadRT_M), notch_doubleLeadRT_M->Timing.stepSize0,
    (&rtmGetErrorStatus(notch_doubleLeadRT_M)));

  /* Start for S-Function (hil_initialize_block): '<S1>/HIL Initialize1' */

  /* S-Function Block: notch_doubleLeadRT/Quarc_Plant/HIL Initialize1 (hil_initialize_block) */
  {
    static const t_uint analog_input_channels[2U] = {
      0
      , 1
    };

    static const t_double analog_input_minimums[2U] = {
      -10.0
      , -10.0
    };

    static const t_double analog_input_maximums[2U] = {
      10.0
      , 10.0
    };

    static const t_uint analog_output_channels[2U] = {
      0
      , 1
    };

    static const t_double analog_output_minimums[2U] = {
      -10.0
      , -10.0
    };

    static const t_double analog_output_maximums[2U] = {
      10.0
      , 10.0
    };

    static const t_double initial_analog_outputs[2U] = {
      0.0
      , 0.0
    };

    static const t_uint encoder_input_channels[2U] = {
      0
      , 1
    };

    static const t_encoder_quadrature_mode encoder_quadrature[2U] = {
      ENCODER_QUADRATURE_4X
      , ENCODER_QUADRATURE_4X
    };

    static const t_int32 initial_encoder_counts[2U] = {
      0
      , 0
    };

    t_int result;
    t_boolean is_switching;
    result = hil_open("q2_usb", "0", &notch_doubleLeadRT_DW.HILInitialize1_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
      return;
    }

    is_switching = false;
    result = hil_set_card_specific_options
      (notch_doubleLeadRT_DW.HILInitialize1_Card,
       "d0=digital;d1=digital;led=auto;update_rate=normal", 50);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
      return;
    }

    result = hil_watchdog_clear(notch_doubleLeadRT_DW.HILInitialize1_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
      return;
    }

    if (!is_switching) {
      result = hil_set_analog_input_ranges
        (notch_doubleLeadRT_DW.HILInitialize1_Card, analog_input_channels, 2U,
         analog_input_minimums, analog_input_maximums);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
        return;
      }
    }

    if (!is_switching) {
      result = hil_set_analog_output_ranges
        (notch_doubleLeadRT_DW.HILInitialize1_Card, analog_output_channels, 2U,
         analog_output_minimums, analog_output_maximums);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
        return;
      }
    }

    if (!is_switching) {
      result = hil_write_analog(notch_doubleLeadRT_DW.HILInitialize1_Card,
        analog_output_channels, 2U, initial_analog_outputs);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
        return;
      }
    }

    if (!is_switching) {
      result = hil_set_encoder_quadrature_mode
        (notch_doubleLeadRT_DW.HILInitialize1_Card, encoder_input_channels, 2U,
         (t_encoder_quadrature_mode *) encoder_quadrature);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
        return;
      }
    }

    if (!is_switching) {
      result = hil_set_encoder_counts(notch_doubleLeadRT_DW.HILInitialize1_Card,
        encoder_input_channels, 2U, initial_encoder_counts);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_encoder_timebase_block): '<S1>/HIL Read Encoder Timebase' */

  /* S-Function Block: notch_doubleLeadRT/Quarc_Plant/HIL Read Encoder Timebase (hil_read_encoder_timebase_block) */
  {
    t_error result;
    result = hil_task_create_encoder_reader
      (notch_doubleLeadRT_DW.HILInitialize1_Card,
       notch_doubleLeadRT_P.HILReadEncoderTimebase_samples_,
       notch_doubleLeadRT_P.HILReadEncoderTimebase_channels, 2,
       &notch_doubleLeadRT_DW.HILReadEncoderTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
    }
  }

  /* Start for FromWorkspace: '<Root>/From Workspace' */
  {
    static real_T pTimeValues0[] = { 0.0, 0.001, 0.002, 0.003, 0.004, 0.005,
      0.006, 0.007, 0.008, 0.0090000000000000011, 0.01, 0.011, 0.012,
      0.013000000000000001, 0.014, 0.015, 0.016, 0.017, 0.018000000000000002,
      0.019, 0.02, 0.021, 0.022, 0.023, 0.024, 0.025, 0.026000000000000002,
      0.027, 0.028, 0.029, 0.03, 0.031, 0.032, 0.033, 0.034, 0.035,
      0.036000000000000004, 0.037, 0.038, 0.039, 0.04, 0.041, 0.042,
      0.043000000000000003, 0.044, 0.045, 0.046, 0.047, 0.048, 0.049, 0.05,
      0.051000000000000004, 0.052000000000000005, 0.053, 0.054, 0.055, 0.056,
      0.057, 0.058, 0.059000000000000004, 0.06, 0.061, 0.062, 0.063, 0.064,
      0.065, 0.066, 0.067, 0.068, 0.069, 0.07, 0.071000000000000008,
      0.072000000000000008, 0.073, 0.074, 0.075, 0.076, 0.077, 0.078, 0.079,
      0.08, 0.081, 0.082, 0.083, 0.084, 0.085, 0.086000000000000007,
      0.087000000000000008, 0.088, 0.089, 0.09, 0.091, 0.092, 0.093, 0.094,
      0.095, 0.096, 0.097, 0.098, 0.099, 0.1, 0.101, 0.10200000000000001,
      0.10300000000000001, 0.10400000000000001, 0.10500000000000001,
      0.10600000000000001, 0.10700000000000001, 0.10800000000000001,
      0.10900000000000001, 0.11000000000000001, 0.11100000000000002,
      0.11200000000000002, 0.113, 0.114, 0.115, 0.116, 0.117,
      0.11800000000000001, 0.11900000000000001, 0.12000000000000001,
      0.12100000000000001, 0.12200000000000001, 0.12300000000000001,
      0.12400000000000001, 0.125, 0.126, 0.127, 0.128, 0.129, 0.13, 0.131, 0.132,
      0.133, 0.134, 0.135, 0.136, 0.137, 0.138, 0.139, 0.14, 0.14100000000000001,
      0.14200000000000002, 0.14300000000000002, 0.14400000000000002,
      0.14500000000000002, 0.14600000000000002, 0.14700000000000002,
      0.14800000000000002, 0.14900000000000002, 0.15000000000000002,
      0.15100000000000002, 0.15200000000000002, 0.15300000000000002,
      0.15400000000000003, 0.15500000000000003, 0.15600000000000003, 0.157,
      0.158, 0.159, 0.16, 0.161, 0.162, 0.163, 0.164, 0.165, 0.166, 0.167, 0.168,
      0.169, 0.17, 0.171, 0.17200000000000001, 0.17300000000000001,
      0.17400000000000002, 0.17500000000000002, 0.17600000000000002,
      0.17700000000000002, 0.17800000000000002, 0.17900000000000002,
      0.18000000000000002, 0.18100000000000002, 0.182, 0.183, 0.184, 0.185,
      0.186, 0.187, 0.188, 0.189, 0.19, 0.191, 0.192, 0.193, 0.194, 0.195, 0.196,
      0.197, 0.198, 0.199, 0.2, 0.201, 0.202, 0.203, 0.20400000000000001,
      0.20500000000000002, 0.20600000000000002, 0.20700000000000002,
      0.20800000000000002, 0.20900000000000002, 0.21000000000000002,
      0.21100000000000002, 0.21200000000000002, 0.21300000000000002,
      0.21400000000000002, 0.21500000000000002, 0.21600000000000003,
      0.21700000000000003, 0.21800000000000003, 0.219, 0.22, 0.221, 0.222, 0.223,
      0.224, 0.225, 0.226, 0.227, 0.228, 0.229, 0.23, 0.231, 0.232, 0.233, 0.234,
      0.23500000000000001, 0.23600000000000002, 0.23700000000000002,
      0.23800000000000002, 0.23900000000000002, 0.24000000000000002,
      0.24100000000000002, 0.24200000000000002, 0.24300000000000002, 0.244,
      0.245, 0.246, 0.247, 0.248, 0.249, 0.25, 0.251, 0.252, 0.253, 0.254, 0.255,
      0.256, 0.257, 0.258, 0.259, 0.26, 0.261, 0.262, 0.263, 0.264, 0.265, 0.266,
      0.267, 0.268, 0.269, 0.27, 0.271, 0.272, 0.273, 0.274, 0.275, 0.276, 0.277,
      0.278, 0.279, 0.28, 0.281, 0.28200000000000003, 0.28300000000000003,
      0.28400000000000003, 0.28500000000000003, 0.28600000000000003,
      0.28700000000000003, 0.28800000000000003, 0.28900000000000003,
      0.29000000000000004, 0.29100000000000004, 0.29200000000000004,
      0.29300000000000004, 0.29400000000000004, 0.29500000000000004,
      0.29600000000000004, 0.29700000000000004, 0.29800000000000004,
      0.29900000000000004, 0.30000000000000004, 0.30100000000000005,
      0.30200000000000005, 0.30300000000000005, 0.30400000000000005, 0.305,
      0.306, 0.307, 0.308, 0.309, 0.31, 0.311, 0.312, 0.313, 0.314, 0.315, 0.316,
      0.317, 0.318, 0.319, 0.32, 0.321, 0.322, 0.323, 0.324, 0.325, 0.326, 0.327,
      0.328, 0.329, 0.33, 0.331, 0.332, 0.333, 0.334, 0.335, 0.336, 0.337, 0.338,
      0.339, 0.34, 0.341, 0.342, 0.343, 0.34400000000000003, 0.345, 0.346, 0.347,
      0.348, 0.349, 0.35, 0.351, 0.352, 0.353, 0.354, 0.355, 0.356, 0.357, 0.358,
      0.359, 0.36, 0.361, 0.362, 0.363, 0.364, 0.365, 0.366, 0.367, 0.368, 0.369,
      0.37, 0.371, 0.372, 0.373, 0.374, 0.375, 0.376, 0.377, 0.378, 0.379, 0.38,
      0.381, 0.382, 0.383, 0.384, 0.385, 0.386, 0.387, 0.388, 0.389, 0.39, 0.391,
      0.392, 0.393, 0.394, 0.395, 0.396, 0.397, 0.398, 0.399, 0.4, 0.401, 0.402,
      0.403, 0.404, 0.405, 0.406, 0.40700000000000003, 0.40800000000000003,
      0.40900000000000003, 0.41000000000000003, 0.41100000000000003,
      0.41200000000000003, 0.413, 0.414, 0.415, 0.416, 0.417, 0.418, 0.419, 0.42,
      0.421, 0.422, 0.423, 0.424, 0.425, 0.426, 0.427, 0.428, 0.429, 0.43, 0.431,
      0.432, 0.433, 0.434, 0.435, 0.436, 0.437, 0.438, 0.439, 0.44, 0.441, 0.442,
      0.443, 0.444, 0.445, 0.446, 0.447, 0.448, 0.449, 0.45, 0.451, 0.452, 0.453,
      0.454, 0.455, 0.456, 0.457, 0.458, 0.459, 0.46, 0.461, 0.462, 0.463, 0.464,
      0.465, 0.466, 0.467, 0.468, 0.46900000000000003, 0.47000000000000003,
      0.471, 0.472, 0.473, 0.474, 0.475, 0.476, 0.477, 0.478, 0.479, 0.48, 0.481,
      0.482, 0.483, 0.484, 0.485, 0.486, 0.487, 0.488, 0.489, 0.49, 0.491, 0.492,
      0.493, 0.494, 0.495, 0.496, 0.497, 0.498, 0.499, 0.5, 0.501, 0.502, 0.503,
      0.504, 0.505, 0.506, 0.507, 0.508, 0.509, 0.51, 0.511, 0.512, 0.513, 0.514,
      0.515, 0.516, 0.517, 0.518, 0.519, 0.52, 0.521, 0.522, 0.523, 0.524, 0.525,
      0.526, 0.527, 0.528, 0.529, 0.53, 0.531, 0.532, 0.533, 0.534, 0.535, 0.536,
      0.537, 0.538, 0.539, 0.54, 0.541, 0.542, 0.543, 0.544, 0.54499999999999993,
      0.546, 0.54699999999999993, 0.548, 0.54899999999999993, 0.55,
      0.55099999999999993, 0.552, 0.55299999999999994, 0.554,
      0.55499999999999994, 0.556, 0.55699999999999994, 0.558,
      0.55899999999999994, 0.56, 0.56099999999999994, 0.562, 0.563,
      0.56400000000000006, 0.565, 0.56600000000000006, 0.567,
      0.56800000000000006, 0.569, 0.57000000000000006, 0.571,
      0.57200000000000006, 0.573, 0.57400000000000007, 0.575,
      0.57600000000000007, 0.577, 0.57800000000000007, 0.579,
      0.58000000000000007, 0.581, 0.58200000000000007, 0.583,
      0.58400000000000007, 0.585, 0.58600000000000008, 0.587,
      0.58800000000000008, 0.589, 0.59000000000000008, 0.591,
      0.59200000000000008, 0.593, 0.59400000000000008, 0.595,
      0.59600000000000009, 0.597, 0.59800000000000009, 0.599,
      0.60000000000000009, 0.601, 0.60200000000000009, 0.603,
      0.60400000000000009, 0.605, 0.60600000000000009, 0.607, 0.6080000000000001,
      0.609, 0.6100000000000001, 0.611, 0.6120000000000001, 0.613, 0.614, 0.615,
      0.616, 0.617, 0.618, 0.619, 0.62, 0.621, 0.622, 0.623, 0.624, 0.625, 0.626,
      0.627, 0.628, 0.629, 0.63, 0.631, 0.632, 0.633, 0.634, 0.635, 0.636, 0.637,
      0.638, 0.639, 0.64, 0.641, 0.642, 0.643, 0.644, 0.645, 0.646, 0.647, 0.648,
      0.649, 0.65, 0.651, 0.652, 0.653, 0.654, 0.655, 0.656, 0.657,
      0.65799999999999992, 0.659, 0.65999999999999992, 0.661,
      0.66199999999999992, 0.663, 0.66399999999999992, 0.665,
      0.66599999999999993, 0.667, 0.66799999999999993, 0.669,
      0.66999999999999993, 0.671, 0.67199999999999993, 0.673,
      0.67399999999999993, 0.675, 0.67599999999999993, 0.677,
      0.67799999999999994, 0.679, 0.67999999999999994, 0.681,
      0.68199999999999994, 0.683, 0.68399999999999994, 0.685,
      0.68599999999999994, 0.687, 0.688, 0.68900000000000006, 0.69,
      0.69100000000000006, 0.692, 0.69300000000000006, 0.694,
      0.69500000000000006, 0.696, 0.69700000000000006, 0.698,
      0.69900000000000007, 0.7, 0.70100000000000007, 0.702, 0.70300000000000007,
      0.704, 0.70500000000000007, 0.706, 0.70700000000000007, 0.708,
      0.70900000000000007, 0.71, 0.71100000000000008, 0.712, 0.71300000000000008,
      0.714, 0.71500000000000008, 0.716, 0.71700000000000008, 0.718,
      0.71900000000000008, 0.72, 0.72100000000000009, 0.722, 0.72300000000000009,
      0.724, 0.72500000000000009, 0.726, 0.72700000000000009, 0.728,
      0.72900000000000009, 0.73, 0.73100000000000009, 0.732, 0.7330000000000001,
      0.734, 0.7350000000000001, 0.736, 0.7370000000000001, 0.738,
      0.7390000000000001, 0.74, 0.7410000000000001, 0.742, 0.7430000000000001,
      0.744, 0.74499999999999988, 0.746, 0.74699999999999989, 0.748,
      0.74899999999999989, 0.75, 0.75099999999999989, 0.752, 0.75299999999999989,
      0.754, 0.75499999999999989, 0.756, 0.7569999999999999, 0.758,
      0.7589999999999999, 0.76, 0.7609999999999999, 0.762, 0.7629999999999999,
      0.764, 0.7649999999999999, 0.766, 0.7669999999999999, 0.768,
      0.76899999999999991, 0.77, 0.77099999999999991, 0.772, 0.77299999999999991,
      0.774, 0.77499999999999991, 0.776, 0.77699999999999991, 0.778,
      0.77899999999999991, 0.78, 0.78099999999999992, 0.782, 0.78299999999999992,
      0.784, 0.78499999999999992, 0.786, 0.78699999999999992, 0.788,
      0.78899999999999992, 0.79, 0.79099999999999993, 0.792, 0.79299999999999993,
      0.794, 0.79499999999999993, 0.796, 0.79699999999999993, 0.798,
      0.79899999999999993, 0.8, 0.80099999999999993, 0.802, 0.80299999999999994,
      0.804, 0.80499999999999994, 0.806, 0.80699999999999994, 0.808,
      0.80899999999999994, 0.81, 0.81099999999999994, 0.812, 0.813,
      0.81400000000000006, 0.815, 0.81600000000000006, 0.817,
      0.81800000000000006, 0.819, 0.82000000000000006, 0.821,
      0.82200000000000006, 0.823, 0.82400000000000007, 0.825,
      0.82600000000000007, 0.827, 0.82800000000000007, 0.829,
      0.83000000000000007, 0.831, 0.83200000000000007, 0.833,
      0.83400000000000007, 0.835, 0.83600000000000008, 0.837,
      0.83800000000000008, 0.839, 0.84000000000000008, 0.841,
      0.84200000000000008, 0.843, 0.84400000000000008, 0.845,
      0.84599999999999986, 0.847, 0.84799999999999986, 0.849,
      0.84999999999999987, 0.851, 0.85199999999999987, 0.853,
      0.85399999999999987, 0.855, 0.85599999999999987, 0.857,
      0.85799999999999987, 0.859, 0.85999999999999988, 0.861,
      0.86199999999999988, 0.863, 0.86399999999999988, 0.865,
      0.86599999999999988, 0.867, 0.86799999999999988, 0.869,
      0.86999999999999988, 0.871, 0.87199999999999989, 0.873,
      0.87399999999999989, 0.875, 0.87599999999999989, 0.877,
      0.87799999999999989, 0.879, 0.87999999999999989, 0.881, 0.8819999999999999,
      0.883, 0.8839999999999999, 0.885, 0.8859999999999999, 0.887,
      0.8879999999999999, 0.889, 0.8899999999999999, 0.891, 0.8919999999999999,
      0.893, 0.89399999999999991, 0.895, 0.89599999999999991, 0.897,
      0.89799999999999991, 0.899, 0.89999999999999991, 0.901,
      0.90199999999999991, 0.903, 0.90399999999999991, 0.905,
      0.90599999999999992, 0.907, 0.90799999999999992, 0.909,
      0.90999999999999992, 0.911, 0.91199999999999992, 0.913,
      0.91399999999999992, 0.915, 0.91599999999999993, 0.917,
      0.91799999999999993, 0.919, 0.91999999999999993, 0.921,
      0.92199999999999993, 0.923, 0.92399999999999993, 0.925,
      0.92599999999999993, 0.927, 0.92799999999999994, 0.929,
      0.92999999999999994, 0.931, 0.93199999999999994, 0.933,
      0.93399999999999994, 0.935, 0.93599999999999994, 0.937, 0.938,
      0.93900000000000006, 0.94, 0.94100000000000006, 0.942, 0.94300000000000006,
      0.944, 0.94500000000000006, 0.946, 0.94700000000000006, 0.948,
      0.94900000000000007, 0.95, 0.95100000000000007, 0.952, 0.95300000000000007,
      0.954, 0.95500000000000007, 0.956, 0.95700000000000007, 0.958,
      0.95900000000000007, 0.96, 0.96100000000000008, 0.962, 0.96300000000000008,
      0.964, 0.96500000000000008, 0.966, 0.96700000000000008, 0.968,
      0.96900000000000008, 0.97, 0.97099999999999986, 0.972, 0.97299999999999986,
      0.974, 0.97499999999999987, 0.976, 0.97699999999999987, 0.978,
      0.97899999999999987, 0.98, 0.98099999999999987, 0.982, 0.98299999999999987,
      0.984, 0.98499999999999988, 0.986, 0.98699999999999988, 0.988,
      0.98899999999999988, 0.99, 0.99099999999999988, 0.992, 0.99299999999999988,
      0.994, 0.99499999999999988, 0.996, 0.99699999999999989, 0.998,
      0.99899999999999989, 1.0, 1.0010000000000001, 1.002, 1.0030000000000001,
      1.004, 1.0050000000000001, 1.006, 1.0070000000000001, 1.008,
      1.0090000000000001, 1.01, 1.0110000000000001, 1.012, 1.0130000000000001,
      1.014, 1.0150000000000001, 1.016, 1.0170000000000001, 1.018,
      1.0190000000000001, 1.02, 1.0210000000000001, 1.022, 1.0230000000000001,
      1.024, 1.0250000000000001, 1.026, 1.0270000000000001, 1.028,
      1.0290000000000001, 1.03, 1.0310000000000001, 1.032, 1.0330000000000001,
      1.034, 1.0350000000000001, 1.036, 1.0370000000000001, 1.038,
      1.0390000000000001, 1.04, 1.0410000000000001, 1.042, 1.0430000000000001,
      1.044, 1.045, 1.046, 1.047, 1.048, 1.049, 1.05, 1.051, 1.052, 1.053, 1.054,
      1.055, 1.056, 1.057, 1.058, 1.059, 1.06, 1.061, 1.062, 1.063, 1.064, 1.065,
      1.066, 1.067, 1.068, 1.069, 1.07, 1.071, 1.072, 1.073, 1.074, 1.075, 1.076,
      1.077, 1.078, 1.079, 1.08, 1.081, 1.082, 1.083, 1.084, 1.085, 1.086, 1.087,
      1.088, 1.089, 1.09, 1.091, 1.092, 1.093, 1.094, 1.095, 1.096, 1.097, 1.098,
      1.099, 1.1, 1.101, 1.102, 1.103, 1.104, 1.105, 1.106, 1.107, 1.108, 1.109,
      1.11, 1.111, 1.112, 1.113, 1.114, 1.115, 1.116, 1.117, 1.118, 1.119, 1.12,
      1.121, 1.122, 1.123, 1.124, 1.125, 1.1260000000000001, 1.127,
      1.1280000000000001, 1.129, 1.1300000000000001, 1.131, 1.1320000000000001,
      1.133, 1.1340000000000001, 1.135, 1.1360000000000001, 1.137,
      1.1380000000000001, 1.139, 1.1400000000000001, 1.141, 1.1420000000000001,
      1.143, 1.1440000000000001, 1.145, 1.1460000000000001, 1.147,
      1.1480000000000001, 1.149, 1.1500000000000001, 1.151, 1.1520000000000001,
      1.153, 1.1540000000000001, 1.155, 1.1560000000000001, 1.157, 1.158, 1.159,
      1.16, 1.161, 1.162, 1.163, 1.164, 1.165, 1.166, 1.167, 1.168, 1.169, 1.17,
      1.171, 1.172, 1.173, 1.174, 1.175, 1.176, 1.177, 1.178, 1.179, 1.18, 1.181,
      1.182, 1.183, 1.184, 1.185, 1.186, 1.187, 1.188, 1.189, 1.19, 1.191, 1.192,
      1.193, 1.194, 1.195, 1.196, 1.197, 1.198, 1.199, 1.2, 1.201, 1.202, 1.203,
      1.204, 1.205, 1.206, 1.207, 1.208, 1.209, 1.21, 1.211, 1.212, 1.213, 1.214,
      1.215, 1.216, 1.217, 1.218, 1.219, 1.22, 1.221, 1.222, 1.223, 1.224, 1.225,
      1.226, 1.227, 1.228, 1.229, 1.23, 1.231, 1.232, 1.233, 1.234, 1.235, 1.236,
      1.237, 1.238, 1.239, 1.24, 1.241, 1.242, 1.243, 1.244, 1.2449999999999999,
      1.246, 1.2469999999999999, 1.248, 1.2489999999999999, 1.25, 1.251, 1.252,
      1.253, 1.254, 1.255, 1.256, 1.257, 1.258, 1.259, 1.26, 1.261, 1.262, 1.263,
      1.264, 1.265, 1.266, 1.267, 1.268, 1.269, 1.27, 1.271, 1.272, 1.273, 1.274,
      1.275, 1.276, 1.277, 1.278, 1.279, 1.28, 1.281, 1.282, 1.283, 1.284, 1.285,
      1.286, 1.287, 1.288, 1.289, 1.29, 1.291, 1.292, 1.293, 1.294, 1.295, 1.296,
      1.297, 1.298, 1.299, 1.3, 1.301, 1.302, 1.303, 1.304, 1.305, 1.306, 1.307,
      1.308, 1.309, 1.31, 1.311, 1.312, 1.313, 1.314, 1.315, 1.316, 1.317, 1.318,
      1.319, 1.32, 1.321, 1.322, 1.323, 1.324, 1.325, 1.326, 1.327, 1.328, 1.329,
      1.33, 1.331, 1.332, 1.333, 1.334, 1.335, 1.336, 1.337, 1.338, 1.339, 1.34,
      1.341, 1.342, 1.343, 1.344, 1.345, 1.3459999999999999, 1.347,
      1.3479999999999999, 1.349, 1.3499999999999999, 1.351, 1.3519999999999999,
      1.353, 1.3539999999999999, 1.355, 1.3559999999999999, 1.357,
      1.3579999999999999, 1.359, 1.3599999999999999, 1.361, 1.3619999999999999,
      1.363, 1.3639999999999999, 1.365, 1.3659999999999999, 1.367,
      1.3679999999999999, 1.369, 1.3699999999999999, 1.371, 1.3719999999999999,
      1.373, 1.3739999999999999, 1.375, 1.376, 1.377, 1.378, 1.379, 1.38, 1.381,
      1.382, 1.383, 1.384, 1.385, 1.386, 1.387, 1.388, 1.389, 1.39, 1.391, 1.392,
      1.393, 1.394, 1.395, 1.396, 1.397, 1.398, 1.399, 1.4, 1.401, 1.402, 1.403,
      1.404, 1.405, 1.406, 1.407, 1.408, 1.409, 1.41, 1.411, 1.412, 1.413, 1.414,
      1.415, 1.416, 1.417, 1.418, 1.419, 1.42, 1.421, 1.422, 1.423, 1.424, 1.425,
      1.426, 1.427, 1.428, 1.429, 1.43, 1.431, 1.432, 1.433, 1.434, 1.435, 1.436,
      1.437, 1.438, 1.439, 1.44, 1.441, 1.442, 1.443, 1.444, 1.445, 1.446, 1.447,
      1.448, 1.449, 1.45, 1.451, 1.452, 1.453, 1.454, 1.455, 1.456, 1.457, 1.458,
      1.459, 1.46, 1.461, 1.462, 1.463, 1.464, 1.465, 1.466, 1.467, 1.468, 1.469,
      1.47, 1.4709999999999999, 1.472, 1.4729999999999999, 1.474,
      1.4749999999999999, 1.476, 1.4769999999999999, 1.478, 1.4789999999999999,
      1.48, 1.4809999999999999, 1.482, 1.4829999999999999, 1.484,
      1.4849999999999999, 1.486, 1.4869999999999999, 1.488, 1.4889999999999999,
      1.49, 1.4909999999999999, 1.492, 1.4929999999999999, 1.494,
      1.4949999999999999, 1.496, 1.4969999999999999, 1.498, 1.4989999999999999,
      1.5, 1.5010000000000001, 1.502, 1.5030000000000001, 1.504,
      1.5050000000000001, 1.506, 1.5070000000000001, 1.508, 1.5090000000000001,
      1.51, 1.5110000000000001, 1.512, 1.5130000000000001, 1.514,
      1.5150000000000001, 1.516, 1.5170000000000001, 1.518, 1.5190000000000001,
      1.52, 1.5210000000000001, 1.522, 1.5230000000000001, 1.524,
      1.5250000000000001, 1.526, 1.5270000000000001, 1.528, 1.5290000000000001,
      1.53, 1.5310000000000001, 1.532, 1.5330000000000001, 1.534,
      1.5350000000000001, 1.536, 1.5370000000000001, 1.538, 1.5390000000000001,
      1.54, 1.5410000000000001, 1.542, 1.5430000000000001, 1.544, 1.545, 1.546,
      1.547, 1.548, 1.549, 1.55, 1.551, 1.552, 1.553, 1.554, 1.555, 1.556, 1.557,
      1.558, 1.559, 1.56, 1.561, 1.562, 1.563, 1.564, 1.565, 1.566, 1.567, 1.568,
      1.569, 1.57, 1.571, 1.572, 1.573, 1.574, 1.575, 1.576, 1.577, 1.578, 1.579,
      1.58, 1.581, 1.582, 1.583, 1.584, 1.585, 1.586, 1.587, 1.588, 1.589, 1.59,
      1.591, 1.592, 1.593, 1.594, 1.595, 1.596, 1.597, 1.598, 1.599, 1.6, 1.601,
      1.602, 1.603, 1.604, 1.605, 1.606, 1.607, 1.608, 1.609, 1.61, 1.611, 1.612,
      1.613, 1.614, 1.615, 1.616, 1.617, 1.618, 1.619, 1.62, 1.621, 1.622, 1.623,
      1.624, 1.625, 1.6260000000000001, 1.627, 1.6280000000000001, 1.629,
      1.6300000000000001, 1.631, 1.6320000000000001, 1.633, 1.6340000000000001,
      1.635, 1.6360000000000001, 1.637, 1.6380000000000001, 1.639,
      1.6400000000000001, 1.641, 1.6420000000000001, 1.643, 1.6440000000000001,
      1.645, 1.6460000000000001, 1.647, 1.6480000000000001, 1.649,
      1.6500000000000001, 1.651, 1.6520000000000001, 1.653, 1.6540000000000001,
      1.655, 1.6560000000000001, 1.657, 1.658, 1.659, 1.66, 1.661, 1.662, 1.663,
      1.664, 1.665, 1.666, 1.667, 1.668, 1.669, 1.67, 1.671, 1.672, 1.673, 1.674,
      1.675, 1.676, 1.677, 1.678, 1.679, 1.68, 1.681, 1.682, 1.683, 1.684, 1.685,
      1.686, 1.687, 1.688, 1.689, 1.69, 1.691, 1.692, 1.693, 1.694, 1.695, 1.696,
      1.697, 1.698, 1.699, 1.7, 1.701, 1.702, 1.703, 1.704, 1.705, 1.706, 1.707,
      1.708, 1.709, 1.71, 1.711, 1.712, 1.713, 1.714, 1.715, 1.716, 1.717, 1.718,
      1.719, 1.72, 1.721, 1.722, 1.723, 1.724, 1.725, 1.726, 1.727, 1.728, 1.729,
      1.73, 1.731, 1.732, 1.733, 1.734, 1.735, 1.736, 1.737, 1.738, 1.739, 1.74,
      1.741, 1.742, 1.743, 1.744, 1.7449999999999999, 1.746, 1.7469999999999999,
      1.748, 1.7489999999999999, 1.75, 1.751, 1.752, 1.753, 1.754, 1.755, 1.756,
      1.757, 1.758, 1.759, 1.76, 1.761, 1.762, 1.763, 1.764, 1.765, 1.766, 1.767,
      1.768, 1.769, 1.77, 1.771, 1.772, 1.773, 1.774, 1.775, 1.776, 1.777, 1.778,
      1.779, 1.78, 1.781, 1.782, 1.783, 1.784, 1.785, 1.786, 1.787, 1.788, 1.789,
      1.79, 1.791, 1.792, 1.793, 1.794, 1.795, 1.796, 1.797, 1.798, 1.799, 1.8,
      1.801, 1.802, 1.803, 1.804, 1.805, 1.806, 1.807, 1.808, 1.809, 1.81, 1.811,
      1.812, 1.813, 1.814, 1.815, 1.816, 1.817, 1.818, 1.819, 1.82, 1.821, 1.822,
      1.823, 1.824, 1.825, 1.826, 1.827, 1.828, 1.829, 1.83, 1.831, 1.832, 1.833,
      1.834, 1.835, 1.836, 1.837, 1.838, 1.839, 1.84, 1.841, 1.842, 1.843, 1.844,
      1.845, 1.8459999999999999, 1.847, 1.8479999999999999, 1.849,
      1.8499999999999999, 1.851, 1.8519999999999999, 1.853, 1.8539999999999999,
      1.855, 1.8559999999999999, 1.857, 1.8579999999999999, 1.859,
      1.8599999999999999, 1.861, 1.8619999999999999, 1.863, 1.8639999999999999,
      1.865, 1.8659999999999999, 1.867, 1.8679999999999999, 1.869,
      1.8699999999999999, 1.871, 1.8719999999999999, 1.873, 1.8739999999999999,
      1.875, 1.876, 1.877, 1.878, 1.879, 1.88, 1.881, 1.882, 1.883, 1.884, 1.885,
      1.886, 1.887, 1.888, 1.889, 1.89, 1.891, 1.892, 1.893, 1.894, 1.895, 1.896,
      1.897, 1.898, 1.899, 1.9, 1.901, 1.902, 1.903, 1.904, 1.905, 1.906, 1.907,
      1.908, 1.909, 1.91, 1.911, 1.912, 1.913, 1.914, 1.915, 1.916, 1.917, 1.918,
      1.919, 1.92, 1.921, 1.922, 1.923, 1.924, 1.925, 1.926, 1.927, 1.928, 1.929,
      1.93, 1.931, 1.932, 1.933, 1.934, 1.935, 1.936, 1.937, 1.938, 1.939, 1.94,
      1.941, 1.942, 1.943, 1.944, 1.945, 1.946, 1.947, 1.948, 1.949, 1.95, 1.951,
      1.952, 1.953, 1.954, 1.955, 1.956, 1.957, 1.958, 1.959, 1.96, 1.961, 1.962,
      1.963, 1.964, 1.965, 1.966, 1.967, 1.968, 1.969, 1.97, 1.9709999999999999,
      1.972, 1.9729999999999999, 1.974, 1.9749999999999999, 1.976,
      1.9769999999999999, 1.978, 1.9789999999999999, 1.98, 1.9809999999999999,
      1.982, 1.9829999999999999, 1.984, 1.9849999999999999, 1.986,
      1.9869999999999999, 1.988, 1.9889999999999999, 1.99, 1.9909999999999999,
      1.992, 1.9929999999999999, 1.994, 1.9949999999999999, 1.996,
      1.9969999999999999, 1.998, 1.9989999999999999, 2.0, 2.0, 2.001, 2.002,
      2.003, 2.004, 2.005, 2.006, 2.007, 2.008, 2.009, 2.01, 2.011, 2.012, 2.013,
      2.014, 2.015, 2.016, 2.017, 2.018, 2.019, 2.02, 2.021, 2.022, 2.023, 2.024,
      2.025, 2.026, 2.027, 2.028, 2.029, 2.03, 2.031, 2.032, 2.033, 2.034, 2.035,
      2.036, 2.037, 2.038, 2.039, 2.04, 2.041, 2.042, 2.043, 2.044, 2.045, 2.046,
      2.047, 2.048, 2.049, 2.05, 2.051, 2.052, 2.053, 2.054, 2.055, 2.056, 2.057,
      2.058, 2.059, 2.06, 2.061, 2.062, 2.063, 2.064, 2.065, 2.066, 2.067, 2.068,
      2.069, 2.07, 2.071, 2.072, 2.073, 2.074, 2.075, 2.076, 2.077, 2.078, 2.079,
      2.08, 2.081, 2.082, 2.083, 2.084, 2.085, 2.086, 2.087, 2.088, 2.089, 2.09,
      2.091, 2.092, 2.093, 2.094, 2.095, 2.096, 2.097, 2.098, 2.099, 2.1, 2.101,
      2.102, 2.103, 2.104, 2.105, 2.106, 2.107, 2.108, 2.109, 2.11, 2.111, 2.112,
      2.113, 2.114, 2.115, 2.116, 2.117, 2.118, 2.119, 2.12, 2.121, 2.122, 2.123,
      2.124, 2.125, 2.126, 2.127, 2.128, 2.129, 2.13, 2.1310000000000002, 2.132,
      2.133, 2.134, 2.135, 2.136, 2.137, 2.138, 2.1390000000000002, 2.14, 2.141,
      2.142, 2.143, 2.144, 2.145, 2.146, 2.1470000000000002, 2.148, 2.149, 2.15,
      2.151, 2.152, 2.153, 2.154, 2.1550000000000002, 2.156, 2.157, 2.158, 2.159,
      2.16, 2.161, 2.162, 2.163, 2.164, 2.165, 2.166, 2.167, 2.168, 2.169, 2.17,
      2.171, 2.172, 2.173, 2.174, 2.175, 2.176, 2.177, 2.178, 2.179, 2.18, 2.181,
      2.182, 2.183, 2.184, 2.185, 2.186, 2.187, 2.188, 2.189, 2.19, 2.191, 2.192,
      2.193, 2.194, 2.195, 2.196, 2.197, 2.198, 2.199, 2.2, 2.201, 2.202, 2.203,
      2.204, 2.205, 2.206, 2.207, 2.208, 2.209, 2.21, 2.211, 2.212, 2.213, 2.214,
      2.215, 2.216, 2.217, 2.218, 2.219, 2.22, 2.221, 2.222, 2.223, 2.224, 2.225,
      2.226, 2.227, 2.228, 2.229, 2.23, 2.231, 2.232, 2.233, 2.234, 2.235, 2.236,
      2.237, 2.238, 2.239, 2.24, 2.241, 2.242, 2.243, 2.2439999999999998, 2.245,
      2.246, 2.247, 2.248, 2.249, 2.25, 2.251, 2.252, 2.253, 2.254, 2.255,
      2.2560000000000002, 2.257, 2.258, 2.259, 2.26, 2.261, 2.262, 2.263,
      2.2640000000000002, 2.265, 2.266, 2.267, 2.268, 2.269, 2.27, 2.271,
      2.2720000000000002, 2.273, 2.274, 2.275, 2.276, 2.277, 2.278, 2.279,
      2.2800000000000002, 2.281, 2.282, 2.283, 2.284, 2.285, 2.286, 2.287,
      2.2880000000000003, 2.289, 2.29, 2.291, 2.292, 2.293, 2.294, 2.295,
      2.2960000000000003, 2.297, 2.298, 2.299, 2.3, 2.301, 2.302, 2.303,
      2.3040000000000003, 2.305, 2.306, 2.307, 2.308, 2.309, 2.31, 2.311, 2.312,
      2.313, 2.314, 2.315, 2.316, 2.317, 2.318, 2.319, 2.32, 2.321, 2.322, 2.323,
      2.324, 2.325, 2.326, 2.327, 2.328, 2.329, 2.33, 2.331, 2.332, 2.333, 2.334,
      2.335, 2.336, 2.337, 2.338, 2.339, 2.34, 2.341, 2.342, 2.343, 2.344,
      2.3449999999999998, 2.346, 2.347, 2.348, 2.349, 2.35, 2.351, 2.352,
      2.3529999999999998, 2.354, 2.355, 2.356, 2.357, 2.358, 2.359, 2.36,
      2.3609999999999998, 2.362, 2.363, 2.364, 2.365, 2.366, 2.367, 2.368,
      2.3689999999999998, 2.37, 2.371, 2.372, 2.373, 2.374, 2.375, 2.376, 2.377,
      2.378, 2.379, 2.38, 2.3810000000000002, 2.382, 2.383, 2.384, 2.385, 2.386,
      2.387, 2.388, 2.3890000000000002, 2.39, 2.391, 2.392, 2.393, 2.394, 2.395,
      2.396, 2.3970000000000002, 2.398, 2.399, 2.4, 2.401, 2.402, 2.403, 2.404,
      2.4050000000000002, 2.406, 2.407, 2.408, 2.409, 2.41, 2.411, 2.412, 2.413,
      2.414, 2.415, 2.416, 2.417, 2.418, 2.419, 2.42, 2.421, 2.422, 2.423, 2.424,
      2.425, 2.426, 2.427, 2.428, 2.429, 2.43, 2.431, 2.432, 2.433, 2.434, 2.435,
      2.436, 2.437, 2.438, 2.439, 2.44, 2.441, 2.442, 2.443, 2.444, 2.445, 2.446,
      2.447, 2.448, 2.449, 2.45, 2.451, 2.452, 2.453, 2.454, 2.455, 2.456, 2.457,
      2.458, 2.459, 2.46, 2.461, 2.462, 2.463, 2.464, 2.465, 2.466, 2.467, 2.468,
      2.469, 2.47, 2.471, 2.472, 2.473, 2.474, 2.475, 2.476, 2.477,
      2.4779999999999998, 2.479, 2.48, 2.481, 2.482, 2.483, 2.484, 2.485,
      2.4859999999999998, 2.487, 2.488, 2.489, 2.49, 2.491, 2.492, 2.493,
      2.4939999999999998, 2.495, 2.496, 2.497, 2.498, 2.499, 2.5, 2.501, 2.502,
      2.503, 2.504, 2.505, 2.5060000000000002, 2.507, 2.508, 2.509, 2.51, 2.511,
      2.512, 2.513, 2.5140000000000002, 2.515, 2.516, 2.517, 2.518, 2.519, 2.52,
      2.521, 2.5220000000000002, 2.523, 2.524, 2.525, 2.526, 2.527, 2.528, 2.529,
      2.5300000000000002, 2.531, 2.532, 2.533, 2.534, 2.535, 2.536, 2.537,
      2.5380000000000003, 2.539, 2.54, 2.541, 2.542, 2.543, 2.544, 2.545,
      2.5460000000000003, 2.5469999999999997, 2.548, 2.549, 2.55, 2.551, 2.552,
      2.553, 2.5540000000000003, 2.5549999999999997, 2.556, 2.557, 2.558, 2.559,
      2.56, 2.561, 2.5620000000000003, 2.5629999999999997, 2.564, 2.565, 2.566,
      2.567, 2.568, 2.569, 2.5700000000000003, 2.5709999999999997, 2.572, 2.573,
      2.574, 2.575, 2.576, 2.577, 2.5780000000000003, 2.5789999999999997, 2.58,
      2.581, 2.582, 2.583, 2.584, 2.585, 2.5860000000000003, 2.5869999999999997,
      2.588, 2.589, 2.59, 2.591, 2.592, 2.593, 2.5940000000000003,
      2.5949999999999998, 2.596, 2.597, 2.598, 2.599, 2.6, 2.601,
      2.6020000000000003, 2.6029999999999998, 2.604, 2.605, 2.606, 2.607, 2.608,
      2.609, 2.6100000000000003, 2.6109999999999998, 2.612, 2.613, 2.614, 2.615,
      2.616, 2.617, 2.618, 2.6189999999999998, 2.62, 2.621, 2.622, 2.623, 2.624,
      2.625, 2.626, 2.627, 2.628, 2.629, 2.63, 2.6310000000000002, 2.632, 2.633,
      2.634, 2.635, 2.636, 2.637, 2.638, 2.6390000000000002, 2.64, 2.641, 2.642,
      2.643, 2.644, 2.645, 2.646, 2.6470000000000002, 2.648, 2.649, 2.65, 2.651,
      2.652, 2.653, 2.654, 2.6550000000000002, 2.656, 2.657, 2.658, 2.659, 2.66,
      2.661, 2.662, 2.6630000000000003, 2.6639999999999997, 2.665, 2.666, 2.667,
      2.668, 2.669, 2.67, 2.6710000000000003, 2.6719999999999997, 2.673, 2.674,
      2.675, 2.676, 2.677, 2.678, 2.6790000000000003, 2.6799999999999997, 2.681,
      2.682, 2.683, 2.684, 2.685, 2.686, 2.6870000000000003, 2.6879999999999997,
      2.689, 2.69, 2.691, 2.692, 2.693, 2.694, 2.6950000000000003,
      2.6959999999999997, 2.697, 2.698, 2.699, 2.7, 2.701, 2.702,
      2.7030000000000003, 2.7039999999999997, 2.705, 2.706, 2.707, 2.708, 2.709,
      2.71, 2.7110000000000003, 2.7119999999999997, 2.713, 2.714, 2.715, 2.716,
      2.717, 2.718, 2.7190000000000003, 2.7199999999999998, 2.721, 2.722, 2.723,
      2.724, 2.725, 2.726, 2.7270000000000003, 2.7279999999999998, 2.729, 2.73,
      2.731, 2.732, 2.733, 2.734, 2.7350000000000003, 2.7359999999999998, 2.737,
      2.738, 2.739, 2.74, 2.741, 2.742, 2.7430000000000003, 2.7439999999999998,
      2.745, 2.746, 2.747, 2.748, 2.7489999999999997, 2.75, 2.751, 2.752, 2.753,
      2.754, 2.755, 2.7560000000000002, 2.7569999999999997, 2.758, 2.759, 2.76,
      2.761, 2.762, 2.763, 2.7640000000000002, 2.7649999999999997, 2.766, 2.767,
      2.768, 2.769, 2.77, 2.771, 2.7720000000000002, 2.7729999999999997, 2.774,
      2.775, 2.776, 2.777, 2.778, 2.779, 2.7800000000000002, 2.7809999999999997,
      2.782, 2.783, 2.784, 2.785, 2.786, 2.787, 2.7880000000000003,
      2.7889999999999997, 2.79, 2.791, 2.792, 2.793, 2.794, 2.795,
      2.7960000000000003, 2.7969999999999997, 2.798, 2.799, 2.8, 2.801, 2.802,
      2.803, 2.8040000000000003, 2.8049999999999997, 2.806, 2.807, 2.808, 2.809,
      2.81, 2.811, 2.8120000000000003, 2.8129999999999997, 2.814, 2.815, 2.816,
      2.817, 2.818, 2.819, 2.8200000000000003, 2.8209999999999997, 2.822, 2.823,
      2.824, 2.825, 2.826, 2.827, 2.8280000000000003, 2.8289999999999997, 2.83,
      2.831, 2.832, 2.833, 2.834, 2.835, 2.8360000000000003, 2.8369999999999997,
      2.838, 2.839, 2.84, 2.841, 2.842, 2.843, 2.8440000000000003,
      2.8449999999999998, 2.846, 2.847, 2.848, 2.849, 2.8499999999999996, 2.851,
      2.852, 2.8529999999999998, 2.854, 2.855, 2.856, 2.857, 2.8579999999999997,
      2.859, 2.86, 2.8609999999999998, 2.862, 2.863, 2.864, 2.865,
      2.8659999999999997, 2.867, 2.868, 2.8689999999999998, 2.87, 2.871, 2.872,
      2.873, 2.8739999999999997, 2.875, 2.876, 2.877, 2.878, 2.879, 2.88,
      2.8810000000000002, 2.8819999999999997, 2.883, 2.884, 2.885, 2.886, 2.887,
      2.888, 2.8890000000000002, 2.8899999999999997, 2.891, 2.892, 2.893, 2.894,
      2.895, 2.896, 2.8970000000000002, 2.8979999999999997, 2.899, 2.9, 2.901,
      2.902, 2.903, 2.904, 2.9050000000000002, 2.9059999999999997, 2.907, 2.908,
      2.909, 2.91, 2.911, 2.912, 2.9130000000000003, 2.9139999999999997, 2.915,
      2.916, 2.917, 2.918, 2.919, 2.92, 2.9210000000000003, 2.9219999999999997,
      2.923, 2.924, 2.925, 2.926, 2.927, 2.928, 2.9290000000000003,
      2.9299999999999997, 2.931, 2.932, 2.933, 2.934, 2.935, 2.936,
      2.9370000000000003, 2.9379999999999997, 2.939, 2.94, 2.941, 2.942, 2.943,
      2.944, 2.9450000000000003, 2.9459999999999997, 2.947, 2.948, 2.949, 2.95,
      2.951, 2.952, 2.9530000000000003, 2.9539999999999997, 2.955, 2.956, 2.957,
      2.958, 2.959, 2.96, 2.9610000000000003, 2.9619999999999997, 2.963, 2.964,
      2.965, 2.966, 2.967, 2.968, 2.9690000000000003, 2.9699999999999998, 2.971,
      2.972, 2.973, 2.974, 2.9749999999999996, 2.976, 2.977, 2.9779999999999998,
      2.979, 2.98, 2.981, 2.982, 2.9829999999999997, 2.984, 2.985,
      2.9859999999999998, 2.987, 2.988, 2.989, 2.99, 2.9909999999999997, 2.992,
      2.993, 2.9939999999999998, 2.995, 2.996, 2.997, 2.998, 2.9989999999999997,
      3.0, 3.0010000000000003, 3.002, 3.003, 3.004, 3.005, 3.0060000000000002,
      3.007, 3.008, 3.0090000000000003, 3.01, 3.011, 3.012, 3.013,
      3.0140000000000002, 3.015, 3.016, 3.0170000000000003, 3.018, 3.019, 3.02,
      3.021, 3.0220000000000002, 3.023, 3.024, 3.0250000000000004, 3.026, 3.027,
      3.028, 3.029, 3.0300000000000002, 3.031, 3.032, 3.0330000000000004, 3.034,
      3.035, 3.036, 3.037, 3.0380000000000003, 3.039, 3.04, 3.0410000000000004,
      3.042, 3.043, 3.044, 3.045, 3.0460000000000003, 3.0469999999999997, 3.048,
      3.049, 3.05, 3.051, 3.052, 3.053, 3.0540000000000003, 3.0549999999999997,
      3.056, 3.057, 3.058, 3.059, 3.06, 3.061, 3.0620000000000003,
      3.0629999999999997, 3.064, 3.065, 3.066, 3.067, 3.068, 3.069,
      3.0700000000000003, 3.0709999999999997, 3.072, 3.073, 3.074, 3.075, 3.076,
      3.077, 3.0780000000000003, 3.0789999999999997, 3.08, 3.081, 3.082, 3.083,
      3.084, 3.085, 3.0860000000000003, 3.0869999999999997, 3.088, 3.089, 3.09,
      3.091, 3.092, 3.093, 3.0940000000000003, 3.0949999999999998, 3.096, 3.097,
      3.098, 3.099, 3.1, 3.101, 3.1020000000000003, 3.1029999999999998, 3.104,
      3.105, 3.106, 3.107, 3.108, 3.109, 3.1100000000000003, 3.1109999999999998,
      3.112, 3.113, 3.114, 3.115, 3.116, 3.117, 3.1180000000000003,
      3.1189999999999998, 3.12, 3.121, 3.122, 3.123, 3.124, 3.125,
      3.1260000000000003, 3.127, 3.128, 3.129, 3.13, 3.1310000000000002, 3.132,
      3.133, 3.1340000000000003, 3.135, 3.136, 3.137, 3.138, 3.1390000000000002,
      3.14, 3.141, 3.1420000000000003, 3.143, 3.144, 3.145, 3.146,
      3.1470000000000002, 3.148, 3.149, 3.1500000000000004, 3.151, 3.152, 3.153,
      3.154, 3.1550000000000002, 3.156, 3.157, 3.158, 3.159, 3.16, 3.161, 3.162,
      3.1630000000000003, 3.1639999999999997, 3.165, 3.166, 3.167, 3.168, 3.169,
      3.17, 3.1710000000000003, 3.1719999999999997, 3.173, 3.174, 3.175, 3.176,
      3.177, 3.178, 3.1790000000000003, 3.1799999999999997, 3.181, 3.182, 3.183,
      3.184, 3.185, 3.186, 3.1870000000000003, 3.1879999999999997, 3.189, 3.19,
      3.191, 3.192, 3.193, 3.194, 3.1950000000000003, 3.1959999999999997, 3.197,
      3.198, 3.199, 3.2, 3.201, 3.202, 3.2030000000000003, 3.2039999999999997,
      3.205, 3.206, 3.207, 3.208, 3.209, 3.21, 3.2110000000000003,
      3.2119999999999997, 3.213, 3.214, 3.215, 3.216, 3.217, 3.218,
      3.2190000000000003, 3.2199999999999998, 3.221, 3.222, 3.223, 3.224, 3.225,
      3.226, 3.2270000000000003, 3.2279999999999998, 3.229, 3.23, 3.231, 3.232,
      3.233, 3.234, 3.2350000000000003, 3.2359999999999998, 3.237, 3.238, 3.239,
      3.24, 3.241, 3.242, 3.2430000000000003, 3.2439999999999998, 3.245, 3.246,
      3.247, 3.248, 3.2489999999999997, 3.25, 3.251, 3.252, 3.253, 3.254, 3.255,
      3.2560000000000002, 3.2569999999999997, 3.258, 3.259, 3.26, 3.261, 3.262,
      3.263, 3.2640000000000002, 3.2649999999999997, 3.266, 3.267, 3.268, 3.269,
      3.27, 3.271, 3.2720000000000002, 3.2729999999999997, 3.274, 3.275, 3.276,
      3.277, 3.278, 3.279, 3.2800000000000002, 3.2809999999999997, 3.282, 3.283,
      3.284, 3.285, 3.286, 3.287, 3.2880000000000003, 3.2889999999999997, 3.29,
      3.291, 3.292, 3.293, 3.294, 3.295, 3.2960000000000003, 3.2969999999999997,
      3.298, 3.299, 3.3, 3.301, 3.302, 3.303, 3.3040000000000003,
      3.3049999999999997, 3.306, 3.307, 3.308, 3.309, 3.31, 3.311,
      3.3120000000000003, 3.3129999999999997, 3.314, 3.315, 3.316, 3.317, 3.318,
      3.319, 3.3200000000000003, 3.3209999999999997, 3.322, 3.323, 3.324, 3.325,
      3.326, 3.327, 3.3280000000000003, 3.3289999999999997, 3.33, 3.331, 3.332,
      3.333, 3.334, 3.335, 3.3360000000000003, 3.3369999999999997, 3.338, 3.339,
      3.34, 3.341, 3.342, 3.343, 3.3440000000000003, 3.3449999999999998, 3.346,
      3.347, 3.348, 3.349, 3.3499999999999996, 3.351, 3.352, 3.3529999999999998,
      3.354, 3.355, 3.356, 3.357, 3.3579999999999997, 3.359, 3.36,
      3.3609999999999998, 3.362, 3.363, 3.364, 3.365, 3.3659999999999997, 3.367,
      3.368, 3.3689999999999998, 3.37, 3.371, 3.372, 3.373, 3.3739999999999997,
      3.375, 3.376, 3.377, 3.378, 3.379, 3.38, 3.3810000000000002,
      3.3819999999999997, 3.383, 3.384, 3.385, 3.386, 3.387, 3.388,
      3.3890000000000002, 3.3899999999999997, 3.391, 3.392, 3.393, 3.394, 3.395,
      3.396, 3.3970000000000002, 3.3979999999999997, 3.399, 3.4, 3.401, 3.402,
      3.403, 3.404, 3.4050000000000002, 3.4059999999999997, 3.407, 3.408, 3.409,
      3.41, 3.411, 3.412, 3.4130000000000003, 3.4139999999999997, 3.415, 3.416,
      3.417, 3.418, 3.419, 3.42, 3.4210000000000003, 3.4219999999999997, 3.423,
      3.424, 3.425, 3.426, 3.427, 3.428, 3.4290000000000003, 3.4299999999999997,
      3.431, 3.432, 3.433, 3.434, 3.435, 3.436, 3.4370000000000003,
      3.4379999999999997, 3.439, 3.44, 3.441, 3.442, 3.443, 3.444,
      3.4450000000000003, 3.4459999999999997, 3.447, 3.448, 3.449, 3.45, 3.451,
      3.452, 3.4530000000000003, 3.4539999999999997, 3.455, 3.456, 3.457, 3.458,
      3.459, 3.46, 3.4610000000000003, 3.4619999999999997, 3.463, 3.464, 3.465,
      3.466, 3.467, 3.468, 3.4690000000000003, 3.4699999999999998, 3.471, 3.472,
      3.473, 3.474, 3.4749999999999996, 3.476, 3.477, 3.4779999999999998, 3.479,
      3.48, 3.481, 3.482, 3.4829999999999997, 3.484, 3.485, 3.4859999999999998,
      3.487, 3.488, 3.489, 3.49, 3.4909999999999997, 3.492, 3.493,
      3.4939999999999998, 3.495, 3.496, 3.497, 3.498, 3.4989999999999997, 3.5,
      3.5010000000000003, 3.502, 3.503, 3.504, 3.505, 3.5060000000000002, 3.507,
      3.508, 3.5090000000000003, 3.51, 3.511, 3.512, 3.513, 3.5140000000000002,
      3.515, 3.516, 3.5170000000000003, 3.518, 3.519, 3.52, 3.521,
      3.5220000000000002, 3.523, 3.524, 3.5250000000000004, 3.526, 3.527, 3.528,
      3.529, 3.5300000000000002, 3.531, 3.532, 3.5330000000000004, 3.534, 3.535,
      3.536, 3.537, 3.5380000000000003, 3.539, 3.54, 3.5410000000000004, 3.542,
      3.543, 3.544, 3.545, 3.5460000000000003, 3.5469999999999997, 3.548, 3.549,
      3.55, 3.551, 3.552, 3.553, 3.5540000000000003, 3.5549999999999997, 3.556,
      3.557, 3.558, 3.559, 3.56, 3.561, 3.5620000000000003, 3.5629999999999997,
      3.564, 3.565, 3.566, 3.567, 3.568, 3.569, 3.5700000000000003,
      3.5709999999999997, 3.572, 3.573, 3.574, 3.575, 3.576, 3.577,
      3.5780000000000003, 3.5789999999999997, 3.58, 3.581, 3.582, 3.583, 3.584,
      3.585, 3.5860000000000003, 3.5869999999999997, 3.588, 3.589, 3.59, 3.591,
      3.592, 3.593, 3.5940000000000003, 3.5949999999999998, 3.596, 3.597, 3.598,
      3.599, 3.6, 3.601, 3.6020000000000003, 3.6029999999999998, 3.604, 3.605,
      3.606, 3.607, 3.608, 3.609, 3.6100000000000003, 3.6109999999999998, 3.612,
      3.613, 3.614, 3.615, 3.616, 3.617, 3.6180000000000003, 3.6189999999999998,
      3.62, 3.621, 3.622, 3.623, 3.624, 3.625, 3.6260000000000003, 3.627, 3.628,
      3.629, 3.63, 3.6310000000000002, 3.632, 3.633, 3.6340000000000003, 3.635,
      3.636, 3.637, 3.638, 3.6390000000000002, 3.64, 3.641, 3.6420000000000003,
      3.643, 3.644, 3.645, 3.646, 3.6470000000000002, 3.648, 3.649,
      3.6500000000000004, 3.651, 3.652, 3.653, 3.654, 3.6550000000000002, 3.656,
      3.657, 3.658, 3.659, 3.66, 3.661, 3.662, 3.6630000000000003,
      3.6639999999999997, 3.665, 3.666, 3.667, 3.668, 3.669, 3.67,
      3.6710000000000003, 3.6719999999999997, 3.673, 3.674, 3.675, 3.676, 3.677,
      3.678, 3.6790000000000003, 3.6799999999999997, 3.681, 3.682, 3.683, 3.684,
      3.685, 3.686, 3.6870000000000003, 3.6879999999999997, 3.689, 3.69, 3.691,
      3.692, 3.693, 3.694, 3.6950000000000003, 3.6959999999999997, 3.697, 3.698,
      3.699, 3.7, 3.701, 3.702, 3.7030000000000003, 3.7039999999999997, 3.705,
      3.706, 3.707, 3.708, 3.709, 3.71, 3.7110000000000003, 3.7119999999999997,
      3.713, 3.714, 3.715, 3.716, 3.717, 3.718, 3.7190000000000003,
      3.7199999999999998, 3.721, 3.722, 3.723, 3.724, 3.725, 3.726,
      3.7270000000000003, 3.7279999999999998, 3.729, 3.73, 3.731, 3.732, 3.733,
      3.734, 3.7350000000000003, 3.7359999999999998, 3.737, 3.738, 3.739, 3.74,
      3.741, 3.742, 3.7430000000000003, 3.7439999999999998, 3.745, 3.746, 3.747,
      3.748, 3.7489999999999997, 3.75, 3.751, 3.752, 3.753, 3.754, 3.755,
      3.7560000000000002, 3.7569999999999997, 3.758, 3.759, 3.76, 3.761, 3.762,
      3.763, 3.7640000000000002, 3.7649999999999997, 3.766, 3.767, 3.768, 3.769,
      3.77, 3.771, 3.7720000000000002, 3.7729999999999997, 3.774, 3.775, 3.776,
      3.777, 3.778, 3.779, 3.7800000000000002, 3.7809999999999997, 3.782, 3.783,
      3.784, 3.785, 3.786, 3.787, 3.7880000000000003, 3.7889999999999997, 3.79,
      3.791, 3.792, 3.793, 3.794, 3.795, 3.7960000000000003, 3.7969999999999997,
      3.798, 3.799, 3.8, 3.801, 3.802, 3.803, 3.8040000000000003,
      3.8049999999999997, 3.806, 3.807, 3.808, 3.809, 3.81, 3.811,
      3.8120000000000003, 3.8129999999999997, 3.814, 3.815, 3.816, 3.817, 3.818,
      3.819, 3.8200000000000003, 3.8209999999999997, 3.822, 3.823, 3.824, 3.825,
      3.826, 3.827, 3.8280000000000003, 3.8289999999999997, 3.83, 3.831, 3.832,
      3.833, 3.834, 3.835, 3.8360000000000003, 3.8369999999999997, 3.838, 3.839,
      3.84, 3.841, 3.842, 3.843, 3.8440000000000003, 3.8449999999999998, 3.846,
      3.847, 3.848, 3.849, 3.8499999999999996, 3.851, 3.852, 3.8529999999999998,
      3.854, 3.855, 3.856, 3.857, 3.8579999999999997, 3.859, 3.86,
      3.8609999999999998, 3.862, 3.863, 3.864, 3.865, 3.8659999999999997, 3.867,
      3.868, 3.8689999999999998, 3.87, 3.871, 3.872, 3.873, 3.8739999999999997,
      3.875, 3.876, 3.877, 3.878, 3.879, 3.88, 3.8810000000000002,
      3.8819999999999997, 3.883, 3.884, 3.885, 3.886, 3.887, 3.888,
      3.8890000000000002, 3.8899999999999997, 3.891, 3.892, 3.893, 3.894, 3.895,
      3.896, 3.8970000000000002, 3.8979999999999997, 3.899, 3.9, 3.901, 3.902,
      3.903, 3.904, 3.9050000000000002, 3.9059999999999997, 3.907, 3.908, 3.909,
      3.91, 3.911, 3.912, 3.9130000000000003, 3.9139999999999997, 3.915, 3.916,
      3.917, 3.918, 3.919, 3.92, 3.9210000000000003, 3.9219999999999997, 3.923,
      3.924, 3.925, 3.926, 3.927, 3.928, 3.9290000000000003, 3.9299999999999997,
      3.931, 3.932, 3.933, 3.934, 3.935, 3.936, 3.9370000000000003,
      3.9379999999999997, 3.939, 3.94, 3.941, 3.942, 3.943, 3.944,
      3.9450000000000003, 3.9459999999999997, 3.947, 3.948, 3.949, 3.95, 3.951,
      3.952, 3.9530000000000003, 3.9539999999999997, 3.955, 3.956, 3.957, 3.958,
      3.959, 3.96, 3.9610000000000003, 3.9619999999999997, 3.963, 3.964, 3.965,
      3.966, 3.967, 3.968, 3.9690000000000003, 3.9699999999999998, 3.971, 3.972,
      3.973, 3.974, 3.9749999999999996, 3.976, 3.977, 3.9779999999999998, 3.979,
      3.98, 3.981, 3.982, 3.9829999999999997, 3.984, 3.985, 3.9859999999999998,
      3.987, 3.988, 3.989, 3.99, 3.9909999999999997, 3.992, 3.993,
      3.9939999999999998, 3.995, 3.996, 3.997, 3.998, 3.9989999999999997, 4.0,
      4.0, 4.001, 4.002, 4.003, 4.004, 4.005, 4.006, 4.007, 4.008, 4.009, 4.01,
      4.011, 4.012, 4.013, 4.014, 4.015, 4.016, 4.017, 4.018, 4.019, 4.02, 4.021,
      4.022, 4.023, 4.024, 4.025, 4.026, 4.027, 4.028, 4.029, 4.03, 4.031, 4.032,
      4.033, 4.034, 4.035, 4.036, 4.037, 4.038, 4.039, 4.04, 4.041, 4.042, 4.043,
      4.044, 4.045, 4.046, 4.047, 4.048, 4.049, 4.05, 4.051, 4.052, 4.053, 4.054,
      4.055, 4.056, 4.057, 4.058, 4.059, 4.06, 4.061, 4.062, 4.063, 4.064, 4.065,
      4.066, 4.067, 4.068, 4.069, 4.07, 4.071, 4.072, 4.073, 4.074, 4.075, 4.076,
      4.077, 4.078, 4.079, 4.08, 4.081, 4.082, 4.083, 4.084, 4.085, 4.086, 4.087,
      4.088, 4.089, 4.09, 4.091, 4.092, 4.093, 4.094, 4.095, 4.096, 4.097, 4.098,
      4.099, 4.1, 4.101, 4.102, 4.103, 4.104, 4.105, 4.106, 4.107, 4.108, 4.109,
      4.11, 4.111, 4.112, 4.113, 4.114, 4.115, 4.116, 4.117, 4.118, 4.119, 4.12,
      4.121, 4.122, 4.123, 4.124, 4.125, 4.126, 4.127, 4.128, 4.129, 4.13, 4.131,
      4.132, 4.133, 4.134, 4.135, 4.136, 4.1370000000000005, 4.138, 4.139, 4.14,
      4.141, 4.142, 4.143, 4.144, 4.145, 4.146, 4.147, 4.148, 4.149, 4.15, 4.151,
      4.152, 4.1530000000000005, 4.154, 4.155, 4.156, 4.157, 4.158, 4.159, 4.16,
      4.161, 4.162, 4.163, 4.164, 4.165, 4.166, 4.167, 4.168, 4.169, 4.17, 4.171,
      4.172, 4.173, 4.174, 4.175, 4.176, 4.177, 4.178, 4.179, 4.18, 4.181, 4.182,
      4.183, 4.184, 4.185, 4.186, 4.187, 4.188, 4.189, 4.19, 4.191, 4.192, 4.193,
      4.194, 4.195, 4.196, 4.197, 4.198, 4.199, 4.2, 4.201, 4.202, 4.203, 4.204,
      4.205, 4.206, 4.207, 4.208, 4.209, 4.21, 4.211, 4.212, 4.213, 4.214, 4.215,
      4.216, 4.217, 4.218, 4.219, 4.22, 4.221, 4.222, 4.223, 4.224, 4.225, 4.226,
      4.227, 4.228, 4.229, 4.23, 4.231, 4.232, 4.233, 4.234, 4.235, 4.236, 4.237,
      4.238, 4.239, 4.24, 4.241, 4.242, 4.243, 4.244, 4.245, 4.246, 4.247, 4.248,
      4.249, 4.25, 4.251, 4.252, 4.253, 4.254, 4.255, 4.256, 4.257, 4.258, 4.259,
      4.26, 4.261, 4.2620000000000005, 4.263, 4.264, 4.265, 4.266, 4.267, 4.268,
      4.269, 4.27, 4.271, 4.272, 4.273, 4.274, 4.275, 4.276, 4.277,
      4.2780000000000005, 4.279, 4.28, 4.281, 4.282, 4.283, 4.284, 4.285, 4.286,
      4.287, 4.288, 4.289, 4.29, 4.291, 4.292, 4.293, 4.2940000000000005, 4.295,
      4.296, 4.297, 4.298, 4.299, 4.3, 4.301, 4.302, 4.303, 4.304, 4.305, 4.306,
      4.307, 4.308, 4.309, 4.31, 4.311, 4.312, 4.313, 4.314, 4.315, 4.316, 4.317,
      4.318, 4.319, 4.32, 4.321, 4.322, 4.323, 4.324, 4.325, 4.326, 4.327, 4.328,
      4.329, 4.33, 4.331, 4.332, 4.333, 4.334, 4.335, 4.336, 4.337, 4.338, 4.339,
      4.34, 4.341, 4.342, 4.343, 4.344, 4.345, 4.346, 4.3469999999999995, 4.348,
      4.349, 4.35, 4.351, 4.352, 4.353, 4.354, 4.355, 4.356, 4.357, 4.358, 4.359,
      4.36, 4.361, 4.362, 4.3629999999999995, 4.364, 4.365, 4.366, 4.367, 4.368,
      4.369, 4.37, 4.371, 4.372, 4.373, 4.374, 4.375, 4.376, 4.377, 4.378, 4.379,
      4.38, 4.381, 4.382, 4.383, 4.384, 4.385, 4.386, 4.3870000000000005, 4.388,
      4.389, 4.39, 4.391, 4.392, 4.393, 4.394, 4.395, 4.396, 4.397, 4.398, 4.399,
      4.4, 4.401, 4.402, 4.4030000000000005, 4.404, 4.405, 4.406, 4.407, 4.408,
      4.409, 4.41, 4.411, 4.412, 4.413, 4.414, 4.415, 4.416, 4.417, 4.418, 4.419,
      4.42, 4.421, 4.422, 4.423, 4.424, 4.425, 4.426, 4.427, 4.428, 4.429, 4.43,
      4.431, 4.432, 4.433, 4.434, 4.435, 4.436, 4.437, 4.438, 4.439, 4.44, 4.441,
      4.442, 4.443, 4.444, 4.445, 4.446, 4.447, 4.448, 4.449, 4.45, 4.451, 4.452,
      4.453, 4.454, 4.455, 4.456, 4.457, 4.458, 4.459, 4.46, 4.461, 4.462, 4.463,
      4.464, 4.465, 4.466, 4.467, 4.468, 4.469, 4.47, 4.471, 4.4719999999999995,
      4.473, 4.474, 4.475, 4.476, 4.477, 4.478, 4.479, 4.48, 4.481, 4.482, 4.483,
      4.484, 4.485, 4.486, 4.487, 4.4879999999999995, 4.489, 4.49, 4.491, 4.492,
      4.493, 4.494, 4.495, 4.496, 4.497, 4.498, 4.499, 4.5, 4.501, 4.502, 4.503,
      4.504, 4.505, 4.506, 4.507, 4.508, 4.509, 4.51, 4.511, 4.5120000000000005,
      4.513, 4.514, 4.515, 4.516, 4.517, 4.518, 4.519, 4.52, 4.521, 4.522, 4.523,
      4.524, 4.525, 4.526, 4.527, 4.5280000000000005, 4.529, 4.53, 4.531, 4.532,
      4.533, 4.534, 4.535, 4.536, 4.537, 4.538, 4.539, 4.54, 4.541, 4.542, 4.543,
      4.5440000000000005, 4.545, 4.546, 4.547, 4.548, 4.5489999999999995, 4.55,
      4.551, 4.552, 4.553, 4.554, 4.555, 4.556, 4.557, 4.558, 4.559,
      4.5600000000000005, 4.561, 4.562, 4.563, 4.564, 4.5649999999999995, 4.566,
      4.567, 4.568, 4.569, 4.57, 4.571, 4.572, 4.573, 4.574, 4.575,
      4.5760000000000005, 4.577, 4.578, 4.579, 4.58, 4.5809999999999995, 4.582,
      4.583, 4.584, 4.585, 4.586, 4.587, 4.588, 4.589, 4.59, 4.591,
      4.5920000000000005, 4.593, 4.594, 4.595, 4.596, 4.5969999999999995, 4.598,
      4.599, 4.6, 4.601, 4.602, 4.603, 4.604, 4.605, 4.606, 4.607,
      4.6080000000000005, 4.609, 4.61, 4.611, 4.612, 4.6129999999999995, 4.614,
      4.615, 4.616, 4.617, 4.618, 4.619, 4.62, 4.621, 4.622, 4.623, 4.624, 4.625,
      4.626, 4.627, 4.628, 4.629, 4.63, 4.631, 4.632, 4.633, 4.634, 4.635, 4.636,
      4.6370000000000005, 4.638, 4.639, 4.64, 4.641, 4.642, 4.643, 4.644, 4.645,
      4.646, 4.647, 4.648, 4.649, 4.65, 4.651, 4.652, 4.6530000000000005, 4.654,
      4.655, 4.656, 4.657, 4.6579999999999995, 4.659, 4.66, 4.661, 4.662, 4.663,
      4.664, 4.665, 4.666, 4.667, 4.668, 4.6690000000000005, 4.67, 4.671, 4.672,
      4.673, 4.6739999999999995, 4.675, 4.676, 4.677, 4.678, 4.679, 4.68, 4.681,
      4.682, 4.683, 4.684, 4.6850000000000005, 4.686, 4.687, 4.688, 4.689,
      4.6899999999999995, 4.691, 4.692, 4.693, 4.694, 4.695, 4.696, 4.697, 4.698,
      4.699, 4.7, 4.7010000000000005, 4.702, 4.703, 4.704, 4.705,
      4.7059999999999995, 4.707, 4.708, 4.709, 4.71, 4.711, 4.712, 4.713, 4.714,
      4.715, 4.716, 4.7170000000000005, 4.718, 4.719, 4.72, 4.721,
      4.7219999999999995, 4.723, 4.724, 4.725, 4.726, 4.727, 4.728, 4.729, 4.73,
      4.731, 4.732, 4.7330000000000005, 4.734, 4.735, 4.736, 4.737,
      4.7379999999999995, 4.739, 4.74, 4.741, 4.742, 4.743, 4.744, 4.745, 4.746,
      4.747, 4.748, 4.749, 4.75, 4.7509999999999994, 4.752, 4.753, 4.754, 4.755,
      4.756, 4.757, 4.758, 4.759, 4.76, 4.761, 4.7620000000000005, 4.763, 4.764,
      4.765, 4.766, 4.7669999999999995, 4.768, 4.769, 4.77, 4.771, 4.772, 4.773,
      4.774, 4.775, 4.776, 4.777, 4.7780000000000005, 4.779, 4.78, 4.781, 4.782,
      4.7829999999999995, 4.784, 4.785, 4.786, 4.787, 4.788, 4.789, 4.79, 4.791,
      4.792, 4.793, 4.7940000000000005, 4.795, 4.796, 4.797, 4.798,
      4.7989999999999995, 4.8, 4.801, 4.802, 4.803, 4.804, 4.805, 4.806, 4.807,
      4.808, 4.809, 4.8100000000000005, 4.811, 4.812, 4.813, 4.814,
      4.8149999999999995, 4.816, 4.817, 4.818, 4.819, 4.82, 4.821, 4.822, 4.823,
      4.824, 4.825, 4.8260000000000005, 4.827, 4.828, 4.829, 4.83,
      4.8309999999999995, 4.832, 4.833, 4.834, 4.835, 4.836, 4.837, 4.838, 4.839,
      4.84, 4.841, 4.8420000000000005, 4.843, 4.844, 4.845, 4.846,
      4.8469999999999995, 4.848, 4.849, 4.85, 4.851, 4.852, 4.853, 4.854, 4.855,
      4.856, 4.857, 4.858, 4.859, 4.8599999999999994, 4.861, 4.862,
      4.8629999999999995, 4.864, 4.865, 4.866, 4.867, 4.868, 4.869, 4.87, 4.871,
      4.872, 4.873, 4.874, 4.875, 4.8759999999999994, 4.877, 4.878, 4.879, 4.88,
      4.881, 4.882, 4.883, 4.884, 4.885, 4.886, 4.8870000000000005, 4.888, 4.889,
      4.89, 4.891, 4.8919999999999995, 4.893, 4.894, 4.895, 4.896, 4.897, 4.898,
      4.899, 4.9, 4.901, 4.902, 4.9030000000000005, 4.904, 4.905, 4.906, 4.907,
      4.9079999999999995, 4.909, 4.91, 4.911, 4.912, 4.913, 4.914, 4.915, 4.916,
      4.917, 4.918, 4.9190000000000005, 4.92, 4.921, 4.922, 4.923,
      4.9239999999999995, 4.925, 4.926, 4.927, 4.928, 4.929, 4.93, 4.931, 4.932,
      4.933, 4.934, 4.9350000000000005, 4.936, 4.937, 4.938, 4.939,
      4.9399999999999995, 4.941, 4.942, 4.943, 4.944, 4.945, 4.946, 4.947, 4.948,
      4.949, 4.95, 4.9510000000000005, 4.952, 4.953, 4.954, 4.955,
      4.9559999999999995, 4.957, 4.958, 4.959, 4.96, 4.961, 4.962, 4.963, 4.964,
      4.965, 4.966, 4.9670000000000005, 4.968, 4.969, 4.97, 4.971,
      4.9719999999999995, 4.973, 4.974, 4.975, 4.976, 4.977, 4.978, 4.979, 4.98,
      4.981, 4.982, 4.983, 4.984, 4.9849999999999994, 4.986, 4.987,
      4.9879999999999995, 4.989, 4.99, 4.991, 4.992, 4.993, 4.994, 4.995, 4.996,
      4.997, 4.998, 4.999, 5.0, 5.001, 5.002, 5.003, 5.004, 5.005, 5.006, 5.007,
      5.008, 5.009, 5.01, 5.011, 5.0120000000000005, 5.013, 5.014,
      5.0150000000000006, 5.016, 5.017, 5.018, 5.019, 5.02, 5.021, 5.022, 5.023,
      5.024, 5.025, 5.026, 5.027, 5.0280000000000005, 5.029, 5.03,
      5.0310000000000006, 5.032, 5.033, 5.034, 5.035, 5.036, 5.037, 5.038, 5.039,
      5.04, 5.041, 5.042, 5.043, 5.0440000000000005, 5.045, 5.046, 5.047, 5.048,
      5.0489999999999995, 5.05, 5.051, 5.052, 5.053, 5.054, 5.055, 5.056, 5.057,
      5.058, 5.059, 5.0600000000000005, 5.061, 5.062, 5.063, 5.064,
      5.0649999999999995, 5.066, 5.067, 5.068, 5.069, 5.07, 5.071, 5.072, 5.073,
      5.074, 5.075, 5.0760000000000005, 5.077, 5.078, 5.079, 5.08,
      5.0809999999999995, 5.082, 5.083, 5.084, 5.085, 5.086, 5.087, 5.088, 5.089,
      5.09, 5.091, 5.0920000000000005, 5.093, 5.094, 5.095, 5.096,
      5.0969999999999995, 5.098, 5.099, 5.1, 5.101, 5.102, 5.103, 5.104, 5.105,
      5.106, 5.107, 5.1080000000000005, 5.109, 5.11, 5.111, 5.112,
      5.1129999999999995, 5.114, 5.115, 5.116, 5.117, 5.118, 5.119, 5.12, 5.121,
      5.122, 5.123, 5.1240000000000006, 5.125, 5.126, 5.127, 5.128, 5.129, 5.13,
      5.131, 5.132, 5.133, 5.134, 5.135, 5.136, 5.1370000000000005, 5.138, 5.139,
      5.1400000000000006, 5.141, 5.142, 5.143, 5.144, 5.145, 5.146, 5.147, 5.148,
      5.149, 5.15, 5.151, 5.152, 5.1530000000000005, 5.154, 5.155,
      5.1560000000000006, 5.157, 5.1579999999999995, 5.159, 5.16, 5.161, 5.162,
      5.163, 5.164, 5.165, 5.166, 5.167, 5.168, 5.1690000000000005, 5.17, 5.171,
      5.172, 5.173, 5.1739999999999995, 5.175, 5.176, 5.177, 5.178, 5.179, 5.18,
      5.181, 5.182, 5.183, 5.184, 5.1850000000000005, 5.186, 5.187, 5.188, 5.189,
      5.1899999999999995, 5.191, 5.192, 5.193, 5.194, 5.195, 5.196, 5.197, 5.198,
      5.199, 5.2, 5.2010000000000005, 5.202, 5.203, 5.204, 5.205,
      5.2059999999999995, 5.207, 5.208, 5.209, 5.21, 5.211, 5.212, 5.213, 5.214,
      5.215, 5.216, 5.2170000000000005, 5.218, 5.219, 5.22, 5.221,
      5.2219999999999995, 5.223, 5.224, 5.225, 5.226, 5.227, 5.228, 5.229, 5.23,
      5.231, 5.232, 5.2330000000000005, 5.234, 5.235, 5.236, 5.237,
      5.2379999999999995, 5.239, 5.24, 5.241, 5.242, 5.243, 5.244, 5.245, 5.246,
      5.247, 5.248, 5.249, 5.25, 5.2509999999999994, 5.252, 5.253, 5.254, 5.255,
      5.256, 5.257, 5.258, 5.259, 5.26, 5.261, 5.2620000000000005, 5.263, 5.264,
      5.265, 5.266, 5.2669999999999995, 5.268, 5.269, 5.27, 5.271, 5.272, 5.273,
      5.274, 5.275, 5.276, 5.277, 5.2780000000000005, 5.279, 5.28, 5.281, 5.282,
      5.2829999999999995, 5.284, 5.285, 5.286, 5.287, 5.288, 5.289, 5.29, 5.291,
      5.292, 5.293, 5.2940000000000005, 5.295, 5.296, 5.297, 5.298,
      5.2989999999999995, 5.3, 5.301, 5.302, 5.303, 5.304, 5.305, 5.306, 5.307,
      5.308, 5.309, 5.3100000000000005, 5.311, 5.312, 5.313, 5.314,
      5.3149999999999995, 5.316, 5.317, 5.318, 5.319, 5.32, 5.321, 5.322, 5.323,
      5.324, 5.325, 5.3260000000000005, 5.327, 5.328, 5.329, 5.33,
      5.3309999999999995, 5.332, 5.333, 5.334, 5.335, 5.336, 5.337, 5.338, 5.339,
      5.34, 5.341, 5.3420000000000005, 5.343, 5.344, 5.345, 5.346,
      5.3469999999999995, 5.348, 5.349, 5.35, 5.351, 5.352, 5.353, 5.354, 5.355,
      5.356, 5.357, 5.358, 5.359, 5.3599999999999994, 5.361, 5.362,
      5.3629999999999995, 5.364, 5.365, 5.366, 5.367, 5.368, 5.369, 5.37, 5.371,
      5.372, 5.373, 5.374, 5.375, 5.3759999999999994, 5.377, 5.378, 5.379, 5.38,
      5.381, 5.382, 5.383, 5.384, 5.385, 5.386, 5.3870000000000005, 5.388, 5.389,
      5.39, 5.391, 5.3919999999999995, 5.393, 5.394, 5.395, 5.396, 5.397, 5.398,
      5.399, 5.4, 5.401, 5.402, 5.4030000000000005, 5.404, 5.405, 5.406, 5.407,
      5.4079999999999995, 5.409, 5.41, 5.411, 5.412, 5.413, 5.414, 5.415, 5.416,
      5.417, 5.418, 5.4190000000000005, 5.42, 5.421, 5.422, 5.423,
      5.4239999999999995, 5.425, 5.426, 5.427, 5.428, 5.429, 5.43, 5.431, 5.432,
      5.433, 5.434, 5.4350000000000005, 5.436, 5.437, 5.438, 5.439,
      5.4399999999999995, 5.441, 5.442, 5.443, 5.444, 5.445, 5.446, 5.447, 5.448,
      5.449, 5.45, 5.4510000000000005, 5.452, 5.453, 5.454, 5.455,
      5.4559999999999995, 5.457, 5.458, 5.459, 5.46, 5.461, 5.462, 5.463, 5.464,
      5.465, 5.466, 5.4670000000000005, 5.468, 5.469, 5.47, 5.471,
      5.4719999999999995, 5.473, 5.474, 5.475, 5.476, 5.477, 5.478, 5.479, 5.48,
      5.481, 5.482, 5.483, 5.484, 5.4849999999999994, 5.486, 5.487,
      5.4879999999999995, 5.489, 5.49, 5.491, 5.492, 5.493, 5.494, 5.495, 5.496,
      5.497, 5.498, 5.499, 5.5, 5.501, 5.502, 5.503, 5.504, 5.505, 5.506, 5.507,
      5.508, 5.509, 5.51, 5.511, 5.5120000000000005, 5.513, 5.514,
      5.5150000000000006, 5.516, 5.517, 5.518, 5.519, 5.52, 5.521, 5.522, 5.523,
      5.524, 5.525, 5.526, 5.527, 5.5280000000000005, 5.529, 5.53,
      5.5310000000000006, 5.532, 5.533, 5.534, 5.535, 5.536, 5.537, 5.538, 5.539,
      5.54, 5.541, 5.542, 5.543, 5.5440000000000005, 5.545, 5.546, 5.547, 5.548,
      5.5489999999999995, 5.55, 5.551, 5.552, 5.553, 5.554, 5.555, 5.556, 5.557,
      5.558, 5.559, 5.5600000000000005, 5.561, 5.562, 5.563, 5.564,
      5.5649999999999995, 5.566, 5.567, 5.568, 5.569, 5.57, 5.571, 5.572, 5.573,
      5.574, 5.575, 5.5760000000000005, 5.577, 5.578, 5.579, 5.58,
      5.5809999999999995, 5.582, 5.583, 5.584, 5.585, 5.586, 5.587, 5.588, 5.589,
      5.59, 5.591, 5.5920000000000005, 5.593, 5.594, 5.595, 5.596,
      5.5969999999999995, 5.598, 5.599, 5.6, 5.601, 5.602, 5.603, 5.604, 5.605,
      5.606, 5.607, 5.6080000000000005, 5.609, 5.61, 5.611, 5.612,
      5.6129999999999995, 5.614, 5.615, 5.616, 5.617, 5.618, 5.619, 5.62, 5.621,
      5.622, 5.623, 5.6240000000000006, 5.625, 5.626, 5.627, 5.628, 5.629, 5.63,
      5.631, 5.632, 5.633, 5.634, 5.635, 5.636, 5.6370000000000005, 5.638, 5.639,
      5.6400000000000006, 5.641, 5.642, 5.643, 5.644, 5.645, 5.646, 5.647, 5.648,
      5.649, 5.65, 5.651, 5.652, 5.6530000000000005, 5.654, 5.655,
      5.6560000000000006, 5.657, 5.6579999999999995, 5.659, 5.66, 5.661, 5.662,
      5.663, 5.664, 5.665, 5.666, 5.667, 5.668, 5.6690000000000005, 5.67, 5.671,
      5.672, 5.673, 5.6739999999999995, 5.675, 5.676, 5.677, 5.678, 5.679, 5.68,
      5.681, 5.682, 5.683, 5.684, 5.6850000000000005, 5.686, 5.687, 5.688, 5.689,
      5.6899999999999995, 5.691, 5.692, 5.693, 5.694, 5.695, 5.696, 5.697, 5.698,
      5.699, 5.7, 5.7010000000000005, 5.702, 5.703, 5.704, 5.705,
      5.7059999999999995, 5.707, 5.708, 5.709, 5.71, 5.711, 5.712, 5.713, 5.714,
      5.715, 5.716, 5.7170000000000005, 5.718, 5.719, 5.72, 5.721,
      5.7219999999999995, 5.723, 5.724, 5.725, 5.726, 5.727, 5.728, 5.729, 5.73,
      5.731, 5.732, 5.7330000000000005, 5.734, 5.735, 5.736, 5.737,
      5.7379999999999995, 5.739, 5.74, 5.741, 5.742, 5.743, 5.744, 5.745, 5.746,
      5.747, 5.748, 5.749, 5.75, 5.7509999999999994, 5.752, 5.753, 5.754, 5.755,
      5.756, 5.757, 5.758, 5.759, 5.76, 5.761, 5.7620000000000005, 5.763, 5.764,
      5.765, 5.766, 5.7669999999999995, 5.768, 5.769, 5.77, 5.771, 5.772, 5.773,
      5.774, 5.775, 5.776, 5.777, 5.7780000000000005, 5.779, 5.78, 5.781, 5.782,
      5.7829999999999995, 5.784, 5.785, 5.786, 5.787, 5.788, 5.789, 5.79, 5.791,
      5.792, 5.793, 5.7940000000000005, 5.795, 5.796, 5.797, 5.798,
      5.7989999999999995, 5.8, 5.801, 5.802, 5.803, 5.804, 5.805, 5.806, 5.807,
      5.808, 5.809, 5.8100000000000005, 5.811, 5.812, 5.813, 5.814,
      5.8149999999999995, 5.816, 5.817, 5.818, 5.819, 5.82, 5.821, 5.822, 5.823,
      5.824, 5.825, 5.8260000000000005, 5.827, 5.828, 5.829, 5.83,
      5.8309999999999995, 5.832, 5.833, 5.834, 5.835, 5.836, 5.837, 5.838, 5.839,
      5.84, 5.841, 5.8420000000000005, 5.843, 5.844, 5.845, 5.846,
      5.8469999999999995, 5.848, 5.849, 5.85, 5.851, 5.852, 5.853, 5.854, 5.855,
      5.856, 5.857, 5.858, 5.859, 5.8599999999999994, 5.861, 5.862,
      5.8629999999999995, 5.864, 5.865, 5.866, 5.867, 5.868, 5.869, 5.87, 5.871,
      5.872, 5.873, 5.874, 5.875, 5.8759999999999994, 5.877, 5.878, 5.879, 5.88,
      5.881, 5.882, 5.883, 5.884, 5.885, 5.886, 5.8870000000000005, 5.888, 5.889,
      5.89, 5.891, 5.8919999999999995, 5.893, 5.894, 5.895, 5.896, 5.897, 5.898,
      5.899, 5.9, 5.901, 5.902, 5.9030000000000005, 5.904, 5.905, 5.906, 5.907,
      5.9079999999999995, 5.909, 5.91, 5.911, 5.912, 5.913, 5.914, 5.915, 5.916,
      5.917, 5.918, 5.9190000000000005, 5.92, 5.921, 5.922, 5.923,
      5.9239999999999995, 5.925, 5.926, 5.927, 5.928, 5.929, 5.93, 5.931, 5.932,
      5.933, 5.934, 5.9350000000000005, 5.936, 5.937, 5.938, 5.939,
      5.9399999999999995, 5.941, 5.942, 5.943, 5.944, 5.945, 5.946, 5.947, 5.948,
      5.949, 5.95, 5.9510000000000005, 5.952, 5.953, 5.954, 5.955,
      5.9559999999999995, 5.957, 5.958, 5.959, 5.96, 5.961, 5.962, 5.963, 5.964,
      5.965, 5.966, 5.9670000000000005, 5.968, 5.969, 5.97, 5.971,
      5.9719999999999995, 5.973, 5.974, 5.975, 5.976, 5.977, 5.978, 5.979, 5.98,
      5.981, 5.982, 5.983, 5.984, 5.9849999999999994, 5.986, 5.987,
      5.9879999999999995, 5.989, 5.99, 5.991, 5.992, 5.993, 5.994, 5.995, 5.996,
      5.997, 5.998, 5.999, 6.0, 6.0, 6.0009999999999994, 6.002, 6.003, 6.004,
      6.005, 6.006, 6.007, 6.008, 6.009, 6.01, 6.011, 6.0120000000000005, 6.013,
      6.0139999999999993, 6.0150000000000006, 6.016, 6.0169999999999995, 6.018,
      6.019, 6.02, 6.021, 6.022, 6.023, 6.024, 6.025, 6.026, 6.027,
      6.0280000000000005, 6.029, 6.0299999999999994, 6.0310000000000006, 6.032,
      6.0329999999999995, 6.034, 6.035, 6.036, 6.037, 6.038, 6.039, 6.04, 6.041,
      6.042, 6.043, 6.0440000000000005, 6.045, 6.0459999999999994,
      6.0470000000000006, 6.048, 6.0489999999999995, 6.05, 6.051, 6.052, 6.053,
      6.054, 6.055, 6.056, 6.057, 6.058, 6.059, 6.0600000000000005, 6.061,
      6.0619999999999994, 6.0630000000000006, 6.064, 6.0649999999999995, 6.066,
      6.067, 6.068, 6.069, 6.07, 6.071, 6.072, 6.073, 6.074, 6.075,
      6.0760000000000005, 6.077, 6.0779999999999994, 6.0790000000000006, 6.08,
      6.0809999999999995, 6.082, 6.083, 6.084, 6.085, 6.086, 6.087, 6.088, 6.089,
      6.09, 6.091, 6.0920000000000005, 6.093, 6.0939999999999994,
      6.0950000000000006, 6.096, 6.0969999999999995, 6.098, 6.099, 6.1, 6.101,
      6.102, 6.103, 6.104, 6.105, 6.106, 6.107, 6.1080000000000005, 6.109,
      6.1099999999999994, 6.1110000000000007, 6.112, 6.1129999999999995, 6.114,
      6.115, 6.116, 6.117, 6.118, 6.119, 6.12, 6.121, 6.122, 6.123,
      6.1240000000000006, 6.125, 6.1259999999999994, 6.127, 6.128, 6.129, 6.13,
      6.131, 6.132, 6.133, 6.134, 6.135, 6.136, 6.1370000000000005, 6.138, 6.139,
      6.1400000000000006, 6.141, 6.1419999999999995, 6.143, 6.144, 6.145, 6.146,
      6.147, 6.148, 6.149, 6.15, 6.151, 6.152, 6.1530000000000005, 6.154, 6.155,
      6.1560000000000006, 6.157, 6.1579999999999995, 6.159, 6.16, 6.161, 6.162,
      6.163, 6.164, 6.165, 6.166, 6.167, 6.168, 6.1690000000000005, 6.17,
      6.1709999999999994, 6.1720000000000006, 6.173, 6.1739999999999995, 6.175,
      6.176, 6.177, 6.178, 6.179, 6.18, 6.181, 6.182, 6.183, 6.184,
      6.1850000000000005, 6.186, 6.1869999999999994, 6.1880000000000006, 6.189,
      6.1899999999999995, 6.191, 6.192, 6.193, 6.194, 6.195, 6.196, 6.197, 6.198,
      6.199, 6.2, 6.2010000000000005, 6.202, 6.2029999999999994,
      6.2040000000000006, 6.205, 6.2059999999999995, 6.207, 6.208, 6.209, 6.21,
      6.211, 6.212, 6.213, 6.214, 6.215, 6.216, 6.2170000000000005, 6.218,
      6.2189999999999994, 6.2200000000000006, 6.221, 6.2219999999999995, 6.223,
      6.224, 6.225, 6.226, 6.227, 6.228, 6.229, 6.23, 6.231, 6.232,
      6.2330000000000005, 6.234, 6.2349999999999994, 6.2360000000000007, 6.237,
      6.2379999999999995, 6.239, 6.24, 6.241, 6.242, 6.243, 6.244, 6.245, 6.246,
      6.247, 6.248, 6.2490000000000006, 6.25, 6.2509999999999994, 6.252, 6.253,
      6.254, 6.255, 6.256, 6.257, 6.258, 6.259, 6.26, 6.261, 6.2620000000000005,
      6.263, 6.264, 6.2650000000000006, 6.266, 6.2669999999999995, 6.268, 6.269,
      6.27, 6.271, 6.272, 6.273, 6.274, 6.275, 6.276, 6.277, 6.2780000000000005,
      6.279, 6.28, 6.2810000000000006, 6.282, 6.2829999999999995, 6.284, 6.285,
      6.286, 6.287, 6.288, 6.289, 6.29, 6.291, 6.292, 6.293, 6.2940000000000005,
      6.295, 6.296, 6.2970000000000006, 6.298, 6.2989999999999995, 6.3, 6.301,
      6.302, 6.303, 6.304, 6.305, 6.306, 6.307, 6.308, 6.309, 6.3100000000000005,
      6.311, 6.3119999999999994, 6.3130000000000006, 6.314, 6.3149999999999995,
      6.316, 6.317, 6.318, 6.319, 6.32, 6.321, 6.322, 6.323, 6.324, 6.325,
      6.3260000000000005, 6.327, 6.3279999999999994, 6.3290000000000006, 6.33,
      6.3309999999999995, 6.332, 6.333, 6.334, 6.335, 6.336, 6.337, 6.338, 6.339,
      6.34, 6.341, 6.3420000000000005, 6.343, 6.3439999999999994, 6.345, 6.346,
      6.3469999999999995, 6.348, 6.349, 6.35, 6.351, 6.352, 6.353, 6.354, 6.355,
      6.356, 6.357, 6.3580000000000005, 6.359, 6.3599999999999994, 6.361, 6.362,
      6.3629999999999995, 6.364, 6.365, 6.366, 6.367, 6.368, 6.369, 6.37, 6.371,
      6.372, 6.373, 6.3740000000000006, 6.375, 6.3759999999999994, 6.377, 6.378,
      6.379, 6.38, 6.381, 6.382, 6.383, 6.384, 6.385, 6.386, 6.3870000000000005,
      6.388, 6.389, 6.3900000000000006, 6.391, 6.3919999999999995, 6.393, 6.394,
      6.395, 6.396, 6.397, 6.398, 6.399, 6.4, 6.401, 6.402, 6.4030000000000005,
      6.404, 6.405, 6.4060000000000006, 6.407, 6.4079999999999995, 6.409, 6.41,
      6.411, 6.412, 6.413, 6.414, 6.415, 6.416, 6.417, 6.418, 6.4190000000000005,
      6.42, 6.4209999999999994, 6.4220000000000006, 6.423, 6.4239999999999995,
      6.425, 6.426, 6.427, 6.428, 6.429, 6.43, 6.431, 6.432, 6.433, 6.434,
      6.4350000000000005, 6.436, 6.4369999999999994, 6.4380000000000006, 6.439,
      6.4399999999999995, 6.441, 6.442, 6.443, 6.444, 6.445, 6.446, 6.447, 6.448,
      6.449, 6.45, 6.4510000000000005, 6.452, 6.4529999999999994,
      6.4540000000000006, 6.455, 6.4559999999999995, 6.457, 6.458, 6.459, 6.46,
      6.461, 6.462, 6.463, 6.464, 6.465, 6.466, 6.4670000000000005, 6.468,
      6.4689999999999994, 6.4700000000000006, 6.471, 6.4719999999999995, 6.473,
      6.474, 6.475, 6.476, 6.477, 6.478, 6.479, 6.48, 6.481, 6.482,
      6.4830000000000005, 6.484, 6.4849999999999994, 6.486, 6.487,
      6.4879999999999995, 6.489, 6.49, 6.491, 6.492, 6.493, 6.494, 6.495, 6.496,
      6.497, 6.498, 6.4990000000000006, 6.5, 6.5009999999999994, 6.502, 6.503,
      6.504, 6.505, 6.506, 6.507, 6.508, 6.509, 6.51, 6.511, 6.5120000000000005,
      6.513, 6.514, 6.5150000000000006, 6.516, 6.5169999999999995, 6.518, 6.519,
      6.52, 6.521, 6.522, 6.523, 6.524, 6.525, 6.526, 6.527, 6.5280000000000005,
      6.529, 6.53, 6.5310000000000006, 6.532, 6.5329999999999995, 6.534, 6.535,
      6.536, 6.537, 6.538, 6.539, 6.54, 6.541, 6.542, 6.543, 6.5440000000000005,
      6.545, 6.546, 6.547, 6.548, 6.5489999999999995, 6.55, 6.551, 6.552, 6.553,
      6.554, 6.555, 6.556, 6.557, 6.558, 6.559, 6.5600000000000005, 6.561, 6.562,
      6.563, 6.564, 6.5649999999999995, 6.566, 6.567, 6.568, 6.569, 6.57, 6.571,
      6.572, 6.573, 6.574, 6.575, 6.5760000000000005, 6.577, 6.578, 6.579, 6.58,
      6.5809999999999995, 6.582, 6.583, 6.584, 6.585, 6.586, 6.587, 6.588, 6.589,
      6.59, 6.591, 6.5920000000000005, 6.593, 6.594, 6.595, 6.596,
      6.5969999999999995, 6.598, 6.599, 6.6, 6.601, 6.602, 6.603, 6.604, 6.605,
      6.606, 6.607, 6.6080000000000005, 6.609, 6.61, 6.611, 6.612,
      6.6129999999999995, 6.614, 6.615, 6.616, 6.617, 6.618, 6.619, 6.62, 6.621,
      6.622, 6.623, 6.6240000000000006, 6.625, 6.6259999999999994, 6.627, 6.628,
      6.629, 6.63, 6.631, 6.632, 6.633, 6.634, 6.635, 6.636, 6.6370000000000005,
      6.638, 6.639, 6.6400000000000006, 6.641, 6.6419999999999995, 6.643, 6.644,
      6.645, 6.646, 6.647, 6.648, 6.649, 6.65, 6.651, 6.652, 6.6530000000000005,
      6.654, 6.655, 6.6560000000000006, 6.657, 6.6579999999999995, 6.659, 6.66,
      6.661, 6.662, 6.663, 6.664, 6.665, 6.666, 6.667, 6.668, 6.6690000000000005,
      6.67, 6.671, 6.672, 6.673, 6.6739999999999995, 6.675, 6.676, 6.677, 6.678,
      6.679, 6.68, 6.681, 6.682, 6.683, 6.684, 6.6850000000000005, 6.686, 6.687,
      6.688, 6.689, 6.6899999999999995, 6.691, 6.692, 6.693, 6.694, 6.695, 6.696,
      6.697, 6.698, 6.699, 6.7, 6.7010000000000005, 6.702, 6.703, 6.704, 6.705,
      6.7059999999999995, 6.707, 6.708, 6.709, 6.71, 6.711, 6.712, 6.713, 6.714,
      6.715, 6.716, 6.7170000000000005, 6.718, 6.719, 6.72, 6.721,
      6.7219999999999995, 6.723, 6.724, 6.725, 6.726, 6.727, 6.728, 6.729, 6.73,
      6.731, 6.732, 6.7330000000000005, 6.734, 6.735, 6.736, 6.737,
      6.7379999999999995, 6.739, 6.74, 6.741, 6.742, 6.743, 6.744, 6.745, 6.746,
      6.747, 6.748, 6.749, 6.75, 6.7509999999999994, 6.752, 6.753, 6.754, 6.755,
      6.756, 6.757, 6.758, 6.759, 6.76, 6.761, 6.7620000000000005, 6.763, 6.764,
      6.765, 6.766, 6.7669999999999995, 6.768, 6.769, 6.77, 6.771, 6.772, 6.773,
      6.774, 6.775, 6.776, 6.777, 6.7780000000000005, 6.779, 6.78, 6.781, 6.782,
      6.7829999999999995, 6.784, 6.785, 6.786, 6.787, 6.788, 6.789, 6.79, 6.791,
      6.792, 6.793, 6.7940000000000005, 6.795, 6.796, 6.797, 6.798,
      6.7989999999999995, 6.8, 6.801, 6.802, 6.803, 6.804, 6.805, 6.806, 6.807,
      6.808, 6.809, 6.8100000000000005, 6.811, 6.812, 6.813, 6.814,
      6.8149999999999995, 6.816, 6.817, 6.818, 6.819, 6.82, 6.821, 6.822, 6.823,
      6.824, 6.825, 6.8260000000000005, 6.827, 6.828, 6.829, 6.83,
      6.8309999999999995, 6.832, 6.833, 6.834, 6.835, 6.836, 6.837, 6.838, 6.839,
      6.84, 6.841, 6.8420000000000005, 6.843, 6.844, 6.845, 6.846,
      6.8469999999999995, 6.848, 6.849, 6.85, 6.851, 6.852, 6.853, 6.854, 6.855,
      6.856, 6.857, 6.858, 6.859, 6.8599999999999994, 6.861, 6.862,
      6.8629999999999995, 6.864, 6.865, 6.866, 6.867, 6.868, 6.869, 6.87, 6.871,
      6.872, 6.873, 6.874, 6.875, 6.8759999999999994, 6.877, 6.878, 6.879, 6.88,
      6.881, 6.882, 6.883, 6.884, 6.885, 6.886, 6.8870000000000005, 6.888, 6.889,
      6.89, 6.891, 6.8919999999999995, 6.893, 6.894, 6.895, 6.896, 6.897, 6.898,
      6.899, 6.9, 6.901, 6.902, 6.9030000000000005, 6.904, 6.905, 6.906, 6.907,
      6.9079999999999995, 6.909, 6.91, 6.911, 6.912, 6.913, 6.914, 6.915, 6.916,
      6.917, 6.918, 6.9190000000000005, 6.92, 6.921, 6.922, 6.923,
      6.9239999999999995, 6.925, 6.926, 6.927, 6.928, 6.929, 6.93, 6.931, 6.932,
      6.933, 6.934, 6.9350000000000005, 6.936, 6.937, 6.938, 6.939,
      6.9399999999999995, 6.941, 6.942, 6.943, 6.944, 6.945, 6.946, 6.947, 6.948,
      6.949, 6.95, 6.9510000000000005, 6.952, 6.953, 6.954, 6.955,
      6.9559999999999995, 6.957, 6.958, 6.959, 6.96, 6.961, 6.962, 6.963, 6.964,
      6.965, 6.966, 6.9670000000000005, 6.968, 6.969, 6.97, 6.971,
      6.9719999999999995, 6.973, 6.974, 6.975, 6.976, 6.977, 6.978, 6.979, 6.98,
      6.981, 6.982, 6.983, 6.984, 6.9849999999999994, 6.986, 6.987,
      6.9879999999999995, 6.989, 6.99, 6.991, 6.992, 6.993, 6.994, 6.995, 6.996,
      6.997, 6.998, 6.999, 7.0, 7.001, 7.002, 7.003, 7.004, 7.005, 7.006, 7.007,
      7.008, 7.009, 7.01, 7.011, 7.0120000000000005, 7.013, 7.014,
      7.0150000000000006, 7.016, 7.017, 7.018, 7.019, 7.02, 7.021, 7.022, 7.023,
      7.024, 7.025, 7.026, 7.027, 7.0280000000000005, 7.029, 7.03,
      7.0310000000000006, 7.032, 7.033, 7.034, 7.035, 7.036, 7.037, 7.038, 7.039,
      7.04, 7.041, 7.042, 7.043, 7.0440000000000005, 7.045, 7.046, 7.047, 7.048,
      7.0489999999999995, 7.05, 7.051, 7.052, 7.053, 7.054, 7.055, 7.056, 7.057,
      7.058, 7.059, 7.0600000000000005, 7.061, 7.062, 7.063, 7.064,
      7.0649999999999995, 7.066, 7.067, 7.068, 7.069, 7.07, 7.071, 7.072, 7.073,
      7.074, 7.075, 7.0760000000000005, 7.077, 7.078, 7.079, 7.08,
      7.0809999999999995, 7.082, 7.083, 7.084, 7.085, 7.086, 7.087, 7.088, 7.089,
      7.09, 7.091, 7.0920000000000005, 7.093, 7.094, 7.095, 7.096,
      7.0969999999999995, 7.098, 7.099, 7.1, 7.101, 7.102, 7.103, 7.104, 7.105,
      7.106, 7.107, 7.1080000000000005, 7.109, 7.11, 7.111, 7.112,
      7.1129999999999995, 7.114, 7.115, 7.116, 7.117, 7.118, 7.119, 7.12, 7.121,
      7.122, 7.123, 7.1240000000000006, 7.125, 7.126, 7.127, 7.128, 7.129, 7.13,
      7.131, 7.132, 7.133, 7.134, 7.135, 7.136, 7.1370000000000005, 7.138, 7.139,
      7.1400000000000006, 7.141, 7.142, 7.143, 7.144, 7.145, 7.146, 7.147, 7.148,
      7.149, 7.15, 7.151, 7.152, 7.1530000000000005, 7.154, 7.155,
      7.1560000000000006, 7.157, 7.1579999999999995, 7.159, 7.16, 7.161, 7.162,
      7.163, 7.164, 7.165, 7.166, 7.167, 7.168, 7.1690000000000005, 7.17, 7.171,
      7.172, 7.173, 7.1739999999999995, 7.175, 7.176, 7.177, 7.178, 7.179, 7.18,
      7.181, 7.182, 7.183, 7.184, 7.1850000000000005, 7.186, 7.187, 7.188, 7.189,
      7.1899999999999995, 7.191, 7.192, 7.193, 7.194, 7.195, 7.196, 7.197, 7.198,
      7.199, 7.2, 7.2010000000000005, 7.202, 7.203, 7.204, 7.205,
      7.2059999999999995, 7.207, 7.208, 7.209, 7.21, 7.211, 7.212, 7.213, 7.214,
      7.215, 7.216, 7.2170000000000005, 7.218, 7.219, 7.22, 7.221,
      7.2219999999999995, 7.223, 7.224, 7.225, 7.226, 7.227, 7.228, 7.229, 7.23,
      7.231, 7.232, 7.2330000000000005, 7.234, 7.235, 7.236, 7.237,
      7.2379999999999995, 7.239, 7.24, 7.241, 7.242, 7.243, 7.244, 7.245, 7.246,
      7.247, 7.248, 7.249, 7.25, 7.2509999999999994, 7.252, 7.253, 7.254, 7.255,
      7.256, 7.257, 7.258, 7.259, 7.26, 7.261, 7.2620000000000005, 7.263, 7.264,
      7.265, 7.266, 7.2669999999999995, 7.268, 7.269, 7.27, 7.271, 7.272, 7.273,
      7.274, 7.275, 7.276, 7.277, 7.2780000000000005, 7.279, 7.28, 7.281, 7.282,
      7.2829999999999995, 7.284, 7.285, 7.286, 7.287, 7.288, 7.289, 7.29, 7.291,
      7.292, 7.293, 7.2940000000000005, 7.295, 7.296, 7.297, 7.298,
      7.2989999999999995, 7.3, 7.301, 7.302, 7.303, 7.304, 7.305, 7.306, 7.307,
      7.308, 7.309, 7.3100000000000005, 7.311, 7.312, 7.313, 7.314,
      7.3149999999999995, 7.316, 7.317, 7.318, 7.319, 7.32, 7.321, 7.322, 7.323,
      7.324, 7.325, 7.3260000000000005, 7.327, 7.328, 7.329, 7.33,
      7.3309999999999995, 7.332, 7.333, 7.334, 7.335, 7.336, 7.337, 7.338, 7.339,
      7.34, 7.341, 7.3420000000000005, 7.343, 7.344, 7.345, 7.346,
      7.3469999999999995, 7.348, 7.349, 7.35, 7.351, 7.352, 7.353, 7.354, 7.355,
      7.356, 7.357, 7.358, 7.359, 7.3599999999999994, 7.361, 7.362,
      7.3629999999999995, 7.364, 7.365, 7.366, 7.367, 7.368, 7.369, 7.37, 7.371,
      7.372, 7.373, 7.374, 7.375, 7.3759999999999994, 7.377, 7.378, 7.379, 7.38,
      7.381, 7.382, 7.383, 7.384, 7.385, 7.386, 7.3870000000000005, 7.388, 7.389,
      7.39, 7.391, 7.3919999999999995, 7.393, 7.394, 7.395, 7.396, 7.397, 7.398,
      7.399, 7.4, 7.401, 7.402, 7.4030000000000005, 7.404, 7.405, 7.406, 7.407,
      7.4079999999999995, 7.409, 7.41, 7.411, 7.412, 7.413, 7.414, 7.415, 7.416,
      7.417, 7.418, 7.4190000000000005, 7.42, 7.421, 7.422, 7.423,
      7.4239999999999995, 7.425, 7.426, 7.427, 7.428, 7.429, 7.43, 7.431, 7.432,
      7.433, 7.434, 7.4350000000000005, 7.436, 7.437, 7.438, 7.439,
      7.4399999999999995, 7.441, 7.442, 7.443, 7.444, 7.445, 7.446, 7.447, 7.448,
      7.449, 7.45, 7.4510000000000005, 7.452, 7.453, 7.454, 7.455,
      7.4559999999999995, 7.457, 7.458, 7.459, 7.46, 7.461, 7.462, 7.463, 7.464,
      7.465, 7.466, 7.4670000000000005, 7.468, 7.469, 7.47, 7.471,
      7.4719999999999995, 7.473, 7.474, 7.475, 7.476, 7.477, 7.478, 7.479, 7.48,
      7.481, 7.482, 7.483, 7.484, 7.4849999999999994, 7.486, 7.487,
      7.4879999999999995, 7.489, 7.49, 7.491, 7.492, 7.493, 7.494, 7.495, 7.496,
      7.497, 7.498, 7.499, 7.5, 7.501, 7.502, 7.503, 7.504, 7.505, 7.506, 7.507,
      7.508, 7.509, 7.51, 7.511, 7.5120000000000005, 7.513, 7.514,
      7.5150000000000006, 7.516, 7.517, 7.518, 7.519, 7.52, 7.521, 7.522, 7.523,
      7.524, 7.525, 7.526, 7.527, 7.5280000000000005, 7.529, 7.53,
      7.5310000000000006, 7.532, 7.533, 7.534, 7.535, 7.536, 7.537, 7.538, 7.539,
      7.54, 7.541, 7.542, 7.543, 7.5440000000000005, 7.545, 7.546, 7.547, 7.548,
      7.5489999999999995, 7.55, 7.551, 7.552, 7.553, 7.554, 7.555, 7.556, 7.557,
      7.558, 7.559, 7.5600000000000005, 7.561, 7.562, 7.563, 7.564,
      7.5649999999999995, 7.566, 7.567, 7.568, 7.569, 7.57, 7.571, 7.572, 7.573,
      7.574, 7.575, 7.5760000000000005, 7.577, 7.578, 7.579, 7.58,
      7.5809999999999995, 7.582, 7.583, 7.584, 7.585, 7.586, 7.587, 7.588, 7.589,
      7.59, 7.591, 7.5920000000000005, 7.593, 7.594, 7.595, 7.596,
      7.5969999999999995, 7.598, 7.599, 7.6, 7.601, 7.602, 7.603, 7.604, 7.605,
      7.606, 7.607, 7.6080000000000005, 7.609, 7.61, 7.611, 7.612,
      7.6129999999999995, 7.614, 7.615, 7.616, 7.617, 7.618, 7.619, 7.62, 7.621,
      7.622, 7.623, 7.6240000000000006, 7.625, 7.626, 7.627, 7.628, 7.629, 7.63,
      7.631, 7.632, 7.633, 7.634, 7.635, 7.636, 7.6370000000000005, 7.638, 7.639,
      7.6400000000000006, 7.641, 7.642, 7.643, 7.644, 7.645, 7.646, 7.647, 7.648,
      7.649, 7.65, 7.651, 7.652, 7.6530000000000005, 7.654, 7.655,
      7.6560000000000006, 7.657, 7.6579999999999995, 7.659, 7.66, 7.661, 7.662,
      7.663, 7.664, 7.665, 7.666, 7.667, 7.668, 7.6690000000000005, 7.67, 7.671,
      7.672, 7.673, 7.6739999999999995, 7.675, 7.676, 7.677, 7.678, 7.679, 7.68,
      7.681, 7.682, 7.683, 7.684, 7.6850000000000005, 7.686, 7.687, 7.688, 7.689,
      7.6899999999999995, 7.691, 7.692, 7.693, 7.694, 7.695, 7.696, 7.697, 7.698,
      7.699, 7.7, 7.7010000000000005, 7.702, 7.703, 7.704, 7.705,
      7.7059999999999995, 7.707, 7.708, 7.709, 7.71, 7.711, 7.712, 7.713, 7.714,
      7.715, 7.716, 7.7170000000000005, 7.718, 7.719, 7.72, 7.721,
      7.7219999999999995, 7.723, 7.724, 7.725, 7.726, 7.727, 7.728, 7.729, 7.73,
      7.731, 7.732, 7.7330000000000005, 7.734, 7.735, 7.736, 7.737,
      7.7379999999999995, 7.739, 7.74, 7.741, 7.742, 7.743, 7.744, 7.745, 7.746,
      7.747, 7.748, 7.749, 7.75, 7.7509999999999994, 7.752, 7.753, 7.754, 7.755,
      7.756, 7.757, 7.758, 7.759, 7.76, 7.761, 7.7620000000000005, 7.763, 7.764,
      7.765, 7.766, 7.7669999999999995, 7.768, 7.769, 7.77, 7.771, 7.772, 7.773,
      7.774, 7.775, 7.776, 7.777, 7.7780000000000005, 7.779, 7.78, 7.781, 7.782,
      7.7829999999999995, 7.784, 7.785, 7.786, 7.787, 7.788, 7.789, 7.79, 7.791,
      7.792, 7.793, 7.7940000000000005, 7.795, 7.796, 7.797, 7.798,
      7.7989999999999995, 7.8, 7.801, 7.802, 7.803, 7.804, 7.805, 7.806, 7.807,
      7.808, 7.809, 7.8100000000000005, 7.811, 7.812, 7.813, 7.814,
      7.8149999999999995, 7.816, 7.817, 7.818, 7.819, 7.82, 7.821, 7.822, 7.823,
      7.824, 7.825, 7.8260000000000005, 7.827, 7.828, 7.829, 7.83,
      7.8309999999999995, 7.832, 7.833, 7.834, 7.835, 7.836, 7.837, 7.838, 7.839,
      7.84, 7.841, 7.8420000000000005, 7.843, 7.844, 7.845, 7.846,
      7.8469999999999995, 7.848, 7.849, 7.85, 7.851, 7.852, 7.853, 7.854, 7.855,
      7.856, 7.857, 7.858, 7.859, 7.8599999999999994, 7.861, 7.862,
      7.8629999999999995, 7.864, 7.865, 7.866, 7.867, 7.868, 7.869, 7.87, 7.871,
      7.872, 7.873, 7.874, 7.875, 7.8759999999999994, 7.877, 7.878, 7.879, 7.88,
      7.881, 7.882, 7.883, 7.884, 7.885, 7.886, 7.8870000000000005, 7.888, 7.889,
      7.89, 7.891, 7.8919999999999995, 7.893, 7.894, 7.895, 7.896, 7.897, 7.898,
      7.899, 7.9, 7.901, 7.902, 7.9030000000000005, 7.904, 7.905, 7.906, 7.907,
      7.9079999999999995, 7.909, 7.91, 7.911, 7.912, 7.913, 7.914, 7.915, 7.916,
      7.917, 7.918, 7.9190000000000005, 7.92, 7.921, 7.922, 7.923,
      7.9239999999999995, 7.925, 7.926, 7.927, 7.928, 7.929, 7.93, 7.931, 7.932,
      7.933, 7.934, 7.9350000000000005, 7.936, 7.937, 7.938, 7.939,
      7.9399999999999995, 7.941, 7.942, 7.943, 7.944, 7.945, 7.946, 7.947, 7.948,
      7.949, 7.95, 7.9510000000000005, 7.952, 7.953, 7.954, 7.955,
      7.9559999999999995, 7.957, 7.958, 7.959, 7.96, 7.961, 7.962, 7.963, 7.964,
      7.965, 7.966, 7.9670000000000005, 7.968, 7.969, 7.97, 7.971,
      7.9719999999999995, 7.973, 7.974, 7.975, 7.976, 7.977, 7.978, 7.979, 7.98,
      7.981, 7.982, 7.983, 7.984, 7.9849999999999994, 7.986, 7.987,
      7.9879999999999995, 7.989, 7.99, 7.991, 7.992, 7.993, 7.994, 7.995, 7.996,
      7.997, 7.998, 7.999, 8.0 } ;

    static real_T pDataValues0[] = { 0.0, 0.00025, 0.001, 0.00225, 0.004,
      0.0062499999999999995, 0.009, 0.01225, 0.016, 0.02025,
      0.024999999999999998, 0.030249999999999992, 0.036, 0.04225, 0.049,
      0.056249999999999994, 0.064, 0.072250000000000009, 0.081, 0.09025,
      0.099999999999999992, 0.11025000000000001, 0.12099999999999997,
      0.13224999999999998, 0.144, 0.15625, 0.169, 0.18225, 0.196,
      0.21025000000000002, 0.22499999999999998, 0.24025, 0.256, 0.27225,
      0.28900000000000003, 0.30625, 0.324, 0.34224999999999994, 0.361,
      0.38024999999999992, 0.39999999999999997, 0.42025000000000007,
      0.44100000000000006, 0.46225000000000005, 0.48399999999999987, 0.50625,
      0.52899999999999991, 0.55225, 0.576, 0.60025000000000006, 0.625,
      0.65025000000000011, 0.676, 0.70224999999999993, 0.729, 0.75625, 0.784,
      0.81225, 0.84100000000000008, 0.87025, 0.89999999999999991,
      0.93024999999999991, 0.961, 0.99224999999999985, 1.024, 1.05625, 1.089,
      1.12225, 1.1560000000000001, 1.19025, 1.225, 1.26025, 1.296,
      1.3322499999999997, 1.3689999999999998, 1.40625, 1.444, 1.4822499999999998,
      1.5209999999999997, 1.56025, 1.5999999999999999, 1.64025,
      1.6810000000000003, 1.72225, 1.7640000000000002, 1.80625,
      1.8490000000000002, 1.8922500000000002, 1.9359999999999995, 1.98025, 2.025,
      2.0702499999999997, 2.1159999999999997, 2.1622500000000002, 2.209,
      2.2562499999999996, 2.304, 2.35225, 2.4010000000000002, 2.45025, 2.5,
      2.5502500000000006, 2.6010000000000004, 2.6522500000000004, 2.704,
      2.7562500000000005, 2.8090000000000006, 2.8622500000000004, 2.916,
      2.9702500000000009, 3.0250000000000008, 3.0802500000000008,
      3.136000000000001, 3.19225, 3.249, 3.30625, 3.3640000000000003,
      3.4222500000000005, 3.481, 3.5402500000000003, 3.600000000000001,
      3.6602500000000009, 3.721, 3.7822500000000003, 3.8440000000000012, 3.90625,
      3.9689999999999994, 4.0322499999999994, 4.096, 4.1602500000000004, 4.225,
      4.29025, 4.356, 4.42225, 4.489, 4.55625, 4.6240000000000006,
      4.6922500000000005, 4.761, 4.83025, 4.9, 4.9702500000000009, 5.041,
      5.11225, 5.184, 5.2562500000000005, 5.3290000000000015, 5.4022500000000013,
      5.4760000000000018, 5.5502500000000019, 5.6250000000000009,
      5.7002500000000005, 5.7760000000000016, 5.8522500000000015,
      5.929000000000002, 6.0062500000000014, 6.0840000000000023, 6.16225, 6.241,
      6.3202500000000006, 6.3999999999999995, 6.4802500000000007, 6.561,
      6.6422500000000007, 6.7240000000000011, 6.80625, 6.889, 6.97225,
      7.0560000000000009, 7.1402500000000009, 7.225, 7.3102500000000008,
      7.3960000000000008, 7.4822500000000014, 7.5690000000000008,
      7.6562500000000009, 7.7440000000000007, 7.832250000000001,
      7.9210000000000012, 8.010250000000001, 8.1000000000000014, 8.19025,
      8.2809999999999988, 8.37225, 8.4639999999999986, 8.5562499999999986,
      8.6490000000000009, 8.7422499999999985, 8.836, 8.93025, 9.0249999999999986,
      9.12025, 9.216, 9.31225, 9.409, 9.50625, 9.604000000000001, 9.70225, 9.801,
      9.9002500000000015, 10.0, 10.1, 10.2, 10.3, 10.4, 10.5, 10.6, 10.7, 10.8,
      10.9, 11.0, 11.1, 11.2, 11.3, 11.4, 11.5, 11.6, 11.7, 11.8, 11.9, 12.0,
      12.1, 12.2, 12.3, 12.4, 12.5, 12.6, 12.7, 12.8, 12.9, 13.0, 13.1, 13.2,
      13.3, 13.4, 13.5, 13.600000000000001, 13.7, 13.8, 13.9, 14.0,
      14.100000000000001, 14.2, 14.3, 14.399999999999999, 14.5, 14.6, 14.7, 14.8,
      14.9, 15.0, 15.100000000000001, 15.2, 15.3, 15.4, 15.5, 15.600000000000001,
      15.7, 15.8, 15.9, 16.0, 16.1, 16.2, 16.3, 16.4, 16.5, 16.6, 16.7, 16.8,
      16.9, 17.0, 17.1, 17.200000000000003, 17.3, 17.4, 17.5, 17.6, 17.7, 17.8,
      17.9, 18.0, 18.1, 18.200000000000003, 18.3, 18.4, 18.5, 18.6,
      18.700000000000003, 18.799999999999997, 18.9, 19.0, 19.1, 19.2, 19.3, 19.4,
      19.5, 19.6, 19.700000000000003, 19.8, 19.9, 20.0, 20.1, 20.200000000000003,
      20.3, 20.4, 20.5, 20.6, 20.7, 20.8, 20.9, 21.0, 21.1, 21.200000000000003,
      21.3, 21.4, 21.5, 21.6, 21.700000000000003, 21.8, 21.9, 22.0, 22.1, 22.2,
      22.3, 22.4, 22.5, 22.6, 22.7, 22.8, 22.9, 23.0, 23.1, 23.200000000000003,
      23.3, 23.4, 23.5, 23.6, 23.700000000000003, 23.8, 23.900000000000002, 24.0,
      24.1, 24.200000000000003, 24.3, 24.400000000000002, 24.5, 24.6, 24.7,
      24.799999999999997, 24.9, 25.0, 25.1, 25.2, 25.299999999999997, 25.4, 25.5,
      25.599999999999998, 25.699999999999996, 25.799999999999997, 25.9, 26.0,
      26.099999999999998, 26.2, 26.299999999999997, 26.4, 26.499999999999996,
      26.599999999999998, 26.7, 26.799999999999997, 26.9, 27.0,
      27.099999999999998, 27.2, 27.299999999999997, 27.4, 27.5,
      27.599999999999998, 27.7, 27.8, 27.9, 28.0, 28.099999999999998, 28.2, 28.3,
      28.4, 28.5, 28.6, 28.7, 28.8, 28.9, 29.0, 29.1, 29.2, 29.3,
      29.400000000000002, 29.5, 29.599999999999998, 29.7, 29.799999999999997,
      29.9, 30.0, 30.099999999999998, 30.2, 30.299999999999997, 30.4, 30.5,
      30.599999999999998, 30.7, 30.8, 30.9, 31.0, 31.099999999999998, 31.2,
      31.299999999999997, 31.4, 31.499999999999996, 31.599999999999998,
      31.699999999999996, 31.799999999999997, 31.9, 31.999999999999996,
      32.099999999999994, 32.199999999999996, 32.3, 32.4, 32.5,
      32.599999999999994, 32.7, 32.8, 32.9, 33.0, 33.099999999999994, 33.2, 33.3,
      33.4, 33.5, 33.599999999999994, 33.7, 33.8, 33.9, 34.0, 34.099999999999994,
      34.2, 34.3, 34.4, 34.5, 34.6, 34.7, 34.8, 34.9, 35.0, 35.1, 35.2, 35.3,
      35.4, 35.5, 35.6, 35.7, 35.8, 35.900000000000006, 36.0, 36.1, 36.2, 36.3,
      36.400000000000006, 36.5, 36.6, 36.7, 36.8, 36.900000000000006, 37.0,
      37.099999999999994, 37.199999999999996, 37.3, 37.399999999999991, 37.5,
      37.599999999999994, 37.699999999999996, 37.8, 37.9, 38.0,
      38.099999999999994, 38.199999999999996, 38.3, 38.4, 38.5,
      38.599999999999994, 38.7, 38.8, 38.9, 39.0, 39.099999999999994, 39.2, 39.3,
      39.4, 39.5, 39.599999999999994, 39.7, 39.8, 39.9, 40.0, 40.09975, 40.199,
      40.29775, 40.396, 40.49375, 40.591, 40.68775, 40.784, 40.87975, 40.975,
      41.06975, 41.164, 41.25775, 41.351, 41.44375, 41.536, 41.62775, 41.719,
      41.80975, 41.9, 41.98975, 42.079, 42.16775, 42.256, 42.34375, 42.431,
      42.51775, 42.604, 42.689750000000004, 42.775, 42.85975, 42.944, 43.02775,
      43.111, 43.19375, 43.276, 43.35775, 43.439, 43.51975, 43.6, 43.67975,
      43.759, 43.83775, 43.916, 43.99375, 44.071, 44.14775, 44.224000000000004,
      44.29975, 44.375, 44.44975, 44.524, 44.59775, 44.671, 44.74375, 44.816,
      44.88775, 44.959, 45.02975, 45.1, 45.16975, 45.239, 45.30775, 45.376,
      45.44375, 45.511, 45.57775, 45.644, 45.70975, 45.775, 45.83975, 45.904,
      45.96775, 46.031, 46.09375, 46.156, 46.21775, 46.278999999999996, 46.33975,
      46.4, 46.45975, 46.519, 46.57775, 46.636, 46.69375, 46.751000000000005,
      46.80775, 46.864, 46.91975, 46.975, 47.02975, 47.084, 47.13775, 47.191,
      47.24375, 47.296, 47.347750000000005, 47.399, 47.44975, 47.5, 47.54975,
      47.599000000000004, 47.64775, 47.696, 47.74375, 47.791, 47.83775, 47.884,
      47.92975, 47.975, 48.01975, 48.064, 48.10775, 48.150999999999996, 48.19375,
      48.236000000000004, 48.27775, 48.319, 48.35975, 48.4, 48.439750000000004,
      48.479, 48.51775, 48.556, 48.59375, 48.631, 48.66775, 48.704, 48.73975,
      48.775, 48.80975, 48.844, 48.87775, 48.911, 48.94375, 48.976, 49.00775,
      49.039, 49.06975, 49.1, 49.12975, 49.159, 49.18775, 49.216,
      49.243750000000006, 49.271, 49.29775, 49.324, 49.34975, 49.375, 49.39975,
      49.424, 49.44775, 49.471000000000004, 49.493750000000006,
      49.516000000000005, 49.53775, 49.559, 49.579750000000004, 49.6,
      49.619749999999996, 49.638999999999996, 49.65775, 49.676, 49.69375, 49.711,
      49.72775, 49.744, 49.75975, 49.775, 49.78975, 49.804, 49.817750000000004,
      49.831, 49.84375, 49.856, 49.86775, 49.879, 49.88975, 49.900000000000006,
      49.90975, 49.919, 49.92775, 49.936, 49.94375, 49.951, 49.957750000000004,
      49.964, 49.96975, 49.975, 49.97975, 49.984, 49.98775, 49.991, 49.99375,
      49.996, 49.997749999999996, 49.999, 49.99975, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 49.99975, 49.999, 49.99775,
      49.996, 49.99375, 49.991, 49.98775, 49.984, 49.97975, 49.975, 49.96975,
      49.964, 49.95775, 49.951, 49.94375, 49.936, 49.92775, 49.919, 49.90975,
      49.9, 49.88975, 49.879, 49.86775, 49.856, 49.84375, 49.831, 49.81775,
      49.804, 49.78975, 49.775, 49.75975, 49.744, 49.72775, 49.711, 49.69375,
      49.676, 49.65775, 49.639, 49.61975, 49.6, 49.57975, 49.559, 49.53775,
      49.516, 49.49375, 49.471000000000004, 49.44775, 49.424, 49.39975, 49.375,
      49.34975, 49.324, 49.29775, 49.271, 49.24375, 49.216, 49.18775, 49.159,
      49.12975, 49.1, 49.06975, 49.039, 49.00775, 48.976, 48.94375, 48.911,
      48.87775, 48.844, 48.80975, 48.775, 48.73975, 48.704, 48.66775, 48.631,
      48.59375, 48.556, 48.51775, 48.479, 48.439750000000004, 48.4, 48.35975,
      48.319, 48.27775, 48.236, 48.19375, 48.150999999999996, 48.10775, 48.064,
      48.01975, 47.975, 47.92975, 47.884, 47.83775, 47.791, 47.74375, 47.696,
      47.64775, 47.599, 47.54975, 47.5, 47.44975, 47.399, 47.34775, 47.296,
      47.24375, 47.191, 47.13775, 47.084, 47.02975, 46.975, 46.91975, 46.864,
      46.80775, 46.751, 46.69375, 46.636, 46.57775, 46.519, 46.45975, 46.4,
      46.33975, 46.278999999999996, 46.21775, 46.156, 46.09375, 46.031, 45.96775,
      45.903999999999996, 45.83975, 45.775, 45.70975, 45.644, 45.57775, 45.511,
      45.44375, 45.376, 45.30775, 45.239, 45.16975, 45.1, 45.02975, 44.959,
      44.88775, 44.816, 44.74375, 44.671, 44.59775, 44.524, 44.449749999999995,
      44.375, 44.29975, 44.224, 44.14775, 44.071, 43.99375, 43.916, 43.83775,
      43.759, 43.67975, 43.6, 43.51975, 43.439, 43.357749999999996,
      43.275999999999996, 43.19375, 43.111, 43.02775, 42.944, 42.85975, 42.775,
      42.68975, 42.604, 42.51775, 42.431, 42.34375, 42.256, 42.16775, 42.079,
      41.98975, 41.9, 41.80975, 41.719, 41.62775, 41.536, 41.44375, 41.351,
      41.25775, 41.164, 41.06975, 40.975, 40.87975, 40.784, 40.68775, 40.591,
      40.49375, 40.396, 40.29775, 40.199, 40.09975, 40.0, 39.9, 39.8, 39.7, 39.6,
      39.5, 39.4, 39.3, 39.2, 39.1, 39.0, 38.9, 38.8, 38.7, 38.6, 38.5, 38.4,
      38.3, 38.2, 38.1, 38.0, 37.9, 37.8, 37.7, 37.6, 37.5, 37.4, 37.3, 37.2,
      37.1, 37.0, 36.9, 36.8, 36.7, 36.6, 36.5, 36.4, 36.3, 36.2, 36.1, 36.0,
      35.9, 35.8, 35.7, 35.6, 35.5, 35.4, 35.3, 35.2, 35.1, 35.0, 34.9, 34.8,
      34.7, 34.6, 34.5, 34.4, 34.3, 34.2, 34.1, 34.0, 33.9, 33.8, 33.7, 33.6,
      33.5, 33.4, 33.3, 33.2, 33.1, 33.0, 32.9, 32.8, 32.7, 32.6, 32.5, 32.4,
      32.3, 32.2, 32.1, 32.0, 31.9, 31.799999999999997, 31.7, 31.6, 31.5, 31.4,
      31.299999999999997, 31.200000000000003, 31.1, 31.0, 30.9, 30.8, 30.7, 30.6,
      30.5, 30.4, 30.299999999999997, 30.2, 30.1, 30.0, 29.9, 29.799999999999997,
      29.7, 29.6, 29.5, 29.4, 29.3, 29.2, 29.1, 29.0, 28.9, 28.799999999999997,
      28.7, 28.6, 28.5, 28.4, 28.299999999999997, 28.2, 28.1, 28.0, 27.9, 27.8,
      27.7, 27.6, 27.5, 27.4, 27.3, 27.2, 27.1, 27.0, 26.9, 26.799999999999997,
      26.7, 26.6, 26.5, 26.4, 26.299999999999997, 26.2, 26.099999999999998, 26.0,
      25.9, 25.799999999999997, 25.7, 25.599999999999998, 25.5, 25.4, 25.3,
      25.200000000000003, 25.1, 25.0, 24.9, 24.8, 24.700000000000003, 24.6, 24.5,
      24.400000000000002, 24.300000000000004, 24.200000000000003, 24.1, 24.0,
      23.900000000000002, 23.8, 23.700000000000003, 23.6, 23.500000000000004,
      23.400000000000002, 23.3, 23.200000000000003, 23.1, 23.0,
      22.900000000000002, 22.8, 22.700000000000003, 22.6, 22.5,
      22.400000000000002, 22.3, 22.2, 22.1, 22.0, 21.900000000000002, 21.8, 21.7,
      21.6, 21.5, 21.4, 21.3, 21.2, 21.1, 21.0, 20.9, 20.8, 20.7,
      20.599999999999998, 20.5, 20.400000000000002, 20.3, 20.200000000000003,
      20.1, 20.0, 19.900000000000002, 19.8, 19.700000000000003, 19.6, 19.5,
      19.400000000000002, 19.3, 19.2, 19.1, 19.0, 18.900000000000002, 18.8,
      18.700000000000003, 18.6, 18.500000000000004, 18.400000000000002,
      18.300000000000004, 18.200000000000003, 18.1, 18.000000000000004,
      17.900000000000002, 17.800000000000004, 17.700000000000003, 17.6,
      17.500000000000004, 17.400000000000002, 17.3, 17.200000000000003, 17.1,
      17.0, 16.900000000000002, 16.8, 16.700000000000003, 16.6, 16.5,
      16.400000000000002, 16.3, 16.200000000000003, 16.1, 16.0,
      15.900000000000002, 15.8, 15.7, 15.600000000000001, 15.5,
      15.399999999999999, 15.3, 15.2, 15.100000000000001, 15.0,
      14.899999999999999, 14.8, 14.7, 14.600000000000001, 14.5,
      14.399999999999999, 14.3, 14.2, 14.099999999999998, 14.0,
      13.899999999999999, 13.799999999999997, 13.7, 13.599999999999998, 13.5,
      13.399999999999999, 13.299999999999997, 13.2, 13.099999999999998, 13.0,
      12.900000000000002, 12.800000000000004, 12.700000000000003,
      12.600000000000005, 12.500000000000004, 12.400000000000002,
      12.300000000000004, 12.200000000000003, 12.100000000000001,
      12.000000000000004, 11.900000000000002, 11.800000000000004,
      11.700000000000003, 11.600000000000001, 11.500000000000004,
      11.400000000000002, 11.3, 11.200000000000003, 11.100000000000001,
      11.000000000000004, 10.900000000000002, 10.8, 10.700000000000003,
      10.600000000000001, 10.5, 10.400000000000002, 10.3, 10.200000000000003,
      10.100000000000001, 10.0, 9.90025, 9.801, 9.70225, 9.604, 9.50625, 9.409,
      9.31225, 9.216, 9.12025, 9.025, 8.9302500000000009, 8.836,
      8.7422499999999985, 8.649, 8.55625, 8.464, 8.3722500000000011,
      8.2809999999999988, 8.1902499999999989, 8.1, 8.010250000000001,
      7.9210000000000012, 7.8322499999999993, 7.744, 7.65625, 7.5690000000000008,
      7.48225, 7.395999999999999, 7.31025, 7.225, 7.14025, 7.056,
      6.9722499999999989, 6.8889999999999993, 6.80625, 6.7239999999999993,
      6.6422500000000007, 6.5610000000000008, 6.48025, 6.4, 6.3202499999999988,
      6.2410000000000005, 6.16225, 6.0840000000000005, 6.00625, 5.929,
      5.8522500000000006, 5.7760000000000007, 5.7002500000000005, 5.625,
      5.5502499999999992, 5.476, 5.4022499999999996, 5.3289999999999988, 5.25625,
      5.184, 5.11225, 5.0409999999999986, 4.97025, 4.9, 4.8302499999999995,
      4.761, 4.6922500000000005, 4.6240000000000006, 4.55625, 4.489,
      4.4222499999999991, 4.356, 4.29025, 4.2249999999999988, 4.1602499999999987,
      4.0959999999999983, 4.03225, 3.9690000000000003, 3.90625,
      3.8439999999999994, 3.7822500000000003, 3.7209999999999992,
      3.6602499999999996, 3.5999999999999996, 3.5402500000000003, 3.481, 3.42225,
      3.363999999999999, 3.3062500000000004, 3.2489999999999988,
      3.1922499999999996, 3.136000000000001, 3.0802499999999995,
      3.0250000000000004, 2.97025, 2.9160000000000004, 2.8622499999999995,
      2.8089999999999993, 2.7562499999999996, 2.7040000000000006,
      2.6522499999999987, 2.6009999999999991, 2.55025, 2.5, 2.4502499999999987,
      2.401, 2.3522499999999997, 2.3040000000000003, 2.2562499999999996,
      2.2089999999999996, 2.1622499999999985, 2.1159999999999997,
      2.0702499999999979, 2.0249999999999986, 1.9802499999999998, 1.936,
      1.8922499999999989, 1.8490000000000002, 1.8062500000000004,
      1.7639999999999993, 1.7222499999999989, 1.6809999999999992, 1.64025,
      1.5999999999999996, 1.56025, 1.520999999999999, 1.4822500000000005,
      1.4439999999999991, 1.40625, 1.3689999999999998, 1.3322500000000002,
      1.2959999999999994, 1.260250000000001, 1.2249999999999996,
      1.1902499999999989, 1.1559999999999988, 1.1222499999999993,
      1.0890000000000004, 1.0562500000000004, 1.0239999999999991,
      0.9922500000000003, 0.96099999999999852, 0.93024999999999736,
      0.89999999999999858, 0.87024999999999864, 0.8409999999999993,
      0.81224999999999881, 0.78399999999999892, 0.75624999999999964,
      0.7289999999999992, 0.70224999999999937, 0.67600000000000016,
      0.65024999999999977, 0.62499999999999822, 0.60024999999999906,
      0.57599999999999874, 0.552249999999999, 0.52899999999999991,
      0.50624999999999787, 0.484, 0.46225000000000094, 0.44099999999999895,
      0.42025000000000112, 0.39999999999999858, 0.3802500000000002,
      0.36100000000000065, 0.34224999999999994, 0.32399999999999807,
      0.30624999999999858, 0.28899999999999793, 0.27224999999999966,
      0.25600000000000023, 0.24024999999999963, 0.22500000000000142,
      0.21024999999999849, 0.19599999999999795, 0.1822499999999998,
      0.16900000000000048, 0.15625, 0.14399999999999835, 0.13224999999999909,
      0.12099999999999866, 0.11024999999999707, 0.099999999999997868,
      0.0902499999999975, 0.080999999999999517, 0.072250000000000369,
      0.064000000000000057, 0.056249999999998579, 0.048999999999999488,
      0.042249999999999233, 0.035999999999997812, 0.030250000000002331,
      0.024999999999998579, 0.020249999999997215, 0.016000000000001791,
      0.012249999999998096, 0.0089999999999967883, 0.0062500000000014211,
      0.0039999999999977831, 0.0022500000000000853, 0.0010000000000012221,
      0.000249999999997641, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00025, 0.001, 0.00225, 0.004,
      0.0062499999999999995, 0.009, 0.01225, 0.016, 0.02025,
      0.024999999999999998, 0.030249999999999992, 0.036, 0.04225, 0.049,
      0.056249999999999994, 0.064, 0.072250000000000009, 0.081, 0.09025,
      0.099999999999999992, 0.11025000000000001, 0.12099999999999997,
      0.13224999999999998, 0.144, 0.15625, 0.169, 0.18225, 0.196,
      0.21025000000000002, 0.22499999999999998, 0.24025, 0.256, 0.27225,
      0.28900000000000003, 0.30625, 0.324, 0.34224999999999994, 0.361,
      0.38024999999999992, 0.39999999999999997, 0.42025000000000007,
      0.44100000000000006, 0.46225000000000005, 0.48399999999999987, 0.50625,
      0.52899999999999991, 0.55225, 0.576, 0.60025000000000006, 0.625,
      0.65025000000000011, 0.676, 0.70224999999999993, 0.729, 0.75625, 0.784,
      0.81225, 0.84100000000000008, 0.87025, 0.89999999999999991,
      0.93024999999999991, 0.961, 0.99224999999999985, 1.024, 1.05625, 1.089,
      1.12225, 1.1560000000000001, 1.19025, 1.225, 1.26025, 1.296,
      1.3322499999999997, 1.3689999999999998, 1.40625, 1.444, 1.4822499999999998,
      1.5209999999999997, 1.56025, 1.5999999999999999, 1.64025,
      1.6810000000000003, 1.72225, 1.7640000000000002, 1.80625,
      1.8490000000000002, 1.8922500000000002, 1.9359999999999995, 1.98025, 2.025,
      2.0702499999999997, 2.1159999999999997, 2.1622500000000002, 2.209,
      2.2562499999999996, 2.304, 2.35225, 2.4010000000000002, 2.45025, 2.5,
      2.5502500000000006, 2.6010000000000004, 2.6522500000000004, 2.704,
      2.7562500000000005, 2.8090000000000006, 2.8622500000000004, 2.916,
      2.9702500000000009, 3.0250000000000008, 3.0802500000000008,
      3.136000000000001, 3.19225, 3.249, 3.30625, 3.3640000000000003,
      3.4222500000000005, 3.481, 3.5402500000000003, 3.600000000000001,
      3.6602500000000009, 3.721, 3.7822500000000003, 3.8440000000000012, 3.90625,
      3.9689999999999994, 4.0322499999999994, 4.096, 4.1602500000000004, 4.225,
      4.29025, 4.356, 4.42225, 4.489, 4.55625, 4.6240000000000006,
      4.6922500000000005, 4.761, 4.83025, 4.9, 4.9702500000000009, 5.041,
      5.11225, 5.184, 5.2562500000000005, 5.3290000000000015, 5.4022500000000013,
      5.4760000000000018, 5.5502500000000019, 5.6250000000000009,
      5.7002500000000005, 5.7760000000000016, 5.8522500000000015,
      5.929000000000002, 6.0062500000000014, 6.0840000000000023, 6.16225, 6.241,
      6.3202500000000006, 6.3999999999999995, 6.4802500000000007, 6.561,
      6.6422500000000007, 6.7240000000000011, 6.80625, 6.889, 6.97225,
      7.0560000000000009, 7.1402500000000009, 7.225, 7.3102500000000008,
      7.3960000000000008, 7.4822500000000014, 7.5690000000000008,
      7.6562500000000009, 7.7440000000000007, 7.832250000000001,
      7.9210000000000012, 8.010250000000001, 8.1000000000000014, 8.19025,
      8.2809999999999988, 8.37225, 8.4639999999999986, 8.5562499999999986,
      8.6490000000000009, 8.7422499999999985, 8.836, 8.93025, 9.0249999999999986,
      9.12025, 9.216, 9.31225, 9.409, 9.50625, 9.604000000000001, 9.70225, 9.801,
      9.9002500000000015, 10.0, 10.1, 10.2, 10.3, 10.4, 10.5, 10.6, 10.7, 10.8,
      10.9, 11.0, 11.1, 11.2, 11.3, 11.4, 11.5, 11.6, 11.7, 11.8, 11.9, 12.0,
      12.1, 12.2, 12.3, 12.4, 12.5, 12.6, 12.7, 12.8, 12.9, 13.0, 13.1, 13.2,
      13.3, 13.4, 13.5, 13.600000000000001, 13.7, 13.8, 13.9, 14.0,
      14.100000000000001, 14.2, 14.3, 14.399999999999999, 14.5, 14.6, 14.7, 14.8,
      14.9, 15.0, 15.100000000000001, 15.2, 15.3, 15.4, 15.5, 15.600000000000001,
      15.7, 15.8, 15.9, 16.0, 16.1, 16.2, 16.3, 16.4, 16.5, 16.6, 16.7, 16.8,
      16.9, 17.0, 17.1, 17.200000000000003, 17.3, 17.4, 17.5, 17.6, 17.7, 17.8,
      17.9, 18.0, 18.1, 18.200000000000003, 18.3, 18.4, 18.5, 18.6,
      18.700000000000003, 18.799999999999997, 18.9, 19.0, 19.1, 19.2, 19.3, 19.4,
      19.5, 19.6, 19.700000000000003, 19.8, 19.9, 20.0, 20.1, 20.200000000000003,
      20.3, 20.4, 20.5, 20.6, 20.7, 20.8, 20.9, 21.0, 21.1, 21.200000000000003,
      21.3, 21.4, 21.5, 21.6, 21.700000000000003, 21.8, 21.9, 22.0, 22.1, 22.2,
      22.3, 22.4, 22.5, 22.6, 22.7, 22.8, 22.9, 23.0, 23.1, 23.200000000000003,
      23.3, 23.4, 23.5, 23.6, 23.700000000000003, 23.8, 23.900000000000002, 24.0,
      24.1, 24.200000000000003, 24.3, 24.400000000000002, 24.5, 24.6, 24.7,
      24.799999999999997, 24.9, 25.0, 25.1, 25.2, 25.299999999999997, 25.4, 25.5,
      25.599999999999998, 25.699999999999996, 25.799999999999997, 25.9, 26.0,
      26.099999999999998, 26.2, 26.299999999999997, 26.4, 26.499999999999996,
      26.599999999999998, 26.7, 26.799999999999997, 26.9, 27.0,
      27.099999999999998, 27.2, 27.299999999999997, 27.4, 27.5,
      27.599999999999998, 27.7, 27.8, 27.9, 28.0, 28.099999999999998, 28.2, 28.3,
      28.4, 28.5, 28.6, 28.7, 28.8, 28.9, 29.0, 29.1, 29.2, 29.3,
      29.400000000000002, 29.5, 29.599999999999998, 29.7, 29.799999999999997,
      29.9, 30.0, 30.099999999999998, 30.2, 30.299999999999997, 30.4, 30.5,
      30.599999999999998, 30.7, 30.8, 30.9, 31.0, 31.099999999999998, 31.2,
      31.299999999999997, 31.4, 31.499999999999996, 31.599999999999998,
      31.699999999999996, 31.799999999999997, 31.9, 31.999999999999996,
      32.099999999999994, 32.199999999999996, 32.3, 32.4, 32.5,
      32.599999999999994, 32.7, 32.8, 32.9, 33.0, 33.099999999999994, 33.2, 33.3,
      33.4, 33.5, 33.599999999999994, 33.7, 33.8, 33.9, 34.0, 34.099999999999994,
      34.2, 34.3, 34.4, 34.5, 34.6, 34.7, 34.8, 34.9, 35.0, 35.1, 35.2, 35.3,
      35.4, 35.5, 35.6, 35.7, 35.8, 35.900000000000006, 36.0, 36.1, 36.2, 36.3,
      36.400000000000006, 36.5, 36.6, 36.7, 36.8, 36.900000000000006, 37.0,
      37.099999999999994, 37.199999999999996, 37.3, 37.399999999999991, 37.5,
      37.599999999999994, 37.699999999999996, 37.8, 37.9, 38.0,
      38.099999999999994, 38.199999999999996, 38.3, 38.4, 38.5,
      38.599999999999994, 38.7, 38.8, 38.9, 39.0, 39.099999999999994, 39.2, 39.3,
      39.4, 39.5, 39.599999999999994, 39.7, 39.8, 39.9, 40.0, 40.09975, 40.199,
      40.29775, 40.396, 40.49375, 40.591, 40.68775, 40.784, 40.87975, 40.975,
      41.06975, 41.164, 41.25775, 41.351, 41.44375, 41.536, 41.62775, 41.719,
      41.80975, 41.9, 41.98975, 42.079, 42.16775, 42.256, 42.34375, 42.431,
      42.51775, 42.604, 42.689750000000004, 42.775, 42.85975, 42.944, 43.02775,
      43.111, 43.19375, 43.276, 43.35775, 43.439, 43.51975, 43.6, 43.67975,
      43.759, 43.83775, 43.916, 43.99375, 44.071, 44.14775, 44.224000000000004,
      44.29975, 44.375, 44.44975, 44.524, 44.59775, 44.671, 44.74375, 44.816,
      44.88775, 44.959, 45.02975, 45.1, 45.16975, 45.239, 45.30775, 45.376,
      45.44375, 45.511, 45.57775, 45.644, 45.70975, 45.775, 45.83975, 45.904,
      45.96775, 46.031, 46.09375, 46.156, 46.21775, 46.278999999999996, 46.33975,
      46.4, 46.45975, 46.519, 46.57775, 46.636, 46.69375, 46.751000000000005,
      46.80775, 46.864, 46.91975, 46.975, 47.02975, 47.084, 47.13775, 47.191,
      47.24375, 47.296, 47.347750000000005, 47.399, 47.44975, 47.5, 47.54975,
      47.599000000000004, 47.64775, 47.696, 47.74375, 47.791, 47.83775, 47.884,
      47.92975, 47.975, 48.01975, 48.064, 48.10775, 48.150999999999996, 48.19375,
      48.236000000000004, 48.27775, 48.319, 48.35975, 48.4, 48.439750000000004,
      48.479, 48.51775, 48.556, 48.59375, 48.631, 48.66775, 48.704, 48.73975,
      48.775, 48.80975, 48.844, 48.87775, 48.911, 48.94375, 48.976, 49.00775,
      49.039, 49.06975, 49.1, 49.12975, 49.159, 49.18775, 49.216,
      49.243750000000006, 49.271, 49.29775, 49.324, 49.34975, 49.375, 49.39975,
      49.424, 49.44775, 49.471000000000004, 49.493750000000006,
      49.516000000000005, 49.53775, 49.559, 49.579750000000004, 49.6,
      49.619749999999996, 49.638999999999996, 49.65775, 49.676, 49.69375, 49.711,
      49.72775, 49.744, 49.75975, 49.775, 49.78975, 49.804, 49.817750000000004,
      49.831, 49.84375, 49.856, 49.86775, 49.879, 49.88975, 49.900000000000006,
      49.90975, 49.919, 49.92775, 49.936, 49.94375, 49.951, 49.957750000000004,
      49.964, 49.96975, 49.975, 49.97975, 49.984, 49.98775, 49.991, 49.99375,
      49.996, 49.997749999999996, 49.999, 49.99975, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 49.99975, 49.999, 49.99775,
      49.996, 49.99375, 49.991, 49.98775, 49.984, 49.97975, 49.975, 49.96975,
      49.964, 49.95775, 49.951, 49.94375, 49.936, 49.92775, 49.919, 49.90975,
      49.9, 49.88975, 49.879, 49.86775, 49.856, 49.84375, 49.831, 49.81775,
      49.804, 49.78975, 49.775, 49.75975, 49.744, 49.72775, 49.711, 49.69375,
      49.676, 49.65775, 49.639, 49.61975, 49.6, 49.57975, 49.559, 49.53775,
      49.516, 49.49375, 49.471000000000004, 49.44775, 49.424, 49.39975, 49.375,
      49.34975, 49.324, 49.29775, 49.271, 49.24375, 49.216, 49.18775, 49.159,
      49.12975, 49.1, 49.06975, 49.039, 49.00775, 48.976, 48.94375, 48.911,
      48.87775, 48.844, 48.80975, 48.775, 48.73975, 48.704, 48.66775, 48.631,
      48.59375, 48.556, 48.51775, 48.479, 48.439750000000004, 48.4, 48.35975,
      48.319, 48.27775, 48.236, 48.19375, 48.150999999999996, 48.10775, 48.064,
      48.01975, 47.975, 47.92975, 47.884, 47.83775, 47.791, 47.74375, 47.696,
      47.64775, 47.599, 47.54975, 47.5, 47.44975, 47.399, 47.34775, 47.296,
      47.24375, 47.191, 47.13775, 47.084, 47.02975, 46.975, 46.91975, 46.864,
      46.80775, 46.751, 46.69375, 46.636, 46.57775, 46.519, 46.45975, 46.4,
      46.33975, 46.278999999999996, 46.21775, 46.156, 46.09375, 46.031, 45.96775,
      45.903999999999996, 45.83975, 45.775, 45.70975, 45.644, 45.57775, 45.511,
      45.44375, 45.376, 45.30775, 45.239, 45.16975, 45.1, 45.02975, 44.959,
      44.88775, 44.816, 44.74375, 44.671, 44.59775, 44.524, 44.449749999999995,
      44.375, 44.29975, 44.224, 44.14775, 44.071, 43.99375, 43.916, 43.83775,
      43.759, 43.67975, 43.6, 43.51975, 43.439, 43.357749999999996,
      43.275999999999996, 43.19375, 43.111, 43.02775, 42.944, 42.85975, 42.775,
      42.68975, 42.604, 42.51775, 42.431, 42.34375, 42.256, 42.16775, 42.079,
      41.98975, 41.9, 41.80975, 41.719, 41.62775, 41.536, 41.44375, 41.351,
      41.25775, 41.164, 41.06975, 40.975, 40.87975, 40.784, 40.68775, 40.591,
      40.49375, 40.396, 40.29775, 40.199, 40.09975, 40.0, 39.9, 39.8, 39.7, 39.6,
      39.5, 39.4, 39.3, 39.2, 39.1, 39.0, 38.9, 38.8, 38.7, 38.6, 38.5, 38.4,
      38.3, 38.2, 38.1, 38.0, 37.9, 37.8, 37.7, 37.6, 37.5, 37.4, 37.3, 37.2,
      37.1, 37.0, 36.9, 36.8, 36.7, 36.6, 36.5, 36.4, 36.3, 36.2, 36.1, 36.0,
      35.9, 35.8, 35.7, 35.6, 35.5, 35.4, 35.3, 35.2, 35.1, 35.0, 34.9, 34.8,
      34.7, 34.6, 34.5, 34.4, 34.3, 34.2, 34.1, 34.0, 33.9, 33.8, 33.7, 33.6,
      33.5, 33.4, 33.3, 33.2, 33.1, 33.0, 32.9, 32.8, 32.7, 32.6, 32.5, 32.4,
      32.3, 32.2, 32.1, 32.0, 31.9, 31.799999999999997, 31.7, 31.6, 31.5, 31.4,
      31.299999999999997, 31.200000000000003, 31.1, 31.0, 30.9, 30.8, 30.7, 30.6,
      30.5, 30.4, 30.299999999999997, 30.2, 30.1, 30.0, 29.9, 29.799999999999997,
      29.7, 29.6, 29.5, 29.4, 29.3, 29.2, 29.1, 29.0, 28.9, 28.799999999999997,
      28.7, 28.6, 28.5, 28.4, 28.299999999999997, 28.2, 28.1, 28.0, 27.9, 27.8,
      27.7, 27.6, 27.5, 27.4, 27.3, 27.2, 27.1, 27.0, 26.9, 26.799999999999997,
      26.7, 26.6, 26.5, 26.4, 26.299999999999997, 26.2, 26.099999999999998, 26.0,
      25.9, 25.799999999999997, 25.7, 25.599999999999998, 25.5, 25.4, 25.3,
      25.200000000000003, 25.1, 25.0, 24.9, 24.8, 24.700000000000003, 24.6, 24.5,
      24.400000000000002, 24.300000000000004, 24.200000000000003, 24.1, 24.0,
      23.900000000000002, 23.8, 23.700000000000003, 23.6, 23.500000000000004,
      23.400000000000002, 23.3, 23.200000000000003, 23.1, 23.0,
      22.900000000000002, 22.8, 22.700000000000003, 22.6, 22.5,
      22.400000000000002, 22.3, 22.2, 22.1, 22.0, 21.900000000000002, 21.8, 21.7,
      21.6, 21.5, 21.4, 21.3, 21.2, 21.1, 21.0, 20.9, 20.8, 20.7,
      20.599999999999998, 20.5, 20.400000000000002, 20.3, 20.200000000000003,
      20.1, 20.0, 19.900000000000002, 19.8, 19.700000000000003, 19.6, 19.5,
      19.400000000000002, 19.3, 19.2, 19.1, 19.0, 18.900000000000002, 18.8,
      18.700000000000003, 18.6, 18.500000000000004, 18.400000000000002,
      18.300000000000004, 18.200000000000003, 18.1, 18.000000000000004,
      17.900000000000002, 17.800000000000004, 17.700000000000003, 17.6,
      17.500000000000004, 17.400000000000002, 17.3, 17.200000000000003, 17.1,
      17.0, 16.900000000000002, 16.8, 16.700000000000003, 16.6, 16.5,
      16.400000000000002, 16.3, 16.200000000000003, 16.1, 16.0,
      15.900000000000002, 15.8, 15.7, 15.600000000000001, 15.5,
      15.399999999999999, 15.3, 15.2, 15.100000000000001, 15.0,
      14.899999999999999, 14.8, 14.7, 14.600000000000001, 14.5,
      14.399999999999999, 14.3, 14.2, 14.099999999999998, 14.0,
      13.899999999999999, 13.799999999999997, 13.7, 13.599999999999998, 13.5,
      13.399999999999999, 13.299999999999997, 13.2, 13.099999999999998, 13.0,
      12.900000000000002, 12.800000000000004, 12.700000000000003,
      12.600000000000005, 12.500000000000004, 12.400000000000002,
      12.300000000000004, 12.200000000000003, 12.100000000000001,
      12.000000000000004, 11.900000000000002, 11.800000000000004,
      11.700000000000003, 11.600000000000001, 11.500000000000004,
      11.400000000000002, 11.3, 11.200000000000003, 11.100000000000001,
      11.000000000000004, 10.900000000000002, 10.8, 10.700000000000003,
      10.600000000000001, 10.5, 10.400000000000002, 10.3, 10.200000000000003,
      10.100000000000001, 10.0, 9.90025, 9.801, 9.70225, 9.604, 9.50625, 9.409,
      9.31225, 9.216, 9.12025, 9.025, 8.9302500000000009, 8.836,
      8.7422499999999985, 8.649, 8.55625, 8.464, 8.3722500000000011,
      8.2809999999999988, 8.1902499999999989, 8.1, 8.010250000000001,
      7.9210000000000012, 7.8322499999999993, 7.744, 7.65625, 7.5690000000000008,
      7.48225, 7.395999999999999, 7.31025, 7.225, 7.14025, 7.056,
      6.9722499999999989, 6.8889999999999993, 6.80625, 6.7239999999999993,
      6.6422500000000007, 6.5610000000000008, 6.48025, 6.4, 6.3202499999999988,
      6.2410000000000005, 6.16225, 6.0840000000000005, 6.00625, 5.929,
      5.8522500000000006, 5.7760000000000007, 5.7002500000000005, 5.625,
      5.5502499999999992, 5.476, 5.4022499999999996, 5.3289999999999988, 5.25625,
      5.184, 5.11225, 5.0409999999999986, 4.97025, 4.9, 4.8302499999999995,
      4.761, 4.6922500000000005, 4.6240000000000006, 4.55625, 4.489,
      4.4222499999999991, 4.356, 4.29025, 4.2249999999999988, 4.1602499999999987,
      4.0959999999999983, 4.03225, 3.9690000000000003, 3.90625,
      3.8439999999999994, 3.7822500000000003, 3.7209999999999992,
      3.6602499999999996, 3.5999999999999996, 3.5402500000000003, 3.481, 3.42225,
      3.363999999999999, 3.3062500000000004, 3.2489999999999988,
      3.1922499999999996, 3.136000000000001, 3.0802499999999995,
      3.0250000000000004, 2.97025, 2.9160000000000004, 2.8622499999999995,
      2.8089999999999993, 2.7562499999999996, 2.7040000000000006,
      2.6522499999999987, 2.6009999999999991, 2.55025, 2.5, 2.4502499999999987,
      2.401, 2.3522499999999997, 2.3040000000000003, 2.2562499999999996,
      2.2089999999999996, 2.1622499999999985, 2.1159999999999997,
      2.0702499999999979, 2.0249999999999986, 1.9802499999999998, 1.936,
      1.8922499999999989, 1.8490000000000002, 1.8062500000000004,
      1.7639999999999993, 1.7222499999999989, 1.6809999999999992, 1.64025,
      1.5999999999999996, 1.56025, 1.520999999999999, 1.4822500000000005,
      1.4439999999999991, 1.40625, 1.3689999999999998, 1.3322500000000002,
      1.2959999999999994, 1.260250000000001, 1.2249999999999996,
      1.1902499999999989, 1.1559999999999988, 1.1222499999999993,
      1.0890000000000004, 1.0562500000000004, 1.0239999999999991,
      0.9922500000000003, 0.96099999999999852, 0.93024999999999736,
      0.89999999999999858, 0.87024999999999864, 0.8409999999999993,
      0.81224999999999881, 0.78399999999999892, 0.75624999999999964,
      0.7289999999999992, 0.70224999999999937, 0.67600000000000016,
      0.65024999999999977, 0.62499999999999822, 0.60024999999999906,
      0.57599999999999874, 0.552249999999999, 0.52899999999999991,
      0.50624999999999787, 0.484, 0.46225000000000094, 0.44099999999999895,
      0.42025000000000112, 0.39999999999999858, 0.3802500000000002,
      0.36100000000000065, 0.34224999999999994, 0.32399999999999807,
      0.30624999999999858, 0.28899999999999793, 0.27224999999999966,
      0.25600000000000023, 0.24024999999999963, 0.22500000000000142,
      0.21024999999999849, 0.19599999999999795, 0.1822499999999998,
      0.16900000000000048, 0.15625, 0.14399999999999835, 0.13224999999999909,
      0.12099999999999866, 0.11024999999999707, 0.099999999999997868,
      0.0902499999999975, 0.080999999999999517, 0.072250000000000369,
      0.064000000000000057, 0.056249999999998579, 0.048999999999999488,
      0.042249999999999233, 0.035999999999997812, 0.030250000000002331,
      0.024999999999998579, 0.020249999999997215, 0.016000000000001791,
      0.012249999999998096, 0.0089999999999967883, 0.0062500000000014211,
      0.0039999999999977831, 0.0022500000000000853, 0.0010000000000012221,
      0.000249999999997641, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00025, 0.001, 0.00225, 0.004,
      0.0062499999999999995, 0.009, 0.01225, 0.016, 0.02025,
      0.024999999999999998, 0.030249999999999992, 0.036, 0.04225, 0.049,
      0.056249999999999994, 0.064, 0.072250000000000009, 0.081, 0.09025,
      0.099999999999999992, 0.11025000000000001, 0.12099999999999997,
      0.13224999999999998, 0.144, 0.15625, 0.169, 0.18225, 0.196,
      0.21025000000000002, 0.22499999999999998, 0.24025, 0.256, 0.27225,
      0.28900000000000003, 0.30625, 0.324, 0.34224999999999994, 0.361,
      0.38024999999999992, 0.39999999999999997, 0.42025000000000007,
      0.44100000000000006, 0.46225000000000005, 0.48399999999999987, 0.50625,
      0.52899999999999991, 0.55225, 0.576, 0.60025000000000006, 0.625,
      0.65025000000000011, 0.676, 0.70224999999999993, 0.729, 0.75625, 0.784,
      0.81225, 0.84100000000000008, 0.87025, 0.89999999999999991,
      0.93024999999999991, 0.961, 0.99224999999999985, 1.024, 1.05625, 1.089,
      1.12225, 1.1560000000000001, 1.19025, 1.225, 1.26025, 1.296,
      1.3322499999999997, 1.3689999999999998, 1.40625, 1.444, 1.4822499999999998,
      1.5209999999999997, 1.56025, 1.5999999999999999, 1.64025,
      1.6810000000000003, 1.72225, 1.7640000000000002, 1.80625,
      1.8490000000000002, 1.8922500000000002, 1.9359999999999995, 1.98025, 2.025,
      2.0702499999999997, 2.1159999999999997, 2.1622500000000002, 2.209,
      2.2562499999999996, 2.304, 2.35225, 2.4010000000000002, 2.45025, 2.5,
      2.5502500000000006, 2.6010000000000004, 2.6522500000000004, 2.704,
      2.7562500000000005, 2.8090000000000006, 2.8622500000000004, 2.916,
      2.9702500000000009, 3.0250000000000008, 3.0802500000000008,
      3.136000000000001, 3.19225, 3.249, 3.30625, 3.3640000000000003,
      3.4222500000000005, 3.481, 3.5402500000000003, 3.600000000000001,
      3.6602500000000009, 3.721, 3.7822500000000003, 3.8440000000000012, 3.90625,
      3.9689999999999994, 4.0322499999999994, 4.096, 4.1602500000000004, 4.225,
      4.29025, 4.356, 4.42225, 4.489, 4.55625, 4.6240000000000006,
      4.6922500000000005, 4.761, 4.83025, 4.9, 4.9702500000000009, 5.041,
      5.11225, 5.184, 5.2562500000000005, 5.3290000000000015, 5.4022500000000013,
      5.4760000000000018, 5.5502500000000019, 5.6250000000000009,
      5.7002500000000005, 5.7760000000000016, 5.8522500000000015,
      5.929000000000002, 6.0062500000000014, 6.0840000000000023, 6.16225, 6.241,
      6.3202500000000006, 6.3999999999999995, 6.4802500000000007, 6.561,
      6.6422500000000007, 6.7240000000000011, 6.80625, 6.889, 6.97225,
      7.0560000000000009, 7.1402500000000009, 7.225, 7.3102500000000008,
      7.3960000000000008, 7.4822500000000014, 7.5690000000000008,
      7.6562500000000009, 7.7440000000000007, 7.832250000000001,
      7.9210000000000012, 8.010250000000001, 8.1000000000000014, 8.19025,
      8.2809999999999988, 8.37225, 8.4639999999999986, 8.5562499999999986,
      8.6490000000000009, 8.7422499999999985, 8.836, 8.93025, 9.0249999999999986,
      9.12025, 9.216, 9.31225, 9.409, 9.50625, 9.604000000000001, 9.70225, 9.801,
      9.9002500000000015, 10.0, 10.1, 10.2, 10.3, 10.4, 10.5, 10.6, 10.7, 10.8,
      10.9, 11.0, 11.1, 11.2, 11.3, 11.4, 11.5, 11.6, 11.7, 11.8, 11.9, 12.0,
      12.1, 12.2, 12.3, 12.4, 12.5, 12.6, 12.7, 12.8, 12.9, 13.0, 13.1, 13.2,
      13.3, 13.4, 13.5, 13.600000000000001, 13.7, 13.8, 13.9, 14.0,
      14.100000000000001, 14.2, 14.3, 14.399999999999999, 14.5, 14.6, 14.7, 14.8,
      14.9, 15.0, 15.100000000000001, 15.2, 15.3, 15.4, 15.5, 15.600000000000001,
      15.7, 15.8, 15.9, 16.0, 16.1, 16.2, 16.3, 16.4, 16.5, 16.6, 16.7, 16.8,
      16.9, 17.0, 17.1, 17.200000000000003, 17.3, 17.4, 17.5, 17.6, 17.7, 17.8,
      17.9, 18.0, 18.1, 18.200000000000003, 18.3, 18.4, 18.5, 18.6,
      18.700000000000003, 18.799999999999997, 18.9, 19.0, 19.1, 19.2, 19.3, 19.4,
      19.5, 19.6, 19.700000000000003, 19.8, 19.9, 20.0, 20.1, 20.200000000000003,
      20.3, 20.4, 20.5, 20.6, 20.7, 20.8, 20.9, 21.0, 21.1, 21.200000000000003,
      21.3, 21.4, 21.5, 21.6, 21.700000000000003, 21.8, 21.9, 22.0, 22.1, 22.2,
      22.3, 22.4, 22.5, 22.6, 22.7, 22.8, 22.9, 23.0, 23.1, 23.200000000000003,
      23.3, 23.4, 23.5, 23.6, 23.700000000000003, 23.8, 23.900000000000002, 24.0,
      24.1, 24.200000000000003, 24.3, 24.400000000000002, 24.5, 24.6, 24.7,
      24.799999999999997, 24.9, 25.0, 25.1, 25.2, 25.299999999999997, 25.4, 25.5,
      25.599999999999998, 25.699999999999996, 25.799999999999997, 25.9, 26.0,
      26.099999999999998, 26.2, 26.299999999999997, 26.4, 26.499999999999996,
      26.599999999999998, 26.7, 26.799999999999997, 26.9, 27.0,
      27.099999999999998, 27.2, 27.299999999999997, 27.4, 27.5,
      27.599999999999998, 27.7, 27.8, 27.9, 28.0, 28.099999999999998, 28.2, 28.3,
      28.4, 28.5, 28.6, 28.7, 28.8, 28.9, 29.0, 29.1, 29.2, 29.3,
      29.400000000000002, 29.5, 29.599999999999998, 29.7, 29.799999999999997,
      29.9, 30.0, 30.099999999999998, 30.2, 30.299999999999997, 30.4, 30.5,
      30.599999999999998, 30.7, 30.8, 30.9, 31.0, 31.099999999999998, 31.2,
      31.299999999999997, 31.4, 31.499999999999996, 31.599999999999998,
      31.699999999999996, 31.799999999999997, 31.9, 31.999999999999996,
      32.099999999999994, 32.199999999999996, 32.3, 32.4, 32.5,
      32.599999999999994, 32.7, 32.8, 32.9, 33.0, 33.099999999999994, 33.2, 33.3,
      33.4, 33.5, 33.599999999999994, 33.7, 33.8, 33.9, 34.0, 34.099999999999994,
      34.2, 34.3, 34.4, 34.5, 34.6, 34.7, 34.8, 34.9, 35.0, 35.1, 35.2, 35.3,
      35.4, 35.5, 35.6, 35.7, 35.8, 35.900000000000006, 36.0, 36.1, 36.2, 36.3,
      36.400000000000006, 36.5, 36.6, 36.7, 36.8, 36.900000000000006, 37.0,
      37.099999999999994, 37.199999999999996, 37.3, 37.399999999999991, 37.5,
      37.599999999999994, 37.699999999999996, 37.8, 37.9, 38.0,
      38.099999999999994, 38.199999999999996, 38.3, 38.4, 38.5,
      38.599999999999994, 38.7, 38.8, 38.9, 39.0, 39.099999999999994, 39.2, 39.3,
      39.4, 39.5, 39.599999999999994, 39.7, 39.8, 39.9, 40.0, 40.09975, 40.199,
      40.29775, 40.396, 40.49375, 40.591, 40.68775, 40.784, 40.87975, 40.975,
      41.06975, 41.164, 41.25775, 41.351, 41.44375, 41.536, 41.62775, 41.719,
      41.80975, 41.9, 41.98975, 42.079, 42.16775, 42.256, 42.34375, 42.431,
      42.51775, 42.604, 42.689750000000004, 42.775, 42.85975, 42.944, 43.02775,
      43.111, 43.19375, 43.276, 43.35775, 43.439, 43.51975, 43.6, 43.67975,
      43.759, 43.83775, 43.916, 43.99375, 44.071, 44.14775, 44.224000000000004,
      44.29975, 44.375, 44.44975, 44.524, 44.59775, 44.671, 44.74375, 44.816,
      44.88775, 44.959, 45.02975, 45.1, 45.16975, 45.239, 45.30775, 45.376,
      45.44375, 45.511, 45.57775, 45.644, 45.70975, 45.775, 45.83975, 45.904,
      45.96775, 46.031, 46.09375, 46.156, 46.21775, 46.278999999999996, 46.33975,
      46.4, 46.45975, 46.519, 46.57775, 46.636, 46.69375, 46.751000000000005,
      46.80775, 46.864, 46.91975, 46.975, 47.02975, 47.084, 47.13775, 47.191,
      47.24375, 47.296, 47.347750000000005, 47.399, 47.44975, 47.5, 47.54975,
      47.599000000000004, 47.64775, 47.696, 47.74375, 47.791, 47.83775, 47.884,
      47.92975, 47.975, 48.01975, 48.064, 48.10775, 48.150999999999996, 48.19375,
      48.236000000000004, 48.27775, 48.319, 48.35975, 48.4, 48.439750000000004,
      48.479, 48.51775, 48.556, 48.59375, 48.631, 48.66775, 48.704, 48.73975,
      48.775, 48.80975, 48.844, 48.87775, 48.911, 48.94375, 48.976, 49.00775,
      49.039, 49.06975, 49.1, 49.12975, 49.159, 49.18775, 49.216,
      49.243750000000006, 49.271, 49.29775, 49.324, 49.34975, 49.375, 49.39975,
      49.424, 49.44775, 49.471000000000004, 49.493750000000006,
      49.516000000000005, 49.53775, 49.559, 49.579750000000004, 49.6,
      49.619749999999996, 49.638999999999996, 49.65775, 49.676, 49.69375, 49.711,
      49.72775, 49.744, 49.75975, 49.775, 49.78975, 49.804, 49.817750000000004,
      49.831, 49.84375, 49.856, 49.86775, 49.879, 49.88975, 49.900000000000006,
      49.90975, 49.919, 49.92775, 49.936, 49.94375, 49.951, 49.957750000000004,
      49.964, 49.96975, 49.975, 49.97975, 49.984, 49.98775, 49.991, 49.99375,
      49.996, 49.997749999999996, 49.999, 49.99975, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 49.99975, 49.999, 49.99775,
      49.996, 49.99375, 49.991, 49.98775, 49.984, 49.97975, 49.975, 49.96975,
      49.964, 49.95775, 49.951, 49.94375, 49.936, 49.92775, 49.919, 49.90975,
      49.9, 49.88975, 49.879, 49.86775, 49.856, 49.84375, 49.831, 49.81775,
      49.804, 49.78975, 49.775, 49.75975, 49.744, 49.72775, 49.711, 49.69375,
      49.676, 49.65775, 49.639, 49.61975, 49.6, 49.57975, 49.559, 49.53775,
      49.516, 49.49375, 49.471000000000004, 49.44775, 49.424, 49.39975, 49.375,
      49.34975, 49.324, 49.29775, 49.271, 49.24375, 49.216, 49.18775, 49.159,
      49.12975, 49.1, 49.06975, 49.039, 49.00775, 48.976, 48.94375, 48.911,
      48.87775, 48.844, 48.80975, 48.775, 48.73975, 48.704, 48.66775, 48.631,
      48.59375, 48.556, 48.51775, 48.479, 48.439750000000004, 48.4, 48.35975,
      48.319, 48.27775, 48.236, 48.19375, 48.150999999999996, 48.10775, 48.064,
      48.01975, 47.975, 47.92975, 47.884, 47.83775, 47.791, 47.74375, 47.696,
      47.64775, 47.599, 47.54975, 47.5, 47.44975, 47.399, 47.34775, 47.296,
      47.24375, 47.191, 47.13775, 47.084, 47.02975, 46.975, 46.91975, 46.864,
      46.80775, 46.751, 46.69375, 46.636, 46.57775, 46.519, 46.45975, 46.4,
      46.33975, 46.278999999999996, 46.21775, 46.156, 46.09375, 46.031, 45.96775,
      45.903999999999996, 45.83975, 45.775, 45.70975, 45.644, 45.57775, 45.511,
      45.44375, 45.376, 45.30775, 45.239, 45.16975, 45.1, 45.02975, 44.959,
      44.88775, 44.816, 44.74375, 44.671, 44.59775, 44.524, 44.449749999999995,
      44.375, 44.29975, 44.224, 44.14775, 44.071, 43.99375, 43.916, 43.83775,
      43.759, 43.67975, 43.6, 43.51975, 43.439, 43.357749999999996,
      43.275999999999996, 43.19375, 43.111, 43.02775, 42.944, 42.85975, 42.775,
      42.68975, 42.604, 42.51775, 42.431, 42.34375, 42.256, 42.16775, 42.079,
      41.98975, 41.9, 41.80975, 41.719, 41.62775, 41.536, 41.44375, 41.351,
      41.25775, 41.164, 41.06975, 40.975, 40.87975, 40.784, 40.68775, 40.591,
      40.49375, 40.396, 40.29775, 40.199, 40.09975, 40.0, 39.9, 39.8, 39.7, 39.6,
      39.5, 39.4, 39.3, 39.2, 39.1, 39.0, 38.9, 38.8, 38.7, 38.6, 38.5, 38.4,
      38.3, 38.2, 38.1, 38.0, 37.9, 37.8, 37.7, 37.6, 37.5, 37.4, 37.3, 37.2,
      37.1, 37.0, 36.9, 36.8, 36.7, 36.6, 36.5, 36.4, 36.3, 36.2, 36.1, 36.0,
      35.9, 35.8, 35.7, 35.6, 35.5, 35.4, 35.3, 35.2, 35.1, 35.0, 34.9, 34.8,
      34.7, 34.6, 34.5, 34.4, 34.3, 34.2, 34.1, 34.0, 33.9, 33.8, 33.7, 33.6,
      33.5, 33.4, 33.3, 33.2, 33.1, 33.0, 32.9, 32.8, 32.7, 32.6, 32.5, 32.4,
      32.3, 32.2, 32.1, 32.0, 31.9, 31.799999999999997, 31.7, 31.6, 31.5, 31.4,
      31.299999999999997, 31.200000000000003, 31.1, 31.0, 30.9, 30.8, 30.7, 30.6,
      30.5, 30.4, 30.299999999999997, 30.2, 30.1, 30.0, 29.9, 29.799999999999997,
      29.7, 29.6, 29.5, 29.4, 29.3, 29.2, 29.1, 29.0, 28.9, 28.799999999999997,
      28.7, 28.6, 28.5, 28.4, 28.299999999999997, 28.2, 28.1, 28.0, 27.9, 27.8,
      27.7, 27.6, 27.5, 27.4, 27.3, 27.2, 27.1, 27.0, 26.9, 26.799999999999997,
      26.7, 26.6, 26.5, 26.4, 26.299999999999997, 26.2, 26.099999999999998, 26.0,
      25.9, 25.799999999999997, 25.7, 25.599999999999998, 25.5, 25.4, 25.3,
      25.200000000000003, 25.1, 25.0, 24.9, 24.8, 24.700000000000003, 24.6, 24.5,
      24.400000000000002, 24.300000000000004, 24.200000000000003, 24.1, 24.0,
      23.900000000000002, 23.8, 23.700000000000003, 23.6, 23.500000000000004,
      23.400000000000002, 23.3, 23.200000000000003, 23.1, 23.0,
      22.900000000000002, 22.8, 22.700000000000003, 22.6, 22.5,
      22.400000000000002, 22.3, 22.2, 22.1, 22.0, 21.900000000000002, 21.8, 21.7,
      21.6, 21.5, 21.4, 21.3, 21.2, 21.1, 21.0, 20.9, 20.8, 20.7,
      20.599999999999998, 20.5, 20.400000000000002, 20.3, 20.200000000000003,
      20.1, 20.0, 19.900000000000002, 19.8, 19.700000000000003, 19.6, 19.5,
      19.400000000000002, 19.3, 19.2, 19.1, 19.0, 18.900000000000002, 18.8,
      18.700000000000003, 18.6, 18.500000000000004, 18.400000000000002,
      18.300000000000004, 18.200000000000003, 18.1, 18.000000000000004,
      17.900000000000002, 17.800000000000004, 17.700000000000003, 17.6,
      17.500000000000004, 17.400000000000002, 17.3, 17.200000000000003, 17.1,
      17.0, 16.900000000000002, 16.8, 16.700000000000003, 16.6, 16.5,
      16.400000000000002, 16.3, 16.200000000000003, 16.1, 16.0,
      15.900000000000002, 15.8, 15.7, 15.600000000000001, 15.5,
      15.399999999999999, 15.3, 15.2, 15.100000000000001, 15.0,
      14.899999999999999, 14.8, 14.7, 14.600000000000001, 14.5,
      14.399999999999999, 14.3, 14.2, 14.099999999999998, 14.0,
      13.899999999999999, 13.799999999999997, 13.7, 13.599999999999998, 13.5,
      13.399999999999999, 13.299999999999997, 13.2, 13.099999999999998, 13.0,
      12.900000000000002, 12.800000000000004, 12.700000000000003,
      12.600000000000005, 12.500000000000004, 12.400000000000002,
      12.300000000000004, 12.200000000000003, 12.100000000000001,
      12.000000000000004, 11.900000000000002, 11.800000000000004,
      11.700000000000003, 11.600000000000001, 11.500000000000004,
      11.400000000000002, 11.3, 11.200000000000003, 11.100000000000001,
      11.000000000000004, 10.900000000000002, 10.8, 10.700000000000003,
      10.600000000000001, 10.5, 10.400000000000002, 10.3, 10.200000000000003,
      10.100000000000001, 10.0, 9.90025, 9.801, 9.70225, 9.604, 9.50625, 9.409,
      9.31225, 9.216, 9.12025, 9.025, 8.9302500000000009, 8.836,
      8.7422499999999985, 8.649, 8.55625, 8.464, 8.3722500000000011,
      8.2809999999999988, 8.1902499999999989, 8.1, 8.010250000000001,
      7.9210000000000012, 7.8322499999999993, 7.744, 7.65625, 7.5690000000000008,
      7.48225, 7.395999999999999, 7.31025, 7.225, 7.14025, 7.056,
      6.9722499999999989, 6.8889999999999993, 6.80625, 6.7239999999999993,
      6.6422500000000007, 6.5610000000000008, 6.48025, 6.4, 6.3202499999999988,
      6.2410000000000005, 6.16225, 6.0840000000000005, 6.00625, 5.929,
      5.8522500000000006, 5.7760000000000007, 5.7002500000000005, 5.625,
      5.5502499999999992, 5.476, 5.4022499999999996, 5.3289999999999988, 5.25625,
      5.184, 5.11225, 5.0409999999999986, 4.97025, 4.9, 4.8302499999999995,
      4.761, 4.6922500000000005, 4.6240000000000006, 4.55625, 4.489,
      4.4222499999999991, 4.356, 4.29025, 4.2249999999999988, 4.1602499999999987,
      4.0959999999999983, 4.03225, 3.9690000000000003, 3.90625,
      3.8439999999999994, 3.7822500000000003, 3.7209999999999992,
      3.6602499999999996, 3.5999999999999996, 3.5402500000000003, 3.481, 3.42225,
      3.363999999999999, 3.3062500000000004, 3.2489999999999988,
      3.1922499999999996, 3.136000000000001, 3.0802499999999995,
      3.0250000000000004, 2.97025, 2.9160000000000004, 2.8622499999999995,
      2.8089999999999993, 2.7562499999999996, 2.7040000000000006,
      2.6522499999999987, 2.6009999999999991, 2.55025, 2.5, 2.4502499999999987,
      2.401, 2.3522499999999997, 2.3040000000000003, 2.2562499999999996,
      2.2089999999999996, 2.1622499999999985, 2.1159999999999997,
      2.0702499999999979, 2.0249999999999986, 1.9802499999999998, 1.936,
      1.8922499999999989, 1.8490000000000002, 1.8062500000000004,
      1.7639999999999993, 1.7222499999999989, 1.6809999999999992, 1.64025,
      1.5999999999999996, 1.56025, 1.520999999999999, 1.4822500000000005,
      1.4439999999999991, 1.40625, 1.3689999999999998, 1.3322500000000002,
      1.2959999999999994, 1.260250000000001, 1.2249999999999996,
      1.1902499999999989, 1.1559999999999988, 1.1222499999999993,
      1.0890000000000004, 1.0562500000000004, 1.0239999999999991,
      0.9922500000000003, 0.96099999999999852, 0.93024999999999736,
      0.89999999999999858, 0.87024999999999864, 0.8409999999999993,
      0.81224999999999881, 0.78399999999999892, 0.75624999999999964,
      0.7289999999999992, 0.70224999999999937, 0.67600000000000016,
      0.65024999999999977, 0.62499999999999822, 0.60024999999999906,
      0.57599999999999874, 0.552249999999999, 0.52899999999999991,
      0.50624999999999787, 0.484, 0.46225000000000094, 0.44099999999999895,
      0.42025000000000112, 0.39999999999999858, 0.3802500000000002,
      0.36100000000000065, 0.34224999999999994, 0.32399999999999807,
      0.30624999999999858, 0.28899999999999793, 0.27224999999999966,
      0.25600000000000023, 0.24024999999999963, 0.22500000000000142,
      0.21024999999999849, 0.19599999999999795, 0.1822499999999998,
      0.16900000000000048, 0.15625, 0.14399999999999835, 0.13224999999999909,
      0.12099999999999866, 0.11024999999999707, 0.099999999999997868,
      0.0902499999999975, 0.080999999999999517, 0.072250000000000369,
      0.064000000000000057, 0.056249999999998579, 0.048999999999999488,
      0.042249999999999233, 0.035999999999997812, 0.030250000000002331,
      0.024999999999998579, 0.020249999999997215, 0.016000000000001791,
      0.012249999999998096, 0.0089999999999967883, 0.0062500000000014211,
      0.0039999999999977831, 0.0022500000000000853, 0.0010000000000012221,
      0.000249999999997641, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00025, 0.001, 0.00225, 0.004,
      0.0062499999999999995, 0.009, 0.01225, 0.016, 0.02025,
      0.024999999999999998, 0.030249999999999992, 0.036, 0.04225, 0.049,
      0.056249999999999994, 0.064, 0.072250000000000009, 0.081, 0.09025,
      0.099999999999999992, 0.11025000000000001, 0.12099999999999997,
      0.13224999999999998, 0.144, 0.15625, 0.169, 0.18225, 0.196,
      0.21025000000000002, 0.22499999999999998, 0.24025, 0.256, 0.27225,
      0.28900000000000003, 0.30625, 0.324, 0.34224999999999994, 0.361,
      0.38024999999999992, 0.39999999999999997, 0.42025000000000007,
      0.44100000000000006, 0.46225000000000005, 0.48399999999999987, 0.50625,
      0.52899999999999991, 0.55225, 0.576, 0.60025000000000006, 0.625,
      0.65025000000000011, 0.676, 0.70224999999999993, 0.729, 0.75625, 0.784,
      0.81225, 0.84100000000000008, 0.87025, 0.89999999999999991,
      0.93024999999999991, 0.961, 0.99224999999999985, 1.024, 1.05625, 1.089,
      1.12225, 1.1560000000000001, 1.19025, 1.225, 1.26025, 1.296,
      1.3322499999999997, 1.3689999999999998, 1.40625, 1.444, 1.4822499999999998,
      1.5209999999999997, 1.56025, 1.5999999999999999, 1.64025,
      1.6810000000000003, 1.72225, 1.7640000000000002, 1.80625,
      1.8490000000000002, 1.8922500000000002, 1.9359999999999995, 1.98025, 2.025,
      2.0702499999999997, 2.1159999999999997, 2.1622500000000002, 2.209,
      2.2562499999999996, 2.304, 2.35225, 2.4010000000000002, 2.45025, 2.5,
      2.5502500000000006, 2.6010000000000004, 2.6522500000000004, 2.704,
      2.7562500000000005, 2.8090000000000006, 2.8622500000000004, 2.916,
      2.9702500000000009, 3.0250000000000008, 3.0802500000000008,
      3.136000000000001, 3.19225, 3.249, 3.30625, 3.3640000000000003,
      3.4222500000000005, 3.481, 3.5402500000000003, 3.600000000000001,
      3.6602500000000009, 3.721, 3.7822500000000003, 3.8440000000000012, 3.90625,
      3.9689999999999994, 4.0322499999999994, 4.096, 4.1602500000000004, 4.225,
      4.29025, 4.356, 4.42225, 4.489, 4.55625, 4.6240000000000006,
      4.6922500000000005, 4.761, 4.83025, 4.9, 4.9702500000000009, 5.041,
      5.11225, 5.184, 5.2562500000000005, 5.3290000000000015, 5.4022500000000013,
      5.4760000000000018, 5.5502500000000019, 5.6250000000000009,
      5.7002500000000005, 5.7760000000000016, 5.8522500000000015,
      5.929000000000002, 6.0062500000000014, 6.0840000000000023, 6.16225, 6.241,
      6.3202500000000006, 6.3999999999999995, 6.4802500000000007, 6.561,
      6.6422500000000007, 6.7240000000000011, 6.80625, 6.889, 6.97225,
      7.0560000000000009, 7.1402500000000009, 7.225, 7.3102500000000008,
      7.3960000000000008, 7.4822500000000014, 7.5690000000000008,
      7.6562500000000009, 7.7440000000000007, 7.832250000000001,
      7.9210000000000012, 8.010250000000001, 8.1000000000000014, 8.19025,
      8.2809999999999988, 8.37225, 8.4639999999999986, 8.5562499999999986,
      8.6490000000000009, 8.7422499999999985, 8.836, 8.93025, 9.0249999999999986,
      9.12025, 9.216, 9.31225, 9.409, 9.50625, 9.604000000000001, 9.70225, 9.801,
      9.9002500000000015, 10.0, 10.1, 10.2, 10.3, 10.4, 10.5, 10.6, 10.7, 10.8,
      10.9, 11.0, 11.1, 11.2, 11.3, 11.4, 11.5, 11.6, 11.7, 11.8, 11.9, 12.0,
      12.1, 12.2, 12.3, 12.4, 12.5, 12.6, 12.7, 12.8, 12.9, 13.0, 13.1, 13.2,
      13.3, 13.4, 13.5, 13.600000000000001, 13.7, 13.8, 13.9, 14.0,
      14.100000000000001, 14.2, 14.3, 14.399999999999999, 14.5, 14.6, 14.7, 14.8,
      14.9, 15.0, 15.100000000000001, 15.2, 15.3, 15.4, 15.5, 15.600000000000001,
      15.7, 15.8, 15.9, 16.0, 16.1, 16.2, 16.3, 16.4, 16.5, 16.6, 16.7, 16.8,
      16.9, 17.0, 17.1, 17.200000000000003, 17.3, 17.4, 17.5, 17.6, 17.7, 17.8,
      17.9, 18.0, 18.1, 18.200000000000003, 18.3, 18.4, 18.5, 18.6,
      18.700000000000003, 18.799999999999997, 18.9, 19.0, 19.1, 19.2, 19.3, 19.4,
      19.5, 19.6, 19.700000000000003, 19.8, 19.9, 20.0, 20.1, 20.200000000000003,
      20.3, 20.4, 20.5, 20.6, 20.7, 20.8, 20.9, 21.0, 21.1, 21.200000000000003,
      21.3, 21.4, 21.5, 21.6, 21.700000000000003, 21.8, 21.9, 22.0, 22.1, 22.2,
      22.3, 22.4, 22.5, 22.6, 22.7, 22.8, 22.9, 23.0, 23.1, 23.200000000000003,
      23.3, 23.4, 23.5, 23.6, 23.700000000000003, 23.8, 23.900000000000002, 24.0,
      24.1, 24.200000000000003, 24.3, 24.400000000000002, 24.5, 24.6, 24.7,
      24.799999999999997, 24.9, 25.0, 25.1, 25.2, 25.299999999999997, 25.4, 25.5,
      25.599999999999998, 25.699999999999996, 25.799999999999997, 25.9, 26.0,
      26.099999999999998, 26.2, 26.299999999999997, 26.4, 26.499999999999996,
      26.599999999999998, 26.7, 26.799999999999997, 26.9, 27.0,
      27.099999999999998, 27.2, 27.299999999999997, 27.4, 27.5,
      27.599999999999998, 27.7, 27.8, 27.9, 28.0, 28.099999999999998, 28.2, 28.3,
      28.4, 28.5, 28.6, 28.7, 28.8, 28.9, 29.0, 29.1, 29.2, 29.3,
      29.400000000000002, 29.5, 29.599999999999998, 29.7, 29.799999999999997,
      29.9, 30.0, 30.099999999999998, 30.2, 30.299999999999997, 30.4, 30.5,
      30.599999999999998, 30.7, 30.8, 30.9, 31.0, 31.099999999999998, 31.2,
      31.299999999999997, 31.4, 31.499999999999996, 31.599999999999998,
      31.699999999999996, 31.799999999999997, 31.9, 31.999999999999996,
      32.099999999999994, 32.199999999999996, 32.3, 32.4, 32.5,
      32.599999999999994, 32.7, 32.8, 32.9, 33.0, 33.099999999999994, 33.2, 33.3,
      33.4, 33.5, 33.599999999999994, 33.7, 33.8, 33.9, 34.0, 34.099999999999994,
      34.2, 34.3, 34.4, 34.5, 34.6, 34.7, 34.8, 34.9, 35.0, 35.1, 35.2, 35.3,
      35.4, 35.5, 35.6, 35.7, 35.8, 35.900000000000006, 36.0, 36.1, 36.2, 36.3,
      36.400000000000006, 36.5, 36.6, 36.7, 36.8, 36.900000000000006, 37.0,
      37.099999999999994, 37.199999999999996, 37.3, 37.399999999999991, 37.5,
      37.599999999999994, 37.699999999999996, 37.8, 37.9, 38.0,
      38.099999999999994, 38.199999999999996, 38.3, 38.4, 38.5,
      38.599999999999994, 38.7, 38.8, 38.9, 39.0, 39.099999999999994, 39.2, 39.3,
      39.4, 39.5, 39.599999999999994, 39.7, 39.8, 39.9, 40.0, 40.09975, 40.199,
      40.29775, 40.396, 40.49375, 40.591, 40.68775, 40.784, 40.87975, 40.975,
      41.06975, 41.164, 41.25775, 41.351, 41.44375, 41.536, 41.62775, 41.719,
      41.80975, 41.9, 41.98975, 42.079, 42.16775, 42.256, 42.34375, 42.431,
      42.51775, 42.604, 42.689750000000004, 42.775, 42.85975, 42.944, 43.02775,
      43.111, 43.19375, 43.276, 43.35775, 43.439, 43.51975, 43.6, 43.67975,
      43.759, 43.83775, 43.916, 43.99375, 44.071, 44.14775, 44.224000000000004,
      44.29975, 44.375, 44.44975, 44.524, 44.59775, 44.671, 44.74375, 44.816,
      44.88775, 44.959, 45.02975, 45.1, 45.16975, 45.239, 45.30775, 45.376,
      45.44375, 45.511, 45.57775, 45.644, 45.70975, 45.775, 45.83975, 45.904,
      45.96775, 46.031, 46.09375, 46.156, 46.21775, 46.278999999999996, 46.33975,
      46.4, 46.45975, 46.519, 46.57775, 46.636, 46.69375, 46.751000000000005,
      46.80775, 46.864, 46.91975, 46.975, 47.02975, 47.084, 47.13775, 47.191,
      47.24375, 47.296, 47.347750000000005, 47.399, 47.44975, 47.5, 47.54975,
      47.599000000000004, 47.64775, 47.696, 47.74375, 47.791, 47.83775, 47.884,
      47.92975, 47.975, 48.01975, 48.064, 48.10775, 48.150999999999996, 48.19375,
      48.236000000000004, 48.27775, 48.319, 48.35975, 48.4, 48.439750000000004,
      48.479, 48.51775, 48.556, 48.59375, 48.631, 48.66775, 48.704, 48.73975,
      48.775, 48.80975, 48.844, 48.87775, 48.911, 48.94375, 48.976, 49.00775,
      49.039, 49.06975, 49.1, 49.12975, 49.159, 49.18775, 49.216,
      49.243750000000006, 49.271, 49.29775, 49.324, 49.34975, 49.375, 49.39975,
      49.424, 49.44775, 49.471000000000004, 49.493750000000006,
      49.516000000000005, 49.53775, 49.559, 49.579750000000004, 49.6,
      49.619749999999996, 49.638999999999996, 49.65775, 49.676, 49.69375, 49.711,
      49.72775, 49.744, 49.75975, 49.775, 49.78975, 49.804, 49.817750000000004,
      49.831, 49.84375, 49.856, 49.86775, 49.879, 49.88975, 49.900000000000006,
      49.90975, 49.919, 49.92775, 49.936, 49.94375, 49.951, 49.957750000000004,
      49.964, 49.96975, 49.975, 49.97975, 49.984, 49.98775, 49.991, 49.99375,
      49.996, 49.997749999999996, 49.999, 49.99975, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 49.99975, 49.999, 49.99775,
      49.996, 49.99375, 49.991, 49.98775, 49.984, 49.97975, 49.975, 49.96975,
      49.964, 49.95775, 49.951, 49.94375, 49.936, 49.92775, 49.919, 49.90975,
      49.9, 49.88975, 49.879, 49.86775, 49.856, 49.84375, 49.831, 49.81775,
      49.804, 49.78975, 49.775, 49.75975, 49.744, 49.72775, 49.711, 49.69375,
      49.676, 49.65775, 49.639, 49.61975, 49.6, 49.57975, 49.559, 49.53775,
      49.516, 49.49375, 49.471000000000004, 49.44775, 49.424, 49.39975, 49.375,
      49.34975, 49.324, 49.29775, 49.271, 49.24375, 49.216, 49.18775, 49.159,
      49.12975, 49.1, 49.06975, 49.039, 49.00775, 48.976, 48.94375, 48.911,
      48.87775, 48.844, 48.80975, 48.775, 48.73975, 48.704, 48.66775, 48.631,
      48.59375, 48.556, 48.51775, 48.479, 48.439750000000004, 48.4, 48.35975,
      48.319, 48.27775, 48.236, 48.19375, 48.150999999999996, 48.10775, 48.064,
      48.01975, 47.975, 47.92975, 47.884, 47.83775, 47.791, 47.74375, 47.696,
      47.64775, 47.599, 47.54975, 47.5, 47.44975, 47.399, 47.34775, 47.296,
      47.24375, 47.191, 47.13775, 47.084, 47.02975, 46.975, 46.91975, 46.864,
      46.80775, 46.751, 46.69375, 46.636, 46.57775, 46.519, 46.45975, 46.4,
      46.33975, 46.278999999999996, 46.21775, 46.156, 46.09375, 46.031, 45.96775,
      45.903999999999996, 45.83975, 45.775, 45.70975, 45.644, 45.57775, 45.511,
      45.44375, 45.376, 45.30775, 45.239, 45.16975, 45.1, 45.02975, 44.959,
      44.88775, 44.816, 44.74375, 44.671, 44.59775, 44.524, 44.449749999999995,
      44.375, 44.29975, 44.224, 44.14775, 44.071, 43.99375, 43.916, 43.83775,
      43.759, 43.67975, 43.6, 43.51975, 43.439, 43.357749999999996,
      43.275999999999996, 43.19375, 43.111, 43.02775, 42.944, 42.85975, 42.775,
      42.68975, 42.604, 42.51775, 42.431, 42.34375, 42.256, 42.16775, 42.079,
      41.98975, 41.9, 41.80975, 41.719, 41.62775, 41.536, 41.44375, 41.351,
      41.25775, 41.164, 41.06975, 40.975, 40.87975, 40.784, 40.68775, 40.591,
      40.49375, 40.396, 40.29775, 40.199, 40.09975, 40.0, 39.9, 39.8, 39.7, 39.6,
      39.5, 39.4, 39.3, 39.2, 39.1, 39.0, 38.9, 38.8, 38.7, 38.6, 38.5, 38.4,
      38.3, 38.2, 38.1, 38.0, 37.9, 37.8, 37.7, 37.6, 37.5, 37.4, 37.3, 37.2,
      37.1, 37.0, 36.9, 36.8, 36.7, 36.6, 36.5, 36.4, 36.3, 36.2, 36.1, 36.0,
      35.9, 35.8, 35.7, 35.6, 35.5, 35.4, 35.3, 35.2, 35.1, 35.0, 34.9, 34.8,
      34.7, 34.6, 34.5, 34.4, 34.3, 34.2, 34.1, 34.0, 33.9, 33.8, 33.7, 33.6,
      33.5, 33.4, 33.3, 33.2, 33.1, 33.0, 32.9, 32.8, 32.7, 32.6, 32.5, 32.4,
      32.3, 32.2, 32.1, 32.0, 31.9, 31.799999999999997, 31.7, 31.6, 31.5, 31.4,
      31.299999999999997, 31.200000000000003, 31.1, 31.0, 30.9, 30.8, 30.7, 30.6,
      30.5, 30.4, 30.299999999999997, 30.2, 30.1, 30.0, 29.9, 29.799999999999997,
      29.7, 29.6, 29.5, 29.4, 29.3, 29.2, 29.1, 29.0, 28.9, 28.799999999999997,
      28.7, 28.6, 28.5, 28.4, 28.299999999999997, 28.2, 28.1, 28.0, 27.9, 27.8,
      27.7, 27.6, 27.5, 27.4, 27.3, 27.2, 27.1, 27.0, 26.9, 26.799999999999997,
      26.7, 26.6, 26.5, 26.4, 26.299999999999997, 26.2, 26.099999999999998, 26.0,
      25.9, 25.799999999999997, 25.7, 25.599999999999998, 25.5, 25.4, 25.3,
      25.200000000000003, 25.1, 25.0, 24.9, 24.8, 24.700000000000003, 24.6, 24.5,
      24.400000000000002, 24.300000000000004, 24.200000000000003, 24.1, 24.0,
      23.900000000000002, 23.8, 23.700000000000003, 23.6, 23.500000000000004,
      23.400000000000002, 23.3, 23.200000000000003, 23.1, 23.0,
      22.900000000000002, 22.8, 22.700000000000003, 22.6, 22.5,
      22.400000000000002, 22.3, 22.2, 22.1, 22.0, 21.900000000000002, 21.8, 21.7,
      21.6, 21.5, 21.4, 21.3, 21.2, 21.1, 21.0, 20.9, 20.8, 20.7,
      20.599999999999998, 20.5, 20.400000000000002, 20.3, 20.200000000000003,
      20.1, 20.0, 19.900000000000002, 19.8, 19.700000000000003, 19.6, 19.5,
      19.400000000000002, 19.3, 19.2, 19.1, 19.0, 18.900000000000002, 18.8,
      18.700000000000003, 18.6, 18.500000000000004, 18.400000000000002,
      18.300000000000004, 18.200000000000003, 18.1, 18.000000000000004,
      17.900000000000002, 17.800000000000004, 17.700000000000003, 17.6,
      17.500000000000004, 17.400000000000002, 17.3, 17.200000000000003, 17.1,
      17.0, 16.900000000000002, 16.8, 16.700000000000003, 16.6, 16.5,
      16.400000000000002, 16.3, 16.200000000000003, 16.1, 16.0,
      15.900000000000002, 15.8, 15.7, 15.600000000000001, 15.5,
      15.399999999999999, 15.3, 15.2, 15.100000000000001, 15.0,
      14.899999999999999, 14.8, 14.7, 14.600000000000001, 14.5,
      14.399999999999999, 14.3, 14.2, 14.099999999999998, 14.0,
      13.899999999999999, 13.799999999999997, 13.7, 13.599999999999998, 13.5,
      13.399999999999999, 13.299999999999997, 13.2, 13.099999999999998, 13.0,
      12.900000000000002, 12.800000000000004, 12.700000000000003,
      12.600000000000005, 12.500000000000004, 12.400000000000002,
      12.300000000000004, 12.200000000000003, 12.100000000000001,
      12.000000000000004, 11.900000000000002, 11.800000000000004,
      11.700000000000003, 11.600000000000001, 11.500000000000004,
      11.400000000000002, 11.3, 11.200000000000003, 11.100000000000001,
      11.000000000000004, 10.900000000000002, 10.8, 10.700000000000003,
      10.600000000000001, 10.5, 10.400000000000002, 10.3, 10.200000000000003,
      10.100000000000001, 10.0, 9.90025, 9.801, 9.70225, 9.604, 9.50625, 9.409,
      9.31225, 9.216, 9.12025, 9.025, 8.9302500000000009, 8.836,
      8.7422499999999985, 8.649, 8.55625, 8.464, 8.3722500000000011,
      8.2809999999999988, 8.1902499999999989, 8.1, 8.010250000000001,
      7.9210000000000012, 7.8322499999999993, 7.744, 7.65625, 7.5690000000000008,
      7.48225, 7.395999999999999, 7.31025, 7.225, 7.14025, 7.056,
      6.9722499999999989, 6.8889999999999993, 6.80625, 6.7239999999999993,
      6.6422500000000007, 6.5610000000000008, 6.48025, 6.4, 6.3202499999999988,
      6.2410000000000005, 6.16225, 6.0840000000000005, 6.00625, 5.929,
      5.8522500000000006, 5.7760000000000007, 5.7002500000000005, 5.625,
      5.5502499999999992, 5.476, 5.4022499999999996, 5.3289999999999988, 5.25625,
      5.184, 5.11225, 5.0409999999999986, 4.97025, 4.9, 4.8302499999999995,
      4.761, 4.6922500000000005, 4.6240000000000006, 4.55625, 4.489,
      4.4222499999999991, 4.356, 4.29025, 4.2249999999999988, 4.1602499999999987,
      4.0959999999999983, 4.03225, 3.9690000000000003, 3.90625,
      3.8439999999999994, 3.7822500000000003, 3.7209999999999992,
      3.6602499999999996, 3.5999999999999996, 3.5402500000000003, 3.481, 3.42225,
      3.363999999999999, 3.3062500000000004, 3.2489999999999988,
      3.1922499999999996, 3.136000000000001, 3.0802499999999995,
      3.0250000000000004, 2.97025, 2.9160000000000004, 2.8622499999999995,
      2.8089999999999993, 2.7562499999999996, 2.7040000000000006,
      2.6522499999999987, 2.6009999999999991, 2.55025, 2.5, 2.4502499999999987,
      2.401, 2.3522499999999997, 2.3040000000000003, 2.2562499999999996,
      2.2089999999999996, 2.1622499999999985, 2.1159999999999997,
      2.0702499999999979, 2.0249999999999986, 1.9802499999999998, 1.936,
      1.8922499999999989, 1.8490000000000002, 1.8062500000000004,
      1.7639999999999993, 1.7222499999999989, 1.6809999999999992, 1.64025,
      1.5999999999999996, 1.56025, 1.520999999999999, 1.4822500000000005,
      1.4439999999999991, 1.40625, 1.3689999999999998, 1.3322500000000002,
      1.2959999999999994, 1.260250000000001, 1.2249999999999996,
      1.1902499999999989, 1.1559999999999988, 1.1222499999999993,
      1.0890000000000004, 1.0562500000000004, 1.0239999999999991,
      0.9922500000000003, 0.96099999999999852, 0.93024999999999736,
      0.89999999999999858, 0.87024999999999864, 0.8409999999999993,
      0.81224999999999881, 0.78399999999999892, 0.75624999999999964,
      0.7289999999999992, 0.70224999999999937, 0.67600000000000016,
      0.65024999999999977, 0.62499999999999822, 0.60024999999999906,
      0.57599999999999874, 0.552249999999999, 0.52899999999999991,
      0.50624999999999787, 0.484, 0.46225000000000094, 0.44099999999999895,
      0.42025000000000112, 0.39999999999999858, 0.3802500000000002,
      0.36100000000000065, 0.34224999999999994, 0.32399999999999807,
      0.30624999999999858, 0.28899999999999793, 0.27224999999999966,
      0.25600000000000023, 0.24024999999999963, 0.22500000000000142,
      0.21024999999999849, 0.19599999999999795, 0.1822499999999998,
      0.16900000000000048, 0.15625, 0.14399999999999835, 0.13224999999999909,
      0.12099999999999866, 0.11024999999999707, 0.099999999999997868,
      0.0902499999999975, 0.080999999999999517, 0.072250000000000369,
      0.064000000000000057, 0.056249999999998579, 0.048999999999999488,
      0.042249999999999233, 0.035999999999997812, 0.030250000000002331,
      0.024999999999998579, 0.020249999999997215, 0.016000000000001791,
      0.012249999999998096, 0.0089999999999967883, 0.0062500000000014211,
      0.0039999999999977831, 0.0022500000000000853, 0.0010000000000012221,
      0.000249999999997641, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0 } ;

    notch_doubleLeadRT_DW.FromWorkspace_PWORK.TimePtr = (void *) pTimeValues0;
    notch_doubleLeadRT_DW.FromWorkspace_PWORK.DataPtr = (void *) pDataValues0;
    notch_doubleLeadRT_DW.FromWorkspace_IWORK.PrevIndex = 0;
  }

  /* Start for Scope: '<Root>/uSim' */
  {
    RTWLogSignalInfo rt_ScopeSignalInfo;
    static int_T rt_ScopeSignalWidths[] = { 1 };

    static int_T rt_ScopeSignalNumDimensions[] = { 1 };

    static int_T rt_ScopeSignalDimensions[] = { 1 };

    static void *rt_ScopeCurrSigDims[] = { (NULL) };

    static int_T rt_ScopeCurrSigDimsSize[] = { 4 };

    static const char_T *rt_ScopeSignalLabels[] = { "" };

    static char_T rt_ScopeSignalTitles[] = "";
    static int_T rt_ScopeSignalTitleLengths[] = { 0 };

    static boolean_T rt_ScopeSignalIsVarDims[] = { 0 };

    static int_T rt_ScopeSignalPlotStyles[] = { 0 };

    BuiltInDTypeId dTypes[1] = { SS_DOUBLE };

    static char_T rt_ScopeBlockName[] = "notch_doubleLeadRT/uSim";
    rt_ScopeSignalInfo.numSignals = 1;
    rt_ScopeSignalInfo.numCols = rt_ScopeSignalWidths;
    rt_ScopeSignalInfo.numDims = rt_ScopeSignalNumDimensions;
    rt_ScopeSignalInfo.dims = rt_ScopeSignalDimensions;
    rt_ScopeSignalInfo.isVarDims = rt_ScopeSignalIsVarDims;
    rt_ScopeSignalInfo.currSigDims = rt_ScopeCurrSigDims;
    rt_ScopeSignalInfo.currSigDimsSize = rt_ScopeCurrSigDimsSize;
    rt_ScopeSignalInfo.dataTypes = dTypes;
    rt_ScopeSignalInfo.complexSignals = (NULL);
    rt_ScopeSignalInfo.frameData = (NULL);
    rt_ScopeSignalInfo.labels.cptr = rt_ScopeSignalLabels;
    rt_ScopeSignalInfo.titles = rt_ScopeSignalTitles;
    rt_ScopeSignalInfo.titleLengths = rt_ScopeSignalTitleLengths;
    rt_ScopeSignalInfo.plotStyles = rt_ScopeSignalPlotStyles;
    rt_ScopeSignalInfo.blockNames.cptr = (NULL);
    rt_ScopeSignalInfo.stateNames.cptr = (NULL);
    rt_ScopeSignalInfo.crossMdlRef = (NULL);
    rt_ScopeSignalInfo.dataTypeConvert = (NULL);
    notch_doubleLeadRT_DW.uSim_PWORK.LoggedData = rt_CreateStructLogVar(
      notch_doubleLeadRT_M->rtwLogInfo,
      0.0,
      rtmGetTFinal(notch_doubleLeadRT_M),
      notch_doubleLeadRT_M->Timing.stepSize0,
      (&rtmGetErrorStatus(notch_doubleLeadRT_M)),
      "uSim",
      1,
      0,
      1,
      0.001,
      &rt_ScopeSignalInfo,
      rt_ScopeBlockName);
    if (notch_doubleLeadRT_DW.uSim_PWORK.LoggedData == (NULL))
      return;
  }

  /* Start for Scope: '<Root>/x1sim' */
  {
    RTWLogSignalInfo rt_ScopeSignalInfo;
    static int_T rt_ScopeSignalWidths[] = { 1 };

    static int_T rt_ScopeSignalNumDimensions[] = { 1 };

    static int_T rt_ScopeSignalDimensions[] = { 1 };

    static void *rt_ScopeCurrSigDims[] = { (NULL) };

    static int_T rt_ScopeCurrSigDimsSize[] = { 4 };

    static const char_T *rt_ScopeSignalLabels[] = { "" };

    static char_T rt_ScopeSignalTitles[] = "";
    static int_T rt_ScopeSignalTitleLengths[] = { 0 };

    static boolean_T rt_ScopeSignalIsVarDims[] = { 0 };

    static int_T rt_ScopeSignalPlotStyles[] = { 1 };

    BuiltInDTypeId dTypes[1] = { SS_DOUBLE };

    static char_T rt_ScopeBlockName[] = "notch_doubleLeadRT/x1sim";
    rt_ScopeSignalInfo.numSignals = 1;
    rt_ScopeSignalInfo.numCols = rt_ScopeSignalWidths;
    rt_ScopeSignalInfo.numDims = rt_ScopeSignalNumDimensions;
    rt_ScopeSignalInfo.dims = rt_ScopeSignalDimensions;
    rt_ScopeSignalInfo.isVarDims = rt_ScopeSignalIsVarDims;
    rt_ScopeSignalInfo.currSigDims = rt_ScopeCurrSigDims;
    rt_ScopeSignalInfo.currSigDimsSize = rt_ScopeCurrSigDimsSize;
    rt_ScopeSignalInfo.dataTypes = dTypes;
    rt_ScopeSignalInfo.complexSignals = (NULL);
    rt_ScopeSignalInfo.frameData = (NULL);
    rt_ScopeSignalInfo.labels.cptr = rt_ScopeSignalLabels;
    rt_ScopeSignalInfo.titles = rt_ScopeSignalTitles;
    rt_ScopeSignalInfo.titleLengths = rt_ScopeSignalTitleLengths;
    rt_ScopeSignalInfo.plotStyles = rt_ScopeSignalPlotStyles;
    rt_ScopeSignalInfo.blockNames.cptr = (NULL);
    rt_ScopeSignalInfo.stateNames.cptr = (NULL);
    rt_ScopeSignalInfo.crossMdlRef = (NULL);
    rt_ScopeSignalInfo.dataTypeConvert = (NULL);
    notch_doubleLeadRT_DW.x1sim_PWORK.LoggedData = rt_CreateStructLogVar(
      notch_doubleLeadRT_M->rtwLogInfo,
      0.0,
      rtmGetTFinal(notch_doubleLeadRT_M),
      notch_doubleLeadRT_M->Timing.stepSize0,
      (&rtmGetErrorStatus(notch_doubleLeadRT_M)),
      "x1Sim",
      1,
      0,
      1,
      0.001,
      &rt_ScopeSignalInfo,
      rt_ScopeBlockName);
    if (notch_doubleLeadRT_DW.x1sim_PWORK.LoggedData == (NULL))
      return;
  }

  /* Start for Scope: '<Root>/x2sim ' */
  {
    RTWLogSignalInfo rt_ScopeSignalInfo;
    static int_T rt_ScopeSignalWidths[] = { 1 };

    static int_T rt_ScopeSignalNumDimensions[] = { 1 };

    static int_T rt_ScopeSignalDimensions[] = { 1 };

    static void *rt_ScopeCurrSigDims[] = { (NULL) };

    static int_T rt_ScopeCurrSigDimsSize[] = { 4 };

    static const char_T *rt_ScopeSignalLabels[] = { "" };

    static char_T rt_ScopeSignalTitles[] = "";
    static int_T rt_ScopeSignalTitleLengths[] = { 0 };

    static boolean_T rt_ScopeSignalIsVarDims[] = { 0 };

    static int_T rt_ScopeSignalPlotStyles[] = { 1 };

    BuiltInDTypeId dTypes[1] = { SS_DOUBLE };

    static char_T rt_ScopeBlockName[] = "notch_doubleLeadRT/x2sim\n";
    rt_ScopeSignalInfo.numSignals = 1;
    rt_ScopeSignalInfo.numCols = rt_ScopeSignalWidths;
    rt_ScopeSignalInfo.numDims = rt_ScopeSignalNumDimensions;
    rt_ScopeSignalInfo.dims = rt_ScopeSignalDimensions;
    rt_ScopeSignalInfo.isVarDims = rt_ScopeSignalIsVarDims;
    rt_ScopeSignalInfo.currSigDims = rt_ScopeCurrSigDims;
    rt_ScopeSignalInfo.currSigDimsSize = rt_ScopeCurrSigDimsSize;
    rt_ScopeSignalInfo.dataTypes = dTypes;
    rt_ScopeSignalInfo.complexSignals = (NULL);
    rt_ScopeSignalInfo.frameData = (NULL);
    rt_ScopeSignalInfo.labels.cptr = rt_ScopeSignalLabels;
    rt_ScopeSignalInfo.titles = rt_ScopeSignalTitles;
    rt_ScopeSignalInfo.titleLengths = rt_ScopeSignalTitleLengths;
    rt_ScopeSignalInfo.plotStyles = rt_ScopeSignalPlotStyles;
    rt_ScopeSignalInfo.blockNames.cptr = (NULL);
    rt_ScopeSignalInfo.stateNames.cptr = (NULL);
    rt_ScopeSignalInfo.crossMdlRef = (NULL);
    rt_ScopeSignalInfo.dataTypeConvert = (NULL);
    notch_doubleLeadRT_DW.x2sim_PWORK.LoggedData = rt_CreateStructLogVar(
      notch_doubleLeadRT_M->rtwLogInfo,
      0.0,
      rtmGetTFinal(notch_doubleLeadRT_M),
      notch_doubleLeadRT_M->Timing.stepSize0,
      (&rtmGetErrorStatus(notch_doubleLeadRT_M)),
      "x2Sim",
      1,
      0,
      1,
      0.001,
      &rt_ScopeSignalInfo,
      rt_ScopeBlockName);
    if (notch_doubleLeadRT_DW.x2sim_PWORK.LoggedData == (NULL))
      return;
  }

  /* Start for Scope: '<Root>/xrSim' */
  {
    RTWLogSignalInfo rt_ScopeSignalInfo;
    static int_T rt_ScopeSignalWidths[] = { 1 };

    static int_T rt_ScopeSignalNumDimensions[] = { 1 };

    static int_T rt_ScopeSignalDimensions[] = { 1 };

    static void *rt_ScopeCurrSigDims[] = { (NULL) };

    static int_T rt_ScopeCurrSigDimsSize[] = { 4 };

    static const char_T *rt_ScopeSignalLabels[] = { "" };

    static char_T rt_ScopeSignalTitles[] = "";
    static int_T rt_ScopeSignalTitleLengths[] = { 0 };

    static boolean_T rt_ScopeSignalIsVarDims[] = { 0 };

    static int_T rt_ScopeSignalPlotStyles[] = { 0 };

    BuiltInDTypeId dTypes[1] = { SS_DOUBLE };

    static char_T rt_ScopeBlockName[] = "notch_doubleLeadRT/xrSim";
    rt_ScopeSignalInfo.numSignals = 1;
    rt_ScopeSignalInfo.numCols = rt_ScopeSignalWidths;
    rt_ScopeSignalInfo.numDims = rt_ScopeSignalNumDimensions;
    rt_ScopeSignalInfo.dims = rt_ScopeSignalDimensions;
    rt_ScopeSignalInfo.isVarDims = rt_ScopeSignalIsVarDims;
    rt_ScopeSignalInfo.currSigDims = rt_ScopeCurrSigDims;
    rt_ScopeSignalInfo.currSigDimsSize = rt_ScopeCurrSigDimsSize;
    rt_ScopeSignalInfo.dataTypes = dTypes;
    rt_ScopeSignalInfo.complexSignals = (NULL);
    rt_ScopeSignalInfo.frameData = (NULL);
    rt_ScopeSignalInfo.labels.cptr = rt_ScopeSignalLabels;
    rt_ScopeSignalInfo.titles = rt_ScopeSignalTitles;
    rt_ScopeSignalInfo.titleLengths = rt_ScopeSignalTitleLengths;
    rt_ScopeSignalInfo.plotStyles = rt_ScopeSignalPlotStyles;
    rt_ScopeSignalInfo.blockNames.cptr = (NULL);
    rt_ScopeSignalInfo.stateNames.cptr = (NULL);
    rt_ScopeSignalInfo.crossMdlRef = (NULL);
    rt_ScopeSignalInfo.dataTypeConvert = (NULL);
    notch_doubleLeadRT_DW.xrSim_PWORK.LoggedData = rt_CreateStructLogVar(
      notch_doubleLeadRT_M->rtwLogInfo,
      0.0,
      rtmGetTFinal(notch_doubleLeadRT_M),
      notch_doubleLeadRT_M->Timing.stepSize0,
      (&rtmGetErrorStatus(notch_doubleLeadRT_M)),
      "xrSim",
      1,
      0,
      1,
      0.001,
      &rt_ScopeSignalInfo,
      rt_ScopeBlockName);
    if (notch_doubleLeadRT_DW.xrSim_PWORK.LoggedData == (NULL))
      return;
  }

  /* InitializeConditions for TransferFcn: '<Root>/lead Filter' */
  notch_doubleLeadRT_X.leadFilter_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/lead Filter1' */
  notch_doubleLeadRT_X.leadFilter1_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/notch Filter' */
  notch_doubleLeadRT_X.notchFilter_CSTATE[0] = 0.0;
  notch_doubleLeadRT_X.notchFilter_CSTATE[1] = 0.0;
}

/* Model terminate function */
void notch_doubleLeadRT_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<S1>/HIL Initialize1' */

  /* S-Function Block: notch_doubleLeadRT/Quarc_Plant/HIL Initialize1 (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    static const t_uint analog_output_channels[2U] = {
      0
      , 1
    };

    hil_task_stop_all(notch_doubleLeadRT_DW.HILInitialize1_Card);
    hil_monitor_stop_all(notch_doubleLeadRT_DW.HILInitialize1_Card);
    is_switching = false;
    if ((notch_doubleLeadRT_P.HILInitialize1_set_analog_out_d && !is_switching) ||
        (notch_doubleLeadRT_P.HILInitialize1_set_analog_outpu && is_switching))
    {
      notch_doubleLeadRT_DW.HILInitialize1_AOVoltages[0] =
        notch_doubleLeadRT_P.HILInitialize1_final_analog_out;
      notch_doubleLeadRT_DW.HILInitialize1_AOVoltages[1] =
        notch_doubleLeadRT_P.HILInitialize1_final_analog_out;
      num_final_analog_outputs = 2U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(notch_doubleLeadRT_DW.HILInitialize1_Card,
        analog_output_channels, num_final_analog_outputs,
        &notch_doubleLeadRT_DW.HILInitialize1_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(notch_doubleLeadRT_M, _rt_error_message);
      }
    }

    hil_task_delete_all(notch_doubleLeadRT_DW.HILInitialize1_Card);
    hil_monitor_delete_all(notch_doubleLeadRT_DW.HILInitialize1_Card);
    hil_close(notch_doubleLeadRT_DW.HILInitialize1_Card);
    notch_doubleLeadRT_DW.HILInitialize1_Card = NULL;
  }
}
