/*
 * notch_doubleLeadRT_types.h
 *
 * Code generation for model "notch_doubleLeadRT".
 *
 * Model version              : 1.18
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Mon Mar 06 10:18:13 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_notch_doubleLeadRT_types_h_
#define RTW_HEADER_notch_doubleLeadRT_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_notch_doubleLeadRT_T_ P_notch_doubleLeadRT_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_notch_doubleLeadRT_T RT_MODEL_notch_doubleLeadRT_T;

#endif                                 /* RTW_HEADER_notch_doubleLeadRT_types_h_ */
