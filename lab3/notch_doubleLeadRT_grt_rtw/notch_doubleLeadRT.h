/*
 * notch_doubleLeadRT.h
 *
 * Code generation for model "notch_doubleLeadRT".
 *
 * Model version              : 1.18
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Mon Mar 06 10:18:13 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_notch_doubleLeadRT_h_
#define RTW_HEADER_notch_doubleLeadRT_h_
#include <math.h>
#include <stddef.h>
#include <float.h>
#include <string.h>
#ifndef notch_doubleLeadRT_COMMON_INCLUDES_
# define notch_doubleLeadRT_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_logging.h"
#include "hil.h"
#include "quanser_messages.h"
#include "quanser_extern.h"
#endif                                 /* notch_doubleLeadRT_COMMON_INCLUDES_ */

#include "notch_doubleLeadRT_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetBlkStateChangeFlag
# define rtmGetBlkStateChangeFlag(rtm) ((rtm)->ModelData.blkStateChange)
#endif

#ifndef rtmSetBlkStateChangeFlag
# define rtmSetBlkStateChangeFlag(rtm, val) ((rtm)->ModelData.blkStateChange = (val))
#endif

#ifndef rtmGetContStateDisabled
# define rtmGetContStateDisabled(rtm)  ((rtm)->ModelData.contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
# define rtmSetContStateDisabled(rtm, val) ((rtm)->ModelData.contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
# define rtmGetContStates(rtm)         ((rtm)->ModelData.contStates)
#endif

#ifndef rtmSetContStates
# define rtmSetContStates(rtm, val)    ((rtm)->ModelData.contStates = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
# define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->ModelData.derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
# define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->ModelData.derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetIntgData
# define rtmGetIntgData(rtm)           ((rtm)->ModelData.intgData)
#endif

#ifndef rtmSetIntgData
# define rtmSetIntgData(rtm, val)      ((rtm)->ModelData.intgData = (val))
#endif

#ifndef rtmGetOdeF
# define rtmGetOdeF(rtm)               ((rtm)->ModelData.odeF)
#endif

#ifndef rtmSetOdeF
# define rtmSetOdeF(rtm, val)          ((rtm)->ModelData.odeF = (val))
#endif

#ifndef rtmGetPeriodicContStateIndices
# define rtmGetPeriodicContStateIndices(rtm) ((rtm)->ModelData.periodicContStateIndices)
#endif

#ifndef rtmSetPeriodicContStateIndices
# define rtmSetPeriodicContStateIndices(rtm, val) ((rtm)->ModelData.periodicContStateIndices = (val))
#endif

#ifndef rtmGetPeriodicContStateRanges
# define rtmGetPeriodicContStateRanges(rtm) ((rtm)->ModelData.periodicContStateRanges)
#endif

#ifndef rtmSetPeriodicContStateRanges
# define rtmSetPeriodicContStateRanges(rtm, val) ((rtm)->ModelData.periodicContStateRanges = (val))
#endif

#ifndef rtmGetRTWLogInfo
# define rtmGetRTWLogInfo(rtm)         ((rtm)->rtwLogInfo)
#endif

#ifndef rtmGetZCCacheNeedsReset
# define rtmGetZCCacheNeedsReset(rtm)  ((rtm)->ModelData.zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
# define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->ModelData.zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetdX
# define rtmGetdX(rtm)                 ((rtm)->ModelData.derivs)
#endif

#ifndef rtmSetdX
# define rtmSetdX(rtm, val)            ((rtm)->ModelData.derivs = (val))
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T CartEncodermmcounts;          /* '<S1>/Cart Encoder (mm//counts)' */
  real_T FromWorkspace;                /* '<Root>/From Workspace' */
  real_T Gain;                         /* '<Root>/Gain' */
  real_T leadFilter;                   /* '<Root>/lead Filter' */
  real_T leadFilter1;                  /* '<Root>/lead Filter1' */
  real_T notchFilter;                  /* '<Root>/notch Filter' */
} B_notch_doubleLeadRT_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T HILInitialize1_AOVoltages[2]; /* '<S1>/HIL Initialize1' */
  t_card HILInitialize1_Card;          /* '<S1>/HIL Initialize1' */
  t_task HILReadEncoderTimebase_Task;  /* '<S1>/HIL Read Encoder Timebase' */
  struct {
    void *TimePtr;
    void *DataPtr;
    void *RSimInfoPtr;
  } FromWorkspace_PWORK;               /* '<Root>/From Workspace' */

  void *HILWriteAnalog_PWORK;          /* '<S1>/HIL Write Analog' */
  struct {
    void *LoggedData;
  } uSim_PWORK;                        /* '<Root>/uSim' */

  struct {
    void *LoggedData;
  } x1sim_PWORK;                       /* '<Root>/x1sim' */

  struct {
    void *LoggedData;
  } x2sim_PWORK;                       /* '<Root>/x2sim ' */

  struct {
    void *LoggedData;
  } xrSim_PWORK;                       /* '<Root>/xrSim' */

  int32_T HILInitialize1_ClockModes[3];/* '<S1>/HIL Initialize1' */
  int32_T HILReadEncoderTimebase_Buffer[2];/* '<S1>/HIL Read Encoder Timebase' */
  struct {
    int_T PrevIndex;
  } FromWorkspace_IWORK;               /* '<Root>/From Workspace' */
} DW_notch_doubleLeadRT_T;

/* Continuous states (auto storage) */
typedef struct {
  real_T leadFilter_CSTATE;            /* '<Root>/lead Filter' */
  real_T leadFilter1_CSTATE;           /* '<Root>/lead Filter1' */
  real_T notchFilter_CSTATE[2];        /* '<Root>/notch Filter' */
} X_notch_doubleLeadRT_T;

/* State derivatives (auto storage) */
typedef struct {
  real_T leadFilter_CSTATE;            /* '<Root>/lead Filter' */
  real_T leadFilter1_CSTATE;           /* '<Root>/lead Filter1' */
  real_T notchFilter_CSTATE[2];        /* '<Root>/notch Filter' */
} XDot_notch_doubleLeadRT_T;

/* State disabled  */
typedef struct {
  boolean_T leadFilter_CSTATE;         /* '<Root>/lead Filter' */
  boolean_T leadFilter1_CSTATE;        /* '<Root>/lead Filter1' */
  boolean_T notchFilter_CSTATE[2];     /* '<Root>/notch Filter' */
} XDis_notch_doubleLeadRT_T;

#ifndef ODE1_INTG
#define ODE1_INTG

/* ODE1 Integration Data */
typedef struct {
  real_T *f[1];                        /* derivatives */
} ODE1_IntgData;

#endif

/* Parameters (auto storage) */
struct P_notch_doubleLeadRT_T_ {
  real_T Kp;                           /* Variable: Kp
                                        * Referenced by: '<Root>/Gain'
                                        */
  real_T HILInitialize1_final_analog_out;/* Mask Parameter: HILInitialize1_final_analog_out
                                          * Referenced by: '<S1>/HIL Initialize1'
                                          */
  real_T HILInitialize1_final_pwm_output;/* Mask Parameter: HILInitialize1_final_pwm_output
                                          * Referenced by: '<S1>/HIL Initialize1'
                                          */
  real_T HILInitialize1_set_other_output;/* Mask Parameter: HILInitialize1_set_other_output
                                          * Referenced by: '<S1>/HIL Initialize1'
                                          */
  real_T HILInitialize1_set_other_outp_f;/* Mask Parameter: HILInitialize1_set_other_outp_f
                                          * Referenced by: '<S1>/HIL Initialize1'
                                          */
  int32_T HILReadEncoderTimebase_clock;/* Mask Parameter: HILReadEncoderTimebase_clock
                                        * Referenced by: '<S1>/HIL Read Encoder Timebase'
                                        */
  uint32_T HILReadEncoderTimebase_channels[2];/* Mask Parameter: HILReadEncoderTimebase_channels
                                               * Referenced by: '<S1>/HIL Read Encoder Timebase'
                                               */
  uint32_T HILWriteAnalog_channels;    /* Mask Parameter: HILWriteAnalog_channels
                                        * Referenced by: '<S1>/HIL Write Analog'
                                        */
  uint32_T HILReadEncoderTimebase_samples_;/* Mask Parameter: HILReadEncoderTimebase_samples_
                                            * Referenced by: '<S1>/HIL Read Encoder Timebase'
                                            */
  boolean_T HILInitialize1_active;     /* Mask Parameter: HILInitialize1_active
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  boolean_T HILInitialize1_final_digital_ou;/* Mask Parameter: HILInitialize1_final_digital_ou
                                             * Referenced by: '<S1>/HIL Initialize1'
                                             */
  boolean_T HILInitialize1_set_analog_outpu;/* Mask Parameter: HILInitialize1_set_analog_outpu
                                             * Referenced by: '<S1>/HIL Initialize1'
                                             */
  boolean_T HILInitialize1_set_analog_out_d;/* Mask Parameter: HILInitialize1_set_analog_out_d
                                             * Referenced by: '<S1>/HIL Initialize1'
                                             */
  boolean_T HILInitialize1_set_digital_outp;/* Mask Parameter: HILInitialize1_set_digital_outp
                                             * Referenced by: '<S1>/HIL Initialize1'
                                             */
  boolean_T HILInitialize1_set_digital_ou_p;/* Mask Parameter: HILInitialize1_set_digital_ou_p
                                             * Referenced by: '<S1>/HIL Initialize1'
                                             */
  boolean_T HILInitialize1_set_pwm_outputs_;/* Mask Parameter: HILInitialize1_set_pwm_outputs_
                                             * Referenced by: '<S1>/HIL Initialize1'
                                             */
  boolean_T HILInitialize1_set_pwm_output_e;/* Mask Parameter: HILInitialize1_set_pwm_output_e
                                             * Referenced by: '<S1>/HIL Initialize1'
                                             */
  real_T CartEncodermmcounts_Gain;     /* Expression: 1.664*56*(1/20000)
                                        * Referenced by: '<S1>/Cart Encoder (mm//counts)'
                                        */
  real_T CartEncoder2mmcounts_Gain;    /* Expression: 1.664*56*(1/20000)
                                        * Referenced by: '<S1>/Cart Encoder 2 (mm//counts)'
                                        */
  real_T leadFilter_A;                 /* Computed Parameter: leadFilter_A
                                        * Referenced by: '<Root>/lead Filter'
                                        */
  real_T leadFilter_C;                 /* Computed Parameter: leadFilter_C
                                        * Referenced by: '<Root>/lead Filter'
                                        */
  real_T leadFilter_D;                 /* Computed Parameter: leadFilter_D
                                        * Referenced by: '<Root>/lead Filter'
                                        */
  real_T leadFilter1_A;                /* Computed Parameter: leadFilter1_A
                                        * Referenced by: '<Root>/lead Filter1'
                                        */
  real_T leadFilter1_C;                /* Computed Parameter: leadFilter1_C
                                        * Referenced by: '<Root>/lead Filter1'
                                        */
  real_T leadFilter1_D;                /* Computed Parameter: leadFilter1_D
                                        * Referenced by: '<Root>/lead Filter1'
                                        */
  real_T notchFilter_A[2];             /* Computed Parameter: notchFilter_A
                                        * Referenced by: '<Root>/notch Filter'
                                        */
  real_T notchFilter_C[2];             /* Computed Parameter: notchFilter_C
                                        * Referenced by: '<Root>/notch Filter'
                                        */
  real_T notchFilter_D;                /* Computed Parameter: notchFilter_D
                                        * Referenced by: '<Root>/notch Filter'
                                        */
  boolean_T HILReadEncoderTimebase_Active;/* Computed Parameter: HILReadEncoderTimebase_Active
                                           * Referenced by: '<S1>/HIL Read Encoder Timebase'
                                           */
  boolean_T HILWriteAnalog_Active;     /* Computed Parameter: HILWriteAnalog_Active
                                        * Referenced by: '<S1>/HIL Write Analog'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_notch_doubleLeadRT_T {
  const char_T *errorStatus;
  RTWLogInfo *rtwLogInfo;
  RTWSolverInfo solverInfo;

  /*
   * ModelData:
   * The following substructure contains information regarding
   * the data used in the model.
   */
  struct {
    X_notch_doubleLeadRT_T *contStates;
    int_T *periodicContStateIndices;
    real_T *periodicContStateRanges;
    real_T *derivs;
    boolean_T *contStateDisabled;
    boolean_T zCCacheNeedsReset;
    boolean_T derivCacheNeedsReset;
    boolean_T blkStateChange;
    real_T odeF[1][4];
    ODE1_IntgData intgData;
  } ModelData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    int_T numContStates;
    int_T numPeriodicContStates;
    int_T numSampTimes;
  } Sizes;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    time_T tFinal;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

/* Block parameters (auto storage) */
extern P_notch_doubleLeadRT_T notch_doubleLeadRT_P;

/* Block signals (auto storage) */
extern B_notch_doubleLeadRT_T notch_doubleLeadRT_B;

/* Continuous states (auto storage) */
extern X_notch_doubleLeadRT_T notch_doubleLeadRT_X;

/* Block states (auto storage) */
extern DW_notch_doubleLeadRT_T notch_doubleLeadRT_DW;

/* Model entry point functions */
extern void notch_doubleLeadRT_initialize(void);
extern void notch_doubleLeadRT_step(void);
extern void notch_doubleLeadRT_terminate(void);

/* Real-time Model object */
extern RT_MODEL_notch_doubleLeadRT_T *const notch_doubleLeadRT_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'notch_doubleLeadRT'
 * '<S1>'   : 'notch_doubleLeadRT/Quarc_Plant'
 */
#endif                                 /* RTW_HEADER_notch_doubleLeadRT_h_ */
