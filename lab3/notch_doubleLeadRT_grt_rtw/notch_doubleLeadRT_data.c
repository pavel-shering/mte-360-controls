/*
 * notch_doubleLeadRT_data.c
 *
 * Code generation for model "notch_doubleLeadRT".
 *
 * Model version              : 1.18
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Mon Mar 06 10:18:13 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "notch_doubleLeadRT.h"
#include "notch_doubleLeadRT_private.h"

/* Block parameters (auto storage) */
P_notch_doubleLeadRT_T notch_doubleLeadRT_P = {
  14.660843293981685,                  /* Variable: Kp
                                        * Referenced by: '<Root>/Gain'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_final_analog_out
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_final_pwm_output
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_set_other_output
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_set_other_outp_f
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILReadEncoderTimebase_clock
                                        * Referenced by: '<S1>/HIL Read Encoder Timebase'
                                        */

  /*  Mask Parameter: HILReadEncoderTimebase_channels
   * Referenced by: '<S1>/HIL Read Encoder Timebase'
   */
  { 0U, 1U },
  0U,                                  /* Mask Parameter: HILWriteAnalog_channels
                                        * Referenced by: '<S1>/HIL Write Analog'
                                        */
  1000U,                               /* Mask Parameter: HILReadEncoderTimebase_samples_
                                        * Referenced by: '<S1>/HIL Read Encoder Timebase'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_active
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_final_digital_ou
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_analog_outpu
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_analog_out_d
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_digital_outp
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_digital_ou_p
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_pwm_outputs_
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_pwm_output_e
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0046592000000000005,               /* Expression: 1.664*56*(1/20000)
                                        * Referenced by: '<S1>/Cart Encoder (mm//counts)'
                                        */
  0.0046592000000000005,               /* Expression: 1.664*56*(1/20000)
                                        * Referenced by: '<S1>/Cart Encoder 2 (mm//counts)'
                                        */
  -76.71423378,                        /* Computed Parameter: leadFilter_A
                                        * Referenced by: '<Root>/lead Filter'
                                        */
  -68.56711467,                        /* Computed Parameter: leadFilter_C
                                        * Referenced by: '<Root>/lead Filter'
                                        */
  1.0,                                 /* Computed Parameter: leadFilter_D
                                        * Referenced by: '<Root>/lead Filter'
                                        */
  -76.71423378,                        /* Computed Parameter: leadFilter1_A
                                        * Referenced by: '<Root>/lead Filter1'
                                        */
  -68.56711467,                        /* Computed Parameter: leadFilter1_C
                                        * Referenced by: '<Root>/lead Filter1'
                                        */
  1.0,                                 /* Computed Parameter: leadFilter1_D
                                        * Referenced by: '<Root>/lead Filter1'
                                        */

  /*  Computed Parameter: notchFilter_A
   * Referenced by: '<Root>/notch Filter'
   */
  { -37.28, -347.44960000000003 },

  /*  Computed Parameter: notchFilter_C
   * Referenced by: '<Root>/notch Filter'
   */
  { -27.25168, 0.0 },
  1.0,                                 /* Computed Parameter: notchFilter_D
                                        * Referenced by: '<Root>/notch Filter'
                                        */
  1,                                   /* Computed Parameter: HILReadEncoderTimebase_Active
                                        * Referenced by: '<S1>/HIL Read Encoder Timebase'
                                        */
  0                                    /* Computed Parameter: HILWriteAnalog_Active
                                        * Referenced by: '<S1>/HIL Write Analog'
                                        */
};
