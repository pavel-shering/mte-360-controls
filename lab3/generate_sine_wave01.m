% m file to demonstrate sine-wave fitting % 
% on noisy signals with known frequency   %

clear;

% construct original "clean" signal %
t = linspace(0,4,1000)';                % time      [sec]
A = 1.35;                               % amplitude [V]
pha = (pi/180)*-3;                    % phase     [rad]
w = 2*pi*2;                            % frequency [rad/sec]

% original signal %
x = A*sin(w*t + pha);

% add randon noise with normal distribution %
R = 10;                                % variance of noise [V^2]
n = sqrt(R)*(randn(length(x),1)-0.5);   % generate noise    [V]

% noisy signal %
xn = x + n;                             % add noise on original signal

% find gain and phase using readily available function %
[A_est pha_est] = fit_sine_wave(t,xn,w)

% reconstruct the original "clean" signal based on fit sine wave %
x_est = A_est * sin(w*t + pha_est); % re-constructed signal

% show original, noisy, and re-constructed signals %
figure(1); clf; 
plot(t,x,'r',t,xn,'k',t,x_est,'bx');
ylabel('y [V]'); xlabel('time [sec]');
legend( 'Original Signal (before noise)','Noisy Signal', ... 
        'Re-constructed Signal (after sine wave fitting)');
zoom on;