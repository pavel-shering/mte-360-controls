/*
 *  rtmodel.h:
 *
 * Code generation for model "notch_doubleLead_RTActual".
 *
 * Model version              : 1.62
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Mon Mar 06 10:24:09 2017
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_rtmodel_h_
#define RTW_HEADER_rtmodel_h_

/*
 *  Includes the appropriate headers when we are using rtModel
 */
#include "notch_doubleLead_RTActual.h"
#define GRTINTERFACE                   1
#endif                                 /* RTW_HEADER_rtmodel_h_ */
