%% CONTROLLER DESIGN
% 6.4 Read Stability Margins
% 6.7.2 Lead Compensation
% 7.7.4 Lag Compensation
% 6.7.6 Phase Stabilization i.e notch filter
% 6.5 Bode's Gain-Phase Ralationship
% 6.6 Closed Loop Frequency Response 

% clear all; 
% clc;

%% known parameters
alpha = 0.54; % m2 / m1
beta = 0.1; % b2 / b1
gamma = 0.1; % d2_coul / d1_coul

%% assumed parameters CHANGE THESE ONCES PART 1 is DONE!!!
m1 = 0.00047618; %[V / (mm / sec^2)]
m2 = 0.000257138; %[V / (mm / sec^2)]


b1 = 0.00881; %[V / (mm / sec)]
b2 = 0.000881; %[V / (mm / sec)]

d1_coul = 0.4148; %[V]
d2_coul = 0.04148; %[V]

k = 0.07; %[V / mm]
Kp = 3.3; %[V / mm]
% a = 8;
% b = 75;
c = 0; % lab manual said 3 but should be 0 
d = 0.03; 

%notch filter param
wn = 19.03236; %[rad / sec]  18.6389
zeta = 0.1381; %[]

%% 4a Gain Selection
s = tf('s');

a1 = (m1*b2 + m2*b1 + (m1 + m2)*c) / (m1*m2);
a2 = (b1*b2 + (b1+b2)*c + (m1+m2)*k) / (m1*m2);
a3 = ((b1+b2) * k)/ (m1*m2);

G2 = (1/(m1*m2)*(c*s + k))/(s*(s^3 + a1*s^2 + a2*s + a3))

% specifications for the controller
wc = 25; %[rad / s] cross over frequency
GM = 2.0; % gain margin 
PM = 30; %[deg] phase margin 
% zero steady state error disturbance; 
% minimal tracking error in controlling the position of the 2nd cart (i.e. minimize e = xr - x2 ).

[MAG, PHASE] = bode(G2, 13.3);
MAG * 0.0949
% gain at PM = 0.0216 is C Gain = 0.0949  gain is == 1 

[MAG2, PHASE2] = bode(G2, 9.22);
MAG2 * 0.0884
% gain at PM = 30 is C Gain = 0.0884  gain is == 1

% sisotool(G2) 

sensitivity = feedback(1, G2)
% figure(1)
% bode(sensitivity)

%% 4b Notch Filter Design
% wn and zeta of the oscillatory poles ( p3,4 ) were identified in Part 3c
Gnotch = (s^2 + 2*zeta*wn*s + wn^2) / (s^2 + 2*wn*s + wn^2)
% figure(2)
% bode(G_notch)

C = k * (1/wn^2*s^2 + 2*zeta/wn *s + 1) / ((1 + s/wn)*(1+ s/wn))
% sisotool(G_notch)
% crossover freq = 5.05 rads/s
[mag_notch, phase_notch] = bode(G2*Gnotch, 25);
phase_notch

%% 4c Lead Filter Design
% need two lead fitlers
phase_lead = (-phase_notch - 140)/2 * pi /180
alpha_lead =  (1 - sin(phase_lead)) / (1 + sin(phase_lead))
1/ alpha_lead

a = wc * sqrt(alpha_lead) % zero of lead filter
b = a * 1 / alpha_lead % pole of lead filter

Glead = (s + a) / (s + b)
% figure(3)
% bode(G_lead)

lead_ratio = a/b
phase_lead = asin((1 - lead_ratio) / (1 + lead_ratio))
freq_phase_lead = a / sqrt(lead_ratio)

[mag_G2, phaseG2] = bode(G2, 25)
[mag_Gnotch, phase_Gnotch] = bode(Gnotch, 25)
[mag_Glead, phase_Glead] = bode(Glead, 25)


Kp = 1 / (mag_G2 * mag_Gnotch * (mag_Glead)^2)

%% 4f Design of Lag Filter
% d < c
% alpha = c/d
% alpha > 1
c_lag = wc / 6.5
d_lag = 0.01 * c_lag

Glag = (s + c_lag) / (s + d_lag) 
[mag_Glag, phase_Glag] = bode(Glag, 25)
Kp = 1 / (mag_G2 * mag_Gnotch * (mag_Glead)^2 * mag_Glag)
