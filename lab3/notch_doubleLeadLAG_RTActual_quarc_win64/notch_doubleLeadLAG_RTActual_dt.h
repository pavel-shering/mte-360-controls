/*
 * notch_doubleLeadLAG_RTActual_dt.h
 *
 * Code generation for model "notch_doubleLeadLAG_RTActual".
 *
 * Model version              : 1.63
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Mon Mar 06 10:33:00 2017
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(t_card),
  sizeof(t_task)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "t_card",
  "t_task"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&notch_doubleLeadLAG_RTActual_B.CartEncodermmcounts), 0, 0, 9 }
  ,

  { (char_T *)(&notch_doubleLeadLAG_RTActual_DW.HILInitialize1_AIMinimums[0]), 0,
    0, 12 },

  { (char_T *)(&notch_doubleLeadLAG_RTActual_DW.HILInitialize1_Card), 14, 0, 1 },

  { (char_T *)(&notch_doubleLeadLAG_RTActual_DW.HILReadEncoderTimebase_Task), 15,
    0, 1 },

  { (char_T *)(&notch_doubleLeadLAG_RTActual_DW.FromWorkspace_PWORK.TimePtr), 11,
    0, 6 },

  { (char_T *)(&notch_doubleLeadLAG_RTActual_DW.HILInitialize1_ClockModes[0]), 6,
    0, 9 },

  { (char_T *)(&notch_doubleLeadLAG_RTActual_DW.FromWorkspace_IWORK.PrevIndex),
    10, 0, 1 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  7U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&notch_doubleLeadLAG_RTActual_P.Kp), 0, 0, 16 },

  { (char_T *)(&notch_doubleLeadLAG_RTActual_P.HILReadEncoderTimebase_clock), 6,
    0, 7 },

  { (char_T *)(&notch_doubleLeadLAG_RTActual_P.HILInitialize1_analog_input_cha[0]),
    7, 0, 11 },

  { (char_T *)(&notch_doubleLeadLAG_RTActual_P.HILInitialize1_active), 8, 0, 35
  },

  { (char_T *)(&notch_doubleLeadLAG_RTActual_P.CartEncodermmcounts_Gain), 0, 0,
    16 },

  { (char_T *)(&notch_doubleLeadLAG_RTActual_P.HILReadEncoderTimebase_Active), 8,
    0, 2 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  6U,
  rtPTransitions
};

/* [EOF] notch_doubleLeadLAG_RTActual_dt.h */
