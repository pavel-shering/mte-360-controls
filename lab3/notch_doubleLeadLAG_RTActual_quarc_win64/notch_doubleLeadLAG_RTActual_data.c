/*
 * notch_doubleLeadLAG_RTActual_data.c
 *
 * Code generation for model "notch_doubleLeadLAG_RTActual".
 *
 * Model version              : 1.63
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Mon Mar 06 10:33:00 2017
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "notch_doubleLeadLAG_RTActual.h"
#include "notch_doubleLeadLAG_RTActual_private.h"

/* Block parameters (auto storage) */
P_notch_doubleLeadLAG_RTActua_T notch_doubleLeadLAG_RTActual_P = {
  14.660843293981685,                  /* Variable: Kp
                                        * Referenced by: '<Root>/Gain'
                                        */
  10.0,                                /* Mask Parameter: HILInitialize1_analog_input_max
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  -10.0,                               /* Mask Parameter: HILInitialize1_analog_input_min
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  10.0,                                /* Mask Parameter: HILInitialize1_analog_output_ma
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  -10.0,                               /* Mask Parameter: HILInitialize1_analog_output_mi
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_final_analog_out
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_final_pwm_output
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_initial_analog_o
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_initial_pwm_outp
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  50.0,                                /* Mask Parameter: HILInitialize1_pwm_frequency
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_set_other_output
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_set_other_outp_d
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_set_other_outp_a
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_set_other_outp_k
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_watchdog_analog_
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0,                                 /* Mask Parameter: HILInitialize1_watchdog_pwm_out
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILReadEncoderTimebase_clock
                                        * Referenced by: '<S1>/HIL Read Encoder Timebase'
                                        */

  /*  Mask Parameter: HILInitialize1_hardware_clocks
   * Referenced by: '<S1>/HIL Initialize1'
   */
  { 0, 1, 2 },
  0,                                   /* Mask Parameter: HILInitialize1_initial_encoder_
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_pwm_modes
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_watchdog_digital
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */

  /*  Mask Parameter: HILInitialize1_analog_input_cha
   * Referenced by: '<S1>/HIL Initialize1'
   */
  { 0U, 1U },

  /*  Mask Parameter: HILInitialize1_analog_output_ch
   * Referenced by: '<S1>/HIL Initialize1'
   */
  { 0U, 1U },

  /*  Mask Parameter: HILReadEncoderTimebase_channels
   * Referenced by: '<S1>/HIL Read Encoder Timebase'
   */
  { 0U, 1U },
  0U,                                  /* Mask Parameter: HILWriteAnalog_channels
                                        * Referenced by: '<S1>/HIL Write Analog'
                                        */

  /*  Mask Parameter: HILInitialize1_encoder_channels
   * Referenced by: '<S1>/HIL Initialize1'
   */
  { 0U, 1U },
  4U,                                  /* Mask Parameter: HILInitialize1_quadrature
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1000U,                               /* Mask Parameter: HILReadEncoderTimebase_samples_
                                        * Referenced by: '<S1>/HIL Read Encoder Timebase'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_active
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_final_digital_ou
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_initial_digital_
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_analog_input
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_analog_inp_g
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_analog_outpu
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_analog_out_g
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_analog_out_o
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_analog_out_m
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_analog_out_e
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_analog_out_d
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_analog_out_p
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_clock_freque
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_clock_freq_e
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_clock_params
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_clock_para_b
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_digital_outp
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_digital_ou_p
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_digital_ou_j
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_digital_o_pm
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_digital_o_jg
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_digital_o_ph
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_digital_ou_l
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_encoder_coun
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_encoder_co_m
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_encoder_para
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_encoder_pa_b
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_other_outp_n
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_pwm_outputs_
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_pwm_output_o
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_pwm_output_n
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_pwm_output_c
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_pwm_output_p
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  1,                                   /* Mask Parameter: HILInitialize1_set_pwm_params_a
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0,                                   /* Mask Parameter: HILInitialize1_set_pwm_params_j
                                        * Referenced by: '<S1>/HIL Initialize1'
                                        */
  0.0046592000000000005,               /* Expression: 1.664*56*(1/20000)
                                        * Referenced by: '<S1>/Cart Encoder (mm//counts)'
                                        */
  0.0046592000000000005,               /* Expression: 1.664*56*(1/20000)
                                        * Referenced by: '<S1>/Cart Encoder 2 (mm//counts)'
                                        */
  -0.005,                              /* Computed Parameter: leadFilter2_A
                                        * Referenced by: '<Root>/lead Filter2'
                                        */
  0.495,                               /* Computed Parameter: leadFilter2_C
                                        * Referenced by: '<Root>/lead Filter2'
                                        */
  1.0,                                 /* Computed Parameter: leadFilter2_D
                                        * Referenced by: '<Root>/lead Filter2'
                                        */
  -76.71423378,                        /* Computed Parameter: leadFilter_A
                                        * Referenced by: '<Root>/lead Filter'
                                        */
  -68.56711467,                        /* Computed Parameter: leadFilter_C
                                        * Referenced by: '<Root>/lead Filter'
                                        */
  1.0,                                 /* Computed Parameter: leadFilter_D
                                        * Referenced by: '<Root>/lead Filter'
                                        */
  -76.71423378,                        /* Computed Parameter: leadFilter1_A
                                        * Referenced by: '<Root>/lead Filter1'
                                        */
  -68.56711467,                        /* Computed Parameter: leadFilter1_C
                                        * Referenced by: '<Root>/lead Filter1'
                                        */
  1.0,                                 /* Computed Parameter: leadFilter1_D
                                        * Referenced by: '<Root>/lead Filter1'
                                        */

  /*  Computed Parameter: notchFilter_A
   * Referenced by: '<Root>/notch Filter'
   */
  { -37.28, -347.44960000000003 },

  /*  Computed Parameter: notchFilter_C
   * Referenced by: '<Root>/notch Filter'
   */
  { -27.25168, 0.0 },
  1.0,                                 /* Computed Parameter: notchFilter_D
                                        * Referenced by: '<Root>/notch Filter'
                                        */
  1,                                   /* Computed Parameter: HILReadEncoderTimebase_Active
                                        * Referenced by: '<S1>/HIL Read Encoder Timebase'
                                        */
  0                                    /* Computed Parameter: HILWriteAnalog_Active
                                        * Referenced by: '<S1>/HIL Write Analog'
                                        */
};
