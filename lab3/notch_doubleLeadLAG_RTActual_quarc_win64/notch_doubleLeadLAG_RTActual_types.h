/*
 * notch_doubleLeadLAG_RTActual_types.h
 *
 * Code generation for model "notch_doubleLeadLAG_RTActual".
 *
 * Model version              : 1.63
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Mon Mar 06 10:33:00 2017
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_notch_doubleLeadLAG_RTActual_types_h_
#define RTW_HEADER_notch_doubleLeadLAG_RTActual_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "zero_crossing_types.h"

/* Parameters (auto storage) */
typedef struct P_notch_doubleLeadLAG_RTActua_T_ P_notch_doubleLeadLAG_RTActua_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_notch_doubleLeadLAG_R_T RT_MODEL_notch_doubleLeadLAG__T;

#endif                                 /* RTW_HEADER_notch_doubleLeadLAG_RTActual_types_h_ */
