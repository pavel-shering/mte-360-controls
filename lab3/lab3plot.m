clear all;
close all;
clc;

load('/Users/rishabsareen/Documents/MATLAB/MTE 360 Lab 3/lab3/last_lab_values.mat')
% load('/Users/pshering/Documents/MATLAB/MTE_360/MTE360 Labs/lab2/project2/1e.mat')

t = 0:0.001:4;

%input trajectory
x_r = xr_exp_4j.signals.values;
% experimental data
x_1 = x1_exp_4j.signals.values;
x_2 = x2_exp_4j.signals.values;

%control signal
u = u_exp_4j.signals.values;

%tracking error
e_exp = x_r - x_2;

n=12;
figure(1);                          % opens a figure window
subplot(3,1,1);                     % subplot(rows, columns, position)
    plot(t,x_1);
    hold on;
    plot(t,x_2);
    hold on;
    plot(t,x_r);
    title('Commanded and Actual Position Trajectory');     % creates a title for the plot
    ylabel('Position [mm]');      % labels the y-axis
    legend('Cart 1 Position', 'Cart 2 Position', 'Command Input');
    axis([0 4 0 60])
    set(gca,'FontSize',n);
subplot(3,1,2);
    plot(t,e_exp);
    title('Cart 2 Tracking Error');     % creates a title for the plot
    ylabel('Error [mm]');      % labels the y-axis
    set(gca,'FontSize',n);
subplot(3,1,3);
    plot(t,u);
    title('Control Signal to Cart 1');     % creates a title for the plot
    ylabel('Control Signal [V]');      % labels the y-axis
    xlabel('Time [sec]')              % labels the x-axis
    set(gca,'FontSize',n);