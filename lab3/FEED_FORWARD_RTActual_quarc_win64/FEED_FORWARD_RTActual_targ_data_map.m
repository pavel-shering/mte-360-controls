  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 6;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (FEED_FORWARD_RTActual_P)
    ;%
      section.nData     = 20;
      section.data(20)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_P.Kp
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% FEED_FORWARD_RTActual_P.b1
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% FEED_FORWARD_RTActual_P.b2
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% FEED_FORWARD_RTActual_P.m1
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% FEED_FORWARD_RTActual_P.m2
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_analog_input_max
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_analog_input_min
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 6;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_analog_output_ma
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 7;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_analog_output_mi
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 8;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_final_analog_out
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 9;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_final_pwm_output
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 10;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_initial_analog_o
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 11;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_initial_pwm_outp
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 12;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_pwm_frequency
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 13;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_other_output
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 14;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_other_outp_g
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 15;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_other_outp_h
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 16;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_other_outp_n
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 17;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_watchdog_analog_
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 18;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_watchdog_pwm_out
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 19;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
      section.nData     = 5;
      section.data(5)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_P.HILReadEncoderTimebase_clock
	  section.data(1).logicalSrcIdx = 20;
	  section.data(1).dtTransOffset = 0;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_hardware_clocks
	  section.data(2).logicalSrcIdx = 21;
	  section.data(2).dtTransOffset = 1;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_initial_encoder_
	  section.data(3).logicalSrcIdx = 22;
	  section.data(3).dtTransOffset = 4;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_pwm_modes
	  section.data(4).logicalSrcIdx = 23;
	  section.data(4).dtTransOffset = 5;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_watchdog_digital
	  section.data(5).logicalSrcIdx = 24;
	  section.data(5).dtTransOffset = 6;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(2) = section;
      clear section
      
      section.nData     = 7;
      section.data(7)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_analog_input_cha
	  section.data(1).logicalSrcIdx = 25;
	  section.data(1).dtTransOffset = 0;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_analog_output_ch
	  section.data(2).logicalSrcIdx = 26;
	  section.data(2).dtTransOffset = 2;
	
	  ;% FEED_FORWARD_RTActual_P.HILReadEncoderTimebase_channels
	  section.data(3).logicalSrcIdx = 27;
	  section.data(3).dtTransOffset = 4;
	
	  ;% FEED_FORWARD_RTActual_P.HILWriteAnalog_channels
	  section.data(4).logicalSrcIdx = 28;
	  section.data(4).dtTransOffset = 6;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_encoder_channels
	  section.data(5).logicalSrcIdx = 29;
	  section.data(5).dtTransOffset = 7;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_quadrature
	  section.data(6).logicalSrcIdx = 30;
	  section.data(6).dtTransOffset = 9;
	
	  ;% FEED_FORWARD_RTActual_P.HILReadEncoderTimebase_samples_
	  section.data(7).logicalSrcIdx = 31;
	  section.data(7).dtTransOffset = 10;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(3) = section;
      clear section
      
      section.nData     = 35;
      section.data(35)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_active
	  section.data(1).logicalSrcIdx = 32;
	  section.data(1).dtTransOffset = 0;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_final_digital_ou
	  section.data(2).logicalSrcIdx = 33;
	  section.data(2).dtTransOffset = 1;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_initial_digital_
	  section.data(3).logicalSrcIdx = 34;
	  section.data(3).dtTransOffset = 2;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_analog_input
	  section.data(4).logicalSrcIdx = 35;
	  section.data(4).dtTransOffset = 3;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_analog_inp_n
	  section.data(5).logicalSrcIdx = 36;
	  section.data(5).dtTransOffset = 4;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_analog_outpu
	  section.data(6).logicalSrcIdx = 37;
	  section.data(6).dtTransOffset = 5;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_analog_out_f
	  section.data(7).logicalSrcIdx = 38;
	  section.data(7).dtTransOffset = 6;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_analog_out_j
	  section.data(8).logicalSrcIdx = 39;
	  section.data(8).dtTransOffset = 7;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_analog_out_o
	  section.data(9).logicalSrcIdx = 40;
	  section.data(9).dtTransOffset = 8;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_analog_out_b
	  section.data(10).logicalSrcIdx = 41;
	  section.data(10).dtTransOffset = 9;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_analog_out_c
	  section.data(11).logicalSrcIdx = 42;
	  section.data(11).dtTransOffset = 10;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_analog_out_k
	  section.data(12).logicalSrcIdx = 43;
	  section.data(12).dtTransOffset = 11;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_clock_freque
	  section.data(13).logicalSrcIdx = 44;
	  section.data(13).dtTransOffset = 12;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_clock_freq_a
	  section.data(14).logicalSrcIdx = 45;
	  section.data(14).dtTransOffset = 13;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_clock_params
	  section.data(15).logicalSrcIdx = 46;
	  section.data(15).dtTransOffset = 14;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_clock_para_b
	  section.data(16).logicalSrcIdx = 47;
	  section.data(16).dtTransOffset = 15;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_digital_outp
	  section.data(17).logicalSrcIdx = 48;
	  section.data(17).dtTransOffset = 16;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_digital_ou_g
	  section.data(18).logicalSrcIdx = 49;
	  section.data(18).dtTransOffset = 17;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_digital_ou_j
	  section.data(19).logicalSrcIdx = 50;
	  section.data(19).dtTransOffset = 18;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_digital_ou_n
	  section.data(20).logicalSrcIdx = 51;
	  section.data(20).dtTransOffset = 19;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_digital_ou_o
	  section.data(21).logicalSrcIdx = 52;
	  section.data(21).dtTransOffset = 20;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_digital_o_n0
	  section.data(22).logicalSrcIdx = 53;
	  section.data(22).dtTransOffset = 21;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_digital_ou_d
	  section.data(23).logicalSrcIdx = 54;
	  section.data(23).dtTransOffset = 22;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_encoder_coun
	  section.data(24).logicalSrcIdx = 55;
	  section.data(24).dtTransOffset = 23;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_encoder_co_f
	  section.data(25).logicalSrcIdx = 56;
	  section.data(25).dtTransOffset = 24;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_encoder_para
	  section.data(26).logicalSrcIdx = 57;
	  section.data(26).dtTransOffset = 25;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_encoder_pa_f
	  section.data(27).logicalSrcIdx = 58;
	  section.data(27).dtTransOffset = 26;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_other_outp_c
	  section.data(28).logicalSrcIdx = 59;
	  section.data(28).dtTransOffset = 27;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_pwm_outputs_
	  section.data(29).logicalSrcIdx = 60;
	  section.data(29).dtTransOffset = 28;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_pwm_output_j
	  section.data(30).logicalSrcIdx = 61;
	  section.data(30).dtTransOffset = 29;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_pwm_output_a
	  section.data(31).logicalSrcIdx = 62;
	  section.data(31).dtTransOffset = 30;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_pwm_output_g
	  section.data(32).logicalSrcIdx = 63;
	  section.data(32).dtTransOffset = 31;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_pwm_output_l
	  section.data(33).logicalSrcIdx = 64;
	  section.data(33).dtTransOffset = 32;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_pwm_params_a
	  section.data(34).logicalSrcIdx = 65;
	  section.data(34).dtTransOffset = 33;
	
	  ;% FEED_FORWARD_RTActual_P.HILInitialize1_set_pwm_params_g
	  section.data(35).logicalSrcIdx = 66;
	  section.data(35).dtTransOffset = 34;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(4) = section;
      clear section
      
      section.nData     = 20;
      section.data(20)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_P.CartEncodermmcounts_Gain
	  section.data(1).logicalSrcIdx = 67;
	  section.data(1).dtTransOffset = 0;
	
	  ;% FEED_FORWARD_RTActual_P.CartEncoder2mmcounts_Gain
	  section.data(2).logicalSrcIdx = 68;
	  section.data(2).dtTransOffset = 1;
	
	  ;% FEED_FORWARD_RTActual_P.TransferFcn_A
	  section.data(3).logicalSrcIdx = 69;
	  section.data(3).dtTransOffset = 2;
	
	  ;% FEED_FORWARD_RTActual_P.TransferFcn_C
	  section.data(4).logicalSrcIdx = 70;
	  section.data(4).dtTransOffset = 3;
	
	  ;% FEED_FORWARD_RTActual_P.TransferFcn_D
	  section.data(5).logicalSrcIdx = 71;
	  section.data(5).dtTransOffset = 4;
	
	  ;% FEED_FORWARD_RTActual_P.TransferFcn1_A
	  section.data(6).logicalSrcIdx = 72;
	  section.data(6).dtTransOffset = 5;
	
	  ;% FEED_FORWARD_RTActual_P.TransferFcn1_C
	  section.data(7).logicalSrcIdx = 73;
	  section.data(7).dtTransOffset = 6;
	
	  ;% FEED_FORWARD_RTActual_P.TransferFcn1_D
	  section.data(8).logicalSrcIdx = 74;
	  section.data(8).dtTransOffset = 7;
	
	  ;% FEED_FORWARD_RTActual_P.lagFilter2_A
	  section.data(9).logicalSrcIdx = 75;
	  section.data(9).dtTransOffset = 8;
	
	  ;% FEED_FORWARD_RTActual_P.lagFilter2_C
	  section.data(10).logicalSrcIdx = 76;
	  section.data(10).dtTransOffset = 9;
	
	  ;% FEED_FORWARD_RTActual_P.lagFilter2_D
	  section.data(11).logicalSrcIdx = 77;
	  section.data(11).dtTransOffset = 10;
	
	  ;% FEED_FORWARD_RTActual_P.leadFilter_A
	  section.data(12).logicalSrcIdx = 78;
	  section.data(12).dtTransOffset = 11;
	
	  ;% FEED_FORWARD_RTActual_P.leadFilter_C
	  section.data(13).logicalSrcIdx = 79;
	  section.data(13).dtTransOffset = 12;
	
	  ;% FEED_FORWARD_RTActual_P.leadFilter_D
	  section.data(14).logicalSrcIdx = 80;
	  section.data(14).dtTransOffset = 13;
	
	  ;% FEED_FORWARD_RTActual_P.leadFilter1_A
	  section.data(15).logicalSrcIdx = 81;
	  section.data(15).dtTransOffset = 14;
	
	  ;% FEED_FORWARD_RTActual_P.leadFilter1_C
	  section.data(16).logicalSrcIdx = 82;
	  section.data(16).dtTransOffset = 15;
	
	  ;% FEED_FORWARD_RTActual_P.leadFilter1_D
	  section.data(17).logicalSrcIdx = 83;
	  section.data(17).dtTransOffset = 16;
	
	  ;% FEED_FORWARD_RTActual_P.notchFilter_A
	  section.data(18).logicalSrcIdx = 84;
	  section.data(18).dtTransOffset = 17;
	
	  ;% FEED_FORWARD_RTActual_P.notchFilter_C
	  section.data(19).logicalSrcIdx = 85;
	  section.data(19).dtTransOffset = 19;
	
	  ;% FEED_FORWARD_RTActual_P.notchFilter_D
	  section.data(20).logicalSrcIdx = 86;
	  section.data(20).dtTransOffset = 21;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(5) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_P.HILReadEncoderTimebase_Active
	  section.data(1).logicalSrcIdx = 87;
	  section.data(1).dtTransOffset = 0;
	
	  ;% FEED_FORWARD_RTActual_P.HILWriteAnalog_Active
	  section.data(2).logicalSrcIdx = 88;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(6) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 1;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (FEED_FORWARD_RTActual_B)
    ;%
      section.nData     = 10;
      section.data(10)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_B.CartEncodermmcounts
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% FEED_FORWARD_RTActual_B.CartEncoder2mmcounts
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% FEED_FORWARD_RTActual_B.FromWorkspace
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% FEED_FORWARD_RTActual_B.TransferFcn
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% FEED_FORWARD_RTActual_B.Gain
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% FEED_FORWARD_RTActual_B.lagFilter2
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% FEED_FORWARD_RTActual_B.leadFilter
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 6;
	
	  ;% FEED_FORWARD_RTActual_B.Sum1
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 7;
	
	  ;% FEED_FORWARD_RTActual_B.notchFilter
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 8;
	
	  ;% FEED_FORWARD_RTActual_B.Clock
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 9;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 6;
    sectIdxOffset = 1;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (FEED_FORWARD_RTActual_DW)
    ;%
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_DW.HILInitialize1_AIMinimums
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% FEED_FORWARD_RTActual_DW.HILInitialize1_AIMaximums
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 2;
	
	  ;% FEED_FORWARD_RTActual_DW.HILInitialize1_AOMinimums
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 4;
	
	  ;% FEED_FORWARD_RTActual_DW.HILInitialize1_AOMaximums
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 6;
	
	  ;% FEED_FORWARD_RTActual_DW.HILInitialize1_AOVoltages
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 8;
	
	  ;% FEED_FORWARD_RTActual_DW.HILInitialize1_FilterFrequency
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 10;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_DW.HILInitialize1_Card
	  section.data(1).logicalSrcIdx = 6;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_DW.HILReadEncoderTimebase_Task
	  section.data(1).logicalSrcIdx = 7;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(3) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_DW.FromWorkspace_PWORK.TimePtr
	  section.data(1).logicalSrcIdx = 8;
	  section.data(1).dtTransOffset = 0;
	
	  ;% FEED_FORWARD_RTActual_DW.HILWriteAnalog_PWORK
	  section.data(2).logicalSrcIdx = 9;
	  section.data(2).dtTransOffset = 1;
	
	  ;% FEED_FORWARD_RTActual_DW.uSim_PWORK.LoggedData
	  section.data(3).logicalSrcIdx = 10;
	  section.data(3).dtTransOffset = 2;
	
	  ;% FEED_FORWARD_RTActual_DW.x1sim_PWORK.LoggedData
	  section.data(4).logicalSrcIdx = 11;
	  section.data(4).dtTransOffset = 3;
	
	  ;% FEED_FORWARD_RTActual_DW.x2sim_PWORK.LoggedData
	  section.data(5).logicalSrcIdx = 12;
	  section.data(5).dtTransOffset = 4;
	
	  ;% FEED_FORWARD_RTActual_DW.xrSim_PWORK.LoggedData
	  section.data(6).logicalSrcIdx = 13;
	  section.data(6).dtTransOffset = 5;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(4) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_DW.HILInitialize1_ClockModes
	  section.data(1).logicalSrcIdx = 14;
	  section.data(1).dtTransOffset = 0;
	
	  ;% FEED_FORWARD_RTActual_DW.HILInitialize1_QuadratureModes
	  section.data(2).logicalSrcIdx = 15;
	  section.data(2).dtTransOffset = 3;
	
	  ;% FEED_FORWARD_RTActual_DW.HILInitialize1_InitialEICounts
	  section.data(3).logicalSrcIdx = 16;
	  section.data(3).dtTransOffset = 5;
	
	  ;% FEED_FORWARD_RTActual_DW.HILReadEncoderTimebase_Buffer
	  section.data(4).logicalSrcIdx = 17;
	  section.data(4).dtTransOffset = 7;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(5) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% FEED_FORWARD_RTActual_DW.FromWorkspace_IWORK.PrevIndex
	  section.data(1).logicalSrcIdx = 18;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(6) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 3971116294;
  targMap.checksum1 = 755969028;
  targMap.checksum2 = 380624674;
  targMap.checksum3 = 1680080524;

