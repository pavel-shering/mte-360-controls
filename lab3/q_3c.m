zeta_arr = [];
omega_arr = [];
k_arr = [];

d_1 = 0.4148;
d_2 = 0.04148;
m_1 = 0.00066545;
m_2 = 0.0003599;
b_1 = 0.00881;
b_2 = 0.000881;
m_t = m_1 + m_2;
b_t = b_1 + b_2;
c = 0;
omega_d = 18.85;
Ts = 0.001;
d1=d_1;d2=d_2;m1=m_1;m2=m_2;b1=b_1;b2=b_2;C=c;

%1)
zeta = 0.01;

for x = 1:101

    zeta_arr = [zeta_arr ; zeta];

    %2) 
    omega_n = omega_d/sqrt(1-zeta*zeta);

    %3)
    k = m_1*m_2*omega_n*omega_n/(m_1+m_2);
    k_arr = [k_arr; k];

    %4)
    a_1 = (m_1*b_2 +  m_2*b_1 + (m_t)*c)/(m_1*m_2);
    a_2 = (b_1*b_2 + (b_t)*c + (m_t)*k)/(m_1*m_2);
    a_3 = (b_t)*k/(m_1*m_2);

    s = tf('s');
    G_1 = 1/(s*(s*s*s + a_1*s*s + a_2*s + a_3));
    [omega_n2,zeta2] = damp(G_1);
    omega_arr = [omega_arr; omega_n2(3)];
    zeta = zeta2(3);
end






