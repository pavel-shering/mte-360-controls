/*
 * simulink_model_2e_types.h
 *
 * Code generation for model "simulink_model_2e".
 *
 * Model version              : 1.10
 * Simulink Coder version : 8.9 (R2015b) 13-Aug-2015
 * C source code generated on : Thu Feb  9 22:25:21 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_simulink_model_2e_types_h_
#define RTW_HEADER_simulink_model_2e_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_simulink_model_2e_T_ P_simulink_model_2e_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_simulink_model_2e_T RT_MODEL_simulink_model_2e_T;

#endif                                 /* RTW_HEADER_simulink_model_2e_types_h_ */
