/*
 * simulink_model_2e_data.c
 *
 * Code generation for model "simulink_model_2e".
 *
 * Model version              : 1.10
 * Simulink Coder version : 8.9 (R2015b) 13-Aug-2015
 * C source code generated on : Thu Feb  9 22:25:21 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "simulink_model_2e.h"
#include "simulink_model_2e_private.h"

/* Block parameters (auto storage) */
P_simulink_model_2e_T simulink_model_2e_P = {
  0.0,                                 /* Mask Parameter: DiscreteDerivative_ICPrevScaled
                                        * Referenced by: '<S1>/UD'
                                        */
  -12.953119664947984,                 /* Computed Parameter: TransferFcn_A
                                        * Referenced by: '<Root>/Transfer Fcn'
                                        */
  1508.8083476934169,                  /* Computed Parameter: TransferFcn_C
                                        * Referenced by: '<Root>/Transfer Fcn'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<Root>/Integrator'
                                        */
  0.023,                               /* Expression: 0.023
                                        * Referenced by: '<Root>/Quantizer'
                                        */
  2.1194,                              /* Expression: 2.1194
                                        * Referenced by: '<Root>/Kp'
                                        */
  0.05138,                             /* Expression: 0.05138
                                        * Referenced by: '<Root>/Kd'
                                        */
  1000.0,                              /* Computed Parameter: TSamp_WtEt
                                        * Referenced by: '<S1>/TSamp'
                                        */
  10.0,                                /* Expression: 10
                                        * Referenced by: '<Root>/Saturation'
                                        */
  -10.0                                /* Expression: -10
                                        * Referenced by: '<Root>/Saturation'
                                        */
};
