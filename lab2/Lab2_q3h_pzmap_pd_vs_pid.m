% m file to demonstrate the effect of 
% a zero and complex conjugate l.h.p. poles

% mte 360, k.e.

% user preferences %
re_max = 10;
im_max = 10;
tmax = 0.5;

% define 2nd order transfer function  for Kp 0.6%
wn = 9.0;                   % natural frequency [rad/s]
zeta = 0.8;                 % damping ratio [ ]
G = tf([wn*wn],[1 2*zeta*wn wn*wn]); % first comma is numerator, demo is second 

%PD TF
Kd = 0.05138;
m = 0.0006627747;
Kp = 2.1194;
b = 0.008585;
G2 = tf([Kd/m Kp/m],[1 (b+Kd)/m Kp/m]);

%PID TF
Ki = 63.918;
G3 = tf([Kd/m Kp/m Ki/m],[1 (b+Kd)/m Kp/m Ki/m]);

[xsim , tsim] = step(G,tmax);
[xsim2, tsim2] = step(G2,tmax);
[xsim3, tsim3] = step(G3,tmax);

% prepare step response figure %
figure(1); zoom on;
set(gca,'FontSize',12);
axis([0 tmax -2 2]); grid on;
ylabel('Response'); xlabel('time [s]');
title('step response');
hold on; plot(tsim,xsim);
hold on; plot(tsim2,xsim2);
legend('sys-ideal 2nd order','sys-pd controlled')

figure(2);
pzmap(G,G2)
set(gca,'FontSize',12);
legend('sys-ideal 2nd order','sys-pd controlled')
grid on

figure(3);
pzmap(G2,G3);
set(gca,'FontSize',12);
legend('sys-pd controlled', 'sys-pid controlled', 'Location', 'best');
grid on

damp(G)
damp(G2)
damp(G3)
