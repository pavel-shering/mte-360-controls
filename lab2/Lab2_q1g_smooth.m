clear all;
close all;
clc;

load('/Users/pshering/Documents/MATLAB/MTE_360/MTE360 Labs/lab2/project2/1F.mat')
% load('/Users/pshering/Documents/MATLAB/MTE_360/MTE360 Labs/lab2/project2/1e.mat')

t = 0:0.001:4;

% experimental
xr_input = xr_real;
x_output = x_real.signals.values;

traj_input_to_sim = [t' xr_input];
sim('step_input_1g.mdl'); % runs the simulation

%simulated
x_output_sim = xsim.data;

%tracking error
e_exp = x_output - xr_input;
e_sim = x_output_sim - xr_input;

%control signal
u_control_exp = u_real;
u_control_sim = usim.data;

v_sim = diff(x_output_sim);
v_exp = diff(x_output);

average = abs(sum(e_exp(200:400))/200)
n=12;
figure(1);                          % opens a figure window
subplot(3,1,1);                     % subplot(rows, columns, position)
    plot(t,x_output);
    hold on;
    plot(t,x_output_sim);
    hold on;
    plot(t,xr_input);
    title('Position Trajectory for Smooth Trajectory');     % creates a title for the plot
    ylabel('Comanded Position [mm]');      % labels the y-axis
    legend('Experimental', 'Simulated', 'Command Input');
    set(gca,'FontSize',n);
subplot(3,1,2);
    plot(t,e_exp);
    hold on;
    plot(t,e_sim);
    title('Tracking Error Smooth Trajectory');     % creates a title for the plot
    ylabel('Error [mm]');      % labels the y-axis
    legend('Experimental', 'Simulated');
    set(gca,'FontSize',n);
subplot(3,1,3);
    plot(t,u_control_exp);
    hold on;
    plot(t,u_control_sim);
    title('Control Signal Smooth Trajectory');     % creates a title for the plot
    ylabel('Control Signal [V]');      % labels the y-axis
    xlabel('Time [sec]')              % labels the x-axis
    legend('Experimental', 'Simulated');
    set(gca,'FontSize',n);
    