clc;

t=ScopeData.time;
x=ScopeData.signals.values;
u=usim
xr=xrsim

figure(1)
subplot(2,1,1)
plot(t,xr,'r--',t,x)
ylabel('Position [mm]')
legend('Command','Measured')
subplot(2,1,2)
plot(t,u)
ylabel('Control [V]')
xlabel('Time [sec]')


Kd = 0.05138;
Kp = 2.1194;
m = 0.0006627747;
b = 0.008585;
Ts = 1 / 1000;

texp = linspace(0, 5, 5000)';
xrexp = (square(4*pi*texp) + 1) / 2;
