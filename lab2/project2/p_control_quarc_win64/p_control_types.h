/*
 * p_control_types.h
 *
 * Code generation for model "p_control.mdl".
 *
 * Model version              : 1.54
 * Simulink Coder version : 8.1 (R2011b) 08-Jul-2011
 * C source code generated on : Tue Jan 24 11:27:10 2017
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_p_control_types_h_
#define RTW_HEADER_p_control_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_p_control_ Parameters_p_control;

/* Forward declaration for rtModel */
typedef struct RT_MODEL_p_control RT_MODEL_p_control;

#endif                                 /* RTW_HEADER_p_control_types_h_ */
