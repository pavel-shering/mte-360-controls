/*
 * p_control.c
 *
 * Code generation for model "p_control.mdl".
 *
 * Model version              : 1.54
 * Simulink Coder version : 8.1 (R2011b) 08-Jul-2011
 * C source code generated on : Tue Jan 24 11:27:10 2017
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "p_control.h"
#include "p_control_private.h"
#include "p_control_dt.h"

/* Block signals (auto storage) */
BlockIO_p_control p_control_B;

/* Block states (auto storage) */
D_Work_p_control p_control_DWork;

/* Real-time model */
RT_MODEL_p_control p_control_M_;
RT_MODEL_p_control *const p_control_M = &p_control_M_;

/* Model output function */
void p_control_output(int_T tid)
{
  real_T u;

  /* S-Function (hil_read_encoder_block): '<S2>/HIL Read Encoder1' */

  /* S-Function Block: p_control/Quarc_Plant/HIL Read Encoder1 (hil_read_encoder_block) */
  {
    t_error result = hil_read_encoder(p_control_DWork.HILInitialize_Card,
      &p_control_P.HILReadEncoder1_Channels, 1,
      &p_control_DWork.HILReadEncoder1_Buffer);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(p_control_M, _rt_error_message);
    } else {
      p_control_B.HILReadEncoder1 = p_control_DWork.HILReadEncoder1_Buffer;
    }
  }

  /* S-Function (inverse_modulus_block): '<S2>/Inverse Modulus' */
  /* S-Function Block: p_control/Quarc_Plant/Inverse Modulus (inverse_modulus_block) */
  {
    static const real_T sampling_period = 0.001;
    real_T du, dy;
    if (p_control_DWork.InverseModulus_IWORK.FirstSample) {
      p_control_B.InverseModulus = p_control_B.HILReadEncoder1;
      p_control_DWork.InverseModulus_RWORK.PreviousU =
        p_control_B.HILReadEncoder1;
      p_control_DWork.InverseModulus_RWORK.PreviousY =
        p_control_B.InverseModulus;
      p_control_DWork.InverseModulus_IWORK.FirstSample = 0;
    } else {
      du = p_control_B.HILReadEncoder1 -
        p_control_DWork.InverseModulus_RWORK.PreviousU;
      if (du > 32768.0) {
        dy = du - 65536.0;
      } else if (du < -32768.0) {
        dy = du + 65536.0;
      } else {
        dy = du;
      }

      p_control_B.InverseModulus =
        p_control_DWork.InverseModulus_RWORK.PreviousY + dy;
      p_control_DWork.InverseModulus_RWORK.PreviousU =
        p_control_B.HILReadEncoder1;
      p_control_DWork.InverseModulus_RWORK.PreviousY =
        p_control_B.InverseModulus;
    }
  }

  /* Gain: '<S2>/Cart Encoder (mm//counts)' */
  p_control_B.CartEncodermmcounts = p_control_P.CartEncodermmcounts_Gain *
    p_control_B.InverseModulus;

  /* FromWorkspace: '<Root>/From Workspace' */
  {
    real_T *pDataValues = (real_T *) p_control_DWork.FromWorkspace_PWORK.DataPtr;
    real_T *pTimeValues = (real_T *) p_control_DWork.FromWorkspace_PWORK.TimePtr;
    int_T currTimeIndex = p_control_DWork.FromWorkspace_IWORK.PrevIndex;
    real_T t = p_control_M->Timing.t[0];

    /* get index */
    if (t <= pTimeValues[0]) {
      currTimeIndex = 0;
    } else if (t >= pTimeValues[4009]) {
      currTimeIndex = 4008;
    } else {
      if (t < pTimeValues[currTimeIndex]) {
        while (t < pTimeValues[currTimeIndex]) {
          currTimeIndex--;
        }
      } else {
        while (t >= pTimeValues[currTimeIndex + 1]) {
          currTimeIndex++;
        }
      }
    }

    p_control_DWork.FromWorkspace_IWORK.PrevIndex = currTimeIndex;

    /* post output */
    {
      real_T t1 = pTimeValues[currTimeIndex];
      real_T t2 = pTimeValues[currTimeIndex + 1];
      if (t1 == t2) {
        if (t < t1) {
          p_control_B.CommandedPositionmm = pDataValues[currTimeIndex];
        } else {
          p_control_B.CommandedPositionmm = pDataValues[currTimeIndex + 1];
        }
      } else {
        real_T f1 = (t2 - t) / (t2 - t1);
        real_T f2 = 1.0 - f1;
        real_T d1;
        real_T d2;
        int_T TimeIndex= currTimeIndex;
        d1 = pDataValues[TimeIndex];
        d2 = pDataValues[TimeIndex + 1];
        p_control_B.CommandedPositionmm = (real_T) rtInterpolate(d1, d2, f1, f2);
        pDataValues += 4010;
      }
    }
  }

  /* Sum: '<Root>/Sum' */
  p_control_B.Sum = p_control_B.CommandedPositionmm -
    p_control_B.CartEncodermmcounts;

  /* SampleTimeMath: '<S1>/TSamp'
   *
   * About '<S1>/TSamp':
   *  y = u * K where K = 1 / ( w * Ts )
   */
  p_control_B.TSamp = p_control_B.Sum * p_control_P.TSamp_WtEt;

  /* Gain: '<Root>/Kd' incorporates:
   *  Sum: '<S1>/Diff'
   *  UnitDelay: '<S1>/UD'
   */
  p_control_B.Kd = (p_control_B.TSamp - p_control_DWork.UD_DSTATE) *
    p_control_P.Kd_Gain;

  /* Sum: '<Root>/Sum3' incorporates:
   *  Gain: '<Root>/Kp'
   */
  u = p_control_P.Kp_Gain * p_control_B.Sum + p_control_B.Kd;

  /* Saturate: '<Root>/Saturation1' */
  p_control_B.Saturation1 = u >= p_control_P.Saturation1_UpperSat ?
    p_control_P.Saturation1_UpperSat : u <= p_control_P.Saturation1_LowerSat ?
    p_control_P.Saturation1_LowerSat : u;

  /* Gain: '<S2>/Cable Gain' */
  p_control_B.CableGain = p_control_P.CableGain_Gain * p_control_B.Saturation1;

  /* S-Function (hil_write_analog_block): '<S2>/HIL Write Analog' */

  /* S-Function Block: p_control/Quarc_Plant/HIL Write Analog (hil_write_analog_block) */
  {
    t_error result;
    result = hil_write_analog(p_control_DWork.HILInitialize_Card,
      &p_control_P.HILWriteAnalog_Channels, 1, &p_control_B.CableGain);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(p_control_M, _rt_error_message);
    }
  }

  /* S-Function (hil_read_encoder_block): '<S2>/HIL Read Encoder 2' */

  /* S-Function Block: p_control/Quarc_Plant/HIL Read Encoder 2 (hil_read_encoder_block) */
  {
    t_error result = hil_read_encoder(p_control_DWork.HILInitialize_Card,
      &p_control_P.HILReadEncoder2_Channels, 1,
      &p_control_DWork.HILReadEncoder2_Buffer);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(p_control_M, _rt_error_message);
    } else {
      p_control_B.HILReadEncoder2 = p_control_DWork.HILReadEncoder2_Buffer;
    }
  }

  /* S-Function (inverse_modulus_block): '<S2>/Inverse Modulus1' */
  /* S-Function Block: p_control/Quarc_Plant/Inverse Modulus1 (inverse_modulus_block) */
  {
    static const real_T sampling_period = 0.001;
    real_T du, dy;
    if (p_control_DWork.InverseModulus1_IWORK.FirstSample) {
      p_control_B.InverseModulus1 = p_control_B.HILReadEncoder2;
      p_control_DWork.InverseModulus1_RWORK.PreviousU =
        p_control_B.HILReadEncoder2;
      p_control_DWork.InverseModulus1_RWORK.PreviousY =
        p_control_B.InverseModulus1;
      p_control_DWork.InverseModulus1_IWORK.FirstSample = 0;
    } else {
      du = p_control_B.HILReadEncoder2 -
        p_control_DWork.InverseModulus1_RWORK.PreviousU;
      if (du > 32768.0) {
        dy = du - 65536.0;
      } else if (du < -32768.0) {
        dy = du + 65536.0;
      } else {
        dy = du;
      }

      p_control_B.InverseModulus1 =
        p_control_DWork.InverseModulus1_RWORK.PreviousY + dy;
      p_control_DWork.InverseModulus1_RWORK.PreviousU =
        p_control_B.HILReadEncoder2;
      p_control_DWork.InverseModulus1_RWORK.PreviousY =
        p_control_B.InverseModulus1;
    }
  }

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model update function */
void p_control_update(int_T tid)
{
  /* Update for UnitDelay: '<S1>/UD' */
  p_control_DWork.UD_DSTATE = p_control_B.TSamp;

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++p_control_M->Timing.clockTick0)) {
    ++p_control_M->Timing.clockTickH0;
  }

  p_control_M->Timing.t[0] = p_control_M->Timing.clockTick0 *
    p_control_M->Timing.stepSize0 + p_control_M->Timing.clockTickH0 *
    p_control_M->Timing.stepSize0 * 4294967296.0;

  {
    /* Update absolute timer for sample time: [0.001s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++p_control_M->Timing.clockTick1)) {
      ++p_control_M->Timing.clockTickH1;
    }

    p_control_M->Timing.t[1] = p_control_M->Timing.clockTick1 *
      p_control_M->Timing.stepSize1 + p_control_M->Timing.clockTickH1 *
      p_control_M->Timing.stepSize1 * 4294967296.0;
  }

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model initialize function */
void p_control_initialize(boolean_T firstTime)
{
  (void)firstTime;

  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)p_control_M, 0,
                sizeof(RT_MODEL_p_control));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&p_control_M->solverInfo,
                          &p_control_M->Timing.simTimeStep);
    rtsiSetTPtr(&p_control_M->solverInfo, &rtmGetTPtr(p_control_M));
    rtsiSetStepSizePtr(&p_control_M->solverInfo, &p_control_M->Timing.stepSize0);
    rtsiSetErrorStatusPtr(&p_control_M->solverInfo, (&rtmGetErrorStatus
      (p_control_M)));
    rtsiSetRTModelPtr(&p_control_M->solverInfo, p_control_M);
  }

  rtsiSetSimTimeStep(&p_control_M->solverInfo, MAJOR_TIME_STEP);
  rtsiSetSolverName(&p_control_M->solverInfo,"FixedStepDiscrete");

  /* Initialize timing info */
  {
    int_T *mdlTsMap = p_control_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    p_control_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    p_control_M->Timing.sampleTimes = (&p_control_M->Timing.sampleTimesArray[0]);
    p_control_M->Timing.offsetTimes = (&p_control_M->Timing.offsetTimesArray[0]);

    /* task periods */
    p_control_M->Timing.sampleTimes[0] = (0.0);
    p_control_M->Timing.sampleTimes[1] = (0.001);

    /* task offsets */
    p_control_M->Timing.offsetTimes[0] = (0.0);
    p_control_M->Timing.offsetTimes[1] = (0.0);
  }

  rtmSetTPtr(p_control_M, &p_control_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = p_control_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    p_control_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(p_control_M, 4.0);
  p_control_M->Timing.stepSize0 = 0.001;
  p_control_M->Timing.stepSize1 = 0.001;

  /* external mode info */
  p_control_M->Sizes.checksums[0] = (44223366U);
  p_control_M->Sizes.checksums[1] = (729352232U);
  p_control_M->Sizes.checksums[2] = (23022504U);
  p_control_M->Sizes.checksums[3] = (3503493493U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    p_control_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(p_control_M->extModeInfo,
      &p_control_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(p_control_M->extModeInfo, p_control_M->Sizes.checksums);
    rteiSetTPtr(p_control_M->extModeInfo, rtmGetTPtr(p_control_M));
  }

  p_control_M->solverInfoPtr = (&p_control_M->solverInfo);
  p_control_M->Timing.stepSize = (0.001);
  rtsiSetFixedStepSize(&p_control_M->solverInfo, 0.001);
  rtsiSetSolverMode(&p_control_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  p_control_M->ModelData.blockIO = ((void *) &p_control_B);
  (void) memset(((void *) &p_control_B), 0,
                sizeof(BlockIO_p_control));

  /* parameters */
  p_control_M->ModelData.defaultParam = ((real_T *)&p_control_P);

  /* states (dwork) */
  p_control_M->Work.dwork = ((void *) &p_control_DWork);
  (void) memset((void *)&p_control_DWork, 0,
                sizeof(D_Work_p_control));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    p_control_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 15;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }
}

/* Model terminate function */
void p_control_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<S2>/HIL Initialize' */

  /* S-Function Block: p_control/Quarc_Plant/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(p_control_DWork.HILInitialize_Card);
    hil_monitor_stop_all(p_control_DWork.HILInitialize_Card);
    is_switching = false;
    if ((p_control_P.HILInitialize_AOTerminate && !is_switching) ||
        (p_control_P.HILInitialize_AOExit && is_switching)) {
      p_control_DWork.HILInitialize_AOVoltages[0] =
        p_control_P.HILInitialize_AOFinal;
      p_control_DWork.HILInitialize_AOVoltages[1] =
        p_control_P.HILInitialize_AOFinal;
      p_control_DWork.HILInitialize_AOVoltages[2] =
        p_control_P.HILInitialize_AOFinal;
      p_control_DWork.HILInitialize_AOVoltages[3] =
        p_control_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 4U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(p_control_DWork.HILInitialize_Card,
        p_control_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &p_control_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(p_control_M, _rt_error_message);
      }
    }

    hil_task_delete_all(p_control_DWork.HILInitialize_Card);
    hil_monitor_delete_all(p_control_DWork.HILInitialize_Card);
    hil_close(p_control_DWork.HILInitialize_Card);
    p_control_DWork.HILInitialize_Card = NULL;
  }
}

/*========================================================================*
 * Start of GRT compatible call interface                                 *
 *========================================================================*/
void MdlOutputs(int_T tid)
{
  p_control_output(tid);
}

void MdlUpdate(int_T tid)
{
  p_control_update(tid);
}

void MdlInitializeSizes(void)
{
  p_control_M->Sizes.numContStates = (0);/* Number of continuous states */
  p_control_M->Sizes.numY = (0);       /* Number of model outputs */
  p_control_M->Sizes.numU = (0);       /* Number of model inputs */
  p_control_M->Sizes.sysDirFeedThru = (0);/* The model is not direct feedthrough */
  p_control_M->Sizes.numSampTimes = (2);/* Number of sample times */
  p_control_M->Sizes.numBlocks = (20); /* Number of blocks */
  p_control_M->Sizes.numBlockIO = (11);/* Number of block outputs */
  p_control_M->Sizes.numBlockPrms = (84);/* Sum of parameter "widths" */
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
  /* InitializeConditions for UnitDelay: '<S1>/UD' */
  p_control_DWork.UD_DSTATE = p_control_P.UD_X0;
}

void MdlStart(void)
{
  /* Start for S-Function (hil_initialize_block): '<S2>/HIL Initialize' */

  /* S-Function Block: p_control/Quarc_Plant/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q4", "0", &p_control_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(p_control_M, _rt_error_message);
      return;
    }

    is_switching = false;
    if ((p_control_P.HILInitialize_CKPStart && !is_switching) ||
        (p_control_P.HILInitialize_CKPEnter && is_switching)) {
      result = hil_set_clock_mode(p_control_DWork.HILInitialize_Card, (t_clock *)
        p_control_P.HILInitialize_CKChannels, 2U, (t_clock_mode *)
        p_control_P.HILInitialize_CKModes);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(p_control_M, _rt_error_message);
        return;
      }
    }

    result = hil_watchdog_clear(p_control_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(p_control_M, _rt_error_message);
      return;
    }

    if ((p_control_P.HILInitialize_AIPStart && !is_switching) ||
        (p_control_P.HILInitialize_AIPEnter && is_switching)) {
      p_control_DWork.HILInitialize_AIMinimums[0] =
        p_control_P.HILInitialize_AILow;
      p_control_DWork.HILInitialize_AIMinimums[1] =
        p_control_P.HILInitialize_AILow;
      p_control_DWork.HILInitialize_AIMinimums[2] =
        p_control_P.HILInitialize_AILow;
      p_control_DWork.HILInitialize_AIMinimums[3] =
        p_control_P.HILInitialize_AILow;
      p_control_DWork.HILInitialize_AIMaximums[0] =
        p_control_P.HILInitialize_AIHigh;
      p_control_DWork.HILInitialize_AIMaximums[1] =
        p_control_P.HILInitialize_AIHigh;
      p_control_DWork.HILInitialize_AIMaximums[2] =
        p_control_P.HILInitialize_AIHigh;
      p_control_DWork.HILInitialize_AIMaximums[3] =
        p_control_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges(p_control_DWork.HILInitialize_Card,
        p_control_P.HILInitialize_AIChannels, 4U,
        &p_control_DWork.HILInitialize_AIMinimums[0],
        &p_control_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(p_control_M, _rt_error_message);
        return;
      }
    }

    if ((p_control_P.HILInitialize_AOPStart && !is_switching) ||
        (p_control_P.HILInitialize_AOPEnter && is_switching)) {
      p_control_DWork.HILInitialize_AOMinimums[0] =
        p_control_P.HILInitialize_AOLow;
      p_control_DWork.HILInitialize_AOMinimums[1] =
        p_control_P.HILInitialize_AOLow;
      p_control_DWork.HILInitialize_AOMinimums[2] =
        p_control_P.HILInitialize_AOLow;
      p_control_DWork.HILInitialize_AOMinimums[3] =
        p_control_P.HILInitialize_AOLow;
      p_control_DWork.HILInitialize_AOMaximums[0] =
        p_control_P.HILInitialize_AOHigh;
      p_control_DWork.HILInitialize_AOMaximums[1] =
        p_control_P.HILInitialize_AOHigh;
      p_control_DWork.HILInitialize_AOMaximums[2] =
        p_control_P.HILInitialize_AOHigh;
      p_control_DWork.HILInitialize_AOMaximums[3] =
        p_control_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges(p_control_DWork.HILInitialize_Card,
        p_control_P.HILInitialize_AOChannels, 4U,
        &p_control_DWork.HILInitialize_AOMinimums[0],
        &p_control_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(p_control_M, _rt_error_message);
        return;
      }
    }

    if ((p_control_P.HILInitialize_AOStart && !is_switching) ||
        (p_control_P.HILInitialize_AOEnter && is_switching)) {
      p_control_DWork.HILInitialize_AOVoltages[0] =
        p_control_P.HILInitialize_AOInitial;
      p_control_DWork.HILInitialize_AOVoltages[1] =
        p_control_P.HILInitialize_AOInitial;
      p_control_DWork.HILInitialize_AOVoltages[2] =
        p_control_P.HILInitialize_AOInitial;
      p_control_DWork.HILInitialize_AOVoltages[3] =
        p_control_P.HILInitialize_AOInitial;
      result = hil_write_analog(p_control_DWork.HILInitialize_Card,
        p_control_P.HILInitialize_AOChannels, 4U,
        &p_control_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(p_control_M, _rt_error_message);
        return;
      }
    }

    if (p_control_P.HILInitialize_AOReset) {
      p_control_DWork.HILInitialize_AOVoltages[0] =
        p_control_P.HILInitialize_AOWatchdog;
      p_control_DWork.HILInitialize_AOVoltages[1] =
        p_control_P.HILInitialize_AOWatchdog;
      p_control_DWork.HILInitialize_AOVoltages[2] =
        p_control_P.HILInitialize_AOWatchdog;
      p_control_DWork.HILInitialize_AOVoltages[3] =
        p_control_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (p_control_DWork.HILInitialize_Card,
         p_control_P.HILInitialize_AOChannels, 4U,
         &p_control_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(p_control_M, _rt_error_message);
        return;
      }
    }

    if ((p_control_P.HILInitialize_EIPStart && !is_switching) ||
        (p_control_P.HILInitialize_EIPEnter && is_switching)) {
      p_control_DWork.HILInitialize_QuadratureModes[0] =
        p_control_P.HILInitialize_EIQuadrature;
      p_control_DWork.HILInitialize_QuadratureModes[1] =
        p_control_P.HILInitialize_EIQuadrature;
      p_control_DWork.HILInitialize_QuadratureModes[2] =
        p_control_P.HILInitialize_EIQuadrature;
      p_control_DWork.HILInitialize_QuadratureModes[3] =
        p_control_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode
        (p_control_DWork.HILInitialize_Card,
         p_control_P.HILInitialize_EIChannels, 4U, (t_encoder_quadrature_mode *)
         &p_control_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(p_control_M, _rt_error_message);
        return;
      }

      p_control_DWork.HILInitialize_FilterFrequency[0] =
        p_control_P.HILInitialize_EIFrequency;
      p_control_DWork.HILInitialize_FilterFrequency[1] =
        p_control_P.HILInitialize_EIFrequency;
      p_control_DWork.HILInitialize_FilterFrequency[2] =
        p_control_P.HILInitialize_EIFrequency;
      p_control_DWork.HILInitialize_FilterFrequency[3] =
        p_control_P.HILInitialize_EIFrequency;
      result = hil_set_encoder_filter_frequency
        (p_control_DWork.HILInitialize_Card,
         p_control_P.HILInitialize_EIChannels, 4U,
         &p_control_DWork.HILInitialize_FilterFrequency[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(p_control_M, _rt_error_message);
        return;
      }
    }

    if ((p_control_P.HILInitialize_EIStart && !is_switching) ||
        (p_control_P.HILInitialize_EIEnter && is_switching)) {
      p_control_DWork.HILInitialize_InitialEICounts[0] =
        p_control_P.HILInitialize_EIInitial;
      p_control_DWork.HILInitialize_InitialEICounts[1] =
        p_control_P.HILInitialize_EIInitial;
      p_control_DWork.HILInitialize_InitialEICounts[2] =
        p_control_P.HILInitialize_EIInitial;
      p_control_DWork.HILInitialize_InitialEICounts[3] =
        p_control_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(p_control_DWork.HILInitialize_Card,
        p_control_P.HILInitialize_EIChannels, 4U,
        &p_control_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(p_control_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (inverse_modulus_block): '<S2>/Inverse Modulus' */

  /* S-Function Block: p_control/Quarc_Plant/Inverse Modulus (inverse_modulus_block) */
  {
    /* Initialize previous inputs to zero */
    p_control_DWork.InverseModulus_RWORK.PreviousU = 0.0;

    /* Initialize previous outputs to zero */
    p_control_DWork.InverseModulus_RWORK.PreviousY = 0.0;
    p_control_DWork.InverseModulus_IWORK.FirstSample = 1;
  }

  /* Start for FromWorkspace: '<Root>/From Workspace' */
  {
    static real_T pTimeValues[] = { 0.0, 0.001, 0.002, 0.003, 0.004, 0.005,
      0.006, 0.007, 0.008, 0.0090000000000000011, 0.01, 0.011, 0.012,
      0.013000000000000001, 0.014, 0.015, 0.016, 0.017, 0.018000000000000002,
      0.019, 0.02, 0.021, 0.022, 0.023, 0.024, 0.025, 0.026000000000000002,
      0.027, 0.028, 0.029, 0.03, 0.031, 0.032, 0.033, 0.034, 0.035,
      0.036000000000000004, 0.037, 0.038, 0.039, 0.04, 0.041, 0.042,
      0.043000000000000003, 0.044, 0.045, 0.046, 0.047, 0.048, 0.049, 0.05,
      0.051000000000000004, 0.052000000000000005, 0.053, 0.054, 0.055, 0.056,
      0.057, 0.058, 0.059000000000000004, 0.06, 0.061, 0.062, 0.063, 0.064,
      0.065, 0.066, 0.067, 0.068, 0.069, 0.07, 0.071000000000000008,
      0.072000000000000008, 0.073, 0.074, 0.075, 0.076, 0.077, 0.078, 0.079,
      0.08, 0.081, 0.082, 0.083, 0.084, 0.085, 0.086000000000000007,
      0.087000000000000008, 0.088, 0.089, 0.09, 0.091, 0.092, 0.093, 0.094,
      0.095, 0.096, 0.097, 0.098, 0.099, 0.1, 0.101, 0.10200000000000001,
      0.10300000000000001, 0.10400000000000001, 0.105, 0.106, 0.107, 0.108,
      0.109, 0.11, 0.111, 0.112, 0.113, 0.114, 0.115, 0.116, 0.117,
      0.11800000000000001, 0.11900000000000001, 0.12, 0.121, 0.122, 0.123, 0.124,
      0.125, 0.126, 0.127, 0.128, 0.129, 0.13, 0.131, 0.132, 0.133, 0.134, 0.135,
      0.136, 0.137, 0.138, 0.139, 0.14, 0.14100000000000001, 0.14200000000000002,
      0.14300000000000002, 0.14400000000000002, 0.145, 0.146, 0.147, 0.148,
      0.149, 0.15, 0.151, 0.152, 0.153, 0.154, 0.155, 0.156, 0.157, 0.158, 0.159,
      0.16, 0.161, 0.162, 0.163, 0.164, 0.165, 0.166, 0.167, 0.168, 0.169, 0.17,
      0.171, 0.17200000000000001, 0.17300000000000001, 0.17400000000000002,
      0.17500000000000002, 0.176, 0.177, 0.178, 0.179, 0.18, 0.181, 0.182, 0.183,
      0.184, 0.185, 0.186, 0.187, 0.188, 0.189, 0.19, 0.191, 0.192, 0.193, 0.194,
      0.195, 0.196, 0.197, 0.198, 0.199, 0.2, 0.201, 0.202, 0.203,
      0.20400000000000001, 0.20500000000000002, 0.20600000000000002,
      0.20700000000000002, 0.20800000000000002, 0.209, 0.21, 0.211, 0.212, 0.213,
      0.214, 0.215, 0.216, 0.217, 0.218, 0.219, 0.22, 0.221, 0.222, 0.223, 0.224,
      0.225, 0.226, 0.227, 0.228, 0.229, 0.23, 0.231, 0.232, 0.233, 0.234,
      0.23500000000000001, 0.23600000000000002, 0.23700000000000002,
      0.23800000000000002, 0.23900000000000002, 0.24, 0.241, 0.242, 0.243, 0.244,
      0.245, 0.246, 0.247, 0.248, 0.249, 0.25, 0.251, 0.252, 0.253, 0.254, 0.255,
      0.256, 0.257, 0.258, 0.259, 0.26, 0.261, 0.262, 0.263, 0.264, 0.265, 0.266,
      0.267, 0.268, 0.269, 0.27, 0.271, 0.272, 0.273, 0.274, 0.275, 0.276, 0.277,
      0.278, 0.279, 0.28, 0.281, 0.28200000000000003, 0.28300000000000003,
      0.28400000000000003, 0.28500000000000003, 0.28600000000000003,
      0.28700000000000003, 0.28800000000000003, 0.289, 0.29, 0.291, 0.292, 0.293,
      0.294, 0.295, 0.296, 0.297, 0.298, 0.299, 0.3, 0.301, 0.302, 0.303, 0.304,
      0.305, 0.306, 0.307, 0.308, 0.309, 0.31, 0.311, 0.312, 0.313, 0.314, 0.315,
      0.316, 0.317, 0.318, 0.319, 0.32, 0.321, 0.322, 0.323, 0.324, 0.325, 0.326,
      0.327, 0.328, 0.329, 0.33, 0.331, 0.332, 0.333, 0.334, 0.335, 0.336, 0.337,
      0.338, 0.339, 0.34, 0.341, 0.342, 0.343, 0.34400000000000003,
      0.34500000000000003, 0.34600000000000003, 0.34700000000000003,
      0.34800000000000003, 0.34900000000000003, 0.35000000000000003,
      0.35100000000000003, 0.352, 0.353, 0.354, 0.355, 0.356, 0.357, 0.358,
      0.359, 0.36, 0.361, 0.362, 0.363, 0.364, 0.365, 0.366, 0.367, 0.368, 0.369,
      0.37, 0.371, 0.372, 0.373, 0.374, 0.375, 0.376, 0.377, 0.378, 0.379, 0.38,
      0.381, 0.382, 0.383, 0.384, 0.385, 0.386, 0.387, 0.388, 0.389, 0.39, 0.391,
      0.392, 0.393, 0.394, 0.395, 0.396, 0.397, 0.398, 0.399, 0.4, 0.401, 0.402,
      0.403, 0.404, 0.405, 0.406, 0.40700000000000003, 0.40800000000000003,
      0.40900000000000003, 0.41000000000000003, 0.41100000000000003,
      0.41200000000000003, 0.41300000000000003, 0.41400000000000003,
      0.41500000000000004, 0.41600000000000004, 0.417, 0.418, 0.419, 0.42, 0.421,
      0.422, 0.423, 0.424, 0.425, 0.426, 0.427, 0.428, 0.429, 0.43, 0.431, 0.432,
      0.433, 0.434, 0.435, 0.436, 0.437, 0.438, 0.439, 0.44, 0.441, 0.442, 0.443,
      0.444, 0.445, 0.446, 0.447, 0.448, 0.449, 0.45, 0.451, 0.452, 0.453, 0.454,
      0.455, 0.456, 0.457, 0.458, 0.459, 0.46, 0.461, 0.462, 0.463, 0.464, 0.465,
      0.466, 0.467, 0.468, 0.46900000000000003, 0.47000000000000003,
      0.47100000000000003, 0.47200000000000003, 0.47300000000000003,
      0.47400000000000003, 0.47500000000000003, 0.47600000000000003,
      0.47700000000000004, 0.47800000000000004, 0.47900000000000004, 0.48, 0.481,
      0.482, 0.483, 0.484, 0.485, 0.486, 0.487, 0.488, 0.489, 0.49, 0.491, 0.492,
      0.493, 0.494, 0.495, 0.496, 0.497, 0.498, 0.499, 0.5, 0.501, 0.502, 0.503,
      0.504, 0.505, 0.506, 0.507, 0.508, 0.509, 0.51, 0.511, 0.512, 0.513, 0.514,
      0.515, 0.516, 0.517, 0.518, 0.519, 0.52, 0.521, 0.522, 0.523, 0.524, 0.525,
      0.526, 0.527, 0.528, 0.529, 0.53, 0.531, 0.532, 0.533, 0.534, 0.535, 0.536,
      0.537, 0.538, 0.539, 0.54, 0.541, 0.542, 0.543, 0.544, 0.545, 0.546, 0.547,
      0.548, 0.549, 0.55, 0.551, 0.552, 0.553, 0.554, 0.555, 0.556, 0.557, 0.558,
      0.559, 0.56, 0.561, 0.562, 0.56300000000000006, 0.56400000000000006,
      0.56500000000000006, 0.56600000000000006, 0.56700000000000006,
      0.56800000000000006, 0.56900000000000006, 0.57000000000000006,
      0.57100000000000006, 0.57200000000000006, 0.57300000000000006,
      0.57400000000000007, 0.57500000000000007, 0.57600000000000007, 0.577,
      0.578, 0.579, 0.58, 0.581, 0.582, 0.583, 0.584, 0.585, 0.586, 0.587, 0.588,
      0.589, 0.59, 0.591, 0.592, 0.593, 0.594, 0.595, 0.596, 0.597, 0.598, 0.599,
      0.6, 0.601, 0.602, 0.603, 0.604, 0.605, 0.606, 0.607, 0.608, 0.609, 0.61,
      0.611, 0.612, 0.613, 0.614, 0.615, 0.616, 0.617, 0.618, 0.619, 0.62, 0.621,
      0.622, 0.623, 0.624, 0.625, 0.626, 0.627, 0.628, 0.629, 0.63, 0.631, 0.632,
      0.633, 0.634, 0.635, 0.636, 0.637, 0.638, 0.639, 0.64, 0.641, 0.642, 0.643,
      0.644, 0.645, 0.646, 0.647, 0.648, 0.649, 0.65, 0.651, 0.652, 0.653, 0.654,
      0.655, 0.656, 0.657, 0.658, 0.659, 0.66, 0.661, 0.662, 0.663, 0.664, 0.665,
      0.666, 0.667, 0.668, 0.669, 0.67, 0.671, 0.672, 0.673, 0.674, 0.675, 0.676,
      0.677, 0.678, 0.679, 0.68, 0.681, 0.682, 0.683, 0.684, 0.685, 0.686, 0.687,
      0.68800000000000006, 0.68900000000000006, 0.69000000000000006,
      0.69100000000000006, 0.69200000000000006, 0.69300000000000006,
      0.69400000000000006, 0.69500000000000006, 0.69600000000000006,
      0.69700000000000006, 0.69800000000000006, 0.69900000000000007,
      0.70000000000000007, 0.70100000000000007, 0.70200000000000007,
      0.70300000000000007, 0.704, 0.705, 0.706, 0.707, 0.708, 0.709, 0.71, 0.711,
      0.712, 0.713, 0.714, 0.715, 0.716, 0.717, 0.718, 0.719, 0.72, 0.721, 0.722,
      0.723, 0.724, 0.725, 0.726, 0.727, 0.728, 0.729, 0.73, 0.731, 0.732, 0.733,
      0.734, 0.735, 0.736, 0.737, 0.738, 0.739, 0.74, 0.741, 0.742, 0.743, 0.744,
      0.745, 0.746, 0.747, 0.748, 0.749, 0.75, 0.751, 0.752, 0.753, 0.754, 0.755,
      0.756, 0.757, 0.758, 0.759, 0.76, 0.761, 0.762, 0.763, 0.764, 0.765, 0.766,
      0.767, 0.768, 0.769, 0.77, 0.771, 0.772, 0.773, 0.774, 0.775, 0.776, 0.777,
      0.778, 0.779, 0.78, 0.781, 0.782, 0.783, 0.784, 0.785, 0.786, 0.787, 0.788,
      0.789, 0.79, 0.791, 0.792, 0.793, 0.794, 0.795, 0.796, 0.797, 0.798, 0.799,
      0.8, 0.801, 0.802, 0.803, 0.804, 0.805, 0.806, 0.807, 0.808, 0.809, 0.81,
      0.811, 0.812, 0.81300000000000006, 0.81400000000000006,
      0.81500000000000006, 0.81600000000000006, 0.81700000000000006,
      0.81800000000000006, 0.81900000000000006, 0.82000000000000006,
      0.82100000000000006, 0.82200000000000006, 0.82300000000000006,
      0.82400000000000007, 0.82500000000000007, 0.82600000000000007,
      0.82700000000000007, 0.82800000000000007, 0.82900000000000007,
      0.83000000000000007, 0.83100000000000007, 0.83200000000000007, 0.833,
      0.834, 0.835, 0.836, 0.837, 0.838, 0.839, 0.84, 0.841, 0.842, 0.843, 0.844,
      0.845, 0.846, 0.847, 0.848, 0.849, 0.85, 0.851, 0.852, 0.853, 0.854, 0.855,
      0.856, 0.857, 0.858, 0.859, 0.86, 0.861, 0.862, 0.863, 0.864, 0.865, 0.866,
      0.867, 0.868, 0.869, 0.87, 0.871, 0.872, 0.873, 0.874, 0.875, 0.876, 0.877,
      0.878, 0.879, 0.88, 0.881, 0.882, 0.883, 0.884, 0.885, 0.886, 0.887, 0.888,
      0.889, 0.89, 0.891, 0.892, 0.893, 0.894, 0.895, 0.896, 0.897, 0.898, 0.899,
      0.9, 0.901, 0.902, 0.903, 0.904, 0.905, 0.906, 0.907, 0.908, 0.909, 0.91,
      0.911, 0.912, 0.913, 0.914, 0.915, 0.916, 0.917, 0.918, 0.919, 0.92, 0.921,
      0.922, 0.923, 0.924, 0.925, 0.926, 0.927, 0.928, 0.929, 0.93, 0.931, 0.932,
      0.933, 0.934, 0.935, 0.936, 0.937, 0.93800000000000006,
      0.93900000000000006, 0.94000000000000006, 0.94100000000000006,
      0.94200000000000006, 0.94300000000000006, 0.94400000000000006,
      0.94500000000000006, 0.94600000000000006, 0.94700000000000006,
      0.94800000000000006, 0.94900000000000007, 0.95000000000000007,
      0.95100000000000007, 0.95200000000000007, 0.95300000000000007,
      0.95400000000000007, 0.95500000000000007, 0.95600000000000007,
      0.95700000000000007, 0.95800000000000007, 0.95900000000000007, 0.96, 0.961,
      0.962, 0.963, 0.964, 0.965, 0.966, 0.967, 0.968, 0.969, 0.97, 0.971, 0.972,
      0.973, 0.974, 0.975, 0.976, 0.977, 0.978, 0.979, 0.98, 0.981, 0.982, 0.983,
      0.984, 0.985, 0.986, 0.987, 0.988, 0.989, 0.99, 0.991, 0.992, 0.993, 0.994,
      0.995, 0.996, 0.997, 0.998, 0.999, 1.0, 1.0010000000000001, 1.002,
      1.0030000000000001, 1.004, 1.0050000000000001, 1.006, 1.0070000000000001,
      1.008, 1.0090000000000001, 1.01, 1.0110000000000001, 1.012,
      1.0130000000000001, 1.014, 1.0150000000000001, 1.016, 1.0170000000000001,
      1.018, 1.0190000000000001, 1.02, 1.0210000000000001, 1.022,
      1.0230000000000001, 1.024, 1.025, 1.026, 1.027, 1.028, 1.029, 1.03, 1.031,
      1.032, 1.033, 1.034, 1.035, 1.036, 1.037, 1.038, 1.039, 1.04, 1.041, 1.042,
      1.043, 1.044, 1.045, 1.046, 1.047, 1.048, 1.049, 1.05, 1.051, 1.052, 1.053,
      1.054, 1.055, 1.056, 1.057, 1.058, 1.059, 1.06, 1.061, 1.062, 1.063, 1.064,
      1.065, 1.066, 1.067, 1.068, 1.069, 1.07, 1.071, 1.072, 1.073, 1.074, 1.075,
      1.076, 1.077, 1.078, 1.079, 1.08, 1.081, 1.082, 1.083, 1.084, 1.085, 1.086,
      1.087, 1.088, 1.089, 1.09, 1.091, 1.092, 1.093, 1.094, 1.095, 1.096, 1.097,
      1.098, 1.099, 1.1, 1.101, 1.102, 1.103, 1.104, 1.105, 1.106, 1.107, 1.108,
      1.109, 1.11, 1.111, 1.112, 1.113, 1.114, 1.115, 1.116, 1.117, 1.118, 1.119,
      1.12, 1.121, 1.122, 1.123, 1.124, 1.125, 1.1260000000000001, 1.127,
      1.1280000000000001, 1.129, 1.1300000000000001, 1.131, 1.1320000000000001,
      1.133, 1.1340000000000001, 1.135, 1.1360000000000001, 1.137,
      1.1380000000000001, 1.139, 1.1400000000000001, 1.141, 1.1420000000000001,
      1.143, 1.1440000000000001, 1.145, 1.1460000000000001, 1.147,
      1.1480000000000001, 1.149, 1.1500000000000001, 1.151, 1.1520000000000001,
      1.153, 1.154, 1.155, 1.156, 1.157, 1.158, 1.159, 1.16, 1.161, 1.162, 1.163,
      1.164, 1.165, 1.166, 1.167, 1.168, 1.169, 1.17, 1.171, 1.172, 1.173, 1.174,
      1.175, 1.176, 1.177, 1.178, 1.179, 1.18, 1.181, 1.182, 1.183, 1.184, 1.185,
      1.186, 1.187, 1.188, 1.189, 1.19, 1.191, 1.192, 1.193, 1.194, 1.195, 1.196,
      1.197, 1.198, 1.199, 1.2, 1.201, 1.202, 1.203, 1.204, 1.205, 1.206, 1.207,
      1.208, 1.209, 1.21, 1.211, 1.212, 1.213, 1.214, 1.215, 1.216, 1.217, 1.218,
      1.219, 1.22, 1.221, 1.222, 1.223, 1.224, 1.225, 1.226, 1.227, 1.228, 1.229,
      1.23, 1.231, 1.232, 1.233, 1.234, 1.235, 1.236, 1.237, 1.238, 1.239, 1.24,
      1.241, 1.242, 1.243, 1.244, 1.245, 1.246, 1.247, 1.248, 1.249, 1.25,
      1.2510000000000001, 1.252, 1.2530000000000001, 1.254, 1.2550000000000001,
      1.256, 1.2570000000000001, 1.258, 1.2590000000000001, 1.26,
      1.2610000000000001, 1.262, 1.2630000000000001, 1.264, 1.2650000000000001,
      1.266, 1.2670000000000001, 1.268, 1.2690000000000001, 1.27,
      1.2710000000000001, 1.272, 1.2730000000000001, 1.274, 1.2750000000000001,
      1.276, 1.2770000000000001, 1.278, 1.2790000000000001, 1.28, 1.281, 1.282,
      1.283, 1.284, 1.285, 1.286, 1.287, 1.288, 1.289, 1.29, 1.291, 1.292, 1.293,
      1.294, 1.295, 1.296, 1.297, 1.298, 1.299, 1.3, 1.301, 1.302, 1.303, 1.304,
      1.305, 1.306, 1.307, 1.308, 1.309, 1.31, 1.311, 1.312, 1.313, 1.314, 1.315,
      1.316, 1.317, 1.318, 1.319, 1.32, 1.321, 1.322, 1.323, 1.324, 1.325, 1.326,
      1.327, 1.328, 1.329, 1.33, 1.331, 1.332, 1.333, 1.334, 1.335, 1.336, 1.337,
      1.338, 1.339, 1.34, 1.341, 1.342, 1.343, 1.344, 1.345, 1.346, 1.347, 1.348,
      1.349, 1.35, 1.351, 1.352, 1.353, 1.354, 1.355, 1.356, 1.357, 1.358, 1.359,
      1.36, 1.361, 1.362, 1.363, 1.364, 1.365, 1.366, 1.367, 1.368, 1.369, 1.37,
      1.371, 1.372, 1.373, 1.374, 1.375, 1.3760000000000001, 1.377,
      1.3780000000000001, 1.379, 1.3800000000000001, 1.381, 1.3820000000000001,
      1.383, 1.3840000000000001, 1.385, 1.3860000000000001, 1.387,
      1.3880000000000001, 1.389, 1.3900000000000001, 1.391, 1.3920000000000001,
      1.393, 1.3940000000000001, 1.395, 1.3960000000000001, 1.397,
      1.3980000000000001, 1.399, 1.4000000000000001, 1.401, 1.4020000000000001,
      1.403, 1.4040000000000001, 1.405, 1.4060000000000001, 1.407, 1.408, 1.409,
      1.41, 1.411, 1.412, 1.413, 1.414, 1.415, 1.416, 1.417, 1.418, 1.419, 1.42,
      1.421, 1.422, 1.423, 1.424, 1.425, 1.426, 1.427, 1.428, 1.429, 1.43, 1.431,
      1.432, 1.433, 1.434, 1.435, 1.436, 1.437, 1.438, 1.439, 1.44, 1.441, 1.442,
      1.443, 1.444, 1.445, 1.446, 1.447, 1.448, 1.449, 1.45, 1.451, 1.452, 1.453,
      1.454, 1.455, 1.456, 1.457, 1.458, 1.459, 1.46, 1.461, 1.462, 1.463, 1.464,
      1.465, 1.466, 1.467, 1.468, 1.469, 1.47, 1.471, 1.472, 1.473, 1.474, 1.475,
      1.476, 1.477, 1.478, 1.479, 1.48, 1.481, 1.482, 1.483, 1.484, 1.485, 1.486,
      1.487, 1.488, 1.489, 1.49, 1.491, 1.492, 1.493, 1.494, 1.495, 1.496, 1.497,
      1.498, 1.499, 1.5, 1.5010000000000001, 1.502, 1.5030000000000001, 1.504,
      1.5050000000000001, 1.506, 1.5070000000000001, 1.508, 1.5090000000000001,
      1.51, 1.5110000000000001, 1.512, 1.5130000000000001, 1.514,
      1.5150000000000001, 1.516, 1.5170000000000001, 1.518, 1.5190000000000001,
      1.52, 1.5210000000000001, 1.522, 1.5230000000000001, 1.524,
      1.5250000000000001, 1.526, 1.5270000000000001, 1.528, 1.5290000000000001,
      1.53, 1.5310000000000001, 1.532, 1.5330000000000001, 1.534,
      1.5350000000000001, 1.536, 1.537, 1.538, 1.539, 1.54, 1.541, 1.542, 1.543,
      1.544, 1.545, 1.546, 1.547, 1.548, 1.549, 1.55, 1.551, 1.552, 1.553, 1.554,
      1.555, 1.556, 1.557, 1.558, 1.559, 1.56, 1.561, 1.562, 1.563, 1.564, 1.565,
      1.566, 1.567, 1.568, 1.569, 1.57, 1.571, 1.572, 1.573, 1.574, 1.575, 1.576,
      1.577, 1.578, 1.579, 1.58, 1.581, 1.582, 1.583, 1.584, 1.585, 1.586, 1.587,
      1.588, 1.589, 1.59, 1.591, 1.592, 1.593, 1.594, 1.595, 1.596, 1.597, 1.598,
      1.599, 1.6, 1.601, 1.602, 1.603, 1.604, 1.605, 1.606, 1.607, 1.608, 1.609,
      1.61, 1.611, 1.612, 1.613, 1.614, 1.615, 1.616, 1.617, 1.618, 1.619, 1.62,
      1.621, 1.622, 1.623, 1.624, 1.625, 1.6260000000000001, 1.627,
      1.6280000000000001, 1.629, 1.6300000000000001, 1.631, 1.6320000000000001,
      1.633, 1.6340000000000001, 1.635, 1.6360000000000001, 1.637,
      1.6380000000000001, 1.639, 1.6400000000000001, 1.641, 1.6420000000000001,
      1.643, 1.6440000000000001, 1.645, 1.6460000000000001, 1.647,
      1.6480000000000001, 1.649, 1.6500000000000001, 1.651, 1.6520000000000001,
      1.653, 1.6540000000000001, 1.655, 1.6560000000000001, 1.657,
      1.6580000000000001, 1.659, 1.6600000000000001, 1.661, 1.6620000000000001,
      1.663, 1.6640000000000001, 1.665, 1.666, 1.667, 1.668, 1.669, 1.67, 1.671,
      1.672, 1.673, 1.674, 1.675, 1.676, 1.677, 1.678, 1.679, 1.68, 1.681, 1.682,
      1.683, 1.684, 1.685, 1.686, 1.687, 1.688, 1.689, 1.69, 1.691, 1.692, 1.693,
      1.694, 1.695, 1.696, 1.697, 1.698, 1.699, 1.7, 1.701, 1.702, 1.703, 1.704,
      1.705, 1.706, 1.707, 1.708, 1.709, 1.71, 1.711, 1.712, 1.713, 1.714, 1.715,
      1.716, 1.717, 1.718, 1.719, 1.72, 1.721, 1.722, 1.723, 1.724, 1.725, 1.726,
      1.727, 1.728, 1.729, 1.73, 1.731, 1.732, 1.733, 1.734, 1.735, 1.736, 1.737,
      1.738, 1.739, 1.74, 1.741, 1.742, 1.743, 1.744, 1.745, 1.746, 1.747, 1.748,
      1.749, 1.75, 1.7510000000000001, 1.752, 1.7530000000000001, 1.754,
      1.7550000000000001, 1.756, 1.7570000000000001, 1.758, 1.7590000000000001,
      1.76, 1.7610000000000001, 1.762, 1.7630000000000001, 1.764,
      1.7650000000000001, 1.766, 1.7670000000000001, 1.768, 1.7690000000000001,
      1.77, 1.7710000000000001, 1.772, 1.7730000000000001, 1.774,
      1.7750000000000001, 1.776, 1.7770000000000001, 1.778, 1.7790000000000001,
      1.78, 1.7810000000000001, 1.782, 1.7830000000000001, 1.784,
      1.7850000000000001, 1.786, 1.7870000000000001, 1.788, 1.7890000000000001,
      1.79, 1.7910000000000001, 1.792, 1.793, 1.794, 1.795, 1.796, 1.797, 1.798,
      1.799, 1.8, 1.801, 1.802, 1.803, 1.804, 1.805, 1.806, 1.807, 1.808, 1.809,
      1.81, 1.811, 1.812, 1.813, 1.814, 1.815, 1.816, 1.817, 1.818, 1.819, 1.82,
      1.821, 1.822, 1.823, 1.824, 1.825, 1.826, 1.827, 1.828, 1.829, 1.83, 1.831,
      1.832, 1.833, 1.834, 1.835, 1.836, 1.837, 1.838, 1.839, 1.84, 1.841, 1.842,
      1.843, 1.844, 1.845, 1.846, 1.847, 1.848, 1.849, 1.85, 1.851, 1.852, 1.853,
      1.854, 1.855, 1.856, 1.857, 1.858, 1.859, 1.86, 1.861, 1.862, 1.863, 1.864,
      1.865, 1.866, 1.867, 1.868, 1.869, 1.87, 1.871, 1.872, 1.873, 1.874, 1.875,
      1.8760000000000001, 1.877, 1.8780000000000001, 1.879, 1.8800000000000001,
      1.881, 1.8820000000000001, 1.883, 1.8840000000000001, 1.885,
      1.8860000000000001, 1.887, 1.8880000000000001, 1.889, 1.8900000000000001,
      1.891, 1.8920000000000001, 1.893, 1.8940000000000001, 1.895,
      1.8960000000000001, 1.897, 1.8980000000000001, 1.899, 1.9000000000000001,
      1.901, 1.9020000000000001, 1.903, 1.9040000000000001, 1.905,
      1.9060000000000001, 1.907, 1.9080000000000001, 1.909, 1.9100000000000001,
      1.911, 1.9120000000000001, 1.913, 1.9140000000000001, 1.915,
      1.9160000000000001, 1.917, 1.9180000000000001, 1.919, 1.92, 1.921, 1.922,
      1.923, 1.924, 1.925, 1.926, 1.927, 1.928, 1.929, 1.93, 1.931, 1.932, 1.933,
      1.934, 1.935, 1.936, 1.937, 1.938, 1.939, 1.94, 1.941, 1.942, 1.943, 1.944,
      1.945, 1.946, 1.947, 1.948, 1.949, 1.95, 1.951, 1.952, 1.953, 1.954, 1.955,
      1.956, 1.957, 1.958, 1.959, 1.96, 1.961, 1.962, 1.963, 1.964, 1.965, 1.966,
      1.967, 1.968, 1.969, 1.97, 1.971, 1.972, 1.973, 1.974, 1.975, 1.976, 1.977,
      1.978, 1.979, 1.98, 1.981, 1.982, 1.983, 1.984, 1.985, 1.986, 1.987, 1.988,
      1.989, 1.99, 1.991, 1.992, 1.993, 1.994, 1.995, 1.996, 1.997, 1.998, 1.999,
      2.0, 2.001, 2.0020000000000002, 2.003, 2.004, 2.005, 2.0060000000000002,
      2.007, 2.008, 2.009, 2.0100000000000002, 2.011, 2.012, 2.013,
      2.0140000000000002, 2.015, 2.016, 2.017, 2.0180000000000002, 2.019, 2.02,
      2.021, 2.0220000000000002, 2.023, 2.024, 2.025, 2.0260000000000002, 2.027,
      2.028, 2.029, 2.0300000000000002, 2.031, 2.032, 2.033, 2.0340000000000003,
      2.035, 2.036, 2.037, 2.0380000000000003, 2.039, 2.04, 2.041,
      2.0420000000000003, 2.043, 2.044, 2.045, 2.0460000000000003, 2.047, 2.048,
      2.049, 2.05, 2.051, 2.052, 2.053, 2.054, 2.055, 2.056, 2.057, 2.058, 2.059,
      2.06, 2.061, 2.062, 2.063, 2.064, 2.065, 2.066, 2.067, 2.068, 2.069, 2.07,
      2.071, 2.072, 2.073, 2.074, 2.075, 2.076, 2.077, 2.078, 2.079, 2.08, 2.081,
      2.082, 2.083, 2.084, 2.085, 2.086, 2.087, 2.088, 2.089, 2.09, 2.091, 2.092,
      2.093, 2.094, 2.095, 2.096, 2.097, 2.098, 2.099, 2.1, 2.101, 2.102, 2.103,
      2.104, 2.105, 2.106, 2.107, 2.108, 2.109, 2.11, 2.111, 2.112, 2.113, 2.114,
      2.115, 2.116, 2.117, 2.118, 2.119, 2.12, 2.121, 2.122, 2.123, 2.124, 2.125,
      2.126, 2.1270000000000002, 2.128, 2.129, 2.13, 2.1310000000000002, 2.132,
      2.133, 2.134, 2.1350000000000002, 2.136, 2.137, 2.138, 2.1390000000000002,
      2.14, 2.141, 2.142, 2.1430000000000002, 2.144, 2.145, 2.146,
      2.1470000000000002, 2.148, 2.149, 2.15, 2.1510000000000002, 2.152, 2.153,
      2.154, 2.1550000000000002, 2.156, 2.157, 2.158, 2.1590000000000003, 2.16,
      2.161, 2.162, 2.1630000000000003, 2.164, 2.165, 2.166, 2.1670000000000003,
      2.168, 2.169, 2.17, 2.1710000000000003, 2.172, 2.173, 2.174,
      2.1750000000000003, 2.176, 2.177, 2.178, 2.179, 2.18, 2.181, 2.182, 2.183,
      2.184, 2.185, 2.186, 2.187, 2.188, 2.189, 2.19, 2.191, 2.192, 2.193, 2.194,
      2.195, 2.196, 2.197, 2.198, 2.199, 2.2, 2.201, 2.202, 2.203, 2.204, 2.205,
      2.206, 2.207, 2.208, 2.209, 2.21, 2.211, 2.212, 2.213, 2.214, 2.215, 2.216,
      2.217, 2.218, 2.219, 2.22, 2.221, 2.222, 2.223, 2.224, 2.225, 2.226, 2.227,
      2.228, 2.229, 2.23, 2.231, 2.232, 2.233, 2.234, 2.235, 2.236, 2.237, 2.238,
      2.239, 2.24, 2.241, 2.242, 2.243, 2.244, 2.245, 2.246, 2.247, 2.248, 2.249,
      2.25, 2.251, 2.2520000000000002, 2.253, 2.254, 2.255, 2.2560000000000002,
      2.257, 2.258, 2.259, 2.2600000000000002, 2.261, 2.262, 2.263,
      2.2640000000000002, 2.265, 2.266, 2.267, 2.2680000000000002, 2.269, 2.27,
      2.271, 2.2720000000000002, 2.273, 2.274, 2.275, 2.2760000000000002, 2.277,
      2.278, 2.279, 2.2800000000000002, 2.281, 2.282, 2.283, 2.2840000000000003,
      2.285, 2.286, 2.287, 2.2880000000000003, 2.289, 2.29, 2.291,
      2.2920000000000003, 2.293, 2.294, 2.295, 2.2960000000000003, 2.297, 2.298,
      2.299, 2.3000000000000003, 2.301, 2.302, 2.303, 2.3040000000000003, 2.305,
      2.306, 2.307, 2.308, 2.309, 2.31, 2.311, 2.312, 2.313, 2.314, 2.315, 2.316,
      2.317, 2.318, 2.319, 2.32, 2.321, 2.322, 2.323, 2.324, 2.325, 2.326, 2.327,
      2.328, 2.329, 2.33, 2.331, 2.332, 2.333, 2.334, 2.335, 2.336, 2.337, 2.338,
      2.339, 2.34, 2.341, 2.342, 2.343, 2.344, 2.345, 2.346, 2.347, 2.348, 2.349,
      2.35, 2.351, 2.352, 2.353, 2.354, 2.355, 2.356, 2.357, 2.358, 2.359, 2.36,
      2.361, 2.362, 2.363, 2.364, 2.365, 2.366, 2.367, 2.368, 2.369, 2.37, 2.371,
      2.372, 2.373, 2.374, 2.375, 2.376, 2.3770000000000002, 2.378, 2.379, 2.38,
      2.3810000000000002, 2.382, 2.383, 2.384, 2.3850000000000002, 2.386, 2.387,
      2.388, 2.3890000000000002, 2.39, 2.391, 2.392, 2.3930000000000002, 2.394,
      2.395, 2.396, 2.3970000000000002, 2.398, 2.399, 2.4, 2.4010000000000002,
      2.402, 2.403, 2.404, 2.4050000000000002, 2.406, 2.407, 2.408,
      2.4090000000000003, 2.41, 2.411, 2.412, 2.4130000000000003, 2.414, 2.415,
      2.416, 2.4170000000000003, 2.418, 2.419, 2.42, 2.4210000000000003, 2.422,
      2.423, 2.424, 2.4250000000000003, 2.426, 2.427, 2.428, 2.4290000000000003,
      2.43, 2.431, 2.432, 2.433, 2.434, 2.435, 2.436, 2.437, 2.438, 2.439, 2.44,
      2.441, 2.442, 2.443, 2.444, 2.445, 2.446, 2.447, 2.448, 2.449, 2.45, 2.451,
      2.452, 2.453, 2.454, 2.455, 2.456, 2.457, 2.458, 2.459, 2.46, 2.461, 2.462,
      2.463, 2.464, 2.465, 2.466, 2.467, 2.468, 2.469, 2.47, 2.471, 2.472, 2.473,
      2.474, 2.475, 2.476, 2.477, 2.478, 2.479, 2.48, 2.481, 2.482, 2.483, 2.484,
      2.485, 2.486, 2.487, 2.488, 2.489, 2.49, 2.491, 2.492, 2.493, 2.494, 2.495,
      2.496, 2.497, 2.498, 2.499, 2.5, 2.501, 2.5020000000000002, 2.503, 2.504,
      2.505, 2.5060000000000002, 2.507, 2.508, 2.509, 2.5100000000000002, 2.511,
      2.512, 2.513, 2.5140000000000002, 2.515, 2.516, 2.517, 2.5180000000000002,
      2.519, 2.52, 2.521, 2.5220000000000002, 2.523, 2.524, 2.525,
      2.5260000000000002, 2.527, 2.528, 2.529, 2.5300000000000002, 2.531, 2.532,
      2.533, 2.5340000000000003, 2.535, 2.536, 2.537, 2.5380000000000003, 2.539,
      2.54, 2.541, 2.5420000000000003, 2.543, 2.544, 2.545, 2.5460000000000003,
      2.547, 2.548, 2.549, 2.5500000000000003, 2.551, 2.552, 2.553,
      2.5540000000000003, 2.555, 2.556, 2.557, 2.5580000000000003, 2.559, 2.56,
      2.561, 2.562, 2.563, 2.564, 2.565, 2.566, 2.567, 2.568, 2.569, 2.57, 2.571,
      2.572, 2.573, 2.574, 2.575, 2.576, 2.577, 2.578, 2.579, 2.58, 2.581, 2.582,
      2.583, 2.584, 2.585, 2.586, 2.587, 2.588, 2.589, 2.59, 2.591, 2.592, 2.593,
      2.594, 2.595, 2.596, 2.597, 2.598, 2.599, 2.6, 2.601, 2.602, 2.603, 2.604,
      2.605, 2.606, 2.607, 2.608, 2.609, 2.61, 2.611, 2.612, 2.613, 2.614, 2.615,
      2.616, 2.617, 2.618, 2.619, 2.62, 2.621, 2.622, 2.623, 2.624, 2.625, 2.626,
      2.6270000000000002, 2.628, 2.629, 2.63, 2.6310000000000002, 2.632, 2.633,
      2.634, 2.6350000000000002, 2.636, 2.637, 2.638, 2.6390000000000002, 2.64,
      2.641, 2.642, 2.6430000000000002, 2.644, 2.645, 2.646, 2.6470000000000002,
      2.648, 2.649, 2.65, 2.6510000000000002, 2.652, 2.653, 2.654,
      2.6550000000000002, 2.656, 2.657, 2.658, 2.6590000000000003, 2.66, 2.661,
      2.662, 2.6630000000000003, 2.664, 2.665, 2.666, 2.6670000000000003, 2.668,
      2.669, 2.67, 2.6710000000000003, 2.672, 2.673, 2.674, 2.6750000000000003,
      2.676, 2.677, 2.678, 2.6790000000000003, 2.68, 2.681, 2.682,
      2.6830000000000003, 2.684, 2.685, 2.686, 2.6870000000000003, 2.688, 2.689,
      2.69, 2.691, 2.692, 2.693, 2.694, 2.695, 2.696, 2.697, 2.698, 2.699, 2.7,
      2.701, 2.702, 2.703, 2.704, 2.705, 2.706, 2.707, 2.708, 2.709, 2.71, 2.711,
      2.712, 2.713, 2.714, 2.715, 2.716, 2.717, 2.718, 2.719, 2.72, 2.721, 2.722,
      2.723, 2.724, 2.725, 2.726, 2.727, 2.728, 2.729, 2.73, 2.731, 2.732, 2.733,
      2.734, 2.735, 2.736, 2.737, 2.738, 2.739, 2.74, 2.741, 2.742, 2.743, 2.744,
      2.745, 2.746, 2.747, 2.748, 2.749, 2.75, 2.751, 2.7520000000000002, 2.753,
      2.754, 2.755, 2.7560000000000002, 2.757, 2.758, 2.759, 2.7600000000000002,
      2.761, 2.762, 2.763, 2.7640000000000002, 2.765, 2.766, 2.767,
      2.7680000000000002, 2.769, 2.77, 2.771, 2.7720000000000002, 2.773, 2.774,
      2.775, 2.7760000000000002, 2.777, 2.778, 2.779, 2.7800000000000002, 2.781,
      2.782, 2.783, 2.7840000000000003, 2.785, 2.786, 2.787, 2.7880000000000003,
      2.789, 2.79, 2.791, 2.7920000000000003, 2.793, 2.794, 2.795,
      2.7960000000000003, 2.797, 2.798, 2.799, 2.8000000000000003, 2.801, 2.802,
      2.803, 2.8040000000000003, 2.805, 2.806, 2.807, 2.8080000000000003, 2.809,
      2.81, 2.811, 2.8120000000000003, 2.813, 2.814, 2.815, 2.816, 2.817, 2.818,
      2.819, 2.82, 2.821, 2.822, 2.823, 2.824, 2.825, 2.826, 2.827, 2.828, 2.829,
      2.83, 2.831, 2.832, 2.833, 2.834, 2.835, 2.836, 2.837, 2.838, 2.839, 2.84,
      2.841, 2.842, 2.843, 2.844, 2.845, 2.846, 2.847, 2.848, 2.849, 2.85, 2.851,
      2.852, 2.853, 2.854, 2.855, 2.856, 2.857, 2.858, 2.859, 2.86, 2.861, 2.862,
      2.863, 2.864, 2.865, 2.866, 2.867, 2.868, 2.869, 2.87, 2.871, 2.872, 2.873,
      2.874, 2.875, 2.876, 2.8770000000000002, 2.878, 2.879, 2.88,
      2.8810000000000002, 2.882, 2.883, 2.884, 2.8850000000000002, 2.886, 2.887,
      2.888, 2.8890000000000002, 2.89, 2.891, 2.892, 2.8930000000000002, 2.894,
      2.895, 2.896, 2.8970000000000002, 2.898, 2.899, 2.9, 2.9010000000000002,
      2.902, 2.903, 2.904, 2.9050000000000002, 2.906, 2.907, 2.908,
      2.9090000000000003, 2.91, 2.911, 2.912, 2.9130000000000003, 2.914, 2.915,
      2.916, 2.9170000000000003, 2.918, 2.919, 2.92, 2.9210000000000003, 2.922,
      2.923, 2.924, 2.9250000000000003, 2.926, 2.927, 2.928, 2.9290000000000003,
      2.93, 2.931, 2.932, 2.9330000000000003, 2.934, 2.935, 2.936,
      2.9370000000000003, 2.938, 2.939, 2.94, 2.9410000000000003, 2.942, 2.943,
      2.944, 2.945, 2.946, 2.947, 2.948, 2.949, 2.95, 2.951, 2.952, 2.953, 2.954,
      2.955, 2.956, 2.957, 2.958, 2.959, 2.96, 2.961, 2.962, 2.963, 2.964, 2.965,
      2.966, 2.967, 2.968, 2.969, 2.97, 2.971, 2.972, 2.973, 2.974, 2.975, 2.976,
      2.977, 2.978, 2.979, 2.98, 2.981, 2.982, 2.983, 2.984, 2.985, 2.986, 2.987,
      2.988, 2.989, 2.99, 2.991, 2.992, 2.993, 2.994, 2.995, 2.996, 2.997, 2.998,
      2.999, 3.0, 3.001, 3.0020000000000002, 3.003, 3.004, 3.005,
      3.0060000000000002, 3.007, 3.008, 3.009, 3.0100000000000002, 3.011, 3.012,
      3.013, 3.0140000000000002, 3.015, 3.016, 3.017, 3.0180000000000002, 3.019,
      3.02, 3.021, 3.0220000000000002, 3.023, 3.024, 3.025, 3.0260000000000002,
      3.027, 3.028, 3.029, 3.0300000000000002, 3.031, 3.032, 3.033,
      3.0340000000000003, 3.035, 3.036, 3.037, 3.0380000000000003, 3.039, 3.04,
      3.041, 3.0420000000000003, 3.043, 3.044, 3.045, 3.0460000000000003, 3.047,
      3.048, 3.049, 3.0500000000000003, 3.051, 3.052, 3.053, 3.0540000000000003,
      3.055, 3.056, 3.057, 3.0580000000000003, 3.059, 3.06, 3.061,
      3.0620000000000003, 3.063, 3.064, 3.065, 3.0660000000000003, 3.067, 3.068,
      3.069, 3.0700000000000003, 3.071, 3.072, 3.073, 3.074, 3.075, 3.076, 3.077,
      3.078, 3.079, 3.08, 3.081, 3.082, 3.083, 3.084, 3.085, 3.086, 3.087, 3.088,
      3.089, 3.09, 3.091, 3.092, 3.093, 3.094, 3.095, 3.096, 3.097, 3.098, 3.099,
      3.1, 3.101, 3.102, 3.103, 3.104, 3.105, 3.106, 3.107, 3.108, 3.109, 3.11,
      3.111, 3.112, 3.113, 3.114, 3.115, 3.116, 3.117, 3.118, 3.119, 3.12, 3.121,
      3.122, 3.123, 3.124, 3.125, 3.126, 3.1270000000000002, 3.128, 3.129, 3.13,
      3.1310000000000002, 3.132, 3.133, 3.134, 3.1350000000000002, 3.136, 3.137,
      3.138, 3.1390000000000002, 3.14, 3.141, 3.142, 3.1430000000000002, 3.144,
      3.145, 3.146, 3.1470000000000002, 3.148, 3.149, 3.15, 3.1510000000000002,
      3.152, 3.153, 3.154, 3.1550000000000002, 3.156, 3.157, 3.158,
      3.1590000000000003, 3.16, 3.161, 3.162, 3.1630000000000003, 3.164, 3.165,
      3.166, 3.1670000000000003, 3.168, 3.169, 3.17, 3.1710000000000003, 3.172,
      3.173, 3.174, 3.1750000000000003, 3.176, 3.177, 3.178, 3.1790000000000003,
      3.18, 3.181, 3.182, 3.1830000000000003, 3.184, 3.185, 3.186,
      3.1870000000000003, 3.188, 3.189, 3.19, 3.1910000000000003, 3.192, 3.193,
      3.194, 3.1950000000000003, 3.196, 3.197, 3.198, 3.1990000000000003, 3.2,
      3.201, 3.202, 3.203, 3.204, 3.205, 3.206, 3.207, 3.208, 3.209, 3.21, 3.211,
      3.212, 3.213, 3.214, 3.215, 3.216, 3.217, 3.218, 3.219, 3.22, 3.221, 3.222,
      3.223, 3.224, 3.225, 3.226, 3.227, 3.228, 3.229, 3.23, 3.231, 3.232, 3.233,
      3.234, 3.235, 3.236, 3.237, 3.238, 3.239, 3.24, 3.241, 3.242, 3.243, 3.244,
      3.245, 3.246, 3.247, 3.248, 3.249, 3.25, 3.251, 3.2520000000000002, 3.253,
      3.254, 3.255, 3.2560000000000002, 3.257, 3.258, 3.259, 3.2600000000000002,
      3.261, 3.262, 3.263, 3.2640000000000002, 3.265, 3.266, 3.267,
      3.2680000000000002, 3.269, 3.27, 3.271, 3.2720000000000002, 3.273, 3.274,
      3.275, 3.2760000000000002, 3.277, 3.278, 3.279, 3.2800000000000002, 3.281,
      3.282, 3.283, 3.2840000000000003, 3.285, 3.286, 3.287, 3.2880000000000003,
      3.289, 3.29, 3.291, 3.2920000000000003, 3.293, 3.294, 3.295,
      3.2960000000000003, 3.297, 3.298, 3.299, 3.3000000000000003, 3.301, 3.302,
      3.303, 3.3040000000000003, 3.305, 3.306, 3.307, 3.3080000000000003, 3.309,
      3.31, 3.311, 3.3120000000000003, 3.313, 3.314, 3.315, 3.3160000000000003,
      3.317, 3.318, 3.319, 3.3200000000000003, 3.321, 3.322, 3.323,
      3.3240000000000003, 3.325, 3.326, 3.327, 3.3280000000000003, 3.329, 3.33,
      3.331, 3.332, 3.333, 3.334, 3.335, 3.336, 3.337, 3.338, 3.339, 3.34, 3.341,
      3.342, 3.343, 3.344, 3.345, 3.346, 3.347, 3.348, 3.349, 3.35, 3.351, 3.352,
      3.353, 3.354, 3.355, 3.356, 3.357, 3.358, 3.359, 3.36, 3.361, 3.362, 3.363,
      3.364, 3.365, 3.366, 3.367, 3.368, 3.369, 3.37, 3.371, 3.372, 3.373, 3.374,
      3.375, 3.376, 3.3770000000000002, 3.378, 3.379, 3.38, 3.3810000000000002,
      3.382, 3.383, 3.384, 3.3850000000000002, 3.386, 3.387, 3.388,
      3.3890000000000002, 3.39, 3.391, 3.392, 3.3930000000000002, 3.394, 3.395,
      3.396, 3.3970000000000002, 3.398, 3.399, 3.4, 3.4010000000000002, 3.402,
      3.403, 3.404, 3.4050000000000002, 3.406, 3.407, 3.408, 3.4090000000000003,
      3.41, 3.411, 3.412, 3.4130000000000003, 3.414, 3.415, 3.416,
      3.4170000000000003, 3.418, 3.419, 3.42, 3.4210000000000003, 3.422, 3.423,
      3.424, 3.4250000000000003, 3.426, 3.427, 3.428, 3.4290000000000003, 3.43,
      3.431, 3.432, 3.4330000000000003, 3.434, 3.435, 3.436, 3.4370000000000003,
      3.438, 3.439, 3.44, 3.4410000000000003, 3.442, 3.443, 3.444,
      3.4450000000000003, 3.446, 3.447, 3.448, 3.4490000000000003, 3.45, 3.451,
      3.452, 3.4530000000000003, 3.454, 3.455, 3.456, 3.457, 3.458, 3.459, 3.46,
      3.461, 3.462, 3.463, 3.464, 3.465, 3.466, 3.467, 3.468, 3.469, 3.47, 3.471,
      3.472, 3.473, 3.474, 3.475, 3.476, 3.477, 3.478, 3.479, 3.48, 3.481, 3.482,
      3.483, 3.484, 3.485, 3.486, 3.487, 3.488, 3.489, 3.49, 3.491, 3.492, 3.493,
      3.494, 3.495, 3.496, 3.497, 3.498, 3.499, 3.5, 3.501, 3.5020000000000002,
      3.503, 3.504, 3.505, 3.5060000000000002, 3.507, 3.508, 3.509,
      3.5100000000000002, 3.511, 3.512, 3.513, 3.5140000000000002, 3.515, 3.516,
      3.517, 3.5180000000000002, 3.519, 3.52, 3.521, 3.5220000000000002, 3.523,
      3.524, 3.525, 3.5260000000000002, 3.527, 3.528, 3.529, 3.5300000000000002,
      3.531, 3.532, 3.533, 3.5340000000000003, 3.535, 3.536, 3.537,
      3.5380000000000003, 3.539, 3.54, 3.541, 3.5420000000000003, 3.543, 3.544,
      3.545, 3.5460000000000003, 3.547, 3.548, 3.549, 3.5500000000000003, 3.551,
      3.552, 3.553, 3.5540000000000003, 3.555, 3.556, 3.557, 3.5580000000000003,
      3.559, 3.56, 3.561, 3.5620000000000003, 3.563, 3.564, 3.565,
      3.5660000000000003, 3.567, 3.568, 3.569, 3.5700000000000003, 3.571, 3.572,
      3.573, 3.5740000000000003, 3.575, 3.576, 3.577, 3.5780000000000003, 3.579,
      3.58, 3.581, 3.5820000000000003, 3.583, 3.584, 3.585, 3.586, 3.587, 3.588,
      3.589, 3.59, 3.591, 3.592, 3.593, 3.594, 3.595, 3.596, 3.597, 3.598, 3.599,
      3.6, 3.601, 3.602, 3.603, 3.604, 3.605, 3.606, 3.607, 3.608, 3.609, 3.61,
      3.611, 3.612, 3.613, 3.614, 3.615, 3.616, 3.617, 3.618, 3.619, 3.62, 3.621,
      3.622, 3.623, 3.624, 3.625, 3.626, 3.6270000000000002, 3.628, 3.629, 3.63,
      3.6310000000000002, 3.632, 3.633, 3.634, 3.6350000000000002, 3.636, 3.637,
      3.638, 3.6390000000000002, 3.64, 3.641, 3.642, 3.6430000000000002, 3.644,
      3.645, 3.646, 3.6470000000000002, 3.648, 3.649, 3.65, 3.6510000000000002,
      3.652, 3.653, 3.654, 3.6550000000000002, 3.656, 3.657, 3.658,
      3.6590000000000003, 3.66, 3.661, 3.662, 3.6630000000000003, 3.664, 3.665,
      3.666, 3.6670000000000003, 3.668, 3.669, 3.67, 3.6710000000000003, 3.672,
      3.673, 3.674, 3.6750000000000003, 3.676, 3.677, 3.678, 3.6790000000000003,
      3.68, 3.681, 3.682, 3.6830000000000003, 3.684, 3.685, 3.686,
      3.6870000000000003, 3.688, 3.689, 3.69, 3.6910000000000003, 3.692, 3.693,
      3.694, 3.6950000000000003, 3.696, 3.697, 3.698, 3.6990000000000003, 3.7,
      3.701, 3.702, 3.7030000000000003, 3.704, 3.705, 3.706, 3.7070000000000003,
      3.708, 3.709, 3.71, 3.7110000000000003, 3.712, 3.713, 3.714, 3.715, 3.716,
      3.717, 3.718, 3.719, 3.72, 3.721, 3.722, 3.723, 3.724, 3.725, 3.726, 3.727,
      3.728, 3.729, 3.73, 3.731, 3.732, 3.733, 3.734, 3.735, 3.736, 3.737, 3.738,
      3.739, 3.74, 3.741, 3.742, 3.743, 3.744, 3.745, 3.746, 3.747, 3.748, 3.749,
      3.75, 3.751, 3.7520000000000002, 3.753, 3.754, 3.755, 3.7560000000000002,
      3.757, 3.758, 3.759, 3.7600000000000002, 3.761, 3.762, 3.763,
      3.7640000000000002, 3.765, 3.766, 3.767, 3.7680000000000002, 3.769, 3.77,
      3.771, 3.7720000000000002, 3.773, 3.774, 3.775, 3.7760000000000002, 3.777,
      3.778, 3.779, 3.7800000000000002, 3.781, 3.782, 3.783, 3.7840000000000003,
      3.785, 3.786, 3.787, 3.7880000000000003, 3.789, 3.79, 3.791,
      3.7920000000000003, 3.793, 3.794, 3.795, 3.7960000000000003, 3.797, 3.798,
      3.799, 3.8000000000000003, 3.801, 3.802, 3.803, 3.8040000000000003, 3.805,
      3.806, 3.807, 3.8080000000000003, 3.809, 3.81, 3.811, 3.8120000000000003,
      3.813, 3.814, 3.815, 3.8160000000000003, 3.817, 3.818, 3.819,
      3.8200000000000003, 3.821, 3.822, 3.823, 3.8240000000000003, 3.825, 3.826,
      3.827, 3.8280000000000003, 3.829, 3.83, 3.831, 3.8320000000000003, 3.833,
      3.834, 3.835, 3.8360000000000003, 3.837, 3.838, 3.839, 3.84, 3.841, 3.842,
      3.843, 3.844, 3.845, 3.846, 3.847, 3.848, 3.849, 3.85, 3.851, 3.852, 3.853,
      3.854, 3.855, 3.856, 3.857, 3.858, 3.859, 3.86, 3.861, 3.862, 3.863, 3.864,
      3.865, 3.866, 3.867, 3.868, 3.869, 3.87, 3.871, 3.872, 3.873, 3.874, 3.875,
      3.876, 3.8770000000000002, 3.878, 3.879, 3.88, 3.8810000000000002, 3.882,
      3.883, 3.884, 3.8850000000000002, 3.886, 3.887, 3.888, 3.8890000000000002,
      3.89, 3.891, 3.892, 3.8930000000000002, 3.894, 3.895, 3.896,
      3.8970000000000002, 3.898, 3.899, 3.9, 3.9010000000000002, 3.902, 3.903,
      3.904, 3.9050000000000002, 3.906, 3.907, 3.908, 3.9090000000000003, 3.91,
      3.911, 3.912, 3.9130000000000003, 3.914, 3.915, 3.916, 3.9170000000000003,
      3.918, 3.919, 3.92, 3.9210000000000003, 3.922, 3.923, 3.924,
      3.9250000000000003, 3.926, 3.927, 3.928, 3.9290000000000003, 3.93, 3.931,
      3.932, 3.9330000000000003, 3.934, 3.935, 3.936, 3.9370000000000003, 3.938,
      3.939, 3.94, 3.9410000000000003, 3.942, 3.943, 3.944, 3.9450000000000003,
      3.946, 3.947, 3.948, 3.9490000000000003, 3.95, 3.951, 3.952,
      3.9530000000000003, 3.954, 3.955, 3.956, 3.9570000000000003, 3.958, 3.959,
      3.96, 3.9610000000000003, 3.962, 3.963, 3.964, 3.9650000000000003, 3.966,
      3.967, 3.968, 3.969, 3.97, 3.971, 3.972, 3.973, 3.974, 3.975, 3.976, 3.977,
      3.978, 3.979, 3.98, 3.981, 3.982, 3.983, 3.984, 3.985, 3.986, 3.987, 3.988,
      3.989, 3.99, 3.991, 3.992, 3.993, 3.994, 3.995, 3.996, 3.997, 3.998, 3.999,
      4.0, 4.001, 4.002, 4.003, 4.0040000000000004, 4.005, 4.006, 4.007, 4.008,
      4.009 } ;

    static real_T pDataValues[] = { 0.0, 0.00025, 0.001, 0.00225, 0.004,
      0.0062499999999999995, 0.009, 0.01225, 0.016, 0.02025,
      0.024999999999999998, 0.030249999999999992, 0.036, 0.04225, 0.049,
      0.056249999999999994, 0.064, 0.072250000000000009, 0.081, 0.09025,
      0.099999999999999992, 0.11025000000000001, 0.12099999999999997,
      0.13224999999999998, 0.144, 0.15625, 0.169, 0.18225, 0.196,
      0.21025000000000002, 0.22499999999999998, 0.24025, 0.256, 0.27225,
      0.28900000000000003, 0.30625, 0.324, 0.34224999999999994, 0.361,
      0.38024999999999992, 0.39999999999999997, 0.42025000000000007,
      0.44100000000000006, 0.46225000000000005, 0.48399999999999987, 0.50625,
      0.52899999999999991, 0.55225, 0.576, 0.60025000000000006, 0.625,
      0.65025000000000011, 0.676, 0.70224999999999993, 0.729, 0.75625, 0.784,
      0.81225, 0.84100000000000008, 0.87025, 0.89999999999999991,
      0.93024999999999991, 0.961, 0.99224999999999985, 1.024, 1.05625, 1.089,
      1.12225, 1.1560000000000001, 1.19025, 1.225, 1.26025, 1.296,
      1.3322499999999997, 1.3689999999999998, 1.40625, 1.444, 1.4822499999999998,
      1.5209999999999997, 1.56025, 1.5999999999999999, 1.64025,
      1.6810000000000003, 1.72225, 1.7640000000000002, 1.80625,
      1.8490000000000002, 1.8922500000000002, 1.9359999999999995, 1.98025, 2.025,
      2.0702499999999997, 2.1159999999999997, 2.1622500000000002, 2.209,
      2.2562499999999996, 2.304, 2.35225, 2.4010000000000002, 2.45025, 2.5,
      2.5502500000000006, 2.6010000000000004, 2.6522500000000004, 2.704,
      2.7562500000000005, 2.8090000000000006, 2.8622500000000004, 2.916,
      2.9702500000000009, 3.0250000000000008, 3.0802500000000008,
      3.136000000000001, 3.19225, 3.249, 3.30625, 3.3640000000000003,
      3.4222500000000005, 3.481, 3.5402500000000003, 3.600000000000001,
      3.6602500000000009, 3.721, 3.7822500000000003, 3.8440000000000012, 3.90625,
      3.9689999999999994, 4.0322499999999994, 4.096, 4.1602500000000004, 4.225,
      4.29025, 4.356, 4.42225, 4.489, 4.55625, 4.6240000000000006,
      4.6922500000000005, 4.761, 4.83025, 4.9, 4.9702500000000009, 5.041,
      5.11225, 5.184, 5.2562500000000005, 5.3290000000000015, 5.4022500000000013,
      5.4760000000000018, 5.5502500000000019, 5.6250000000000009,
      5.7002500000000005, 5.7760000000000016, 5.8522500000000015,
      5.929000000000002, 6.0062500000000014, 6.0840000000000023, 6.16225, 6.241,
      6.3202500000000006, 6.3999999999999995, 6.4802500000000007, 6.561,
      6.6422500000000007, 6.7240000000000011, 6.80625, 6.889, 6.97225,
      7.0560000000000009, 7.1402500000000009, 7.225, 7.3102500000000008,
      7.3960000000000008, 7.4822500000000014, 7.5690000000000008,
      7.6562500000000009, 7.7440000000000007, 7.832250000000001,
      7.9210000000000012, 8.010250000000001, 8.1000000000000014, 8.19025,
      8.2809999999999988, 8.37225, 8.4639999999999986, 8.5562499999999986,
      8.6490000000000009, 8.7422499999999985, 8.836, 8.93025, 9.0249999999999986,
      9.12025, 9.216, 9.31225, 9.409, 9.50625, 9.604000000000001, 9.70225, 9.801,
      9.9002500000000015, 10.0, 10.1, 10.2, 10.3, 10.4, 10.5, 10.6, 10.7, 10.8,
      10.9, 11.0, 11.1, 11.2, 11.3, 11.4, 11.5, 11.6, 11.7, 11.8, 11.9, 12.0,
      12.1, 12.2, 12.3, 12.4, 12.5, 12.6, 12.7, 12.8, 12.9, 13.0, 13.1, 13.2,
      13.3, 13.4, 13.5, 13.600000000000001, 13.7, 13.8, 13.9, 14.0,
      14.100000000000001, 14.2, 14.3, 14.399999999999999, 14.5, 14.6, 14.7, 14.8,
      14.9, 15.0, 15.100000000000001, 15.2, 15.3, 15.4, 15.5, 15.600000000000001,
      15.7, 15.8, 15.9, 16.0, 16.1, 16.2, 16.3, 16.4, 16.5, 16.6, 16.7, 16.8,
      16.9, 17.0, 17.1, 17.200000000000003, 17.3, 17.4, 17.5, 17.6, 17.7, 17.8,
      17.9, 18.0, 18.1, 18.200000000000003, 18.3, 18.4, 18.5, 18.6,
      18.700000000000003, 18.799999999999997, 18.9, 19.0, 19.1, 19.2, 19.3, 19.4,
      19.5, 19.6, 19.700000000000003, 19.8, 19.9, 20.0, 20.1, 20.200000000000003,
      20.3, 20.4, 20.5, 20.6, 20.7, 20.8, 20.9, 21.0, 21.1, 21.200000000000003,
      21.3, 21.4, 21.5, 21.6, 21.700000000000003, 21.8, 21.9, 22.0, 22.1, 22.2,
      22.3, 22.4, 22.5, 22.6, 22.7, 22.8, 22.9, 23.0, 23.1, 23.200000000000003,
      23.3, 23.4, 23.5, 23.6, 23.700000000000003, 23.8, 23.900000000000002, 24.0,
      24.1, 24.200000000000003, 24.3, 24.400000000000002, 24.5, 24.6, 24.7,
      24.799999999999997, 24.9, 25.0, 25.1, 25.2, 25.299999999999997, 25.4, 25.5,
      25.599999999999998, 25.699999999999996, 25.799999999999997, 25.9, 26.0,
      26.099999999999998, 26.2, 26.299999999999997, 26.4, 26.499999999999996,
      26.599999999999998, 26.7, 26.799999999999997, 26.9, 27.0,
      27.099999999999998, 27.2, 27.299999999999997, 27.4, 27.5,
      27.599999999999998, 27.7, 27.8, 27.9, 28.0, 28.099999999999998, 28.2, 28.3,
      28.4, 28.5, 28.6, 28.7, 28.8, 28.9, 29.0, 29.1, 29.2, 29.3,
      29.400000000000002, 29.5, 29.599999999999998, 29.7, 29.799999999999997,
      29.9, 30.0, 30.099999999999998, 30.2, 30.299999999999997, 30.4, 30.5,
      30.599999999999998, 30.7, 30.8, 30.9, 31.0, 31.099999999999998, 31.2,
      31.299999999999997, 31.4, 31.499999999999996, 31.599999999999998,
      31.699999999999996, 31.799999999999997, 31.9, 31.999999999999996,
      32.099999999999994, 32.199999999999996, 32.3, 32.4, 32.5,
      32.599999999999994, 32.7, 32.8, 32.9, 33.0, 33.099999999999994, 33.2, 33.3,
      33.4, 33.5, 33.599999999999994, 33.7, 33.8, 33.9, 34.0, 34.099999999999994,
      34.2, 34.3, 34.4, 34.5, 34.6, 34.7, 34.8, 34.9, 35.0, 35.1, 35.2, 35.3,
      35.4, 35.5, 35.6, 35.7, 35.8, 35.900000000000006, 36.0, 36.1, 36.2, 36.3,
      36.400000000000006, 36.5, 36.6, 36.7, 36.8, 36.900000000000006, 37.0,
      37.099999999999994, 37.199999999999996, 37.3, 37.399999999999991, 37.5,
      37.599999999999994, 37.699999999999996, 37.8, 37.9, 38.0,
      38.099999999999994, 38.199999999999996, 38.3, 38.4, 38.5,
      38.599999999999994, 38.7, 38.8, 38.9, 39.0, 39.099999999999994, 39.2, 39.3,
      39.4, 39.5, 39.599999999999994, 39.7, 39.8, 39.9, 40.0, 40.09975, 40.199,
      40.29775, 40.396, 40.49375, 40.591, 40.68775, 40.784, 40.87975, 40.975,
      41.06975, 41.164, 41.25775, 41.351, 41.44375, 41.536, 41.62775, 41.719,
      41.80975, 41.9, 41.98975, 42.079, 42.16775, 42.256, 42.34375, 42.431,
      42.51775, 42.604, 42.689750000000004, 42.775, 42.85975, 42.944, 43.02775,
      43.111, 43.19375, 43.276, 43.35775, 43.439, 43.51975, 43.6, 43.67975,
      43.759, 43.83775, 43.916, 43.99375, 44.071, 44.14775, 44.224000000000004,
      44.29975, 44.375, 44.44975, 44.524, 44.59775, 44.671, 44.74375, 44.816,
      44.88775, 44.959, 45.02975, 45.1, 45.16975, 45.239, 45.30775, 45.376,
      45.44375, 45.511, 45.57775, 45.644, 45.70975, 45.775, 45.83975, 45.904,
      45.96775, 46.031, 46.09375, 46.156, 46.21775, 46.278999999999996, 46.33975,
      46.4, 46.45975, 46.519, 46.57775, 46.636, 46.69375, 46.751000000000005,
      46.80775, 46.864, 46.91975, 46.975, 47.02975, 47.084, 47.13775, 47.191,
      47.24375, 47.296, 47.347750000000005, 47.399, 47.44975, 47.5, 47.54975,
      47.599000000000004, 47.64775, 47.696, 47.74375, 47.791, 47.83775, 47.884,
      47.92975, 47.975, 48.01975, 48.064, 48.10775, 48.150999999999996, 48.19375,
      48.236000000000004, 48.27775, 48.319, 48.35975, 48.4, 48.439750000000004,
      48.479, 48.51775, 48.556, 48.59375, 48.631, 48.66775, 48.704, 48.73975,
      48.775, 48.80975, 48.844, 48.87775, 48.911, 48.94375, 48.976, 49.00775,
      49.039, 49.06975, 49.1, 49.12975, 49.159, 49.18775, 49.216,
      49.243750000000006, 49.271, 49.29775, 49.324, 49.34975, 49.375, 49.39975,
      49.424, 49.44775, 49.471000000000004, 49.493750000000006,
      49.516000000000005, 49.53775, 49.559, 49.579750000000004, 49.6,
      49.619749999999996, 49.638999999999996, 49.65775, 49.676, 49.69375, 49.711,
      49.72775, 49.744, 49.75975, 49.775, 49.78975, 49.804, 49.817750000000004,
      49.831, 49.84375, 49.856, 49.86775, 49.879, 49.88975, 49.900000000000006,
      49.90975, 49.919, 49.92775, 49.936, 49.94375, 49.951, 49.957750000000004,
      49.964, 49.96975, 49.975, 49.97975, 49.984, 49.98775, 49.991, 49.99375,
      49.996, 49.997749999999996, 49.999, 49.99975, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 49.99975, 49.999, 49.997749999999996,
      49.996, 49.99375, 49.991, 49.98775, 49.984, 49.97975, 49.975, 49.96975,
      49.964, 49.957750000000004, 49.951, 49.94375, 49.936, 49.92775, 49.919,
      49.90975, 49.900000000000006, 49.88975, 49.879, 49.86775, 49.856, 49.84375,
      49.831, 49.817750000000004, 49.804, 49.78975, 49.775, 49.75975, 49.744,
      49.72775, 49.711, 49.69375, 49.676, 49.65775, 49.638999999999996,
      49.619749999999996, 49.6, 49.579750000000004, 49.559, 49.53775,
      49.516000000000005, 49.493750000000006, 49.471000000000004, 49.44775,
      49.424, 49.39975, 49.375, 49.34975, 49.324, 49.29775, 49.271,
      49.243750000000006, 49.216, 49.18775, 49.159, 49.12975, 49.1, 49.06975,
      49.039, 49.00775, 48.976, 48.94375, 48.911, 48.87775, 48.844, 48.80975,
      48.775, 48.73975, 48.704, 48.66775, 48.631, 48.59375, 48.556, 48.51775,
      48.479, 48.439750000000004, 48.4, 48.35975, 48.319, 48.27775,
      48.236000000000004, 48.19375, 48.150999999999996, 48.10775, 48.064,
      48.01975, 47.975, 47.92975, 47.884, 47.83775, 47.791, 47.74375, 47.696,
      47.64775, 47.599000000000004, 47.54975, 47.5, 47.44975, 47.399,
      47.347750000000005, 47.296, 47.24375, 47.191, 47.13775, 47.084, 47.02975,
      46.975, 46.91975, 46.864, 46.80775, 46.751000000000005, 46.69375, 46.636,
      46.57775, 46.519, 46.45975, 46.4, 46.33975, 46.278999999999996, 46.21775,
      46.156, 46.09375, 46.031, 45.96775, 45.904, 45.83975, 45.775, 45.70975,
      45.644, 45.57775, 45.511, 45.44375, 45.376, 45.30775, 45.239, 45.16975,
      45.1, 45.02975, 44.959, 44.88775, 44.816, 44.74375, 44.671, 44.59775,
      44.524, 44.44975, 44.375, 44.29975, 44.224000000000004, 44.14775, 44.071,
      43.99375, 43.916, 43.83775, 43.759, 43.67975, 43.6, 43.51975, 43.439,
      43.35775, 43.276, 43.19375, 43.111, 43.02775, 42.944, 42.85975, 42.775,
      42.689750000000004, 42.604, 42.51775, 42.431, 42.34375, 42.256, 42.16775,
      42.079, 41.98975, 41.9, 41.80975, 41.719, 41.62775, 41.536, 41.44375,
      41.351, 41.25775, 41.164, 41.06975, 40.975, 40.87975, 40.784, 40.68775,
      40.591, 40.49375, 40.396, 40.29775, 40.199, 40.09975, 40.0, 39.9, 39.8,
      39.7, 39.599999999999994, 39.5, 39.4, 39.3, 39.2, 39.099999999999994, 39.0,
      38.9, 38.8, 38.7, 38.599999999999994, 38.5, 38.4, 38.3, 38.199999999999996,
      38.099999999999994, 38.0, 37.9, 37.8, 37.699999999999996,
      37.599999999999994, 37.5, 37.399999999999991, 37.3, 37.199999999999996,
      37.099999999999994, 37.0, 36.900000000000006, 36.8, 36.7, 36.6, 36.5,
      36.400000000000006, 36.3, 36.2, 36.1, 36.0, 35.900000000000006, 35.8, 35.7,
      35.6, 35.5, 35.4, 35.3, 35.2, 35.1, 35.0, 34.9, 34.8, 34.7, 34.6, 34.5,
      34.4, 34.3, 34.2, 34.099999999999994, 34.0, 33.9, 33.8, 33.7,
      33.599999999999994, 33.5, 33.4, 33.3, 33.2, 33.099999999999994, 33.0, 32.9,
      32.8, 32.7, 32.599999999999994, 32.5, 32.4, 32.3, 32.199999999999996,
      32.099999999999994, 31.999999999999996, 31.9, 31.799999999999997,
      31.699999999999996, 31.599999999999998, 31.499999999999996, 31.4,
      31.299999999999997, 31.2, 31.099999999999998, 31.0, 30.9, 30.8, 30.7,
      30.599999999999998, 30.5, 30.4, 30.299999999999997, 30.2,
      30.099999999999998, 30.0, 29.9, 29.799999999999997, 29.7,
      29.599999999999998, 29.5, 29.400000000000002, 29.3, 29.2, 29.1, 29.0, 28.9,
      28.8, 28.7, 28.6, 28.5, 28.4, 28.3, 28.2, 28.099999999999998, 28.0, 27.9,
      27.8, 27.7, 27.599999999999998, 27.5, 27.4, 27.299999999999997, 27.2,
      27.099999999999998, 27.0, 26.9, 26.799999999999997, 26.7,
      26.599999999999998, 26.499999999999996, 26.4, 26.299999999999997, 26.2,
      26.099999999999998, 26.0, 25.9, 25.799999999999997, 25.699999999999996,
      25.599999999999998, 25.5, 25.4, 25.299999999999997, 25.2, 25.1, 25.0, 24.9,
      24.799999999999997, 24.7, 24.6, 24.5, 24.400000000000002, 24.3,
      24.200000000000003, 24.1, 24.0, 23.900000000000002, 23.8,
      23.700000000000003, 23.6, 23.5, 23.4, 23.3, 23.200000000000003, 23.1, 23.0,
      22.9, 22.8, 22.7, 22.6, 22.5, 22.4, 22.3, 22.2, 22.1, 22.0, 21.9, 21.8,
      21.700000000000003, 21.6, 21.5, 21.4, 21.3, 21.200000000000003, 21.1, 21.0,
      20.9, 20.8, 20.7, 20.6, 20.5, 20.4, 20.3, 20.200000000000003, 20.1, 20.0,
      19.9, 19.8, 19.700000000000003, 19.6, 19.5, 19.4, 19.3, 19.2, 19.1, 19.0,
      18.9, 18.799999999999997, 18.700000000000003, 18.6, 18.5, 18.4, 18.3,
      18.200000000000003, 18.1, 18.0, 17.9, 17.8, 17.7, 17.6, 17.5, 17.4, 17.3,
      17.200000000000003, 17.1, 17.0, 16.9, 16.8, 16.7, 16.6, 16.5, 16.4, 16.3,
      16.2, 16.1, 16.0, 15.9, 15.8, 15.7, 15.600000000000001, 15.5, 15.4, 15.3,
      15.2, 15.100000000000001, 15.0, 14.9, 14.8, 14.7, 14.6, 14.5,
      14.399999999999999, 14.3, 14.2, 14.100000000000001, 14.0, 13.9, 13.8, 13.7,
      13.600000000000001, 13.5, 13.4, 13.3, 13.2, 13.1, 13.0, 12.9, 12.8, 12.7,
      12.6, 12.5, 12.4, 12.3, 12.2, 12.1, 12.0, 11.9, 11.8, 11.7, 11.6, 11.5,
      11.4, 11.3, 11.2, 11.1, 11.0, 10.9, 10.8, 10.7, 10.6, 10.5, 10.4, 10.3,
      10.2, 10.1, 10.0, 9.9002500000000015, 9.801, 9.70225, 9.604000000000001,
      9.50625, 9.409, 9.31225, 9.216, 9.12025, 9.0249999999999986, 8.93025,
      8.836, 8.7422499999999985, 8.6490000000000009, 8.5562499999999986,
      8.4639999999999986, 8.37225, 8.2809999999999988, 8.19025,
      8.1000000000000014, 8.010250000000001, 7.9210000000000012,
      7.832250000000001, 7.7440000000000007, 7.6562500000000009,
      7.5690000000000008, 7.4822500000000014, 7.3960000000000008,
      7.3102500000000008, 7.225, 7.1402500000000009, 7.0560000000000009, 6.97225,
      6.889, 6.80625, 6.7240000000000011, 6.6422500000000007, 6.561,
      6.4802500000000007, 6.3999999999999995, 6.3202500000000006, 6.241, 6.16225,
      6.0840000000000023, 6.0062500000000014, 5.929000000000002,
      5.8522500000000015, 5.7760000000000016, 5.7002500000000005,
      5.6250000000000009, 5.5502500000000019, 5.4760000000000018,
      5.4022500000000013, 5.3290000000000015, 5.2562500000000005, 5.184, 5.11225,
      5.041, 4.9702500000000009, 4.9, 4.83025, 4.761, 4.6922500000000005,
      4.6240000000000006, 4.55625, 4.489, 4.42225, 4.356, 4.29025, 4.225,
      4.1602500000000004, 4.096, 4.0322499999999994, 3.9689999999999994, 3.90625,
      3.8440000000000012, 3.7822500000000003, 3.721, 3.6602500000000009,
      3.600000000000001, 3.5402500000000003, 3.481, 3.4222500000000005,
      3.3640000000000003, 3.30625, 3.249, 3.19225, 3.136000000000001,
      3.0802500000000008, 3.0250000000000008, 2.9702500000000009, 2.916,
      2.8622500000000004, 2.8090000000000006, 2.7562500000000005, 2.704,
      2.6522500000000004, 2.6010000000000004, 2.5502500000000006, 2.5, 2.45025,
      2.4010000000000002, 2.35225, 2.304, 2.2562499999999996, 2.209,
      2.1622500000000002, 2.1159999999999997, 2.0702499999999997, 2.025, 1.98025,
      1.9359999999999995, 1.8922500000000002, 1.8490000000000002, 1.80625,
      1.7640000000000002, 1.72225, 1.6810000000000003, 1.64025,
      1.5999999999999999, 1.56025, 1.5209999999999997, 1.4822499999999998, 1.444,
      1.40625, 1.3689999999999998, 1.3322499999999997, 1.296, 1.26025, 1.225,
      1.19025, 1.1560000000000001, 1.12225, 1.089, 1.05625, 1.024,
      0.99224999999999985, 0.961, 0.93024999999999991, 0.89999999999999991,
      0.87025, 0.84100000000000008, 0.81225, 0.784, 0.75625, 0.729,
      0.70224999999999993, 0.676, 0.65025000000000011, 0.625,
      0.60025000000000006, 0.576, 0.55225, 0.52899999999999991, 0.50625,
      0.48399999999999987, 0.46225000000000005, 0.44100000000000006,
      0.42025000000000007, 0.39999999999999997, 0.38024999999999992, 0.361,
      0.34224999999999994, 0.324, 0.30625, 0.28900000000000003, 0.27225, 0.256,
      0.24025, 0.22499999999999998, 0.21025000000000002, 0.196, 0.18225, 0.169,
      0.15625, 0.144, 0.13224999999999998, 0.12099999999999997,
      0.11025000000000001, 0.099999999999999992, 0.09025, 0.081,
      0.072250000000000009, 0.064, 0.056249999999999994, 0.049, 0.04225, 0.036,
      0.030249999999999992, 0.024999999999999998, 0.02025, 0.016, 0.01225, 0.009,
      0.0062499999999999995, 0.004, 0.00225, 0.001, 0.00025, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00025, 0.001, 0.00225,
      0.004, 0.0062499999999999995, 0.009, 0.01225, 0.016, 0.02025,
      0.024999999999999998, 0.030249999999999992, 0.036, 0.04225, 0.049,
      0.056249999999999994, 0.064, 0.072250000000000009, 0.081, 0.09025,
      0.099999999999999992, 0.11025000000000001, 0.12099999999999997,
      0.13224999999999998, 0.144, 0.15625, 0.169, 0.18225, 0.196,
      0.21025000000000002, 0.22499999999999998, 0.24025, 0.256, 0.27225,
      0.28900000000000003, 0.30625, 0.324, 0.34224999999999994, 0.361,
      0.38024999999999992, 0.39999999999999997, 0.42025000000000007,
      0.44100000000000006, 0.46225000000000005, 0.48399999999999987, 0.50625,
      0.52899999999999991, 0.55225, 0.576, 0.60025000000000006, 0.625,
      0.65025000000000011, 0.676, 0.70224999999999993, 0.729, 0.75625, 0.784,
      0.81225, 0.84100000000000008, 0.87025, 0.89999999999999991,
      0.93024999999999991, 0.961, 0.99224999999999985, 1.024, 1.05625, 1.089,
      1.12225, 1.1560000000000001, 1.19025, 1.225, 1.26025, 1.296,
      1.3322499999999997, 1.3689999999999998, 1.40625, 1.444, 1.4822499999999998,
      1.5209999999999997, 1.56025, 1.5999999999999999, 1.64025,
      1.6810000000000003, 1.72225, 1.7640000000000002, 1.80625,
      1.8490000000000002, 1.8922500000000002, 1.9359999999999995, 1.98025, 2.025,
      2.0702499999999997, 2.1159999999999997, 2.1622500000000002, 2.209,
      2.2562499999999996, 2.304, 2.35225, 2.4010000000000002, 2.45025, 2.5,
      2.5502500000000006, 2.6010000000000004, 2.6522500000000004, 2.704,
      2.7562500000000005, 2.8090000000000006, 2.8622500000000004, 2.916,
      2.9702500000000009, 3.0250000000000008, 3.0802500000000008,
      3.136000000000001, 3.19225, 3.249, 3.30625, 3.3640000000000003,
      3.4222500000000005, 3.481, 3.5402500000000003, 3.600000000000001,
      3.6602500000000009, 3.721, 3.7822500000000003, 3.8440000000000012, 3.90625,
      3.9689999999999994, 4.0322499999999994, 4.096, 4.1602500000000004, 4.225,
      4.29025, 4.356, 4.42225, 4.489, 4.55625, 4.6240000000000006,
      4.6922500000000005, 4.761, 4.83025, 4.9, 4.9702500000000009, 5.041,
      5.11225, 5.184, 5.2562500000000005, 5.3290000000000015, 5.4022500000000013,
      5.4760000000000018, 5.5502500000000019, 5.6250000000000009,
      5.7002500000000005, 5.7760000000000016, 5.8522500000000015,
      5.929000000000002, 6.0062500000000014, 6.0840000000000023, 6.16225, 6.241,
      6.3202500000000006, 6.3999999999999995, 6.4802500000000007, 6.561,
      6.6422500000000007, 6.7240000000000011, 6.80625, 6.889, 6.97225,
      7.0560000000000009, 7.1402500000000009, 7.225, 7.3102500000000008,
      7.3960000000000008, 7.4822500000000014, 7.5690000000000008,
      7.6562500000000009, 7.7440000000000007, 7.832250000000001,
      7.9210000000000012, 8.010250000000001, 8.1000000000000014, 8.19025,
      8.2809999999999988, 8.37225, 8.4639999999999986, 8.5562499999999986,
      8.6490000000000009, 8.7422499999999985, 8.836, 8.93025, 9.0249999999999986,
      9.12025, 9.216, 9.31225, 9.409, 9.50625, 9.604000000000001, 9.70225, 9.801,
      9.9002500000000015, 10.0, 10.1, 10.2, 10.3, 10.4, 10.5, 10.6, 10.7, 10.8,
      10.9, 11.0, 11.1, 11.2, 11.3, 11.4, 11.5, 11.6, 11.7, 11.8, 11.9, 12.0,
      12.1, 12.2, 12.3, 12.4, 12.5, 12.6, 12.7, 12.8, 12.9, 13.0, 13.1, 13.2,
      13.3, 13.4, 13.5, 13.600000000000001, 13.7, 13.8, 13.9, 14.0,
      14.100000000000001, 14.2, 14.3, 14.399999999999999, 14.5, 14.6, 14.7, 14.8,
      14.9, 15.0, 15.100000000000001, 15.2, 15.3, 15.4, 15.5, 15.600000000000001,
      15.7, 15.8, 15.9, 16.0, 16.1, 16.2, 16.3, 16.4, 16.5, 16.6, 16.7, 16.8,
      16.9, 17.0, 17.1, 17.200000000000003, 17.3, 17.4, 17.5, 17.6, 17.7, 17.8,
      17.9, 18.0, 18.1, 18.200000000000003, 18.3, 18.4, 18.5, 18.6,
      18.700000000000003, 18.799999999999997, 18.9, 19.0, 19.1, 19.2, 19.3, 19.4,
      19.5, 19.6, 19.700000000000003, 19.8, 19.9, 20.0, 20.1, 20.200000000000003,
      20.3, 20.4, 20.5, 20.6, 20.7, 20.8, 20.9, 21.0, 21.1, 21.200000000000003,
      21.3, 21.4, 21.5, 21.6, 21.700000000000003, 21.8, 21.9, 22.0, 22.1, 22.2,
      22.3, 22.4, 22.5, 22.6, 22.7, 22.8, 22.9, 23.0, 23.1, 23.200000000000003,
      23.3, 23.4, 23.5, 23.6, 23.700000000000003, 23.8, 23.900000000000002, 24.0,
      24.1, 24.200000000000003, 24.3, 24.400000000000002, 24.5, 24.6, 24.7,
      24.799999999999997, 24.9, 25.0, 25.1, 25.2, 25.299999999999997, 25.4, 25.5,
      25.599999999999998, 25.699999999999996, 25.799999999999997, 25.9, 26.0,
      26.099999999999998, 26.2, 26.299999999999997, 26.4, 26.499999999999996,
      26.599999999999998, 26.7, 26.799999999999997, 26.9, 27.0,
      27.099999999999998, 27.2, 27.299999999999997, 27.4, 27.5,
      27.599999999999998, 27.7, 27.8, 27.9, 28.0, 28.099999999999998, 28.2, 28.3,
      28.4, 28.5, 28.6, 28.7, 28.8, 28.9, 29.0, 29.1, 29.2, 29.3,
      29.400000000000002, 29.5, 29.599999999999998, 29.7, 29.799999999999997,
      29.9, 30.0, 30.099999999999998, 30.2, 30.299999999999997, 30.4, 30.5,
      30.599999999999998, 30.7, 30.8, 30.9, 31.0, 31.099999999999998, 31.2,
      31.299999999999997, 31.4, 31.499999999999996, 31.599999999999998,
      31.699999999999996, 31.799999999999997, 31.9, 31.999999999999996,
      32.099999999999994, 32.199999999999996, 32.3, 32.4, 32.5,
      32.599999999999994, 32.7, 32.8, 32.9, 33.0, 33.099999999999994, 33.2, 33.3,
      33.4, 33.5, 33.599999999999994, 33.7, 33.8, 33.9, 34.0, 34.099999999999994,
      34.2, 34.3, 34.4, 34.5, 34.6, 34.7, 34.8, 34.9, 35.0, 35.1, 35.2, 35.3,
      35.4, 35.5, 35.6, 35.7, 35.8, 35.900000000000006, 36.0, 36.1, 36.2, 36.3,
      36.400000000000006, 36.5, 36.6, 36.7, 36.8, 36.900000000000006, 37.0,
      37.099999999999994, 37.199999999999996, 37.3, 37.399999999999991, 37.5,
      37.599999999999994, 37.699999999999996, 37.8, 37.9, 38.0,
      38.099999999999994, 38.199999999999996, 38.3, 38.4, 38.5,
      38.599999999999994, 38.7, 38.8, 38.9, 39.0, 39.099999999999994, 39.2, 39.3,
      39.4, 39.5, 39.599999999999994, 39.7, 39.8, 39.9, 40.0, 40.09975, 40.199,
      40.29775, 40.396, 40.49375, 40.591, 40.68775, 40.784, 40.87975, 40.975,
      41.06975, 41.164, 41.25775, 41.351, 41.44375, 41.536, 41.62775, 41.719,
      41.80975, 41.9, 41.98975, 42.079, 42.16775, 42.256, 42.34375, 42.431,
      42.51775, 42.604, 42.689750000000004, 42.775, 42.85975, 42.944, 43.02775,
      43.111, 43.19375, 43.276, 43.35775, 43.439, 43.51975, 43.6, 43.67975,
      43.759, 43.83775, 43.916, 43.99375, 44.071, 44.14775, 44.224000000000004,
      44.29975, 44.375, 44.44975, 44.524, 44.59775, 44.671, 44.74375, 44.816,
      44.88775, 44.959, 45.02975, 45.1, 45.16975, 45.239, 45.30775, 45.376,
      45.44375, 45.511, 45.57775, 45.644, 45.70975, 45.775, 45.83975, 45.904,
      45.96775, 46.031, 46.09375, 46.156, 46.21775, 46.278999999999996, 46.33975,
      46.4, 46.45975, 46.519, 46.57775, 46.636, 46.69375, 46.751000000000005,
      46.80775, 46.864, 46.91975, 46.975, 47.02975, 47.084, 47.13775, 47.191,
      47.24375, 47.296, 47.347750000000005, 47.399, 47.44975, 47.5, 47.54975,
      47.599000000000004, 47.64775, 47.696, 47.74375, 47.791, 47.83775, 47.884,
      47.92975, 47.975, 48.01975, 48.064, 48.10775, 48.150999999999996, 48.19375,
      48.236000000000004, 48.27775, 48.319, 48.35975, 48.4, 48.439750000000004,
      48.479, 48.51775, 48.556, 48.59375, 48.631, 48.66775, 48.704, 48.73975,
      48.775, 48.80975, 48.844, 48.87775, 48.911, 48.94375, 48.976, 49.00775,
      49.039, 49.06975, 49.1, 49.12975, 49.159, 49.18775, 49.216,
      49.243750000000006, 49.271, 49.29775, 49.324, 49.34975, 49.375, 49.39975,
      49.424, 49.44775, 49.471000000000004, 49.493750000000006,
      49.516000000000005, 49.53775, 49.559, 49.579750000000004, 49.6,
      49.619749999999996, 49.638999999999996, 49.65775, 49.676, 49.69375, 49.711,
      49.72775, 49.744, 49.75975, 49.775, 49.78975, 49.804, 49.817750000000004,
      49.831, 49.84375, 49.856, 49.86775, 49.879, 49.88975, 49.900000000000006,
      49.90975, 49.919, 49.92775, 49.936, 49.94375, 49.951, 49.957750000000004,
      49.964, 49.96975, 49.975, 49.97975, 49.984, 49.98775, 49.991, 49.99375,
      49.996, 49.997749999999996, 49.999, 49.99975, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,
      50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 49.99975, 49.999, 49.997749999999996,
      49.996, 49.99375, 49.991, 49.98775, 49.984, 49.97975, 49.975, 49.96975,
      49.964, 49.957750000000004, 49.951, 49.94375, 49.936, 49.92775, 49.919,
      49.90975, 49.900000000000006, 49.88975, 49.879, 49.86775, 49.856, 49.84375,
      49.831, 49.817750000000004, 49.804, 49.78975, 49.775, 49.75975, 49.744,
      49.72775, 49.711, 49.69375, 49.676, 49.65775, 49.638999999999996,
      49.619749999999996, 49.6, 49.579750000000004, 49.559, 49.53775,
      49.516000000000005, 49.493750000000006, 49.471000000000004, 49.44775,
      49.424, 49.39975, 49.375, 49.34975, 49.324, 49.29775, 49.271,
      49.243750000000006, 49.216, 49.18775, 49.159, 49.12975, 49.1, 49.06975,
      49.039, 49.00775, 48.976, 48.94375, 48.911, 48.87775, 48.844, 48.80975,
      48.775, 48.73975, 48.704, 48.66775, 48.631, 48.59375, 48.556, 48.51775,
      48.479, 48.439750000000004, 48.4, 48.35975, 48.319, 48.27775,
      48.236000000000004, 48.19375, 48.150999999999996, 48.10775, 48.064,
      48.01975, 47.975, 47.92975, 47.884, 47.83775, 47.791, 47.74375, 47.696,
      47.64775, 47.599000000000004, 47.54975, 47.5, 47.44975, 47.399,
      47.347750000000005, 47.296, 47.24375, 47.191, 47.13775, 47.084, 47.02975,
      46.975, 46.91975, 46.864, 46.80775, 46.751000000000005, 46.69375, 46.636,
      46.57775, 46.519, 46.45975, 46.4, 46.33975, 46.278999999999996, 46.21775,
      46.156, 46.09375, 46.031, 45.96775, 45.904, 45.83975, 45.775, 45.70975,
      45.644, 45.57775, 45.511, 45.44375, 45.376, 45.30775, 45.239, 45.16975,
      45.1, 45.02975, 44.959, 44.88775, 44.816, 44.74375, 44.671, 44.59775,
      44.524, 44.44975, 44.375, 44.29975, 44.224000000000004, 44.14775, 44.071,
      43.99375, 43.916, 43.83775, 43.759, 43.67975, 43.6, 43.51975, 43.439,
      43.35775, 43.276, 43.19375, 43.111, 43.02775, 42.944, 42.85975, 42.775,
      42.689750000000004, 42.604, 42.51775, 42.431, 42.34375, 42.256, 42.16775,
      42.079, 41.98975, 41.9, 41.80975, 41.719, 41.62775, 41.536, 41.44375,
      41.351, 41.25775, 41.164, 41.06975, 40.975, 40.87975, 40.784, 40.68775,
      40.591, 40.49375, 40.396, 40.29775, 40.199, 40.09975, 40.0, 39.9, 39.8,
      39.7, 39.599999999999994, 39.5, 39.4, 39.3, 39.2, 39.099999999999994, 39.0,
      38.9, 38.8, 38.7, 38.599999999999994, 38.5, 38.4, 38.3, 38.199999999999996,
      38.099999999999994, 38.0, 37.9, 37.8, 37.699999999999996,
      37.599999999999994, 37.5, 37.399999999999991, 37.3, 37.199999999999996,
      37.099999999999994, 37.0, 36.900000000000006, 36.8, 36.7, 36.6, 36.5,
      36.400000000000006, 36.3, 36.2, 36.1, 36.0, 35.900000000000006, 35.8, 35.7,
      35.6, 35.5, 35.4, 35.3, 35.2, 35.1, 35.0, 34.9, 34.8, 34.7, 34.6, 34.5,
      34.4, 34.3, 34.2, 34.099999999999994, 34.0, 33.9, 33.8, 33.7,
      33.599999999999994, 33.5, 33.4, 33.3, 33.2, 33.099999999999994, 33.0, 32.9,
      32.8, 32.7, 32.599999999999994, 32.5, 32.4, 32.3, 32.199999999999996,
      32.099999999999994, 31.999999999999996, 31.9, 31.799999999999997,
      31.699999999999996, 31.599999999999998, 31.499999999999996, 31.4,
      31.299999999999997, 31.2, 31.099999999999998, 31.0, 30.9, 30.8, 30.7,
      30.599999999999998, 30.5, 30.4, 30.299999999999997, 30.2,
      30.099999999999998, 30.0, 29.9, 29.799999999999997, 29.7,
      29.599999999999998, 29.5, 29.400000000000002, 29.3, 29.2, 29.1, 29.0, 28.9,
      28.8, 28.7, 28.6, 28.5, 28.4, 28.3, 28.2, 28.099999999999998, 28.0, 27.9,
      27.8, 27.7, 27.599999999999998, 27.5, 27.4, 27.299999999999997, 27.2,
      27.099999999999998, 27.0, 26.9, 26.799999999999997, 26.7,
      26.599999999999998, 26.499999999999996, 26.4, 26.299999999999997, 26.2,
      26.099999999999998, 26.0, 25.9, 25.799999999999997, 25.699999999999996,
      25.599999999999998, 25.5, 25.4, 25.299999999999997, 25.2, 25.1, 25.0, 24.9,
      24.799999999999997, 24.7, 24.6, 24.5, 24.400000000000002, 24.3,
      24.200000000000003, 24.1, 24.0, 23.900000000000002, 23.8,
      23.700000000000003, 23.6, 23.5, 23.4, 23.3, 23.200000000000003, 23.1, 23.0,
      22.9, 22.8, 22.7, 22.6, 22.5, 22.4, 22.3, 22.2, 22.1, 22.0, 21.9, 21.8,
      21.700000000000003, 21.6, 21.5, 21.4, 21.3, 21.200000000000003, 21.1, 21.0,
      20.9, 20.8, 20.7, 20.6, 20.5, 20.4, 20.3, 20.200000000000003, 20.1, 20.0,
      19.9, 19.8, 19.700000000000003, 19.6, 19.5, 19.4, 19.3, 19.2, 19.1, 19.0,
      18.9, 18.799999999999997, 18.700000000000003, 18.6, 18.5, 18.4, 18.3,
      18.200000000000003, 18.1, 18.0, 17.9, 17.8, 17.7, 17.6, 17.5, 17.4, 17.3,
      17.200000000000003, 17.1, 17.0, 16.9, 16.8, 16.7, 16.6, 16.5, 16.4, 16.3,
      16.2, 16.1, 16.0, 15.9, 15.8, 15.7, 15.600000000000001, 15.5, 15.4, 15.3,
      15.2, 15.100000000000001, 15.0, 14.9, 14.8, 14.7, 14.6, 14.5,
      14.399999999999999, 14.3, 14.2, 14.100000000000001, 14.0, 13.9, 13.8, 13.7,
      13.600000000000001, 13.5, 13.4, 13.3, 13.2, 13.1, 13.0, 12.9, 12.8, 12.7,
      12.6, 12.5, 12.4, 12.3, 12.2, 12.1, 12.0, 11.9, 11.8, 11.7, 11.6, 11.5,
      11.4, 11.3, 11.2, 11.1, 11.0, 10.9, 10.8, 10.7, 10.6, 10.5, 10.4, 10.3,
      10.2, 10.1, 10.0, 9.9002500000000015, 9.801, 9.70225, 9.604000000000001,
      9.50625, 9.409, 9.31225, 9.216, 9.12025, 9.0249999999999986, 8.93025,
      8.836, 8.7422499999999985, 8.6490000000000009, 8.5562499999999986,
      8.4639999999999986, 8.37225, 8.2809999999999988, 8.19025,
      8.1000000000000014, 8.010250000000001, 7.9210000000000012,
      7.832250000000001, 7.7440000000000007, 7.6562500000000009,
      7.5690000000000008, 7.4822500000000014, 7.3960000000000008,
      7.3102500000000008, 7.225, 7.1402500000000009, 7.0560000000000009, 6.97225,
      6.889, 6.80625, 6.7240000000000011, 6.6422500000000007, 6.561,
      6.4802500000000007, 6.3999999999999995, 6.3202500000000006, 6.241, 6.16225,
      6.0840000000000023, 6.0062500000000014, 5.929000000000002,
      5.8522500000000015, 5.7760000000000016, 5.7002500000000005,
      5.6250000000000009, 5.5502500000000019, 5.4760000000000018,
      5.4022500000000013, 5.3290000000000015, 5.2562500000000005, 5.184, 5.11225,
      5.041, 4.9702500000000009, 4.9, 4.83025, 4.761, 4.6922500000000005,
      4.6240000000000006, 4.55625, 4.489, 4.42225, 4.356, 4.29025, 4.225,
      4.1602500000000004, 4.096, 4.0322499999999994, 3.9689999999999994, 3.90625,
      3.8440000000000012, 3.7822500000000003, 3.721, 3.6602500000000009,
      3.600000000000001, 3.5402500000000003, 3.481, 3.4222500000000005,
      3.3640000000000003, 3.30625, 3.249, 3.19225, 3.136000000000001,
      3.0802500000000008, 3.0250000000000008, 2.9702500000000009, 2.916,
      2.8622500000000004, 2.8090000000000006, 2.7562500000000005, 2.704,
      2.6522500000000004, 2.6010000000000004, 2.5502500000000006, 2.5, 2.45025,
      2.4010000000000002, 2.35225, 2.304, 2.2562499999999996, 2.209,
      2.1622500000000002, 2.1159999999999997, 2.0702499999999997, 2.025, 1.98025,
      1.9359999999999995, 1.8922500000000002, 1.8490000000000002, 1.80625,
      1.7640000000000002, 1.72225, 1.6810000000000003, 1.64025,
      1.5999999999999999, 1.56025, 1.5209999999999997, 1.4822499999999998, 1.444,
      1.40625, 1.3689999999999998, 1.3322499999999997, 1.296, 1.26025, 1.225,
      1.19025, 1.1560000000000001, 1.12225, 1.089, 1.05625, 1.024,
      0.99224999999999985, 0.961, 0.93024999999999991, 0.89999999999999991,
      0.87025, 0.84100000000000008, 0.81225, 0.784, 0.75625, 0.729,
      0.70224999999999993, 0.676, 0.65025000000000011, 0.625,
      0.60025000000000006, 0.576, 0.55225, 0.52899999999999991, 0.50625,
      0.48399999999999987, 0.46225000000000005, 0.44100000000000006,
      0.42025000000000007, 0.39999999999999997, 0.38024999999999992, 0.361,
      0.34224999999999994, 0.324, 0.30625, 0.28900000000000003, 0.27225, 0.256,
      0.24025, 0.22499999999999998, 0.21025000000000002, 0.196, 0.18225, 0.169,
      0.15625, 0.144, 0.13224999999999998, 0.12099999999999997,
      0.11025000000000001, 0.099999999999999992, 0.09025, 0.081,
      0.072250000000000009, 0.064, 0.056249999999999994, 0.049, 0.04225, 0.036,
      0.030249999999999992, 0.024999999999999998, 0.02025, 0.016, 0.01225, 0.009,
      0.0062499999999999995, 0.004, 0.00225, 0.001, 0.00025, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 } ;

    p_control_DWork.FromWorkspace_PWORK.TimePtr = (void *) pTimeValues;
    p_control_DWork.FromWorkspace_PWORK.DataPtr = (void *) pDataValues;
    p_control_DWork.FromWorkspace_IWORK.PrevIndex = 0;
  }

  /* Start for S-Function (inverse_modulus_block): '<S2>/Inverse Modulus1' */

  /* S-Function Block: p_control/Quarc_Plant/Inverse Modulus1 (inverse_modulus_block) */
  {
    /* Initialize previous inputs to zero */
    p_control_DWork.InverseModulus1_RWORK.PreviousU = 0.0;

    /* Initialize previous outputs to zero */
    p_control_DWork.InverseModulus1_RWORK.PreviousY = 0.0;
    p_control_DWork.InverseModulus1_IWORK.FirstSample = 1;
  }

  MdlInitialize();
}

void MdlTerminate(void)
{
  p_control_terminate();
}

RT_MODEL_p_control *p_control(void)
{
  p_control_initialize(1);
  return p_control_M;
}

/*========================================================================*
 * End of GRT compatible call interface                                   *
 *========================================================================*/
