clear all;
close all;
clc;

load('/Users/pshering/Documents/MATLAB/MTE_360/MTE360 Labs/lab2/project2/1F.mat')
load('lab2_3f.mat')

t = 0:0.001:4;

% experimental 1f
xr_input_1f = xr_real;
x_output_1f = x_real.signals.values;

% experimental 3f
xr_input_3f = xr_PID_real.signals.values;
x_output_3f = x_PID_real.signals.values;

%tracking error
e_exp_1f = x_output_1f - xr_input_1f;
e_exp_3f = e_PID_real.signals.values;

%control signal
u_control_exp_1f = usim;
u_control_exp_3f = u_PID_real.signals.values;

v_exp_1f = diff(x_output_1f);
v_exp_3f = diff(x_output_3f);

average_1f = abs(sum(e_exp_1f(200:400))/200)
average_3f = abs(sum(e_exp_3f(200:400))/200)
n=12;
figure(1);                          % opens a figure window
subplot(3,1,1);                     % subplot(rows, columns, position)
    plot(t,x_output_1f);
    hold on;
    plot(t,x_output_3f);
    hold on;
    plot(t,xr_real);
    title('Position Trajectory for Smooth Trajectory PD vs PID');     % creates a title for the plot
    ylabel('Comanded Position [mm]');      % labels the y-axis
    legend('PD control (1f)', 'PID Control (3f)', 'Command Input');
    set(gca,'FontSize',n);
subplot(3,1,2);
    plot(t,e_exp_1f);
    hold on;
    plot(t,e_exp_3f);
    title('Tracking Error Smooth Trajectory PD vs PID');     % creates a title for the plot
    ylabel('Error [mm]');      % labels the y-axis
    legend('PD control (1f)', 'PID Control (3f)');
    set(gca,'FontSize',n);
subplot(3,1,3);
    plot(t,u_control_exp_1f);
    hold on;
    plot(t,u_control_exp_3f);
    title('Control Signal Smooth Trajectory PD vs PID');     % creates a title for the plot
    ylabel('Control Signal [V]');      % labels the y-axis
    xlabel('Time [sec]')              % labels the x-axis
    legend('PD control (1f)', 'PID Control (3f)');
    set(gca,'FontSize',n);
    