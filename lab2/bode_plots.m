clear all;
close all;
clc;
 
% define 2nd order transfer function  for Kp 0.6%
wn = 9.0;                   % natural frequency [rad/s]
zeta = 0.8;                 % damping ratio [ ]
G = tf([wn*wn],[1 2*zeta*wn wn*wn]); % first comma is numerator, demo is second 

%PD TF
Kd = 0.05138;
m = 0.0006627747;
Kp = 2.1194;
b = 0.008585;
G2 = tf([Kd/m Kp/m],[1 (b+Kd)/m Kp/m]);

%PID TF
Ki = 63.918;
G3 = tf([Kd/m Kp/m Ki/m],[1 (b+Kd)/m Kp/m Ki/m]);
 
% generate frequency array of interest [rad/s]%
w = logspace(-2,4,1000)'; % logarithmic, from 1e-1 to 1e2 with 1000 points
 
% method 1: directly calculate frequency dependent complex gain % 
% ============================================================= %
jw = j*w;               % j*w vector
r2d = 180/pi;           % radians to degrees conversion factor
numerator = G.num{1};   % extract numerator coefficients [0 0 25]
denominator = G.den{1}; % extract denominator coefficients [1 0 25]
Gf = polyval(numerator,jw)./polyval(denominator,jw); % evaluate complex gain

% for the PD TF
jw = j*w;               % j*w vector
r2d = 180/pi;           % radians to degrees conversion factor
numerator = G2.num{1};   % extract numerator coefficients [0 0 25]
denominator = G2.den{1}; % extract denominator coefficients [1 0 25]
Gf2 = polyval(numerator,jw)./polyval(denominator,jw); % evaluate complex gain

% for the PD TF
jw = j*w;               % j*w vector
r2d = 180/pi;           % radians to degrees conversion factor
numerator = G3.num{1};   % extract numerator coefficients [0 0 25]
denominator = G3.den{1}; % extract denominator coefficients [1 0 25]
Gf3 = polyval(numerator,jw)./polyval(denominator,jw); % evaluate complex gain

figure(3);
bode(G, G2);
set(gca,'FontSize',12);
legend('sys-ideal 2nd order','sys-pd controlled');

figure(4);
bode(G2, G3);
set(gca,'FontSize',12);
legend('sys-pd controlled', 'sys-pid controlled');


