% m file to demonstrate the effect of 
% a zero and complex conjugate l.h.p. poles

% mte 360, k.e.

% user preferences %
re_max = 10;
im_max = 10;
tmax = 0.5;

% define 2nd order transfer function  for Kp 0.6%
wn = 9.0;                   % natural frequency [rad/s]
zeta = 0.8;                 % damping ratio [ ]
G = tf([wn*wn],[1 2*zeta*wn wn*wn]); % first comma is numerator, demo is second 

%PD TF
Kd = 0.05138;
m = 0.0006627747;
Kp = 2.1194;
b = 0.008585;
G2 = tf([Kd/m Kp/m],[1 (b+Kd)/m Kp/m]);

[xsim , tsim]= step(G,tmax);
[xsim2, tsim2] = step(G2,tmax);

% prepare step response figure %
figure(1); zoom on;
set(gca,'FontSize',12);
axis([0 tmax -2 2]); grid on;
ylabel('Response'); xlabel('time [s]');
title('step response');
hold on; plot(tsim,xsim);
hold on; plot(tsim2,xsim2);
legend('sys-ideal 2nd order','sys-pd controlled')


% % m file to demonstrate the effect of 
% % a zero and complex conjugate l.h.p. poles
% 
% % mte 360, k.e.
% 
% % user preferences %
% re_max = 60;
% im_max = 10;
% tmax = 10;
% 
% % prepare pole location figure %
% figure(2); zoom on;
% set(gca,'FontSize',12);
% axis([-re_max re_max -im_max im_max]);
% plot([-re_max re_max],[0 0],'k',[0 0],[-im_max im_max],'k');
% hold on;
% ylabel('imag [rad/s]'); xlabel('real [rad/s]');
% title('Pole location');
% % sgrid;
% grid on;
% 
% re = -zeta*wn;
% im = wn*sqrt(1-zeta*zeta);
% 
% % show and process user input %
% figure(2); 
% set(gca,'FontSize',12);
% % plot([re re],[im -im],'rx', [0 re],[0 im],'b',[0 re],[0 -im],'b');
% plot([re re],[im -im],'x', [0 re],[0 im],[0 re],[0 -im]);
% 
% z = -Kp/Kd;
% 
% % show user input %
% figure(2); 
% plot([z],0,'o');

figure(2);
pzmap(G,G2)
set(gca,'FontSize',12);
legend('sys-ideal 2nd order','sys-pd controlled')
grid on


damp(G)
damp(G2)
