/*
 * step_input_1g_types.h
 *
 * Code generation for model "step_input_1g".
 *
 * Model version              : 1.7
 * Simulink Coder version : 8.9 (R2015b) 13-Aug-2015
 * C source code generated on : Tue Feb  7 19:56:47 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_step_input_1g_types_h_
#define RTW_HEADER_step_input_1g_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_step_input_1g_T_ P_step_input_1g_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_step_input_1g_T RT_MODEL_step_input_1g_T;

#endif                                 /* RTW_HEADER_step_input_1g_types_h_ */
