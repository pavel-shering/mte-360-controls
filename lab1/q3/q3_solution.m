clear all;
close all;
clc;

% define 2nd order transfer function  for Kp 0.6%
wn = 30.088;                     % natural frequency [rad/s]
zeta = 0.2153;                 % damping ratio [ ]
G = tf([wn*wn],[1 2*zeta*wn wn*wn]); % first comma is numerator, demo is second 

% generate frequency array of interest [rad/s]%
w = logspace(-1,2,1000)'; % logarithmic, from 1e-1 to 1e2 with 1000 points

% method 1: directly calculate frequency dependent complex gain % 
% ============================================================= %
jw = j*w;               % j*w vector
r2d = 180/pi;           % radians to degrees conversion factor
numerator = G.num{1};   % extract numerator coefficients [0 0 25]
denominator = G.den{1}; % extract denominator coefficients [1 0 25]
Gf = polyval(numerator,jw)./polyval(denominator,jw); % evaluate complex gain
                            
% % generate Bode plot %
% figure(1); clf; zoom on;
% loglog(w,abs(Gf),'b');
% title('Bode Plot'); ylabel('Magnitude [ ]'); grid on; 
% 
% figure(2);
% semilogx(w,r2d*angle(Gf),'b');
% ylabel('Phase [deg]'); xlabel('Frequency [rad/s]'); grid on;

% generate Bode plot %
figure(1); clf; zoom on;
subplot(2,1,1); loglog(w,abs(Gf),'b');
title('Bode Plot'); ylabel('Magnitude [ ]'); grid on; 
subplot(2,1,2); semilogx(w,r2d*angle(Gf),'b');
ylabel('Phase [deg]'); xlabel('Frequency [rad/s]'); grid on;